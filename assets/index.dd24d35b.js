(function(){const e=document.createElement("link").relList;if(e&&e.supports&&e.supports("modulepreload"))return;for(const r of document.querySelectorAll('link[rel="modulepreload"]'))n(r);new MutationObserver(r=>{for(const s of r)if(s.type==="childList")for(const o of s.addedNodes)o.tagName==="LINK"&&o.rel==="modulepreload"&&n(o)}).observe(document,{childList:!0,subtree:!0});function t(r){const s={};return r.integrity&&(s.integrity=r.integrity),r.referrerpolicy&&(s.referrerPolicy=r.referrerpolicy),r.crossorigin==="use-credentials"?s.credentials="include":r.crossorigin==="anonymous"?s.credentials="omit":s.credentials="same-origin",s}function n(r){if(r.ep)return;r.ep=!0;const s=t(r);fetch(r.href,s)}})();/**
 * @license
 * Copyright 2010-2022 Three.js Authors
 * SPDX-License-Identifier: MIT
 */const Ss="147",Vn={LEFT:0,MIDDLE:1,RIGHT:2,ROTATE:0,DOLLY:1,PAN:2},Dn={ROTATE:0,PAN:1,DOLLY_PAN:2,DOLLY_ROTATE:3},il=0,Us=1,rl=2,xo=1,sl=2,bi=3,ti=0,Vt=1,dn=2,hn=0,$n=1,Ns=2,Fs=3,qs=4,al=5,Qn=100,ol=101,ll=102,Os=103,Bs=104,cl=200,ul=201,dl=202,hl=203,Mo=204,yo=205,fl=206,pl=207,ml=208,gl=209,vl=210,xl=0,Ml=1,yl=2,ds=3,Al=4,_l=5,bl=6,Sl=7,Ao=0,wl=1,El=2,tn=0,Tl=1,Il=2,Rl=3,Cl=4,Ll=5,_o=300,ni=301,ii=302,hs=303,fs=304,gr=306,ps=1e3,Bt=1001,ms=1002,gt=1003,ks=1004,Hs=1005,Ct=1006,Pl=1007,vr=1008,In=1009,zl=1010,Vl=1011,bo=1012,Dl=1013,Sn=1014,wn=1015,Ri=1016,Ul=1017,Nl=1018,ei=1020,Fl=1021,ql=1022,kt=1023,Ol=1024,Bl=1025,En=1026,ri=1027,kl=1028,Hl=1029,Zl=1030,Gl=1031,Wl=1033,Er=33776,Tr=33777,Ir=33778,Rr=33779,Zs=35840,Gs=35841,Ws=35842,Xs=35843,Xl=36196,js=37492,Ys=37496,Ks=37808,Qs=37809,Js=37810,$s=37811,ea=37812,ta=37813,na=37814,ia=37815,ra=37816,sa=37817,aa=37818,oa=37819,la=37820,ca=37821,ua=36492,Rn=3e3,Be=3001,jl=3200,Yl=3201,Kl=0,Ql=1,Zt="srgb",Ci="srgb-linear",Cr=7680,Jl=519,da=35044,ha="300 es",gs=1035;class Pn{addEventListener(e,t){this._listeners===void 0&&(this._listeners={});const n=this._listeners;n[e]===void 0&&(n[e]=[]),n[e].indexOf(t)===-1&&n[e].push(t)}hasEventListener(e,t){if(this._listeners===void 0)return!1;const n=this._listeners;return n[e]!==void 0&&n[e].indexOf(t)!==-1}removeEventListener(e,t){if(this._listeners===void 0)return;const r=this._listeners[e];if(r!==void 0){const s=r.indexOf(t);s!==-1&&r.splice(s,1)}}dispatchEvent(e){if(this._listeners===void 0)return;const n=this._listeners[e.type];if(n!==void 0){e.target=this;const r=n.slice(0);for(let s=0,o=r.length;s<o;s++)r[s].call(this,e);e.target=null}}}const ot=["00","01","02","03","04","05","06","07","08","09","0a","0b","0c","0d","0e","0f","10","11","12","13","14","15","16","17","18","19","1a","1b","1c","1d","1e","1f","20","21","22","23","24","25","26","27","28","29","2a","2b","2c","2d","2e","2f","30","31","32","33","34","35","36","37","38","39","3a","3b","3c","3d","3e","3f","40","41","42","43","44","45","46","47","48","49","4a","4b","4c","4d","4e","4f","50","51","52","53","54","55","56","57","58","59","5a","5b","5c","5d","5e","5f","60","61","62","63","64","65","66","67","68","69","6a","6b","6c","6d","6e","6f","70","71","72","73","74","75","76","77","78","79","7a","7b","7c","7d","7e","7f","80","81","82","83","84","85","86","87","88","89","8a","8b","8c","8d","8e","8f","90","91","92","93","94","95","96","97","98","99","9a","9b","9c","9d","9e","9f","a0","a1","a2","a3","a4","a5","a6","a7","a8","a9","aa","ab","ac","ad","ae","af","b0","b1","b2","b3","b4","b5","b6","b7","b8","b9","ba","bb","bc","bd","be","bf","c0","c1","c2","c3","c4","c5","c6","c7","c8","c9","ca","cb","cc","cd","ce","cf","d0","d1","d2","d3","d4","d5","d6","d7","d8","d9","da","db","dc","dd","de","df","e0","e1","e2","e3","e4","e5","e6","e7","e8","e9","ea","eb","ec","ed","ee","ef","f0","f1","f2","f3","f4","f5","f6","f7","f8","f9","fa","fb","fc","fd","fe","ff"],Lr=Math.PI/180,fa=180/Math.PI;function zi(){const i=Math.random()*4294967295|0,e=Math.random()*4294967295|0,t=Math.random()*4294967295|0,n=Math.random()*4294967295|0;return(ot[i&255]+ot[i>>8&255]+ot[i>>16&255]+ot[i>>24&255]+"-"+ot[e&255]+ot[e>>8&255]+"-"+ot[e>>16&15|64]+ot[e>>24&255]+"-"+ot[t&63|128]+ot[t>>8&255]+"-"+ot[t>>16&255]+ot[t>>24&255]+ot[n&255]+ot[n>>8&255]+ot[n>>16&255]+ot[n>>24&255]).toLowerCase()}function vt(i,e,t){return Math.max(e,Math.min(t,i))}function $l(i,e){return(i%e+e)%e}function Pr(i,e,t){return(1-t)*i+t*e}function pa(i){return(i&i-1)===0&&i!==0}function vs(i){return Math.pow(2,Math.floor(Math.log(i)/Math.LN2))}function Ni(i,e){switch(e.constructor){case Float32Array:return i;case Uint16Array:return i/65535;case Uint8Array:return i/255;case Int16Array:return Math.max(i/32767,-1);case Int8Array:return Math.max(i/127,-1);default:throw new Error("Invalid component type.")}}function yt(i,e){switch(e.constructor){case Float32Array:return i;case Uint16Array:return Math.round(i*65535);case Uint8Array:return Math.round(i*255);case Int16Array:return Math.round(i*32767);case Int8Array:return Math.round(i*127);default:throw new Error("Invalid component type.")}}class Se{constructor(e=0,t=0){Se.prototype.isVector2=!0,this.x=e,this.y=t}get width(){return this.x}set width(e){this.x=e}get height(){return this.y}set height(e){this.y=e}set(e,t){return this.x=e,this.y=t,this}setScalar(e){return this.x=e,this.y=e,this}setX(e){return this.x=e,this}setY(e){return this.y=e,this}setComponent(e,t){switch(e){case 0:this.x=t;break;case 1:this.y=t;break;default:throw new Error("index is out of range: "+e)}return this}getComponent(e){switch(e){case 0:return this.x;case 1:return this.y;default:throw new Error("index is out of range: "+e)}}clone(){return new this.constructor(this.x,this.y)}copy(e){return this.x=e.x,this.y=e.y,this}add(e){return this.x+=e.x,this.y+=e.y,this}addScalar(e){return this.x+=e,this.y+=e,this}addVectors(e,t){return this.x=e.x+t.x,this.y=e.y+t.y,this}addScaledVector(e,t){return this.x+=e.x*t,this.y+=e.y*t,this}sub(e){return this.x-=e.x,this.y-=e.y,this}subScalar(e){return this.x-=e,this.y-=e,this}subVectors(e,t){return this.x=e.x-t.x,this.y=e.y-t.y,this}multiply(e){return this.x*=e.x,this.y*=e.y,this}multiplyScalar(e){return this.x*=e,this.y*=e,this}divide(e){return this.x/=e.x,this.y/=e.y,this}divideScalar(e){return this.multiplyScalar(1/e)}applyMatrix3(e){const t=this.x,n=this.y,r=e.elements;return this.x=r[0]*t+r[3]*n+r[6],this.y=r[1]*t+r[4]*n+r[7],this}min(e){return this.x=Math.min(this.x,e.x),this.y=Math.min(this.y,e.y),this}max(e){return this.x=Math.max(this.x,e.x),this.y=Math.max(this.y,e.y),this}clamp(e,t){return this.x=Math.max(e.x,Math.min(t.x,this.x)),this.y=Math.max(e.y,Math.min(t.y,this.y)),this}clampScalar(e,t){return this.x=Math.max(e,Math.min(t,this.x)),this.y=Math.max(e,Math.min(t,this.y)),this}clampLength(e,t){const n=this.length();return this.divideScalar(n||1).multiplyScalar(Math.max(e,Math.min(t,n)))}floor(){return this.x=Math.floor(this.x),this.y=Math.floor(this.y),this}ceil(){return this.x=Math.ceil(this.x),this.y=Math.ceil(this.y),this}round(){return this.x=Math.round(this.x),this.y=Math.round(this.y),this}roundToZero(){return this.x=this.x<0?Math.ceil(this.x):Math.floor(this.x),this.y=this.y<0?Math.ceil(this.y):Math.floor(this.y),this}negate(){return this.x=-this.x,this.y=-this.y,this}dot(e){return this.x*e.x+this.y*e.y}cross(e){return this.x*e.y-this.y*e.x}lengthSq(){return this.x*this.x+this.y*this.y}length(){return Math.sqrt(this.x*this.x+this.y*this.y)}manhattanLength(){return Math.abs(this.x)+Math.abs(this.y)}normalize(){return this.divideScalar(this.length()||1)}angle(){return Math.atan2(-this.y,-this.x)+Math.PI}distanceTo(e){return Math.sqrt(this.distanceToSquared(e))}distanceToSquared(e){const t=this.x-e.x,n=this.y-e.y;return t*t+n*n}manhattanDistanceTo(e){return Math.abs(this.x-e.x)+Math.abs(this.y-e.y)}setLength(e){return this.normalize().multiplyScalar(e)}lerp(e,t){return this.x+=(e.x-this.x)*t,this.y+=(e.y-this.y)*t,this}lerpVectors(e,t,n){return this.x=e.x+(t.x-e.x)*n,this.y=e.y+(t.y-e.y)*n,this}equals(e){return e.x===this.x&&e.y===this.y}fromArray(e,t=0){return this.x=e[t],this.y=e[t+1],this}toArray(e=[],t=0){return e[t]=this.x,e[t+1]=this.y,e}fromBufferAttribute(e,t){return this.x=e.getX(t),this.y=e.getY(t),this}rotateAround(e,t){const n=Math.cos(t),r=Math.sin(t),s=this.x-e.x,o=this.y-e.y;return this.x=s*n-o*r+e.x,this.y=s*r+o*n+e.y,this}random(){return this.x=Math.random(),this.y=Math.random(),this}*[Symbol.iterator](){yield this.x,yield this.y}}class St{constructor(){St.prototype.isMatrix3=!0,this.elements=[1,0,0,0,1,0,0,0,1]}set(e,t,n,r,s,o,a,l,c){const u=this.elements;return u[0]=e,u[1]=r,u[2]=a,u[3]=t,u[4]=s,u[5]=l,u[6]=n,u[7]=o,u[8]=c,this}identity(){return this.set(1,0,0,0,1,0,0,0,1),this}copy(e){const t=this.elements,n=e.elements;return t[0]=n[0],t[1]=n[1],t[2]=n[2],t[3]=n[3],t[4]=n[4],t[5]=n[5],t[6]=n[6],t[7]=n[7],t[8]=n[8],this}extractBasis(e,t,n){return e.setFromMatrix3Column(this,0),t.setFromMatrix3Column(this,1),n.setFromMatrix3Column(this,2),this}setFromMatrix4(e){const t=e.elements;return this.set(t[0],t[4],t[8],t[1],t[5],t[9],t[2],t[6],t[10]),this}multiply(e){return this.multiplyMatrices(this,e)}premultiply(e){return this.multiplyMatrices(e,this)}multiplyMatrices(e,t){const n=e.elements,r=t.elements,s=this.elements,o=n[0],a=n[3],l=n[6],c=n[1],u=n[4],d=n[7],f=n[2],m=n[5],g=n[8],p=r[0],h=r[3],x=r[6],E=r[1],b=r[4],S=r[7],y=r[2],R=r[5],U=r[8];return s[0]=o*p+a*E+l*y,s[3]=o*h+a*b+l*R,s[6]=o*x+a*S+l*U,s[1]=c*p+u*E+d*y,s[4]=c*h+u*b+d*R,s[7]=c*x+u*S+d*U,s[2]=f*p+m*E+g*y,s[5]=f*h+m*b+g*R,s[8]=f*x+m*S+g*U,this}multiplyScalar(e){const t=this.elements;return t[0]*=e,t[3]*=e,t[6]*=e,t[1]*=e,t[4]*=e,t[7]*=e,t[2]*=e,t[5]*=e,t[8]*=e,this}determinant(){const e=this.elements,t=e[0],n=e[1],r=e[2],s=e[3],o=e[4],a=e[5],l=e[6],c=e[7],u=e[8];return t*o*u-t*a*c-n*s*u+n*a*l+r*s*c-r*o*l}invert(){const e=this.elements,t=e[0],n=e[1],r=e[2],s=e[3],o=e[4],a=e[5],l=e[6],c=e[7],u=e[8],d=u*o-a*c,f=a*l-u*s,m=c*s-o*l,g=t*d+n*f+r*m;if(g===0)return this.set(0,0,0,0,0,0,0,0,0);const p=1/g;return e[0]=d*p,e[1]=(r*c-u*n)*p,e[2]=(a*n-r*o)*p,e[3]=f*p,e[4]=(u*t-r*l)*p,e[5]=(r*s-a*t)*p,e[6]=m*p,e[7]=(n*l-c*t)*p,e[8]=(o*t-n*s)*p,this}transpose(){let e;const t=this.elements;return e=t[1],t[1]=t[3],t[3]=e,e=t[2],t[2]=t[6],t[6]=e,e=t[5],t[5]=t[7],t[7]=e,this}getNormalMatrix(e){return this.setFromMatrix4(e).invert().transpose()}transposeIntoArray(e){const t=this.elements;return e[0]=t[0],e[1]=t[3],e[2]=t[6],e[3]=t[1],e[4]=t[4],e[5]=t[7],e[6]=t[2],e[7]=t[5],e[8]=t[8],this}setUvTransform(e,t,n,r,s,o,a){const l=Math.cos(s),c=Math.sin(s);return this.set(n*l,n*c,-n*(l*o+c*a)+o+e,-r*c,r*l,-r*(-c*o+l*a)+a+t,0,0,1),this}scale(e,t){return this.premultiply(zr.makeScale(e,t)),this}rotate(e){return this.premultiply(zr.makeRotation(-e)),this}translate(e,t){return this.premultiply(zr.makeTranslation(e,t)),this}makeTranslation(e,t){return this.set(1,0,e,0,1,t,0,0,1),this}makeRotation(e){const t=Math.cos(e),n=Math.sin(e);return this.set(t,-n,0,n,t,0,0,0,1),this}makeScale(e,t){return this.set(e,0,0,0,t,0,0,0,1),this}equals(e){const t=this.elements,n=e.elements;for(let r=0;r<9;r++)if(t[r]!==n[r])return!1;return!0}fromArray(e,t=0){for(let n=0;n<9;n++)this.elements[n]=e[n+t];return this}toArray(e=[],t=0){const n=this.elements;return e[t]=n[0],e[t+1]=n[1],e[t+2]=n[2],e[t+3]=n[3],e[t+4]=n[4],e[t+5]=n[5],e[t+6]=n[6],e[t+7]=n[7],e[t+8]=n[8],e}clone(){return new this.constructor().fromArray(this.elements)}}const zr=new St;function So(i){for(let e=i.length-1;e>=0;--e)if(i[e]>=65535)return!0;return!1}function Li(i){return document.createElementNS("http://www.w3.org/1999/xhtml",i)}function Tn(i){return i<.04045?i*.0773993808:Math.pow(i*.9478672986+.0521327014,2.4)}function ur(i){return i<.0031308?i*12.92:1.055*Math.pow(i,.41666)-.055}const Vr={[Zt]:{[Ci]:Tn},[Ci]:{[Zt]:ur}},ut={legacyMode:!0,get workingColorSpace(){return Ci},set workingColorSpace(i){console.warn("THREE.ColorManagement: .workingColorSpace is readonly.")},convert:function(i,e,t){if(this.legacyMode||e===t||!e||!t)return i;if(Vr[e]&&Vr[e][t]!==void 0){const n=Vr[e][t];return i.r=n(i.r),i.g=n(i.g),i.b=n(i.b),i}throw new Error("Unsupported color space conversion.")},fromWorkingColorSpace:function(i,e){return this.convert(i,this.workingColorSpace,e)},toWorkingColorSpace:function(i,e){return this.convert(i,e,this.workingColorSpace)}},wo={aliceblue:15792383,antiquewhite:16444375,aqua:65535,aquamarine:8388564,azure:15794175,beige:16119260,bisque:16770244,black:0,blanchedalmond:16772045,blue:255,blueviolet:9055202,brown:10824234,burlywood:14596231,cadetblue:6266528,chartreuse:8388352,chocolate:13789470,coral:16744272,cornflowerblue:6591981,cornsilk:16775388,crimson:14423100,cyan:65535,darkblue:139,darkcyan:35723,darkgoldenrod:12092939,darkgray:11119017,darkgreen:25600,darkgrey:11119017,darkkhaki:12433259,darkmagenta:9109643,darkolivegreen:5597999,darkorange:16747520,darkorchid:10040012,darkred:9109504,darksalmon:15308410,darkseagreen:9419919,darkslateblue:4734347,darkslategray:3100495,darkslategrey:3100495,darkturquoise:52945,darkviolet:9699539,deeppink:16716947,deepskyblue:49151,dimgray:6908265,dimgrey:6908265,dodgerblue:2003199,firebrick:11674146,floralwhite:16775920,forestgreen:2263842,fuchsia:16711935,gainsboro:14474460,ghostwhite:16316671,gold:16766720,goldenrod:14329120,gray:8421504,green:32768,greenyellow:11403055,grey:8421504,honeydew:15794160,hotpink:16738740,indianred:13458524,indigo:4915330,ivory:16777200,khaki:15787660,lavender:15132410,lavenderblush:16773365,lawngreen:8190976,lemonchiffon:16775885,lightblue:11393254,lightcoral:15761536,lightcyan:14745599,lightgoldenrodyellow:16448210,lightgray:13882323,lightgreen:9498256,lightgrey:13882323,lightpink:16758465,lightsalmon:16752762,lightseagreen:2142890,lightskyblue:8900346,lightslategray:7833753,lightslategrey:7833753,lightsteelblue:11584734,lightyellow:16777184,lime:65280,limegreen:3329330,linen:16445670,magenta:16711935,maroon:8388608,mediumaquamarine:6737322,mediumblue:205,mediumorchid:12211667,mediumpurple:9662683,mediumseagreen:3978097,mediumslateblue:8087790,mediumspringgreen:64154,mediumturquoise:4772300,mediumvioletred:13047173,midnightblue:1644912,mintcream:16121850,mistyrose:16770273,moccasin:16770229,navajowhite:16768685,navy:128,oldlace:16643558,olive:8421376,olivedrab:7048739,orange:16753920,orangered:16729344,orchid:14315734,palegoldenrod:15657130,palegreen:10025880,paleturquoise:11529966,palevioletred:14381203,papayawhip:16773077,peachpuff:16767673,peru:13468991,pink:16761035,plum:14524637,powderblue:11591910,purple:8388736,rebeccapurple:6697881,red:16711680,rosybrown:12357519,royalblue:4286945,saddlebrown:9127187,salmon:16416882,sandybrown:16032864,seagreen:3050327,seashell:16774638,sienna:10506797,silver:12632256,skyblue:8900331,slateblue:6970061,slategray:7372944,slategrey:7372944,snow:16775930,springgreen:65407,steelblue:4620980,tan:13808780,teal:32896,thistle:14204888,tomato:16737095,turquoise:4251856,violet:15631086,wheat:16113331,white:16777215,whitesmoke:16119285,yellow:16776960,yellowgreen:10145074},Ke={r:0,g:0,b:0},Nt={h:0,s:0,l:0},Fi={h:0,s:0,l:0};function Dr(i,e,t){return t<0&&(t+=1),t>1&&(t-=1),t<1/6?i+(e-i)*6*t:t<1/2?e:t<2/3?i+(e-i)*6*(2/3-t):i}function qi(i,e){return e.r=i.r,e.g=i.g,e.b=i.b,e}class qe{constructor(e,t,n){return this.isColor=!0,this.r=1,this.g=1,this.b=1,t===void 0&&n===void 0?this.set(e):this.setRGB(e,t,n)}set(e){return e&&e.isColor?this.copy(e):typeof e=="number"?this.setHex(e):typeof e=="string"&&this.setStyle(e),this}setScalar(e){return this.r=e,this.g=e,this.b=e,this}setHex(e,t=Zt){return e=Math.floor(e),this.r=(e>>16&255)/255,this.g=(e>>8&255)/255,this.b=(e&255)/255,ut.toWorkingColorSpace(this,t),this}setRGB(e,t,n,r=ut.workingColorSpace){return this.r=e,this.g=t,this.b=n,ut.toWorkingColorSpace(this,r),this}setHSL(e,t,n,r=ut.workingColorSpace){if(e=$l(e,1),t=vt(t,0,1),n=vt(n,0,1),t===0)this.r=this.g=this.b=n;else{const s=n<=.5?n*(1+t):n+t-n*t,o=2*n-s;this.r=Dr(o,s,e+1/3),this.g=Dr(o,s,e),this.b=Dr(o,s,e-1/3)}return ut.toWorkingColorSpace(this,r),this}setStyle(e,t=Zt){function n(s){s!==void 0&&parseFloat(s)<1&&console.warn("THREE.Color: Alpha component of "+e+" will be ignored.")}let r;if(r=/^((?:rgb|hsl)a?)\(([^\)]*)\)/.exec(e)){let s;const o=r[1],a=r[2];switch(o){case"rgb":case"rgba":if(s=/^\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(?:,\s*(\d*\.?\d+)\s*)?$/.exec(a))return this.r=Math.min(255,parseInt(s[1],10))/255,this.g=Math.min(255,parseInt(s[2],10))/255,this.b=Math.min(255,parseInt(s[3],10))/255,ut.toWorkingColorSpace(this,t),n(s[4]),this;if(s=/^\s*(\d+)\%\s*,\s*(\d+)\%\s*,\s*(\d+)\%\s*(?:,\s*(\d*\.?\d+)\s*)?$/.exec(a))return this.r=Math.min(100,parseInt(s[1],10))/100,this.g=Math.min(100,parseInt(s[2],10))/100,this.b=Math.min(100,parseInt(s[3],10))/100,ut.toWorkingColorSpace(this,t),n(s[4]),this;break;case"hsl":case"hsla":if(s=/^\s*(\d*\.?\d+)\s*,\s*(\d*\.?\d+)\%\s*,\s*(\d*\.?\d+)\%\s*(?:,\s*(\d*\.?\d+)\s*)?$/.exec(a)){const l=parseFloat(s[1])/360,c=parseFloat(s[2])/100,u=parseFloat(s[3])/100;return n(s[4]),this.setHSL(l,c,u,t)}break}}else if(r=/^\#([A-Fa-f\d]+)$/.exec(e)){const s=r[1],o=s.length;if(o===3)return this.r=parseInt(s.charAt(0)+s.charAt(0),16)/255,this.g=parseInt(s.charAt(1)+s.charAt(1),16)/255,this.b=parseInt(s.charAt(2)+s.charAt(2),16)/255,ut.toWorkingColorSpace(this,t),this;if(o===6)return this.r=parseInt(s.charAt(0)+s.charAt(1),16)/255,this.g=parseInt(s.charAt(2)+s.charAt(3),16)/255,this.b=parseInt(s.charAt(4)+s.charAt(5),16)/255,ut.toWorkingColorSpace(this,t),this}return e&&e.length>0?this.setColorName(e,t):this}setColorName(e,t=Zt){const n=wo[e.toLowerCase()];return n!==void 0?this.setHex(n,t):console.warn("THREE.Color: Unknown color "+e),this}clone(){return new this.constructor(this.r,this.g,this.b)}copy(e){return this.r=e.r,this.g=e.g,this.b=e.b,this}copySRGBToLinear(e){return this.r=Tn(e.r),this.g=Tn(e.g),this.b=Tn(e.b),this}copyLinearToSRGB(e){return this.r=ur(e.r),this.g=ur(e.g),this.b=ur(e.b),this}convertSRGBToLinear(){return this.copySRGBToLinear(this),this}convertLinearToSRGB(){return this.copyLinearToSRGB(this),this}getHex(e=Zt){return ut.fromWorkingColorSpace(qi(this,Ke),e),vt(Ke.r*255,0,255)<<16^vt(Ke.g*255,0,255)<<8^vt(Ke.b*255,0,255)<<0}getHexString(e=Zt){return("000000"+this.getHex(e).toString(16)).slice(-6)}getHSL(e,t=ut.workingColorSpace){ut.fromWorkingColorSpace(qi(this,Ke),t);const n=Ke.r,r=Ke.g,s=Ke.b,o=Math.max(n,r,s),a=Math.min(n,r,s);let l,c;const u=(a+o)/2;if(a===o)l=0,c=0;else{const d=o-a;switch(c=u<=.5?d/(o+a):d/(2-o-a),o){case n:l=(r-s)/d+(r<s?6:0);break;case r:l=(s-n)/d+2;break;case s:l=(n-r)/d+4;break}l/=6}return e.h=l,e.s=c,e.l=u,e}getRGB(e,t=ut.workingColorSpace){return ut.fromWorkingColorSpace(qi(this,Ke),t),e.r=Ke.r,e.g=Ke.g,e.b=Ke.b,e}getStyle(e=Zt){return ut.fromWorkingColorSpace(qi(this,Ke),e),e!==Zt?`color(${e} ${Ke.r} ${Ke.g} ${Ke.b})`:`rgb(${Ke.r*255|0},${Ke.g*255|0},${Ke.b*255|0})`}offsetHSL(e,t,n){return this.getHSL(Nt),Nt.h+=e,Nt.s+=t,Nt.l+=n,this.setHSL(Nt.h,Nt.s,Nt.l),this}add(e){return this.r+=e.r,this.g+=e.g,this.b+=e.b,this}addColors(e,t){return this.r=e.r+t.r,this.g=e.g+t.g,this.b=e.b+t.b,this}addScalar(e){return this.r+=e,this.g+=e,this.b+=e,this}sub(e){return this.r=Math.max(0,this.r-e.r),this.g=Math.max(0,this.g-e.g),this.b=Math.max(0,this.b-e.b),this}multiply(e){return this.r*=e.r,this.g*=e.g,this.b*=e.b,this}multiplyScalar(e){return this.r*=e,this.g*=e,this.b*=e,this}lerp(e,t){return this.r+=(e.r-this.r)*t,this.g+=(e.g-this.g)*t,this.b+=(e.b-this.b)*t,this}lerpColors(e,t,n){return this.r=e.r+(t.r-e.r)*n,this.g=e.g+(t.g-e.g)*n,this.b=e.b+(t.b-e.b)*n,this}lerpHSL(e,t){this.getHSL(Nt),e.getHSL(Fi);const n=Pr(Nt.h,Fi.h,t),r=Pr(Nt.s,Fi.s,t),s=Pr(Nt.l,Fi.l,t);return this.setHSL(n,r,s),this}equals(e){return e.r===this.r&&e.g===this.g&&e.b===this.b}fromArray(e,t=0){return this.r=e[t],this.g=e[t+1],this.b=e[t+2],this}toArray(e=[],t=0){return e[t]=this.r,e[t+1]=this.g,e[t+2]=this.b,e}fromBufferAttribute(e,t){return this.r=e.getX(t),this.g=e.getY(t),this.b=e.getZ(t),this}toJSON(){return this.getHex()}*[Symbol.iterator](){yield this.r,yield this.g,yield this.b}}qe.NAMES=wo;let Un;class Eo{static getDataURL(e){if(/^data:/i.test(e.src)||typeof HTMLCanvasElement>"u")return e.src;let t;if(e instanceof HTMLCanvasElement)t=e;else{Un===void 0&&(Un=Li("canvas")),Un.width=e.width,Un.height=e.height;const n=Un.getContext("2d");e instanceof ImageData?n.putImageData(e,0,0):n.drawImage(e,0,0,e.width,e.height),t=Un}return t.width>2048||t.height>2048?(console.warn("THREE.ImageUtils.getDataURL: Image converted to jpg for performance reasons",e),t.toDataURL("image/jpeg",.6)):t.toDataURL("image/png")}static sRGBToLinear(e){if(typeof HTMLImageElement<"u"&&e instanceof HTMLImageElement||typeof HTMLCanvasElement<"u"&&e instanceof HTMLCanvasElement||typeof ImageBitmap<"u"&&e instanceof ImageBitmap){const t=Li("canvas");t.width=e.width,t.height=e.height;const n=t.getContext("2d");n.drawImage(e,0,0,e.width,e.height);const r=n.getImageData(0,0,e.width,e.height),s=r.data;for(let o=0;o<s.length;o++)s[o]=Tn(s[o]/255)*255;return n.putImageData(r,0,0),t}else if(e.data){const t=e.data.slice(0);for(let n=0;n<t.length;n++)t instanceof Uint8Array||t instanceof Uint8ClampedArray?t[n]=Math.floor(Tn(t[n]/255)*255):t[n]=Tn(t[n]);return{data:t,width:e.width,height:e.height}}else return console.warn("THREE.ImageUtils.sRGBToLinear(): Unsupported image type. No color space conversion applied."),e}}class To{constructor(e=null){this.isSource=!0,this.uuid=zi(),this.data=e,this.version=0}set needsUpdate(e){e===!0&&this.version++}toJSON(e){const t=e===void 0||typeof e=="string";if(!t&&e.images[this.uuid]!==void 0)return e.images[this.uuid];const n={uuid:this.uuid,url:""},r=this.data;if(r!==null){let s;if(Array.isArray(r)){s=[];for(let o=0,a=r.length;o<a;o++)r[o].isDataTexture?s.push(Ur(r[o].image)):s.push(Ur(r[o]))}else s=Ur(r);n.url=s}return t||(e.images[this.uuid]=n),n}}function Ur(i){return typeof HTMLImageElement<"u"&&i instanceof HTMLImageElement||typeof HTMLCanvasElement<"u"&&i instanceof HTMLCanvasElement||typeof ImageBitmap<"u"&&i instanceof ImageBitmap?Eo.getDataURL(i):i.data?{data:Array.from(i.data),width:i.width,height:i.height,type:i.data.constructor.name}:(console.warn("THREE.Texture: Unable to serialize Texture."),{})}let ec=0;class ft extends Pn{constructor(e=ft.DEFAULT_IMAGE,t=ft.DEFAULT_MAPPING,n=Bt,r=Bt,s=Ct,o=vr,a=kt,l=In,c=ft.DEFAULT_ANISOTROPY,u=Rn){super(),this.isTexture=!0,Object.defineProperty(this,"id",{value:ec++}),this.uuid=zi(),this.name="",this.source=new To(e),this.mipmaps=[],this.mapping=t,this.wrapS=n,this.wrapT=r,this.magFilter=s,this.minFilter=o,this.anisotropy=c,this.format=a,this.internalFormat=null,this.type=l,this.offset=new Se(0,0),this.repeat=new Se(1,1),this.center=new Se(0,0),this.rotation=0,this.matrixAutoUpdate=!0,this.matrix=new St,this.generateMipmaps=!0,this.premultiplyAlpha=!1,this.flipY=!0,this.unpackAlignment=4,this.encoding=u,this.userData={},this.version=0,this.onUpdate=null,this.isRenderTargetTexture=!1,this.needsPMREMUpdate=!1}get image(){return this.source.data}set image(e){this.source.data=e}updateMatrix(){this.matrix.setUvTransform(this.offset.x,this.offset.y,this.repeat.x,this.repeat.y,this.rotation,this.center.x,this.center.y)}clone(){return new this.constructor().copy(this)}copy(e){return this.name=e.name,this.source=e.source,this.mipmaps=e.mipmaps.slice(0),this.mapping=e.mapping,this.wrapS=e.wrapS,this.wrapT=e.wrapT,this.magFilter=e.magFilter,this.minFilter=e.minFilter,this.anisotropy=e.anisotropy,this.format=e.format,this.internalFormat=e.internalFormat,this.type=e.type,this.offset.copy(e.offset),this.repeat.copy(e.repeat),this.center.copy(e.center),this.rotation=e.rotation,this.matrixAutoUpdate=e.matrixAutoUpdate,this.matrix.copy(e.matrix),this.generateMipmaps=e.generateMipmaps,this.premultiplyAlpha=e.premultiplyAlpha,this.flipY=e.flipY,this.unpackAlignment=e.unpackAlignment,this.encoding=e.encoding,this.userData=JSON.parse(JSON.stringify(e.userData)),this.needsUpdate=!0,this}toJSON(e){const t=e===void 0||typeof e=="string";if(!t&&e.textures[this.uuid]!==void 0)return e.textures[this.uuid];const n={metadata:{version:4.5,type:"Texture",generator:"Texture.toJSON"},uuid:this.uuid,name:this.name,image:this.source.toJSON(e).uuid,mapping:this.mapping,repeat:[this.repeat.x,this.repeat.y],offset:[this.offset.x,this.offset.y],center:[this.center.x,this.center.y],rotation:this.rotation,wrap:[this.wrapS,this.wrapT],format:this.format,type:this.type,encoding:this.encoding,minFilter:this.minFilter,magFilter:this.magFilter,anisotropy:this.anisotropy,flipY:this.flipY,premultiplyAlpha:this.premultiplyAlpha,unpackAlignment:this.unpackAlignment};return JSON.stringify(this.userData)!=="{}"&&(n.userData=this.userData),t||(e.textures[this.uuid]=n),n}dispose(){this.dispatchEvent({type:"dispose"})}transformUv(e){if(this.mapping!==_o)return e;if(e.applyMatrix3(this.matrix),e.x<0||e.x>1)switch(this.wrapS){case ps:e.x=e.x-Math.floor(e.x);break;case Bt:e.x=e.x<0?0:1;break;case ms:Math.abs(Math.floor(e.x)%2)===1?e.x=Math.ceil(e.x)-e.x:e.x=e.x-Math.floor(e.x);break}if(e.y<0||e.y>1)switch(this.wrapT){case ps:e.y=e.y-Math.floor(e.y);break;case Bt:e.y=e.y<0?0:1;break;case ms:Math.abs(Math.floor(e.y)%2)===1?e.y=Math.ceil(e.y)-e.y:e.y=e.y-Math.floor(e.y);break}return this.flipY&&(e.y=1-e.y),e}set needsUpdate(e){e===!0&&(this.version++,this.source.needsUpdate=!0)}}ft.DEFAULT_IMAGE=null;ft.DEFAULT_MAPPING=_o;ft.DEFAULT_ANISOTROPY=1;class Xe{constructor(e=0,t=0,n=0,r=1){Xe.prototype.isVector4=!0,this.x=e,this.y=t,this.z=n,this.w=r}get width(){return this.z}set width(e){this.z=e}get height(){return this.w}set height(e){this.w=e}set(e,t,n,r){return this.x=e,this.y=t,this.z=n,this.w=r,this}setScalar(e){return this.x=e,this.y=e,this.z=e,this.w=e,this}setX(e){return this.x=e,this}setY(e){return this.y=e,this}setZ(e){return this.z=e,this}setW(e){return this.w=e,this}setComponent(e,t){switch(e){case 0:this.x=t;break;case 1:this.y=t;break;case 2:this.z=t;break;case 3:this.w=t;break;default:throw new Error("index is out of range: "+e)}return this}getComponent(e){switch(e){case 0:return this.x;case 1:return this.y;case 2:return this.z;case 3:return this.w;default:throw new Error("index is out of range: "+e)}}clone(){return new this.constructor(this.x,this.y,this.z,this.w)}copy(e){return this.x=e.x,this.y=e.y,this.z=e.z,this.w=e.w!==void 0?e.w:1,this}add(e){return this.x+=e.x,this.y+=e.y,this.z+=e.z,this.w+=e.w,this}addScalar(e){return this.x+=e,this.y+=e,this.z+=e,this.w+=e,this}addVectors(e,t){return this.x=e.x+t.x,this.y=e.y+t.y,this.z=e.z+t.z,this.w=e.w+t.w,this}addScaledVector(e,t){return this.x+=e.x*t,this.y+=e.y*t,this.z+=e.z*t,this.w+=e.w*t,this}sub(e){return this.x-=e.x,this.y-=e.y,this.z-=e.z,this.w-=e.w,this}subScalar(e){return this.x-=e,this.y-=e,this.z-=e,this.w-=e,this}subVectors(e,t){return this.x=e.x-t.x,this.y=e.y-t.y,this.z=e.z-t.z,this.w=e.w-t.w,this}multiply(e){return this.x*=e.x,this.y*=e.y,this.z*=e.z,this.w*=e.w,this}multiplyScalar(e){return this.x*=e,this.y*=e,this.z*=e,this.w*=e,this}applyMatrix4(e){const t=this.x,n=this.y,r=this.z,s=this.w,o=e.elements;return this.x=o[0]*t+o[4]*n+o[8]*r+o[12]*s,this.y=o[1]*t+o[5]*n+o[9]*r+o[13]*s,this.z=o[2]*t+o[6]*n+o[10]*r+o[14]*s,this.w=o[3]*t+o[7]*n+o[11]*r+o[15]*s,this}divideScalar(e){return this.multiplyScalar(1/e)}setAxisAngleFromQuaternion(e){this.w=2*Math.acos(e.w);const t=Math.sqrt(1-e.w*e.w);return t<1e-4?(this.x=1,this.y=0,this.z=0):(this.x=e.x/t,this.y=e.y/t,this.z=e.z/t),this}setAxisAngleFromRotationMatrix(e){let t,n,r,s;const l=e.elements,c=l[0],u=l[4],d=l[8],f=l[1],m=l[5],g=l[9],p=l[2],h=l[6],x=l[10];if(Math.abs(u-f)<.01&&Math.abs(d-p)<.01&&Math.abs(g-h)<.01){if(Math.abs(u+f)<.1&&Math.abs(d+p)<.1&&Math.abs(g+h)<.1&&Math.abs(c+m+x-3)<.1)return this.set(1,0,0,0),this;t=Math.PI;const b=(c+1)/2,S=(m+1)/2,y=(x+1)/2,R=(u+f)/4,U=(d+p)/4,v=(g+h)/4;return b>S&&b>y?b<.01?(n=0,r=.707106781,s=.707106781):(n=Math.sqrt(b),r=R/n,s=U/n):S>y?S<.01?(n=.707106781,r=0,s=.707106781):(r=Math.sqrt(S),n=R/r,s=v/r):y<.01?(n=.707106781,r=.707106781,s=0):(s=Math.sqrt(y),n=U/s,r=v/s),this.set(n,r,s,t),this}let E=Math.sqrt((h-g)*(h-g)+(d-p)*(d-p)+(f-u)*(f-u));return Math.abs(E)<.001&&(E=1),this.x=(h-g)/E,this.y=(d-p)/E,this.z=(f-u)/E,this.w=Math.acos((c+m+x-1)/2),this}min(e){return this.x=Math.min(this.x,e.x),this.y=Math.min(this.y,e.y),this.z=Math.min(this.z,e.z),this.w=Math.min(this.w,e.w),this}max(e){return this.x=Math.max(this.x,e.x),this.y=Math.max(this.y,e.y),this.z=Math.max(this.z,e.z),this.w=Math.max(this.w,e.w),this}clamp(e,t){return this.x=Math.max(e.x,Math.min(t.x,this.x)),this.y=Math.max(e.y,Math.min(t.y,this.y)),this.z=Math.max(e.z,Math.min(t.z,this.z)),this.w=Math.max(e.w,Math.min(t.w,this.w)),this}clampScalar(e,t){return this.x=Math.max(e,Math.min(t,this.x)),this.y=Math.max(e,Math.min(t,this.y)),this.z=Math.max(e,Math.min(t,this.z)),this.w=Math.max(e,Math.min(t,this.w)),this}clampLength(e,t){const n=this.length();return this.divideScalar(n||1).multiplyScalar(Math.max(e,Math.min(t,n)))}floor(){return this.x=Math.floor(this.x),this.y=Math.floor(this.y),this.z=Math.floor(this.z),this.w=Math.floor(this.w),this}ceil(){return this.x=Math.ceil(this.x),this.y=Math.ceil(this.y),this.z=Math.ceil(this.z),this.w=Math.ceil(this.w),this}round(){return this.x=Math.round(this.x),this.y=Math.round(this.y),this.z=Math.round(this.z),this.w=Math.round(this.w),this}roundToZero(){return this.x=this.x<0?Math.ceil(this.x):Math.floor(this.x),this.y=this.y<0?Math.ceil(this.y):Math.floor(this.y),this.z=this.z<0?Math.ceil(this.z):Math.floor(this.z),this.w=this.w<0?Math.ceil(this.w):Math.floor(this.w),this}negate(){return this.x=-this.x,this.y=-this.y,this.z=-this.z,this.w=-this.w,this}dot(e){return this.x*e.x+this.y*e.y+this.z*e.z+this.w*e.w}lengthSq(){return this.x*this.x+this.y*this.y+this.z*this.z+this.w*this.w}length(){return Math.sqrt(this.x*this.x+this.y*this.y+this.z*this.z+this.w*this.w)}manhattanLength(){return Math.abs(this.x)+Math.abs(this.y)+Math.abs(this.z)+Math.abs(this.w)}normalize(){return this.divideScalar(this.length()||1)}setLength(e){return this.normalize().multiplyScalar(e)}lerp(e,t){return this.x+=(e.x-this.x)*t,this.y+=(e.y-this.y)*t,this.z+=(e.z-this.z)*t,this.w+=(e.w-this.w)*t,this}lerpVectors(e,t,n){return this.x=e.x+(t.x-e.x)*n,this.y=e.y+(t.y-e.y)*n,this.z=e.z+(t.z-e.z)*n,this.w=e.w+(t.w-e.w)*n,this}equals(e){return e.x===this.x&&e.y===this.y&&e.z===this.z&&e.w===this.w}fromArray(e,t=0){return this.x=e[t],this.y=e[t+1],this.z=e[t+2],this.w=e[t+3],this}toArray(e=[],t=0){return e[t]=this.x,e[t+1]=this.y,e[t+2]=this.z,e[t+3]=this.w,e}fromBufferAttribute(e,t){return this.x=e.getX(t),this.y=e.getY(t),this.z=e.getZ(t),this.w=e.getW(t),this}random(){return this.x=Math.random(),this.y=Math.random(),this.z=Math.random(),this.w=Math.random(),this}*[Symbol.iterator](){yield this.x,yield this.y,yield this.z,yield this.w}}class Cn extends Pn{constructor(e=1,t=1,n={}){super(),this.isWebGLRenderTarget=!0,this.width=e,this.height=t,this.depth=1,this.scissor=new Xe(0,0,e,t),this.scissorTest=!1,this.viewport=new Xe(0,0,e,t);const r={width:e,height:t,depth:1};this.texture=new ft(r,n.mapping,n.wrapS,n.wrapT,n.magFilter,n.minFilter,n.format,n.type,n.anisotropy,n.encoding),this.texture.isRenderTargetTexture=!0,this.texture.flipY=!1,this.texture.generateMipmaps=n.generateMipmaps!==void 0?n.generateMipmaps:!1,this.texture.internalFormat=n.internalFormat!==void 0?n.internalFormat:null,this.texture.minFilter=n.minFilter!==void 0?n.minFilter:Ct,this.depthBuffer=n.depthBuffer!==void 0?n.depthBuffer:!0,this.stencilBuffer=n.stencilBuffer!==void 0?n.stencilBuffer:!1,this.depthTexture=n.depthTexture!==void 0?n.depthTexture:null,this.samples=n.samples!==void 0?n.samples:0}setSize(e,t,n=1){(this.width!==e||this.height!==t||this.depth!==n)&&(this.width=e,this.height=t,this.depth=n,this.texture.image.width=e,this.texture.image.height=t,this.texture.image.depth=n,this.dispose()),this.viewport.set(0,0,e,t),this.scissor.set(0,0,e,t)}clone(){return new this.constructor().copy(this)}copy(e){this.width=e.width,this.height=e.height,this.depth=e.depth,this.viewport.copy(e.viewport),this.texture=e.texture.clone(),this.texture.isRenderTargetTexture=!0;const t=Object.assign({},e.texture.image);return this.texture.source=new To(t),this.depthBuffer=e.depthBuffer,this.stencilBuffer=e.stencilBuffer,e.depthTexture!==null&&(this.depthTexture=e.depthTexture.clone()),this.samples=e.samples,this}dispose(){this.dispatchEvent({type:"dispose"})}}class Io extends ft{constructor(e=null,t=1,n=1,r=1){super(null),this.isDataArrayTexture=!0,this.image={data:e,width:t,height:n,depth:r},this.magFilter=gt,this.minFilter=gt,this.wrapR=Bt,this.generateMipmaps=!1,this.flipY=!1,this.unpackAlignment=1}}class tc extends ft{constructor(e=null,t=1,n=1,r=1){super(null),this.isData3DTexture=!0,this.image={data:e,width:t,height:n,depth:r},this.magFilter=gt,this.minFilter=gt,this.wrapR=Bt,this.generateMipmaps=!1,this.flipY=!1,this.unpackAlignment=1}}class Ln{constructor(e=0,t=0,n=0,r=1){this.isQuaternion=!0,this._x=e,this._y=t,this._z=n,this._w=r}static slerpFlat(e,t,n,r,s,o,a){let l=n[r+0],c=n[r+1],u=n[r+2],d=n[r+3];const f=s[o+0],m=s[o+1],g=s[o+2],p=s[o+3];if(a===0){e[t+0]=l,e[t+1]=c,e[t+2]=u,e[t+3]=d;return}if(a===1){e[t+0]=f,e[t+1]=m,e[t+2]=g,e[t+3]=p;return}if(d!==p||l!==f||c!==m||u!==g){let h=1-a;const x=l*f+c*m+u*g+d*p,E=x>=0?1:-1,b=1-x*x;if(b>Number.EPSILON){const y=Math.sqrt(b),R=Math.atan2(y,x*E);h=Math.sin(h*R)/y,a=Math.sin(a*R)/y}const S=a*E;if(l=l*h+f*S,c=c*h+m*S,u=u*h+g*S,d=d*h+p*S,h===1-a){const y=1/Math.sqrt(l*l+c*c+u*u+d*d);l*=y,c*=y,u*=y,d*=y}}e[t]=l,e[t+1]=c,e[t+2]=u,e[t+3]=d}static multiplyQuaternionsFlat(e,t,n,r,s,o){const a=n[r],l=n[r+1],c=n[r+2],u=n[r+3],d=s[o],f=s[o+1],m=s[o+2],g=s[o+3];return e[t]=a*g+u*d+l*m-c*f,e[t+1]=l*g+u*f+c*d-a*m,e[t+2]=c*g+u*m+a*f-l*d,e[t+3]=u*g-a*d-l*f-c*m,e}get x(){return this._x}set x(e){this._x=e,this._onChangeCallback()}get y(){return this._y}set y(e){this._y=e,this._onChangeCallback()}get z(){return this._z}set z(e){this._z=e,this._onChangeCallback()}get w(){return this._w}set w(e){this._w=e,this._onChangeCallback()}set(e,t,n,r){return this._x=e,this._y=t,this._z=n,this._w=r,this._onChangeCallback(),this}clone(){return new this.constructor(this._x,this._y,this._z,this._w)}copy(e){return this._x=e.x,this._y=e.y,this._z=e.z,this._w=e.w,this._onChangeCallback(),this}setFromEuler(e,t){const n=e._x,r=e._y,s=e._z,o=e._order,a=Math.cos,l=Math.sin,c=a(n/2),u=a(r/2),d=a(s/2),f=l(n/2),m=l(r/2),g=l(s/2);switch(o){case"XYZ":this._x=f*u*d+c*m*g,this._y=c*m*d-f*u*g,this._z=c*u*g+f*m*d,this._w=c*u*d-f*m*g;break;case"YXZ":this._x=f*u*d+c*m*g,this._y=c*m*d-f*u*g,this._z=c*u*g-f*m*d,this._w=c*u*d+f*m*g;break;case"ZXY":this._x=f*u*d-c*m*g,this._y=c*m*d+f*u*g,this._z=c*u*g+f*m*d,this._w=c*u*d-f*m*g;break;case"ZYX":this._x=f*u*d-c*m*g,this._y=c*m*d+f*u*g,this._z=c*u*g-f*m*d,this._w=c*u*d+f*m*g;break;case"YZX":this._x=f*u*d+c*m*g,this._y=c*m*d+f*u*g,this._z=c*u*g-f*m*d,this._w=c*u*d-f*m*g;break;case"XZY":this._x=f*u*d-c*m*g,this._y=c*m*d-f*u*g,this._z=c*u*g+f*m*d,this._w=c*u*d+f*m*g;break;default:console.warn("THREE.Quaternion: .setFromEuler() encountered an unknown order: "+o)}return t!==!1&&this._onChangeCallback(),this}setFromAxisAngle(e,t){const n=t/2,r=Math.sin(n);return this._x=e.x*r,this._y=e.y*r,this._z=e.z*r,this._w=Math.cos(n),this._onChangeCallback(),this}setFromRotationMatrix(e){const t=e.elements,n=t[0],r=t[4],s=t[8],o=t[1],a=t[5],l=t[9],c=t[2],u=t[6],d=t[10],f=n+a+d;if(f>0){const m=.5/Math.sqrt(f+1);this._w=.25/m,this._x=(u-l)*m,this._y=(s-c)*m,this._z=(o-r)*m}else if(n>a&&n>d){const m=2*Math.sqrt(1+n-a-d);this._w=(u-l)/m,this._x=.25*m,this._y=(r+o)/m,this._z=(s+c)/m}else if(a>d){const m=2*Math.sqrt(1+a-n-d);this._w=(s-c)/m,this._x=(r+o)/m,this._y=.25*m,this._z=(l+u)/m}else{const m=2*Math.sqrt(1+d-n-a);this._w=(o-r)/m,this._x=(s+c)/m,this._y=(l+u)/m,this._z=.25*m}return this._onChangeCallback(),this}setFromUnitVectors(e,t){let n=e.dot(t)+1;return n<Number.EPSILON?(n=0,Math.abs(e.x)>Math.abs(e.z)?(this._x=-e.y,this._y=e.x,this._z=0,this._w=n):(this._x=0,this._y=-e.z,this._z=e.y,this._w=n)):(this._x=e.y*t.z-e.z*t.y,this._y=e.z*t.x-e.x*t.z,this._z=e.x*t.y-e.y*t.x,this._w=n),this.normalize()}angleTo(e){return 2*Math.acos(Math.abs(vt(this.dot(e),-1,1)))}rotateTowards(e,t){const n=this.angleTo(e);if(n===0)return this;const r=Math.min(1,t/n);return this.slerp(e,r),this}identity(){return this.set(0,0,0,1)}invert(){return this.conjugate()}conjugate(){return this._x*=-1,this._y*=-1,this._z*=-1,this._onChangeCallback(),this}dot(e){return this._x*e._x+this._y*e._y+this._z*e._z+this._w*e._w}lengthSq(){return this._x*this._x+this._y*this._y+this._z*this._z+this._w*this._w}length(){return Math.sqrt(this._x*this._x+this._y*this._y+this._z*this._z+this._w*this._w)}normalize(){let e=this.length();return e===0?(this._x=0,this._y=0,this._z=0,this._w=1):(e=1/e,this._x=this._x*e,this._y=this._y*e,this._z=this._z*e,this._w=this._w*e),this._onChangeCallback(),this}multiply(e){return this.multiplyQuaternions(this,e)}premultiply(e){return this.multiplyQuaternions(e,this)}multiplyQuaternions(e,t){const n=e._x,r=e._y,s=e._z,o=e._w,a=t._x,l=t._y,c=t._z,u=t._w;return this._x=n*u+o*a+r*c-s*l,this._y=r*u+o*l+s*a-n*c,this._z=s*u+o*c+n*l-r*a,this._w=o*u-n*a-r*l-s*c,this._onChangeCallback(),this}slerp(e,t){if(t===0)return this;if(t===1)return this.copy(e);const n=this._x,r=this._y,s=this._z,o=this._w;let a=o*e._w+n*e._x+r*e._y+s*e._z;if(a<0?(this._w=-e._w,this._x=-e._x,this._y=-e._y,this._z=-e._z,a=-a):this.copy(e),a>=1)return this._w=o,this._x=n,this._y=r,this._z=s,this;const l=1-a*a;if(l<=Number.EPSILON){const m=1-t;return this._w=m*o+t*this._w,this._x=m*n+t*this._x,this._y=m*r+t*this._y,this._z=m*s+t*this._z,this.normalize(),this._onChangeCallback(),this}const c=Math.sqrt(l),u=Math.atan2(c,a),d=Math.sin((1-t)*u)/c,f=Math.sin(t*u)/c;return this._w=o*d+this._w*f,this._x=n*d+this._x*f,this._y=r*d+this._y*f,this._z=s*d+this._z*f,this._onChangeCallback(),this}slerpQuaternions(e,t,n){return this.copy(e).slerp(t,n)}random(){const e=Math.random(),t=Math.sqrt(1-e),n=Math.sqrt(e),r=2*Math.PI*Math.random(),s=2*Math.PI*Math.random();return this.set(t*Math.cos(r),n*Math.sin(s),n*Math.cos(s),t*Math.sin(r))}equals(e){return e._x===this._x&&e._y===this._y&&e._z===this._z&&e._w===this._w}fromArray(e,t=0){return this._x=e[t],this._y=e[t+1],this._z=e[t+2],this._w=e[t+3],this._onChangeCallback(),this}toArray(e=[],t=0){return e[t]=this._x,e[t+1]=this._y,e[t+2]=this._z,e[t+3]=this._w,e}fromBufferAttribute(e,t){return this._x=e.getX(t),this._y=e.getY(t),this._z=e.getZ(t),this._w=e.getW(t),this}_onChange(e){return this._onChangeCallback=e,this}_onChangeCallback(){}*[Symbol.iterator](){yield this._x,yield this._y,yield this._z,yield this._w}}class C{constructor(e=0,t=0,n=0){C.prototype.isVector3=!0,this.x=e,this.y=t,this.z=n}set(e,t,n){return n===void 0&&(n=this.z),this.x=e,this.y=t,this.z=n,this}setScalar(e){return this.x=e,this.y=e,this.z=e,this}setX(e){return this.x=e,this}setY(e){return this.y=e,this}setZ(e){return this.z=e,this}setComponent(e,t){switch(e){case 0:this.x=t;break;case 1:this.y=t;break;case 2:this.z=t;break;default:throw new Error("index is out of range: "+e)}return this}getComponent(e){switch(e){case 0:return this.x;case 1:return this.y;case 2:return this.z;default:throw new Error("index is out of range: "+e)}}clone(){return new this.constructor(this.x,this.y,this.z)}copy(e){return this.x=e.x,this.y=e.y,this.z=e.z,this}add(e){return this.x+=e.x,this.y+=e.y,this.z+=e.z,this}addScalar(e){return this.x+=e,this.y+=e,this.z+=e,this}addVectors(e,t){return this.x=e.x+t.x,this.y=e.y+t.y,this.z=e.z+t.z,this}addScaledVector(e,t){return this.x+=e.x*t,this.y+=e.y*t,this.z+=e.z*t,this}sub(e){return this.x-=e.x,this.y-=e.y,this.z-=e.z,this}subScalar(e){return this.x-=e,this.y-=e,this.z-=e,this}subVectors(e,t){return this.x=e.x-t.x,this.y=e.y-t.y,this.z=e.z-t.z,this}multiply(e){return this.x*=e.x,this.y*=e.y,this.z*=e.z,this}multiplyScalar(e){return this.x*=e,this.y*=e,this.z*=e,this}multiplyVectors(e,t){return this.x=e.x*t.x,this.y=e.y*t.y,this.z=e.z*t.z,this}applyEuler(e){return this.applyQuaternion(ma.setFromEuler(e))}applyAxisAngle(e,t){return this.applyQuaternion(ma.setFromAxisAngle(e,t))}applyMatrix3(e){const t=this.x,n=this.y,r=this.z,s=e.elements;return this.x=s[0]*t+s[3]*n+s[6]*r,this.y=s[1]*t+s[4]*n+s[7]*r,this.z=s[2]*t+s[5]*n+s[8]*r,this}applyNormalMatrix(e){return this.applyMatrix3(e).normalize()}applyMatrix4(e){const t=this.x,n=this.y,r=this.z,s=e.elements,o=1/(s[3]*t+s[7]*n+s[11]*r+s[15]);return this.x=(s[0]*t+s[4]*n+s[8]*r+s[12])*o,this.y=(s[1]*t+s[5]*n+s[9]*r+s[13])*o,this.z=(s[2]*t+s[6]*n+s[10]*r+s[14])*o,this}applyQuaternion(e){const t=this.x,n=this.y,r=this.z,s=e.x,o=e.y,a=e.z,l=e.w,c=l*t+o*r-a*n,u=l*n+a*t-s*r,d=l*r+s*n-o*t,f=-s*t-o*n-a*r;return this.x=c*l+f*-s+u*-a-d*-o,this.y=u*l+f*-o+d*-s-c*-a,this.z=d*l+f*-a+c*-o-u*-s,this}project(e){return this.applyMatrix4(e.matrixWorldInverse).applyMatrix4(e.projectionMatrix)}unproject(e){return this.applyMatrix4(e.projectionMatrixInverse).applyMatrix4(e.matrixWorld)}transformDirection(e){const t=this.x,n=this.y,r=this.z,s=e.elements;return this.x=s[0]*t+s[4]*n+s[8]*r,this.y=s[1]*t+s[5]*n+s[9]*r,this.z=s[2]*t+s[6]*n+s[10]*r,this.normalize()}divide(e){return this.x/=e.x,this.y/=e.y,this.z/=e.z,this}divideScalar(e){return this.multiplyScalar(1/e)}min(e){return this.x=Math.min(this.x,e.x),this.y=Math.min(this.y,e.y),this.z=Math.min(this.z,e.z),this}max(e){return this.x=Math.max(this.x,e.x),this.y=Math.max(this.y,e.y),this.z=Math.max(this.z,e.z),this}clamp(e,t){return this.x=Math.max(e.x,Math.min(t.x,this.x)),this.y=Math.max(e.y,Math.min(t.y,this.y)),this.z=Math.max(e.z,Math.min(t.z,this.z)),this}clampScalar(e,t){return this.x=Math.max(e,Math.min(t,this.x)),this.y=Math.max(e,Math.min(t,this.y)),this.z=Math.max(e,Math.min(t,this.z)),this}clampLength(e,t){const n=this.length();return this.divideScalar(n||1).multiplyScalar(Math.max(e,Math.min(t,n)))}floor(){return this.x=Math.floor(this.x),this.y=Math.floor(this.y),this.z=Math.floor(this.z),this}ceil(){return this.x=Math.ceil(this.x),this.y=Math.ceil(this.y),this.z=Math.ceil(this.z),this}round(){return this.x=Math.round(this.x),this.y=Math.round(this.y),this.z=Math.round(this.z),this}roundToZero(){return this.x=this.x<0?Math.ceil(this.x):Math.floor(this.x),this.y=this.y<0?Math.ceil(this.y):Math.floor(this.y),this.z=this.z<0?Math.ceil(this.z):Math.floor(this.z),this}negate(){return this.x=-this.x,this.y=-this.y,this.z=-this.z,this}dot(e){return this.x*e.x+this.y*e.y+this.z*e.z}lengthSq(){return this.x*this.x+this.y*this.y+this.z*this.z}length(){return Math.sqrt(this.x*this.x+this.y*this.y+this.z*this.z)}manhattanLength(){return Math.abs(this.x)+Math.abs(this.y)+Math.abs(this.z)}normalize(){return this.divideScalar(this.length()||1)}setLength(e){return this.normalize().multiplyScalar(e)}lerp(e,t){return this.x+=(e.x-this.x)*t,this.y+=(e.y-this.y)*t,this.z+=(e.z-this.z)*t,this}lerpVectors(e,t,n){return this.x=e.x+(t.x-e.x)*n,this.y=e.y+(t.y-e.y)*n,this.z=e.z+(t.z-e.z)*n,this}cross(e){return this.crossVectors(this,e)}crossVectors(e,t){const n=e.x,r=e.y,s=e.z,o=t.x,a=t.y,l=t.z;return this.x=r*l-s*a,this.y=s*o-n*l,this.z=n*a-r*o,this}projectOnVector(e){const t=e.lengthSq();if(t===0)return this.set(0,0,0);const n=e.dot(this)/t;return this.copy(e).multiplyScalar(n)}projectOnPlane(e){return Nr.copy(this).projectOnVector(e),this.sub(Nr)}reflect(e){return this.sub(Nr.copy(e).multiplyScalar(2*this.dot(e)))}angleTo(e){const t=Math.sqrt(this.lengthSq()*e.lengthSq());if(t===0)return Math.PI/2;const n=this.dot(e)/t;return Math.acos(vt(n,-1,1))}distanceTo(e){return Math.sqrt(this.distanceToSquared(e))}distanceToSquared(e){const t=this.x-e.x,n=this.y-e.y,r=this.z-e.z;return t*t+n*n+r*r}manhattanDistanceTo(e){return Math.abs(this.x-e.x)+Math.abs(this.y-e.y)+Math.abs(this.z-e.z)}setFromSpherical(e){return this.setFromSphericalCoords(e.radius,e.phi,e.theta)}setFromSphericalCoords(e,t,n){const r=Math.sin(t)*e;return this.x=r*Math.sin(n),this.y=Math.cos(t)*e,this.z=r*Math.cos(n),this}setFromCylindrical(e){return this.setFromCylindricalCoords(e.radius,e.theta,e.y)}setFromCylindricalCoords(e,t,n){return this.x=e*Math.sin(t),this.y=n,this.z=e*Math.cos(t),this}setFromMatrixPosition(e){const t=e.elements;return this.x=t[12],this.y=t[13],this.z=t[14],this}setFromMatrixScale(e){const t=this.setFromMatrixColumn(e,0).length(),n=this.setFromMatrixColumn(e,1).length(),r=this.setFromMatrixColumn(e,2).length();return this.x=t,this.y=n,this.z=r,this}setFromMatrixColumn(e,t){return this.fromArray(e.elements,t*4)}setFromMatrix3Column(e,t){return this.fromArray(e.elements,t*3)}setFromEuler(e){return this.x=e._x,this.y=e._y,this.z=e._z,this}equals(e){return e.x===this.x&&e.y===this.y&&e.z===this.z}fromArray(e,t=0){return this.x=e[t],this.y=e[t+1],this.z=e[t+2],this}toArray(e=[],t=0){return e[t]=this.x,e[t+1]=this.y,e[t+2]=this.z,e}fromBufferAttribute(e,t){return this.x=e.getX(t),this.y=e.getY(t),this.z=e.getZ(t),this}random(){return this.x=Math.random(),this.y=Math.random(),this.z=Math.random(),this}randomDirection(){const e=(Math.random()-.5)*2,t=Math.random()*Math.PI*2,n=Math.sqrt(1-e**2);return this.x=n*Math.cos(t),this.y=n*Math.sin(t),this.z=e,this}*[Symbol.iterator](){yield this.x,yield this.y,yield this.z}}const Nr=new C,ma=new Ln;class Vi{constructor(e=new C(1/0,1/0,1/0),t=new C(-1/0,-1/0,-1/0)){this.isBox3=!0,this.min=e,this.max=t}set(e,t){return this.min.copy(e),this.max.copy(t),this}setFromArray(e){let t=1/0,n=1/0,r=1/0,s=-1/0,o=-1/0,a=-1/0;for(let l=0,c=e.length;l<c;l+=3){const u=e[l],d=e[l+1],f=e[l+2];u<t&&(t=u),d<n&&(n=d),f<r&&(r=f),u>s&&(s=u),d>o&&(o=d),f>a&&(a=f)}return this.min.set(t,n,r),this.max.set(s,o,a),this}setFromBufferAttribute(e){let t=1/0,n=1/0,r=1/0,s=-1/0,o=-1/0,a=-1/0;for(let l=0,c=e.count;l<c;l++){const u=e.getX(l),d=e.getY(l),f=e.getZ(l);u<t&&(t=u),d<n&&(n=d),f<r&&(r=f),u>s&&(s=u),d>o&&(o=d),f>a&&(a=f)}return this.min.set(t,n,r),this.max.set(s,o,a),this}setFromPoints(e){this.makeEmpty();for(let t=0,n=e.length;t<n;t++)this.expandByPoint(e[t]);return this}setFromCenterAndSize(e,t){const n=xn.copy(t).multiplyScalar(.5);return this.min.copy(e).sub(n),this.max.copy(e).add(n),this}setFromObject(e,t=!1){return this.makeEmpty(),this.expandByObject(e,t)}clone(){return new this.constructor().copy(this)}copy(e){return this.min.copy(e.min),this.max.copy(e.max),this}makeEmpty(){return this.min.x=this.min.y=this.min.z=1/0,this.max.x=this.max.y=this.max.z=-1/0,this}isEmpty(){return this.max.x<this.min.x||this.max.y<this.min.y||this.max.z<this.min.z}getCenter(e){return this.isEmpty()?e.set(0,0,0):e.addVectors(this.min,this.max).multiplyScalar(.5)}getSize(e){return this.isEmpty()?e.set(0,0,0):e.subVectors(this.max,this.min)}expandByPoint(e){return this.min.min(e),this.max.max(e),this}expandByVector(e){return this.min.sub(e),this.max.add(e),this}expandByScalar(e){return this.min.addScalar(-e),this.max.addScalar(e),this}expandByObject(e,t=!1){e.updateWorldMatrix(!1,!1);const n=e.geometry;if(n!==void 0)if(t&&n.attributes!=null&&n.attributes.position!==void 0){const s=n.attributes.position;for(let o=0,a=s.count;o<a;o++)xn.fromBufferAttribute(s,o).applyMatrix4(e.matrixWorld),this.expandByPoint(xn)}else n.boundingBox===null&&n.computeBoundingBox(),Fr.copy(n.boundingBox),Fr.applyMatrix4(e.matrixWorld),this.union(Fr);const r=e.children;for(let s=0,o=r.length;s<o;s++)this.expandByObject(r[s],t);return this}containsPoint(e){return!(e.x<this.min.x||e.x>this.max.x||e.y<this.min.y||e.y>this.max.y||e.z<this.min.z||e.z>this.max.z)}containsBox(e){return this.min.x<=e.min.x&&e.max.x<=this.max.x&&this.min.y<=e.min.y&&e.max.y<=this.max.y&&this.min.z<=e.min.z&&e.max.z<=this.max.z}getParameter(e,t){return t.set((e.x-this.min.x)/(this.max.x-this.min.x),(e.y-this.min.y)/(this.max.y-this.min.y),(e.z-this.min.z)/(this.max.z-this.min.z))}intersectsBox(e){return!(e.max.x<this.min.x||e.min.x>this.max.x||e.max.y<this.min.y||e.min.y>this.max.y||e.max.z<this.min.z||e.min.z>this.max.z)}intersectsSphere(e){return this.clampPoint(e.center,xn),xn.distanceToSquared(e.center)<=e.radius*e.radius}intersectsPlane(e){let t,n;return e.normal.x>0?(t=e.normal.x*this.min.x,n=e.normal.x*this.max.x):(t=e.normal.x*this.max.x,n=e.normal.x*this.min.x),e.normal.y>0?(t+=e.normal.y*this.min.y,n+=e.normal.y*this.max.y):(t+=e.normal.y*this.max.y,n+=e.normal.y*this.min.y),e.normal.z>0?(t+=e.normal.z*this.min.z,n+=e.normal.z*this.max.z):(t+=e.normal.z*this.max.z,n+=e.normal.z*this.min.z),t<=-e.constant&&n>=-e.constant}intersectsTriangle(e){if(this.isEmpty())return!1;this.getCenter(fi),Oi.subVectors(this.max,fi),Nn.subVectors(e.a,fi),Fn.subVectors(e.b,fi),qn.subVectors(e.c,fi),nn.subVectors(Fn,Nn),rn.subVectors(qn,Fn),Mn.subVectors(Nn,qn);let t=[0,-nn.z,nn.y,0,-rn.z,rn.y,0,-Mn.z,Mn.y,nn.z,0,-nn.x,rn.z,0,-rn.x,Mn.z,0,-Mn.x,-nn.y,nn.x,0,-rn.y,rn.x,0,-Mn.y,Mn.x,0];return!qr(t,Nn,Fn,qn,Oi)||(t=[1,0,0,0,1,0,0,0,1],!qr(t,Nn,Fn,qn,Oi))?!1:(Bi.crossVectors(nn,rn),t=[Bi.x,Bi.y,Bi.z],qr(t,Nn,Fn,qn,Oi))}clampPoint(e,t){return t.copy(e).clamp(this.min,this.max)}distanceToPoint(e){return xn.copy(e).clamp(this.min,this.max).sub(e).length()}getBoundingSphere(e){return this.getCenter(e.center),e.radius=this.getSize(xn).length()*.5,e}intersect(e){return this.min.max(e.min),this.max.min(e.max),this.isEmpty()&&this.makeEmpty(),this}union(e){return this.min.min(e.min),this.max.max(e.max),this}applyMatrix4(e){return this.isEmpty()?this:(Yt[0].set(this.min.x,this.min.y,this.min.z).applyMatrix4(e),Yt[1].set(this.min.x,this.min.y,this.max.z).applyMatrix4(e),Yt[2].set(this.min.x,this.max.y,this.min.z).applyMatrix4(e),Yt[3].set(this.min.x,this.max.y,this.max.z).applyMatrix4(e),Yt[4].set(this.max.x,this.min.y,this.min.z).applyMatrix4(e),Yt[5].set(this.max.x,this.min.y,this.max.z).applyMatrix4(e),Yt[6].set(this.max.x,this.max.y,this.min.z).applyMatrix4(e),Yt[7].set(this.max.x,this.max.y,this.max.z).applyMatrix4(e),this.setFromPoints(Yt),this)}translate(e){return this.min.add(e),this.max.add(e),this}equals(e){return e.min.equals(this.min)&&e.max.equals(this.max)}}const Yt=[new C,new C,new C,new C,new C,new C,new C,new C],xn=new C,Fr=new Vi,Nn=new C,Fn=new C,qn=new C,nn=new C,rn=new C,Mn=new C,fi=new C,Oi=new C,Bi=new C,yn=new C;function qr(i,e,t,n,r){for(let s=0,o=i.length-3;s<=o;s+=3){yn.fromArray(i,s);const a=r.x*Math.abs(yn.x)+r.y*Math.abs(yn.y)+r.z*Math.abs(yn.z),l=e.dot(yn),c=t.dot(yn),u=n.dot(yn);if(Math.max(-Math.max(l,c,u),Math.min(l,c,u))>a)return!1}return!0}const nc=new Vi,pi=new C,Or=new C;class Di{constructor(e=new C,t=-1){this.center=e,this.radius=t}set(e,t){return this.center.copy(e),this.radius=t,this}setFromPoints(e,t){const n=this.center;t!==void 0?n.copy(t):nc.setFromPoints(e).getCenter(n);let r=0;for(let s=0,o=e.length;s<o;s++)r=Math.max(r,n.distanceToSquared(e[s]));return this.radius=Math.sqrt(r),this}copy(e){return this.center.copy(e.center),this.radius=e.radius,this}isEmpty(){return this.radius<0}makeEmpty(){return this.center.set(0,0,0),this.radius=-1,this}containsPoint(e){return e.distanceToSquared(this.center)<=this.radius*this.radius}distanceToPoint(e){return e.distanceTo(this.center)-this.radius}intersectsSphere(e){const t=this.radius+e.radius;return e.center.distanceToSquared(this.center)<=t*t}intersectsBox(e){return e.intersectsSphere(this)}intersectsPlane(e){return Math.abs(e.distanceToPoint(this.center))<=this.radius}clampPoint(e,t){const n=this.center.distanceToSquared(e);return t.copy(e),n>this.radius*this.radius&&(t.sub(this.center).normalize(),t.multiplyScalar(this.radius).add(this.center)),t}getBoundingBox(e){return this.isEmpty()?(e.makeEmpty(),e):(e.set(this.center,this.center),e.expandByScalar(this.radius),e)}applyMatrix4(e){return this.center.applyMatrix4(e),this.radius=this.radius*e.getMaxScaleOnAxis(),this}translate(e){return this.center.add(e),this}expandByPoint(e){if(this.isEmpty())return this.center.copy(e),this.radius=0,this;pi.subVectors(e,this.center);const t=pi.lengthSq();if(t>this.radius*this.radius){const n=Math.sqrt(t),r=(n-this.radius)*.5;this.center.addScaledVector(pi,r/n),this.radius+=r}return this}union(e){return e.isEmpty()?this:this.isEmpty()?(this.copy(e),this):(this.center.equals(e.center)===!0?this.radius=Math.max(this.radius,e.radius):(Or.subVectors(e.center,this.center).setLength(e.radius),this.expandByPoint(pi.copy(e.center).add(Or)),this.expandByPoint(pi.copy(e.center).sub(Or))),this)}equals(e){return e.center.equals(this.center)&&e.radius===this.radius}clone(){return new this.constructor().copy(this)}}const Kt=new C,Br=new C,ki=new C,sn=new C,kr=new C,Hi=new C,Hr=new C;class xr{constructor(e=new C,t=new C(0,0,-1)){this.origin=e,this.direction=t}set(e,t){return this.origin.copy(e),this.direction.copy(t),this}copy(e){return this.origin.copy(e.origin),this.direction.copy(e.direction),this}at(e,t){return t.copy(this.direction).multiplyScalar(e).add(this.origin)}lookAt(e){return this.direction.copy(e).sub(this.origin).normalize(),this}recast(e){return this.origin.copy(this.at(e,Kt)),this}closestPointToPoint(e,t){t.subVectors(e,this.origin);const n=t.dot(this.direction);return n<0?t.copy(this.origin):t.copy(this.direction).multiplyScalar(n).add(this.origin)}distanceToPoint(e){return Math.sqrt(this.distanceSqToPoint(e))}distanceSqToPoint(e){const t=Kt.subVectors(e,this.origin).dot(this.direction);return t<0?this.origin.distanceToSquared(e):(Kt.copy(this.direction).multiplyScalar(t).add(this.origin),Kt.distanceToSquared(e))}distanceSqToSegment(e,t,n,r){Br.copy(e).add(t).multiplyScalar(.5),ki.copy(t).sub(e).normalize(),sn.copy(this.origin).sub(Br);const s=e.distanceTo(t)*.5,o=-this.direction.dot(ki),a=sn.dot(this.direction),l=-sn.dot(ki),c=sn.lengthSq(),u=Math.abs(1-o*o);let d,f,m,g;if(u>0)if(d=o*l-a,f=o*a-l,g=s*u,d>=0)if(f>=-g)if(f<=g){const p=1/u;d*=p,f*=p,m=d*(d+o*f+2*a)+f*(o*d+f+2*l)+c}else f=s,d=Math.max(0,-(o*f+a)),m=-d*d+f*(f+2*l)+c;else f=-s,d=Math.max(0,-(o*f+a)),m=-d*d+f*(f+2*l)+c;else f<=-g?(d=Math.max(0,-(-o*s+a)),f=d>0?-s:Math.min(Math.max(-s,-l),s),m=-d*d+f*(f+2*l)+c):f<=g?(d=0,f=Math.min(Math.max(-s,-l),s),m=f*(f+2*l)+c):(d=Math.max(0,-(o*s+a)),f=d>0?s:Math.min(Math.max(-s,-l),s),m=-d*d+f*(f+2*l)+c);else f=o>0?-s:s,d=Math.max(0,-(o*f+a)),m=-d*d+f*(f+2*l)+c;return n&&n.copy(this.direction).multiplyScalar(d).add(this.origin),r&&r.copy(ki).multiplyScalar(f).add(Br),m}intersectSphere(e,t){Kt.subVectors(e.center,this.origin);const n=Kt.dot(this.direction),r=Kt.dot(Kt)-n*n,s=e.radius*e.radius;if(r>s)return null;const o=Math.sqrt(s-r),a=n-o,l=n+o;return a<0&&l<0?null:a<0?this.at(l,t):this.at(a,t)}intersectsSphere(e){return this.distanceSqToPoint(e.center)<=e.radius*e.radius}distanceToPlane(e){const t=e.normal.dot(this.direction);if(t===0)return e.distanceToPoint(this.origin)===0?0:null;const n=-(this.origin.dot(e.normal)+e.constant)/t;return n>=0?n:null}intersectPlane(e,t){const n=this.distanceToPlane(e);return n===null?null:this.at(n,t)}intersectsPlane(e){const t=e.distanceToPoint(this.origin);return t===0||e.normal.dot(this.direction)*t<0}intersectBox(e,t){let n,r,s,o,a,l;const c=1/this.direction.x,u=1/this.direction.y,d=1/this.direction.z,f=this.origin;return c>=0?(n=(e.min.x-f.x)*c,r=(e.max.x-f.x)*c):(n=(e.max.x-f.x)*c,r=(e.min.x-f.x)*c),u>=0?(s=(e.min.y-f.y)*u,o=(e.max.y-f.y)*u):(s=(e.max.y-f.y)*u,o=(e.min.y-f.y)*u),n>o||s>r||((s>n||isNaN(n))&&(n=s),(o<r||isNaN(r))&&(r=o),d>=0?(a=(e.min.z-f.z)*d,l=(e.max.z-f.z)*d):(a=(e.max.z-f.z)*d,l=(e.min.z-f.z)*d),n>l||a>r)||((a>n||n!==n)&&(n=a),(l<r||r!==r)&&(r=l),r<0)?null:this.at(n>=0?n:r,t)}intersectsBox(e){return this.intersectBox(e,Kt)!==null}intersectTriangle(e,t,n,r,s){kr.subVectors(t,e),Hi.subVectors(n,e),Hr.crossVectors(kr,Hi);let o=this.direction.dot(Hr),a;if(o>0){if(r)return null;a=1}else if(o<0)a=-1,o=-o;else return null;sn.subVectors(this.origin,e);const l=a*this.direction.dot(Hi.crossVectors(sn,Hi));if(l<0)return null;const c=a*this.direction.dot(kr.cross(sn));if(c<0||l+c>o)return null;const u=-a*sn.dot(Hr);return u<0?null:this.at(u/o,s)}applyMatrix4(e){return this.origin.applyMatrix4(e),this.direction.transformDirection(e),this}equals(e){return e.origin.equals(this.origin)&&e.direction.equals(this.direction)}clone(){return new this.constructor().copy(this)}}class ke{constructor(){ke.prototype.isMatrix4=!0,this.elements=[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]}set(e,t,n,r,s,o,a,l,c,u,d,f,m,g,p,h){const x=this.elements;return x[0]=e,x[4]=t,x[8]=n,x[12]=r,x[1]=s,x[5]=o,x[9]=a,x[13]=l,x[2]=c,x[6]=u,x[10]=d,x[14]=f,x[3]=m,x[7]=g,x[11]=p,x[15]=h,this}identity(){return this.set(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1),this}clone(){return new ke().fromArray(this.elements)}copy(e){const t=this.elements,n=e.elements;return t[0]=n[0],t[1]=n[1],t[2]=n[2],t[3]=n[3],t[4]=n[4],t[5]=n[5],t[6]=n[6],t[7]=n[7],t[8]=n[8],t[9]=n[9],t[10]=n[10],t[11]=n[11],t[12]=n[12],t[13]=n[13],t[14]=n[14],t[15]=n[15],this}copyPosition(e){const t=this.elements,n=e.elements;return t[12]=n[12],t[13]=n[13],t[14]=n[14],this}setFromMatrix3(e){const t=e.elements;return this.set(t[0],t[3],t[6],0,t[1],t[4],t[7],0,t[2],t[5],t[8],0,0,0,0,1),this}extractBasis(e,t,n){return e.setFromMatrixColumn(this,0),t.setFromMatrixColumn(this,1),n.setFromMatrixColumn(this,2),this}makeBasis(e,t,n){return this.set(e.x,t.x,n.x,0,e.y,t.y,n.y,0,e.z,t.z,n.z,0,0,0,0,1),this}extractRotation(e){const t=this.elements,n=e.elements,r=1/On.setFromMatrixColumn(e,0).length(),s=1/On.setFromMatrixColumn(e,1).length(),o=1/On.setFromMatrixColumn(e,2).length();return t[0]=n[0]*r,t[1]=n[1]*r,t[2]=n[2]*r,t[3]=0,t[4]=n[4]*s,t[5]=n[5]*s,t[6]=n[6]*s,t[7]=0,t[8]=n[8]*o,t[9]=n[9]*o,t[10]=n[10]*o,t[11]=0,t[12]=0,t[13]=0,t[14]=0,t[15]=1,this}makeRotationFromEuler(e){const t=this.elements,n=e.x,r=e.y,s=e.z,o=Math.cos(n),a=Math.sin(n),l=Math.cos(r),c=Math.sin(r),u=Math.cos(s),d=Math.sin(s);if(e.order==="XYZ"){const f=o*u,m=o*d,g=a*u,p=a*d;t[0]=l*u,t[4]=-l*d,t[8]=c,t[1]=m+g*c,t[5]=f-p*c,t[9]=-a*l,t[2]=p-f*c,t[6]=g+m*c,t[10]=o*l}else if(e.order==="YXZ"){const f=l*u,m=l*d,g=c*u,p=c*d;t[0]=f+p*a,t[4]=g*a-m,t[8]=o*c,t[1]=o*d,t[5]=o*u,t[9]=-a,t[2]=m*a-g,t[6]=p+f*a,t[10]=o*l}else if(e.order==="ZXY"){const f=l*u,m=l*d,g=c*u,p=c*d;t[0]=f-p*a,t[4]=-o*d,t[8]=g+m*a,t[1]=m+g*a,t[5]=o*u,t[9]=p-f*a,t[2]=-o*c,t[6]=a,t[10]=o*l}else if(e.order==="ZYX"){const f=o*u,m=o*d,g=a*u,p=a*d;t[0]=l*u,t[4]=g*c-m,t[8]=f*c+p,t[1]=l*d,t[5]=p*c+f,t[9]=m*c-g,t[2]=-c,t[6]=a*l,t[10]=o*l}else if(e.order==="YZX"){const f=o*l,m=o*c,g=a*l,p=a*c;t[0]=l*u,t[4]=p-f*d,t[8]=g*d+m,t[1]=d,t[5]=o*u,t[9]=-a*u,t[2]=-c*u,t[6]=m*d+g,t[10]=f-p*d}else if(e.order==="XZY"){const f=o*l,m=o*c,g=a*l,p=a*c;t[0]=l*u,t[4]=-d,t[8]=c*u,t[1]=f*d+p,t[5]=o*u,t[9]=m*d-g,t[2]=g*d-m,t[6]=a*u,t[10]=p*d+f}return t[3]=0,t[7]=0,t[11]=0,t[12]=0,t[13]=0,t[14]=0,t[15]=1,this}makeRotationFromQuaternion(e){return this.compose(ic,e,rc)}lookAt(e,t,n){const r=this.elements;return At.subVectors(e,t),At.lengthSq()===0&&(At.z=1),At.normalize(),an.crossVectors(n,At),an.lengthSq()===0&&(Math.abs(n.z)===1?At.x+=1e-4:At.z+=1e-4,At.normalize(),an.crossVectors(n,At)),an.normalize(),Zi.crossVectors(At,an),r[0]=an.x,r[4]=Zi.x,r[8]=At.x,r[1]=an.y,r[5]=Zi.y,r[9]=At.y,r[2]=an.z,r[6]=Zi.z,r[10]=At.z,this}multiply(e){return this.multiplyMatrices(this,e)}premultiply(e){return this.multiplyMatrices(e,this)}multiplyMatrices(e,t){const n=e.elements,r=t.elements,s=this.elements,o=n[0],a=n[4],l=n[8],c=n[12],u=n[1],d=n[5],f=n[9],m=n[13],g=n[2],p=n[6],h=n[10],x=n[14],E=n[3],b=n[7],S=n[11],y=n[15],R=r[0],U=r[4],v=r[8],T=r[12],D=r[1],X=r[5],oe=r[9],N=r[13],z=r[2],W=r[6],K=r[10],j=r[14],G=r[3],te=r[7],J=r[11],F=r[15];return s[0]=o*R+a*D+l*z+c*G,s[4]=o*U+a*X+l*W+c*te,s[8]=o*v+a*oe+l*K+c*J,s[12]=o*T+a*N+l*j+c*F,s[1]=u*R+d*D+f*z+m*G,s[5]=u*U+d*X+f*W+m*te,s[9]=u*v+d*oe+f*K+m*J,s[13]=u*T+d*N+f*j+m*F,s[2]=g*R+p*D+h*z+x*G,s[6]=g*U+p*X+h*W+x*te,s[10]=g*v+p*oe+h*K+x*J,s[14]=g*T+p*N+h*j+x*F,s[3]=E*R+b*D+S*z+y*G,s[7]=E*U+b*X+S*W+y*te,s[11]=E*v+b*oe+S*K+y*J,s[15]=E*T+b*N+S*j+y*F,this}multiplyScalar(e){const t=this.elements;return t[0]*=e,t[4]*=e,t[8]*=e,t[12]*=e,t[1]*=e,t[5]*=e,t[9]*=e,t[13]*=e,t[2]*=e,t[6]*=e,t[10]*=e,t[14]*=e,t[3]*=e,t[7]*=e,t[11]*=e,t[15]*=e,this}determinant(){const e=this.elements,t=e[0],n=e[4],r=e[8],s=e[12],o=e[1],a=e[5],l=e[9],c=e[13],u=e[2],d=e[6],f=e[10],m=e[14],g=e[3],p=e[7],h=e[11],x=e[15];return g*(+s*l*d-r*c*d-s*a*f+n*c*f+r*a*m-n*l*m)+p*(+t*l*m-t*c*f+s*o*f-r*o*m+r*c*u-s*l*u)+h*(+t*c*d-t*a*m-s*o*d+n*o*m+s*a*u-n*c*u)+x*(-r*a*u-t*l*d+t*a*f+r*o*d-n*o*f+n*l*u)}transpose(){const e=this.elements;let t;return t=e[1],e[1]=e[4],e[4]=t,t=e[2],e[2]=e[8],e[8]=t,t=e[6],e[6]=e[9],e[9]=t,t=e[3],e[3]=e[12],e[12]=t,t=e[7],e[7]=e[13],e[13]=t,t=e[11],e[11]=e[14],e[14]=t,this}setPosition(e,t,n){const r=this.elements;return e.isVector3?(r[12]=e.x,r[13]=e.y,r[14]=e.z):(r[12]=e,r[13]=t,r[14]=n),this}invert(){const e=this.elements,t=e[0],n=e[1],r=e[2],s=e[3],o=e[4],a=e[5],l=e[6],c=e[7],u=e[8],d=e[9],f=e[10],m=e[11],g=e[12],p=e[13],h=e[14],x=e[15],E=d*h*c-p*f*c+p*l*m-a*h*m-d*l*x+a*f*x,b=g*f*c-u*h*c-g*l*m+o*h*m+u*l*x-o*f*x,S=u*p*c-g*d*c+g*a*m-o*p*m-u*a*x+o*d*x,y=g*d*l-u*p*l-g*a*f+o*p*f+u*a*h-o*d*h,R=t*E+n*b+r*S+s*y;if(R===0)return this.set(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);const U=1/R;return e[0]=E*U,e[1]=(p*f*s-d*h*s-p*r*m+n*h*m+d*r*x-n*f*x)*U,e[2]=(a*h*s-p*l*s+p*r*c-n*h*c-a*r*x+n*l*x)*U,e[3]=(d*l*s-a*f*s-d*r*c+n*f*c+a*r*m-n*l*m)*U,e[4]=b*U,e[5]=(u*h*s-g*f*s+g*r*m-t*h*m-u*r*x+t*f*x)*U,e[6]=(g*l*s-o*h*s-g*r*c+t*h*c+o*r*x-t*l*x)*U,e[7]=(o*f*s-u*l*s+u*r*c-t*f*c-o*r*m+t*l*m)*U,e[8]=S*U,e[9]=(g*d*s-u*p*s-g*n*m+t*p*m+u*n*x-t*d*x)*U,e[10]=(o*p*s-g*a*s+g*n*c-t*p*c-o*n*x+t*a*x)*U,e[11]=(u*a*s-o*d*s-u*n*c+t*d*c+o*n*m-t*a*m)*U,e[12]=y*U,e[13]=(u*p*r-g*d*r+g*n*f-t*p*f-u*n*h+t*d*h)*U,e[14]=(g*a*r-o*p*r-g*n*l+t*p*l+o*n*h-t*a*h)*U,e[15]=(o*d*r-u*a*r+u*n*l-t*d*l-o*n*f+t*a*f)*U,this}scale(e){const t=this.elements,n=e.x,r=e.y,s=e.z;return t[0]*=n,t[4]*=r,t[8]*=s,t[1]*=n,t[5]*=r,t[9]*=s,t[2]*=n,t[6]*=r,t[10]*=s,t[3]*=n,t[7]*=r,t[11]*=s,this}getMaxScaleOnAxis(){const e=this.elements,t=e[0]*e[0]+e[1]*e[1]+e[2]*e[2],n=e[4]*e[4]+e[5]*e[5]+e[6]*e[6],r=e[8]*e[8]+e[9]*e[9]+e[10]*e[10];return Math.sqrt(Math.max(t,n,r))}makeTranslation(e,t,n){return this.set(1,0,0,e,0,1,0,t,0,0,1,n,0,0,0,1),this}makeRotationX(e){const t=Math.cos(e),n=Math.sin(e);return this.set(1,0,0,0,0,t,-n,0,0,n,t,0,0,0,0,1),this}makeRotationY(e){const t=Math.cos(e),n=Math.sin(e);return this.set(t,0,n,0,0,1,0,0,-n,0,t,0,0,0,0,1),this}makeRotationZ(e){const t=Math.cos(e),n=Math.sin(e);return this.set(t,-n,0,0,n,t,0,0,0,0,1,0,0,0,0,1),this}makeRotationAxis(e,t){const n=Math.cos(t),r=Math.sin(t),s=1-n,o=e.x,a=e.y,l=e.z,c=s*o,u=s*a;return this.set(c*o+n,c*a-r*l,c*l+r*a,0,c*a+r*l,u*a+n,u*l-r*o,0,c*l-r*a,u*l+r*o,s*l*l+n,0,0,0,0,1),this}makeScale(e,t,n){return this.set(e,0,0,0,0,t,0,0,0,0,n,0,0,0,0,1),this}makeShear(e,t,n,r,s,o){return this.set(1,n,s,0,e,1,o,0,t,r,1,0,0,0,0,1),this}compose(e,t,n){const r=this.elements,s=t._x,o=t._y,a=t._z,l=t._w,c=s+s,u=o+o,d=a+a,f=s*c,m=s*u,g=s*d,p=o*u,h=o*d,x=a*d,E=l*c,b=l*u,S=l*d,y=n.x,R=n.y,U=n.z;return r[0]=(1-(p+x))*y,r[1]=(m+S)*y,r[2]=(g-b)*y,r[3]=0,r[4]=(m-S)*R,r[5]=(1-(f+x))*R,r[6]=(h+E)*R,r[7]=0,r[8]=(g+b)*U,r[9]=(h-E)*U,r[10]=(1-(f+p))*U,r[11]=0,r[12]=e.x,r[13]=e.y,r[14]=e.z,r[15]=1,this}decompose(e,t,n){const r=this.elements;let s=On.set(r[0],r[1],r[2]).length();const o=On.set(r[4],r[5],r[6]).length(),a=On.set(r[8],r[9],r[10]).length();this.determinant()<0&&(s=-s),e.x=r[12],e.y=r[13],e.z=r[14],Ft.copy(this);const c=1/s,u=1/o,d=1/a;return Ft.elements[0]*=c,Ft.elements[1]*=c,Ft.elements[2]*=c,Ft.elements[4]*=u,Ft.elements[5]*=u,Ft.elements[6]*=u,Ft.elements[8]*=d,Ft.elements[9]*=d,Ft.elements[10]*=d,t.setFromRotationMatrix(Ft),n.x=s,n.y=o,n.z=a,this}makePerspective(e,t,n,r,s,o){const a=this.elements,l=2*s/(t-e),c=2*s/(n-r),u=(t+e)/(t-e),d=(n+r)/(n-r),f=-(o+s)/(o-s),m=-2*o*s/(o-s);return a[0]=l,a[4]=0,a[8]=u,a[12]=0,a[1]=0,a[5]=c,a[9]=d,a[13]=0,a[2]=0,a[6]=0,a[10]=f,a[14]=m,a[3]=0,a[7]=0,a[11]=-1,a[15]=0,this}makeOrthographic(e,t,n,r,s,o){const a=this.elements,l=1/(t-e),c=1/(n-r),u=1/(o-s),d=(t+e)*l,f=(n+r)*c,m=(o+s)*u;return a[0]=2*l,a[4]=0,a[8]=0,a[12]=-d,a[1]=0,a[5]=2*c,a[9]=0,a[13]=-f,a[2]=0,a[6]=0,a[10]=-2*u,a[14]=-m,a[3]=0,a[7]=0,a[11]=0,a[15]=1,this}equals(e){const t=this.elements,n=e.elements;for(let r=0;r<16;r++)if(t[r]!==n[r])return!1;return!0}fromArray(e,t=0){for(let n=0;n<16;n++)this.elements[n]=e[n+t];return this}toArray(e=[],t=0){const n=this.elements;return e[t]=n[0],e[t+1]=n[1],e[t+2]=n[2],e[t+3]=n[3],e[t+4]=n[4],e[t+5]=n[5],e[t+6]=n[6],e[t+7]=n[7],e[t+8]=n[8],e[t+9]=n[9],e[t+10]=n[10],e[t+11]=n[11],e[t+12]=n[12],e[t+13]=n[13],e[t+14]=n[14],e[t+15]=n[15],e}}const On=new C,Ft=new ke,ic=new C(0,0,0),rc=new C(1,1,1),an=new C,Zi=new C,At=new C,ga=new ke,va=new Ln;class Ui{constructor(e=0,t=0,n=0,r=Ui.DefaultOrder){this.isEuler=!0,this._x=e,this._y=t,this._z=n,this._order=r}get x(){return this._x}set x(e){this._x=e,this._onChangeCallback()}get y(){return this._y}set y(e){this._y=e,this._onChangeCallback()}get z(){return this._z}set z(e){this._z=e,this._onChangeCallback()}get order(){return this._order}set order(e){this._order=e,this._onChangeCallback()}set(e,t,n,r=this._order){return this._x=e,this._y=t,this._z=n,this._order=r,this._onChangeCallback(),this}clone(){return new this.constructor(this._x,this._y,this._z,this._order)}copy(e){return this._x=e._x,this._y=e._y,this._z=e._z,this._order=e._order,this._onChangeCallback(),this}setFromRotationMatrix(e,t=this._order,n=!0){const r=e.elements,s=r[0],o=r[4],a=r[8],l=r[1],c=r[5],u=r[9],d=r[2],f=r[6],m=r[10];switch(t){case"XYZ":this._y=Math.asin(vt(a,-1,1)),Math.abs(a)<.9999999?(this._x=Math.atan2(-u,m),this._z=Math.atan2(-o,s)):(this._x=Math.atan2(f,c),this._z=0);break;case"YXZ":this._x=Math.asin(-vt(u,-1,1)),Math.abs(u)<.9999999?(this._y=Math.atan2(a,m),this._z=Math.atan2(l,c)):(this._y=Math.atan2(-d,s),this._z=0);break;case"ZXY":this._x=Math.asin(vt(f,-1,1)),Math.abs(f)<.9999999?(this._y=Math.atan2(-d,m),this._z=Math.atan2(-o,c)):(this._y=0,this._z=Math.atan2(l,s));break;case"ZYX":this._y=Math.asin(-vt(d,-1,1)),Math.abs(d)<.9999999?(this._x=Math.atan2(f,m),this._z=Math.atan2(l,s)):(this._x=0,this._z=Math.atan2(-o,c));break;case"YZX":this._z=Math.asin(vt(l,-1,1)),Math.abs(l)<.9999999?(this._x=Math.atan2(-u,c),this._y=Math.atan2(-d,s)):(this._x=0,this._y=Math.atan2(a,m));break;case"XZY":this._z=Math.asin(-vt(o,-1,1)),Math.abs(o)<.9999999?(this._x=Math.atan2(f,c),this._y=Math.atan2(a,s)):(this._x=Math.atan2(-u,m),this._y=0);break;default:console.warn("THREE.Euler: .setFromRotationMatrix() encountered an unknown order: "+t)}return this._order=t,n===!0&&this._onChangeCallback(),this}setFromQuaternion(e,t,n){return ga.makeRotationFromQuaternion(e),this.setFromRotationMatrix(ga,t,n)}setFromVector3(e,t=this._order){return this.set(e.x,e.y,e.z,t)}reorder(e){return va.setFromEuler(this),this.setFromQuaternion(va,e)}equals(e){return e._x===this._x&&e._y===this._y&&e._z===this._z&&e._order===this._order}fromArray(e){return this._x=e[0],this._y=e[1],this._z=e[2],e[3]!==void 0&&(this._order=e[3]),this._onChangeCallback(),this}toArray(e=[],t=0){return e[t]=this._x,e[t+1]=this._y,e[t+2]=this._z,e[t+3]=this._order,e}_onChange(e){return this._onChangeCallback=e,this}_onChangeCallback(){}*[Symbol.iterator](){yield this._x,yield this._y,yield this._z,yield this._order}toVector3(){console.error("THREE.Euler: .toVector3() has been removed. Use Vector3.setFromEuler() instead")}}Ui.DefaultOrder="XYZ";Ui.RotationOrders=["XYZ","YZX","ZXY","XZY","YXZ","ZYX"];class ws{constructor(){this.mask=1}set(e){this.mask=(1<<e|0)>>>0}enable(e){this.mask|=1<<e|0}enableAll(){this.mask=-1}toggle(e){this.mask^=1<<e|0}disable(e){this.mask&=~(1<<e|0)}disableAll(){this.mask=0}test(e){return(this.mask&e.mask)!==0}isEnabled(e){return(this.mask&(1<<e|0))!==0}}let sc=0;const xa=new C,Bn=new Ln,Qt=new ke,Gi=new C,mi=new C,ac=new C,oc=new Ln,Ma=new C(1,0,0),ya=new C(0,1,0),Aa=new C(0,0,1),lc={type:"added"},_a={type:"removed"};class pt extends Pn{constructor(){super(),this.isObject3D=!0,Object.defineProperty(this,"id",{value:sc++}),this.uuid=zi(),this.name="",this.type="Object3D",this.parent=null,this.children=[],this.up=pt.DefaultUp.clone();const e=new C,t=new Ui,n=new Ln,r=new C(1,1,1);function s(){n.setFromEuler(t,!1)}function o(){t.setFromQuaternion(n,void 0,!1)}t._onChange(s),n._onChange(o),Object.defineProperties(this,{position:{configurable:!0,enumerable:!0,value:e},rotation:{configurable:!0,enumerable:!0,value:t},quaternion:{configurable:!0,enumerable:!0,value:n},scale:{configurable:!0,enumerable:!0,value:r},modelViewMatrix:{value:new ke},normalMatrix:{value:new St}}),this.matrix=new ke,this.matrixWorld=new ke,this.matrixAutoUpdate=pt.DefaultMatrixAutoUpdate,this.matrixWorldNeedsUpdate=!1,this.matrixWorldAutoUpdate=pt.DefaultMatrixWorldAutoUpdate,this.layers=new ws,this.visible=!0,this.castShadow=!1,this.receiveShadow=!1,this.frustumCulled=!0,this.renderOrder=0,this.animations=[],this.userData={}}onBeforeRender(){}onAfterRender(){}applyMatrix4(e){this.matrixAutoUpdate&&this.updateMatrix(),this.matrix.premultiply(e),this.matrix.decompose(this.position,this.quaternion,this.scale)}applyQuaternion(e){return this.quaternion.premultiply(e),this}setRotationFromAxisAngle(e,t){this.quaternion.setFromAxisAngle(e,t)}setRotationFromEuler(e){this.quaternion.setFromEuler(e,!0)}setRotationFromMatrix(e){this.quaternion.setFromRotationMatrix(e)}setRotationFromQuaternion(e){this.quaternion.copy(e)}rotateOnAxis(e,t){return Bn.setFromAxisAngle(e,t),this.quaternion.multiply(Bn),this}rotateOnWorldAxis(e,t){return Bn.setFromAxisAngle(e,t),this.quaternion.premultiply(Bn),this}rotateX(e){return this.rotateOnAxis(Ma,e)}rotateY(e){return this.rotateOnAxis(ya,e)}rotateZ(e){return this.rotateOnAxis(Aa,e)}translateOnAxis(e,t){return xa.copy(e).applyQuaternion(this.quaternion),this.position.add(xa.multiplyScalar(t)),this}translateX(e){return this.translateOnAxis(Ma,e)}translateY(e){return this.translateOnAxis(ya,e)}translateZ(e){return this.translateOnAxis(Aa,e)}localToWorld(e){return e.applyMatrix4(this.matrixWorld)}worldToLocal(e){return e.applyMatrix4(Qt.copy(this.matrixWorld).invert())}lookAt(e,t,n){e.isVector3?Gi.copy(e):Gi.set(e,t,n);const r=this.parent;this.updateWorldMatrix(!0,!1),mi.setFromMatrixPosition(this.matrixWorld),this.isCamera||this.isLight?Qt.lookAt(mi,Gi,this.up):Qt.lookAt(Gi,mi,this.up),this.quaternion.setFromRotationMatrix(Qt),r&&(Qt.extractRotation(r.matrixWorld),Bn.setFromRotationMatrix(Qt),this.quaternion.premultiply(Bn.invert()))}add(e){if(arguments.length>1){for(let t=0;t<arguments.length;t++)this.add(arguments[t]);return this}return e===this?(console.error("THREE.Object3D.add: object can't be added as a child of itself.",e),this):(e&&e.isObject3D?(e.parent!==null&&e.parent.remove(e),e.parent=this,this.children.push(e),e.dispatchEvent(lc)):console.error("THREE.Object3D.add: object not an instance of THREE.Object3D.",e),this)}remove(e){if(arguments.length>1){for(let n=0;n<arguments.length;n++)this.remove(arguments[n]);return this}const t=this.children.indexOf(e);return t!==-1&&(e.parent=null,this.children.splice(t,1),e.dispatchEvent(_a)),this}removeFromParent(){const e=this.parent;return e!==null&&e.remove(this),this}clear(){for(let e=0;e<this.children.length;e++){const t=this.children[e];t.parent=null,t.dispatchEvent(_a)}return this.children.length=0,this}attach(e){return this.updateWorldMatrix(!0,!1),Qt.copy(this.matrixWorld).invert(),e.parent!==null&&(e.parent.updateWorldMatrix(!0,!1),Qt.multiply(e.parent.matrixWorld)),e.applyMatrix4(Qt),this.add(e),e.updateWorldMatrix(!1,!0),this}getObjectById(e){return this.getObjectByProperty("id",e)}getObjectByName(e){return this.getObjectByProperty("name",e)}getObjectByProperty(e,t){if(this[e]===t)return this;for(let n=0,r=this.children.length;n<r;n++){const o=this.children[n].getObjectByProperty(e,t);if(o!==void 0)return o}}getWorldPosition(e){return this.updateWorldMatrix(!0,!1),e.setFromMatrixPosition(this.matrixWorld)}getWorldQuaternion(e){return this.updateWorldMatrix(!0,!1),this.matrixWorld.decompose(mi,e,ac),e}getWorldScale(e){return this.updateWorldMatrix(!0,!1),this.matrixWorld.decompose(mi,oc,e),e}getWorldDirection(e){this.updateWorldMatrix(!0,!1);const t=this.matrixWorld.elements;return e.set(t[8],t[9],t[10]).normalize()}raycast(){}traverse(e){e(this);const t=this.children;for(let n=0,r=t.length;n<r;n++)t[n].traverse(e)}traverseVisible(e){if(this.visible===!1)return;e(this);const t=this.children;for(let n=0,r=t.length;n<r;n++)t[n].traverseVisible(e)}traverseAncestors(e){const t=this.parent;t!==null&&(e(t),t.traverseAncestors(e))}updateMatrix(){this.matrix.compose(this.position,this.quaternion,this.scale),this.matrixWorldNeedsUpdate=!0}updateMatrixWorld(e){this.matrixAutoUpdate&&this.updateMatrix(),(this.matrixWorldNeedsUpdate||e)&&(this.parent===null?this.matrixWorld.copy(this.matrix):this.matrixWorld.multiplyMatrices(this.parent.matrixWorld,this.matrix),this.matrixWorldNeedsUpdate=!1,e=!0);const t=this.children;for(let n=0,r=t.length;n<r;n++){const s=t[n];(s.matrixWorldAutoUpdate===!0||e===!0)&&s.updateMatrixWorld(e)}}updateWorldMatrix(e,t){const n=this.parent;if(e===!0&&n!==null&&n.matrixWorldAutoUpdate===!0&&n.updateWorldMatrix(!0,!1),this.matrixAutoUpdate&&this.updateMatrix(),this.parent===null?this.matrixWorld.copy(this.matrix):this.matrixWorld.multiplyMatrices(this.parent.matrixWorld,this.matrix),t===!0){const r=this.children;for(let s=0,o=r.length;s<o;s++){const a=r[s];a.matrixWorldAutoUpdate===!0&&a.updateWorldMatrix(!1,!0)}}}toJSON(e){const t=e===void 0||typeof e=="string",n={};t&&(e={geometries:{},materials:{},textures:{},images:{},shapes:{},skeletons:{},animations:{},nodes:{}},n.metadata={version:4.5,type:"Object",generator:"Object3D.toJSON"});const r={};r.uuid=this.uuid,r.type=this.type,this.name!==""&&(r.name=this.name),this.castShadow===!0&&(r.castShadow=!0),this.receiveShadow===!0&&(r.receiveShadow=!0),this.visible===!1&&(r.visible=!1),this.frustumCulled===!1&&(r.frustumCulled=!1),this.renderOrder!==0&&(r.renderOrder=this.renderOrder),JSON.stringify(this.userData)!=="{}"&&(r.userData=this.userData),r.layers=this.layers.mask,r.matrix=this.matrix.toArray(),this.matrixAutoUpdate===!1&&(r.matrixAutoUpdate=!1),this.isInstancedMesh&&(r.type="InstancedMesh",r.count=this.count,r.instanceMatrix=this.instanceMatrix.toJSON(),this.instanceColor!==null&&(r.instanceColor=this.instanceColor.toJSON()));function s(a,l){return a[l.uuid]===void 0&&(a[l.uuid]=l.toJSON(e)),l.uuid}if(this.isScene)this.background&&(this.background.isColor?r.background=this.background.toJSON():this.background.isTexture&&(r.background=this.background.toJSON(e).uuid)),this.environment&&this.environment.isTexture&&this.environment.isRenderTargetTexture!==!0&&(r.environment=this.environment.toJSON(e).uuid);else if(this.isMesh||this.isLine||this.isPoints){r.geometry=s(e.geometries,this.geometry);const a=this.geometry.parameters;if(a!==void 0&&a.shapes!==void 0){const l=a.shapes;if(Array.isArray(l))for(let c=0,u=l.length;c<u;c++){const d=l[c];s(e.shapes,d)}else s(e.shapes,l)}}if(this.isSkinnedMesh&&(r.bindMode=this.bindMode,r.bindMatrix=this.bindMatrix.toArray(),this.skeleton!==void 0&&(s(e.skeletons,this.skeleton),r.skeleton=this.skeleton.uuid)),this.material!==void 0)if(Array.isArray(this.material)){const a=[];for(let l=0,c=this.material.length;l<c;l++)a.push(s(e.materials,this.material[l]));r.material=a}else r.material=s(e.materials,this.material);if(this.children.length>0){r.children=[];for(let a=0;a<this.children.length;a++)r.children.push(this.children[a].toJSON(e).object)}if(this.animations.length>0){r.animations=[];for(let a=0;a<this.animations.length;a++){const l=this.animations[a];r.animations.push(s(e.animations,l))}}if(t){const a=o(e.geometries),l=o(e.materials),c=o(e.textures),u=o(e.images),d=o(e.shapes),f=o(e.skeletons),m=o(e.animations),g=o(e.nodes);a.length>0&&(n.geometries=a),l.length>0&&(n.materials=l),c.length>0&&(n.textures=c),u.length>0&&(n.images=u),d.length>0&&(n.shapes=d),f.length>0&&(n.skeletons=f),m.length>0&&(n.animations=m),g.length>0&&(n.nodes=g)}return n.object=r,n;function o(a){const l=[];for(const c in a){const u=a[c];delete u.metadata,l.push(u)}return l}}clone(e){return new this.constructor().copy(this,e)}copy(e,t=!0){if(this.name=e.name,this.up.copy(e.up),this.position.copy(e.position),this.rotation.order=e.rotation.order,this.quaternion.copy(e.quaternion),this.scale.copy(e.scale),this.matrix.copy(e.matrix),this.matrixWorld.copy(e.matrixWorld),this.matrixAutoUpdate=e.matrixAutoUpdate,this.matrixWorldNeedsUpdate=e.matrixWorldNeedsUpdate,this.matrixWorldAutoUpdate=e.matrixWorldAutoUpdate,this.layers.mask=e.layers.mask,this.visible=e.visible,this.castShadow=e.castShadow,this.receiveShadow=e.receiveShadow,this.frustumCulled=e.frustumCulled,this.renderOrder=e.renderOrder,this.userData=JSON.parse(JSON.stringify(e.userData)),t===!0)for(let n=0;n<e.children.length;n++){const r=e.children[n];this.add(r.clone())}return this}}pt.DefaultUp=new C(0,1,0);pt.DefaultMatrixAutoUpdate=!0;pt.DefaultMatrixWorldAutoUpdate=!0;const qt=new C,Jt=new C,Zr=new C,$t=new C,kn=new C,Hn=new C,ba=new C,Gr=new C,Wr=new C,Xr=new C;class en{constructor(e=new C,t=new C,n=new C){this.a=e,this.b=t,this.c=n}static getNormal(e,t,n,r){r.subVectors(n,t),qt.subVectors(e,t),r.cross(qt);const s=r.lengthSq();return s>0?r.multiplyScalar(1/Math.sqrt(s)):r.set(0,0,0)}static getBarycoord(e,t,n,r,s){qt.subVectors(r,t),Jt.subVectors(n,t),Zr.subVectors(e,t);const o=qt.dot(qt),a=qt.dot(Jt),l=qt.dot(Zr),c=Jt.dot(Jt),u=Jt.dot(Zr),d=o*c-a*a;if(d===0)return s.set(-2,-1,-1);const f=1/d,m=(c*l-a*u)*f,g=(o*u-a*l)*f;return s.set(1-m-g,g,m)}static containsPoint(e,t,n,r){return this.getBarycoord(e,t,n,r,$t),$t.x>=0&&$t.y>=0&&$t.x+$t.y<=1}static getUV(e,t,n,r,s,o,a,l){return this.getBarycoord(e,t,n,r,$t),l.set(0,0),l.addScaledVector(s,$t.x),l.addScaledVector(o,$t.y),l.addScaledVector(a,$t.z),l}static isFrontFacing(e,t,n,r){return qt.subVectors(n,t),Jt.subVectors(e,t),qt.cross(Jt).dot(r)<0}set(e,t,n){return this.a.copy(e),this.b.copy(t),this.c.copy(n),this}setFromPointsAndIndices(e,t,n,r){return this.a.copy(e[t]),this.b.copy(e[n]),this.c.copy(e[r]),this}setFromAttributeAndIndices(e,t,n,r){return this.a.fromBufferAttribute(e,t),this.b.fromBufferAttribute(e,n),this.c.fromBufferAttribute(e,r),this}clone(){return new this.constructor().copy(this)}copy(e){return this.a.copy(e.a),this.b.copy(e.b),this.c.copy(e.c),this}getArea(){return qt.subVectors(this.c,this.b),Jt.subVectors(this.a,this.b),qt.cross(Jt).length()*.5}getMidpoint(e){return e.addVectors(this.a,this.b).add(this.c).multiplyScalar(1/3)}getNormal(e){return en.getNormal(this.a,this.b,this.c,e)}getPlane(e){return e.setFromCoplanarPoints(this.a,this.b,this.c)}getBarycoord(e,t){return en.getBarycoord(e,this.a,this.b,this.c,t)}getUV(e,t,n,r,s){return en.getUV(e,this.a,this.b,this.c,t,n,r,s)}containsPoint(e){return en.containsPoint(e,this.a,this.b,this.c)}isFrontFacing(e){return en.isFrontFacing(this.a,this.b,this.c,e)}intersectsBox(e){return e.intersectsTriangle(this)}closestPointToPoint(e,t){const n=this.a,r=this.b,s=this.c;let o,a;kn.subVectors(r,n),Hn.subVectors(s,n),Gr.subVectors(e,n);const l=kn.dot(Gr),c=Hn.dot(Gr);if(l<=0&&c<=0)return t.copy(n);Wr.subVectors(e,r);const u=kn.dot(Wr),d=Hn.dot(Wr);if(u>=0&&d<=u)return t.copy(r);const f=l*d-u*c;if(f<=0&&l>=0&&u<=0)return o=l/(l-u),t.copy(n).addScaledVector(kn,o);Xr.subVectors(e,s);const m=kn.dot(Xr),g=Hn.dot(Xr);if(g>=0&&m<=g)return t.copy(s);const p=m*c-l*g;if(p<=0&&c>=0&&g<=0)return a=c/(c-g),t.copy(n).addScaledVector(Hn,a);const h=u*g-m*d;if(h<=0&&d-u>=0&&m-g>=0)return ba.subVectors(s,r),a=(d-u)/(d-u+(m-g)),t.copy(r).addScaledVector(ba,a);const x=1/(h+p+f);return o=p*x,a=f*x,t.copy(n).addScaledVector(kn,o).addScaledVector(Hn,a)}equals(e){return e.a.equals(this.a)&&e.b.equals(this.b)&&e.c.equals(this.c)}}let cc=0;class ci extends Pn{constructor(){super(),this.isMaterial=!0,Object.defineProperty(this,"id",{value:cc++}),this.uuid=zi(),this.name="",this.type="Material",this.blending=$n,this.side=ti,this.vertexColors=!1,this.opacity=1,this.transparent=!1,this.blendSrc=Mo,this.blendDst=yo,this.blendEquation=Qn,this.blendSrcAlpha=null,this.blendDstAlpha=null,this.blendEquationAlpha=null,this.depthFunc=ds,this.depthTest=!0,this.depthWrite=!0,this.stencilWriteMask=255,this.stencilFunc=Jl,this.stencilRef=0,this.stencilFuncMask=255,this.stencilFail=Cr,this.stencilZFail=Cr,this.stencilZPass=Cr,this.stencilWrite=!1,this.clippingPlanes=null,this.clipIntersection=!1,this.clipShadows=!1,this.shadowSide=null,this.colorWrite=!0,this.precision=null,this.polygonOffset=!1,this.polygonOffsetFactor=0,this.polygonOffsetUnits=0,this.dithering=!1,this.alphaToCoverage=!1,this.premultipliedAlpha=!1,this.visible=!0,this.toneMapped=!0,this.userData={},this.version=0,this._alphaTest=0}get alphaTest(){return this._alphaTest}set alphaTest(e){this._alphaTest>0!=e>0&&this.version++,this._alphaTest=e}onBuild(){}onBeforeRender(){}onBeforeCompile(){}customProgramCacheKey(){return this.onBeforeCompile.toString()}setValues(e){if(e!==void 0)for(const t in e){const n=e[t];if(n===void 0){console.warn("THREE.Material: '"+t+"' parameter is undefined.");continue}const r=this[t];if(r===void 0){console.warn("THREE."+this.type+": '"+t+"' is not a property of this material.");continue}r&&r.isColor?r.set(n):r&&r.isVector3&&n&&n.isVector3?r.copy(n):this[t]=n}}toJSON(e){const t=e===void 0||typeof e=="string";t&&(e={textures:{},images:{}});const n={metadata:{version:4.5,type:"Material",generator:"Material.toJSON"}};n.uuid=this.uuid,n.type=this.type,this.name!==""&&(n.name=this.name),this.color&&this.color.isColor&&(n.color=this.color.getHex()),this.roughness!==void 0&&(n.roughness=this.roughness),this.metalness!==void 0&&(n.metalness=this.metalness),this.sheen!==void 0&&(n.sheen=this.sheen),this.sheenColor&&this.sheenColor.isColor&&(n.sheenColor=this.sheenColor.getHex()),this.sheenRoughness!==void 0&&(n.sheenRoughness=this.sheenRoughness),this.emissive&&this.emissive.isColor&&(n.emissive=this.emissive.getHex()),this.emissiveIntensity&&this.emissiveIntensity!==1&&(n.emissiveIntensity=this.emissiveIntensity),this.specular&&this.specular.isColor&&(n.specular=this.specular.getHex()),this.specularIntensity!==void 0&&(n.specularIntensity=this.specularIntensity),this.specularColor&&this.specularColor.isColor&&(n.specularColor=this.specularColor.getHex()),this.shininess!==void 0&&(n.shininess=this.shininess),this.clearcoat!==void 0&&(n.clearcoat=this.clearcoat),this.clearcoatRoughness!==void 0&&(n.clearcoatRoughness=this.clearcoatRoughness),this.clearcoatMap&&this.clearcoatMap.isTexture&&(n.clearcoatMap=this.clearcoatMap.toJSON(e).uuid),this.clearcoatRoughnessMap&&this.clearcoatRoughnessMap.isTexture&&(n.clearcoatRoughnessMap=this.clearcoatRoughnessMap.toJSON(e).uuid),this.clearcoatNormalMap&&this.clearcoatNormalMap.isTexture&&(n.clearcoatNormalMap=this.clearcoatNormalMap.toJSON(e).uuid,n.clearcoatNormalScale=this.clearcoatNormalScale.toArray()),this.iridescence!==void 0&&(n.iridescence=this.iridescence),this.iridescenceIOR!==void 0&&(n.iridescenceIOR=this.iridescenceIOR),this.iridescenceThicknessRange!==void 0&&(n.iridescenceThicknessRange=this.iridescenceThicknessRange),this.iridescenceMap&&this.iridescenceMap.isTexture&&(n.iridescenceMap=this.iridescenceMap.toJSON(e).uuid),this.iridescenceThicknessMap&&this.iridescenceThicknessMap.isTexture&&(n.iridescenceThicknessMap=this.iridescenceThicknessMap.toJSON(e).uuid),this.map&&this.map.isTexture&&(n.map=this.map.toJSON(e).uuid),this.matcap&&this.matcap.isTexture&&(n.matcap=this.matcap.toJSON(e).uuid),this.alphaMap&&this.alphaMap.isTexture&&(n.alphaMap=this.alphaMap.toJSON(e).uuid),this.lightMap&&this.lightMap.isTexture&&(n.lightMap=this.lightMap.toJSON(e).uuid,n.lightMapIntensity=this.lightMapIntensity),this.aoMap&&this.aoMap.isTexture&&(n.aoMap=this.aoMap.toJSON(e).uuid,n.aoMapIntensity=this.aoMapIntensity),this.bumpMap&&this.bumpMap.isTexture&&(n.bumpMap=this.bumpMap.toJSON(e).uuid,n.bumpScale=this.bumpScale),this.normalMap&&this.normalMap.isTexture&&(n.normalMap=this.normalMap.toJSON(e).uuid,n.normalMapType=this.normalMapType,n.normalScale=this.normalScale.toArray()),this.displacementMap&&this.displacementMap.isTexture&&(n.displacementMap=this.displacementMap.toJSON(e).uuid,n.displacementScale=this.displacementScale,n.displacementBias=this.displacementBias),this.roughnessMap&&this.roughnessMap.isTexture&&(n.roughnessMap=this.roughnessMap.toJSON(e).uuid),this.metalnessMap&&this.metalnessMap.isTexture&&(n.metalnessMap=this.metalnessMap.toJSON(e).uuid),this.emissiveMap&&this.emissiveMap.isTexture&&(n.emissiveMap=this.emissiveMap.toJSON(e).uuid),this.specularMap&&this.specularMap.isTexture&&(n.specularMap=this.specularMap.toJSON(e).uuid),this.specularIntensityMap&&this.specularIntensityMap.isTexture&&(n.specularIntensityMap=this.specularIntensityMap.toJSON(e).uuid),this.specularColorMap&&this.specularColorMap.isTexture&&(n.specularColorMap=this.specularColorMap.toJSON(e).uuid),this.envMap&&this.envMap.isTexture&&(n.envMap=this.envMap.toJSON(e).uuid,this.combine!==void 0&&(n.combine=this.combine)),this.envMapIntensity!==void 0&&(n.envMapIntensity=this.envMapIntensity),this.reflectivity!==void 0&&(n.reflectivity=this.reflectivity),this.refractionRatio!==void 0&&(n.refractionRatio=this.refractionRatio),this.gradientMap&&this.gradientMap.isTexture&&(n.gradientMap=this.gradientMap.toJSON(e).uuid),this.transmission!==void 0&&(n.transmission=this.transmission),this.transmissionMap&&this.transmissionMap.isTexture&&(n.transmissionMap=this.transmissionMap.toJSON(e).uuid),this.thickness!==void 0&&(n.thickness=this.thickness),this.thicknessMap&&this.thicknessMap.isTexture&&(n.thicknessMap=this.thicknessMap.toJSON(e).uuid),this.attenuationDistance!==void 0&&this.attenuationDistance!==1/0&&(n.attenuationDistance=this.attenuationDistance),this.attenuationColor!==void 0&&(n.attenuationColor=this.attenuationColor.getHex()),this.size!==void 0&&(n.size=this.size),this.shadowSide!==null&&(n.shadowSide=this.shadowSide),this.sizeAttenuation!==void 0&&(n.sizeAttenuation=this.sizeAttenuation),this.blending!==$n&&(n.blending=this.blending),this.side!==ti&&(n.side=this.side),this.vertexColors&&(n.vertexColors=!0),this.opacity<1&&(n.opacity=this.opacity),this.transparent===!0&&(n.transparent=this.transparent),n.depthFunc=this.depthFunc,n.depthTest=this.depthTest,n.depthWrite=this.depthWrite,n.colorWrite=this.colorWrite,n.stencilWrite=this.stencilWrite,n.stencilWriteMask=this.stencilWriteMask,n.stencilFunc=this.stencilFunc,n.stencilRef=this.stencilRef,n.stencilFuncMask=this.stencilFuncMask,n.stencilFail=this.stencilFail,n.stencilZFail=this.stencilZFail,n.stencilZPass=this.stencilZPass,this.rotation!==void 0&&this.rotation!==0&&(n.rotation=this.rotation),this.polygonOffset===!0&&(n.polygonOffset=!0),this.polygonOffsetFactor!==0&&(n.polygonOffsetFactor=this.polygonOffsetFactor),this.polygonOffsetUnits!==0&&(n.polygonOffsetUnits=this.polygonOffsetUnits),this.linewidth!==void 0&&this.linewidth!==1&&(n.linewidth=this.linewidth),this.dashSize!==void 0&&(n.dashSize=this.dashSize),this.gapSize!==void 0&&(n.gapSize=this.gapSize),this.scale!==void 0&&(n.scale=this.scale),this.dithering===!0&&(n.dithering=!0),this.alphaTest>0&&(n.alphaTest=this.alphaTest),this.alphaToCoverage===!0&&(n.alphaToCoverage=this.alphaToCoverage),this.premultipliedAlpha===!0&&(n.premultipliedAlpha=this.premultipliedAlpha),this.wireframe===!0&&(n.wireframe=this.wireframe),this.wireframeLinewidth>1&&(n.wireframeLinewidth=this.wireframeLinewidth),this.wireframeLinecap!=="round"&&(n.wireframeLinecap=this.wireframeLinecap),this.wireframeLinejoin!=="round"&&(n.wireframeLinejoin=this.wireframeLinejoin),this.flatShading===!0&&(n.flatShading=this.flatShading),this.visible===!1&&(n.visible=!1),this.toneMapped===!1&&(n.toneMapped=!1),this.fog===!1&&(n.fog=!1),JSON.stringify(this.userData)!=="{}"&&(n.userData=this.userData);function r(s){const o=[];for(const a in s){const l=s[a];delete l.metadata,o.push(l)}return o}if(t){const s=r(e.textures),o=r(e.images);s.length>0&&(n.textures=s),o.length>0&&(n.images=o)}return n}clone(){return new this.constructor().copy(this)}copy(e){this.name=e.name,this.blending=e.blending,this.side=e.side,this.vertexColors=e.vertexColors,this.opacity=e.opacity,this.transparent=e.transparent,this.blendSrc=e.blendSrc,this.blendDst=e.blendDst,this.blendEquation=e.blendEquation,this.blendSrcAlpha=e.blendSrcAlpha,this.blendDstAlpha=e.blendDstAlpha,this.blendEquationAlpha=e.blendEquationAlpha,this.depthFunc=e.depthFunc,this.depthTest=e.depthTest,this.depthWrite=e.depthWrite,this.stencilWriteMask=e.stencilWriteMask,this.stencilFunc=e.stencilFunc,this.stencilRef=e.stencilRef,this.stencilFuncMask=e.stencilFuncMask,this.stencilFail=e.stencilFail,this.stencilZFail=e.stencilZFail,this.stencilZPass=e.stencilZPass,this.stencilWrite=e.stencilWrite;const t=e.clippingPlanes;let n=null;if(t!==null){const r=t.length;n=new Array(r);for(let s=0;s!==r;++s)n[s]=t[s].clone()}return this.clippingPlanes=n,this.clipIntersection=e.clipIntersection,this.clipShadows=e.clipShadows,this.shadowSide=e.shadowSide,this.colorWrite=e.colorWrite,this.precision=e.precision,this.polygonOffset=e.polygonOffset,this.polygonOffsetFactor=e.polygonOffsetFactor,this.polygonOffsetUnits=e.polygonOffsetUnits,this.dithering=e.dithering,this.alphaTest=e.alphaTest,this.alphaToCoverage=e.alphaToCoverage,this.premultipliedAlpha=e.premultipliedAlpha,this.visible=e.visible,this.toneMapped=e.toneMapped,this.userData=JSON.parse(JSON.stringify(e.userData)),this}dispose(){this.dispatchEvent({type:"dispose"})}set needsUpdate(e){e===!0&&this.version++}}class Es extends ci{constructor(e){super(),this.isMeshBasicMaterial=!0,this.type="MeshBasicMaterial",this.color=new qe(16777215),this.map=null,this.lightMap=null,this.lightMapIntensity=1,this.aoMap=null,this.aoMapIntensity=1,this.specularMap=null,this.alphaMap=null,this.envMap=null,this.combine=Ao,this.reflectivity=1,this.refractionRatio=.98,this.wireframe=!1,this.wireframeLinewidth=1,this.wireframeLinecap="round",this.wireframeLinejoin="round",this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.map=e.map,this.lightMap=e.lightMap,this.lightMapIntensity=e.lightMapIntensity,this.aoMap=e.aoMap,this.aoMapIntensity=e.aoMapIntensity,this.specularMap=e.specularMap,this.alphaMap=e.alphaMap,this.envMap=e.envMap,this.combine=e.combine,this.reflectivity=e.reflectivity,this.refractionRatio=e.refractionRatio,this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this.wireframeLinecap=e.wireframeLinecap,this.wireframeLinejoin=e.wireframeLinejoin,this.fog=e.fog,this}}const We=new C,Wi=new Se;class Dt{constructor(e,t,n){if(Array.isArray(e))throw new TypeError("THREE.BufferAttribute: array should be a Typed Array.");this.isBufferAttribute=!0,this.name="",this.array=e,this.itemSize=t,this.count=e!==void 0?e.length/t:0,this.normalized=n===!0,this.usage=da,this.updateRange={offset:0,count:-1},this.version=0}onUploadCallback(){}set needsUpdate(e){e===!0&&this.version++}setUsage(e){return this.usage=e,this}copy(e){return this.name=e.name,this.array=new e.array.constructor(e.array),this.itemSize=e.itemSize,this.count=e.count,this.normalized=e.normalized,this.usage=e.usage,this}copyAt(e,t,n){e*=this.itemSize,n*=t.itemSize;for(let r=0,s=this.itemSize;r<s;r++)this.array[e+r]=t.array[n+r];return this}copyArray(e){return this.array.set(e),this}applyMatrix3(e){if(this.itemSize===2)for(let t=0,n=this.count;t<n;t++)Wi.fromBufferAttribute(this,t),Wi.applyMatrix3(e),this.setXY(t,Wi.x,Wi.y);else if(this.itemSize===3)for(let t=0,n=this.count;t<n;t++)We.fromBufferAttribute(this,t),We.applyMatrix3(e),this.setXYZ(t,We.x,We.y,We.z);return this}applyMatrix4(e){for(let t=0,n=this.count;t<n;t++)We.fromBufferAttribute(this,t),We.applyMatrix4(e),this.setXYZ(t,We.x,We.y,We.z);return this}applyNormalMatrix(e){for(let t=0,n=this.count;t<n;t++)We.fromBufferAttribute(this,t),We.applyNormalMatrix(e),this.setXYZ(t,We.x,We.y,We.z);return this}transformDirection(e){for(let t=0,n=this.count;t<n;t++)We.fromBufferAttribute(this,t),We.transformDirection(e),this.setXYZ(t,We.x,We.y,We.z);return this}set(e,t=0){return this.array.set(e,t),this}getX(e){let t=this.array[e*this.itemSize];return this.normalized&&(t=Ni(t,this.array)),t}setX(e,t){return this.normalized&&(t=yt(t,this.array)),this.array[e*this.itemSize]=t,this}getY(e){let t=this.array[e*this.itemSize+1];return this.normalized&&(t=Ni(t,this.array)),t}setY(e,t){return this.normalized&&(t=yt(t,this.array)),this.array[e*this.itemSize+1]=t,this}getZ(e){let t=this.array[e*this.itemSize+2];return this.normalized&&(t=Ni(t,this.array)),t}setZ(e,t){return this.normalized&&(t=yt(t,this.array)),this.array[e*this.itemSize+2]=t,this}getW(e){let t=this.array[e*this.itemSize+3];return this.normalized&&(t=Ni(t,this.array)),t}setW(e,t){return this.normalized&&(t=yt(t,this.array)),this.array[e*this.itemSize+3]=t,this}setXY(e,t,n){return e*=this.itemSize,this.normalized&&(t=yt(t,this.array),n=yt(n,this.array)),this.array[e+0]=t,this.array[e+1]=n,this}setXYZ(e,t,n,r){return e*=this.itemSize,this.normalized&&(t=yt(t,this.array),n=yt(n,this.array),r=yt(r,this.array)),this.array[e+0]=t,this.array[e+1]=n,this.array[e+2]=r,this}setXYZW(e,t,n,r,s){return e*=this.itemSize,this.normalized&&(t=yt(t,this.array),n=yt(n,this.array),r=yt(r,this.array),s=yt(s,this.array)),this.array[e+0]=t,this.array[e+1]=n,this.array[e+2]=r,this.array[e+3]=s,this}onUpload(e){return this.onUploadCallback=e,this}clone(){return new this.constructor(this.array,this.itemSize).copy(this)}toJSON(){const e={itemSize:this.itemSize,type:this.array.constructor.name,array:Array.from(this.array),normalized:this.normalized};return this.name!==""&&(e.name=this.name),this.usage!==da&&(e.usage=this.usage),(this.updateRange.offset!==0||this.updateRange.count!==-1)&&(e.updateRange=this.updateRange),e}copyColorsArray(){console.error("THREE.BufferAttribute: copyColorsArray() was removed in r144.")}copyVector2sArray(){console.error("THREE.BufferAttribute: copyVector2sArray() was removed in r144.")}copyVector3sArray(){console.error("THREE.BufferAttribute: copyVector3sArray() was removed in r144.")}copyVector4sArray(){console.error("THREE.BufferAttribute: copyVector4sArray() was removed in r144.")}}class jr extends Dt{constructor(e,t,n){super(new Uint8Array(e),t,n)}}class Ro extends Dt{constructor(e,t,n){super(new Uint16Array(e),t,n)}}class Ts extends Dt{constructor(e,t,n){super(new Uint32Array(e),t,n)}}class Qe extends Dt{constructor(e,t,n){super(new Float32Array(e),t,n)}}let uc=0;const It=new ke,Yr=new pt,Zn=new C,_t=new Vi,gi=new Vi,it=new C;class Ut extends Pn{constructor(){super(),this.isBufferGeometry=!0,Object.defineProperty(this,"id",{value:uc++}),this.uuid=zi(),this.name="",this.type="BufferGeometry",this.index=null,this.attributes={},this.morphAttributes={},this.morphTargetsRelative=!1,this.groups=[],this.boundingBox=null,this.boundingSphere=null,this.drawRange={start:0,count:1/0},this.userData={}}getIndex(){return this.index}setIndex(e){return Array.isArray(e)?this.index=new(So(e)?Ts:Ro)(e,1):this.index=e,this}getAttribute(e){return this.attributes[e]}setAttribute(e,t){return this.attributes[e]=t,this}deleteAttribute(e){return delete this.attributes[e],this}hasAttribute(e){return this.attributes[e]!==void 0}addGroup(e,t,n=0){this.groups.push({start:e,count:t,materialIndex:n})}clearGroups(){this.groups=[]}setDrawRange(e,t){this.drawRange.start=e,this.drawRange.count=t}applyMatrix4(e){const t=this.attributes.position;t!==void 0&&(t.applyMatrix4(e),t.needsUpdate=!0);const n=this.attributes.normal;if(n!==void 0){const s=new St().getNormalMatrix(e);n.applyNormalMatrix(s),n.needsUpdate=!0}const r=this.attributes.tangent;return r!==void 0&&(r.transformDirection(e),r.needsUpdate=!0),this.boundingBox!==null&&this.computeBoundingBox(),this.boundingSphere!==null&&this.computeBoundingSphere(),this}applyQuaternion(e){return It.makeRotationFromQuaternion(e),this.applyMatrix4(It),this}rotateX(e){return It.makeRotationX(e),this.applyMatrix4(It),this}rotateY(e){return It.makeRotationY(e),this.applyMatrix4(It),this}rotateZ(e){return It.makeRotationZ(e),this.applyMatrix4(It),this}translate(e,t,n){return It.makeTranslation(e,t,n),this.applyMatrix4(It),this}scale(e,t,n){return It.makeScale(e,t,n),this.applyMatrix4(It),this}lookAt(e){return Yr.lookAt(e),Yr.updateMatrix(),this.applyMatrix4(Yr.matrix),this}center(){return this.computeBoundingBox(),this.boundingBox.getCenter(Zn).negate(),this.translate(Zn.x,Zn.y,Zn.z),this}setFromPoints(e){const t=[];for(let n=0,r=e.length;n<r;n++){const s=e[n];t.push(s.x,s.y,s.z||0)}return this.setAttribute("position",new Qe(t,3)),this}computeBoundingBox(){this.boundingBox===null&&(this.boundingBox=new Vi);const e=this.attributes.position,t=this.morphAttributes.position;if(e&&e.isGLBufferAttribute){console.error('THREE.BufferGeometry.computeBoundingBox(): GLBufferAttribute requires a manual bounding box. Alternatively set "mesh.frustumCulled" to "false".',this),this.boundingBox.set(new C(-1/0,-1/0,-1/0),new C(1/0,1/0,1/0));return}if(e!==void 0){if(this.boundingBox.setFromBufferAttribute(e),t)for(let n=0,r=t.length;n<r;n++){const s=t[n];_t.setFromBufferAttribute(s),this.morphTargetsRelative?(it.addVectors(this.boundingBox.min,_t.min),this.boundingBox.expandByPoint(it),it.addVectors(this.boundingBox.max,_t.max),this.boundingBox.expandByPoint(it)):(this.boundingBox.expandByPoint(_t.min),this.boundingBox.expandByPoint(_t.max))}}else this.boundingBox.makeEmpty();(isNaN(this.boundingBox.min.x)||isNaN(this.boundingBox.min.y)||isNaN(this.boundingBox.min.z))&&console.error('THREE.BufferGeometry.computeBoundingBox(): Computed min/max have NaN values. The "position" attribute is likely to have NaN values.',this)}computeBoundingSphere(){this.boundingSphere===null&&(this.boundingSphere=new Di);const e=this.attributes.position,t=this.morphAttributes.position;if(e&&e.isGLBufferAttribute){console.error('THREE.BufferGeometry.computeBoundingSphere(): GLBufferAttribute requires a manual bounding sphere. Alternatively set "mesh.frustumCulled" to "false".',this),this.boundingSphere.set(new C,1/0);return}if(e){const n=this.boundingSphere.center;if(_t.setFromBufferAttribute(e),t)for(let s=0,o=t.length;s<o;s++){const a=t[s];gi.setFromBufferAttribute(a),this.morphTargetsRelative?(it.addVectors(_t.min,gi.min),_t.expandByPoint(it),it.addVectors(_t.max,gi.max),_t.expandByPoint(it)):(_t.expandByPoint(gi.min),_t.expandByPoint(gi.max))}_t.getCenter(n);let r=0;for(let s=0,o=e.count;s<o;s++)it.fromBufferAttribute(e,s),r=Math.max(r,n.distanceToSquared(it));if(t)for(let s=0,o=t.length;s<o;s++){const a=t[s],l=this.morphTargetsRelative;for(let c=0,u=a.count;c<u;c++)it.fromBufferAttribute(a,c),l&&(Zn.fromBufferAttribute(e,c),it.add(Zn)),r=Math.max(r,n.distanceToSquared(it))}this.boundingSphere.radius=Math.sqrt(r),isNaN(this.boundingSphere.radius)&&console.error('THREE.BufferGeometry.computeBoundingSphere(): Computed radius is NaN. The "position" attribute is likely to have NaN values.',this)}}computeTangents(){const e=this.index,t=this.attributes;if(e===null||t.position===void 0||t.normal===void 0||t.uv===void 0){console.error("THREE.BufferGeometry: .computeTangents() failed. Missing required attributes (index, position, normal or uv)");return}const n=e.array,r=t.position.array,s=t.normal.array,o=t.uv.array,a=r.length/3;this.hasAttribute("tangent")===!1&&this.setAttribute("tangent",new Dt(new Float32Array(4*a),4));const l=this.getAttribute("tangent").array,c=[],u=[];for(let D=0;D<a;D++)c[D]=new C,u[D]=new C;const d=new C,f=new C,m=new C,g=new Se,p=new Se,h=new Se,x=new C,E=new C;function b(D,X,oe){d.fromArray(r,D*3),f.fromArray(r,X*3),m.fromArray(r,oe*3),g.fromArray(o,D*2),p.fromArray(o,X*2),h.fromArray(o,oe*2),f.sub(d),m.sub(d),p.sub(g),h.sub(g);const N=1/(p.x*h.y-h.x*p.y);!isFinite(N)||(x.copy(f).multiplyScalar(h.y).addScaledVector(m,-p.y).multiplyScalar(N),E.copy(m).multiplyScalar(p.x).addScaledVector(f,-h.x).multiplyScalar(N),c[D].add(x),c[X].add(x),c[oe].add(x),u[D].add(E),u[X].add(E),u[oe].add(E))}let S=this.groups;S.length===0&&(S=[{start:0,count:n.length}]);for(let D=0,X=S.length;D<X;++D){const oe=S[D],N=oe.start,z=oe.count;for(let W=N,K=N+z;W<K;W+=3)b(n[W+0],n[W+1],n[W+2])}const y=new C,R=new C,U=new C,v=new C;function T(D){U.fromArray(s,D*3),v.copy(U);const X=c[D];y.copy(X),y.sub(U.multiplyScalar(U.dot(X))).normalize(),R.crossVectors(v,X);const N=R.dot(u[D])<0?-1:1;l[D*4]=y.x,l[D*4+1]=y.y,l[D*4+2]=y.z,l[D*4+3]=N}for(let D=0,X=S.length;D<X;++D){const oe=S[D],N=oe.start,z=oe.count;for(let W=N,K=N+z;W<K;W+=3)T(n[W+0]),T(n[W+1]),T(n[W+2])}}computeVertexNormals(){const e=this.index,t=this.getAttribute("position");if(t!==void 0){let n=this.getAttribute("normal");if(n===void 0)n=new Dt(new Float32Array(t.count*3),3),this.setAttribute("normal",n);else for(let f=0,m=n.count;f<m;f++)n.setXYZ(f,0,0,0);const r=new C,s=new C,o=new C,a=new C,l=new C,c=new C,u=new C,d=new C;if(e)for(let f=0,m=e.count;f<m;f+=3){const g=e.getX(f+0),p=e.getX(f+1),h=e.getX(f+2);r.fromBufferAttribute(t,g),s.fromBufferAttribute(t,p),o.fromBufferAttribute(t,h),u.subVectors(o,s),d.subVectors(r,s),u.cross(d),a.fromBufferAttribute(n,g),l.fromBufferAttribute(n,p),c.fromBufferAttribute(n,h),a.add(u),l.add(u),c.add(u),n.setXYZ(g,a.x,a.y,a.z),n.setXYZ(p,l.x,l.y,l.z),n.setXYZ(h,c.x,c.y,c.z)}else for(let f=0,m=t.count;f<m;f+=3)r.fromBufferAttribute(t,f+0),s.fromBufferAttribute(t,f+1),o.fromBufferAttribute(t,f+2),u.subVectors(o,s),d.subVectors(r,s),u.cross(d),n.setXYZ(f+0,u.x,u.y,u.z),n.setXYZ(f+1,u.x,u.y,u.z),n.setXYZ(f+2,u.x,u.y,u.z);this.normalizeNormals(),n.needsUpdate=!0}}merge(){return console.error("THREE.BufferGeometry.merge() has been removed. Use THREE.BufferGeometryUtils.mergeBufferGeometries() instead."),this}normalizeNormals(){const e=this.attributes.normal;for(let t=0,n=e.count;t<n;t++)it.fromBufferAttribute(e,t),it.normalize(),e.setXYZ(t,it.x,it.y,it.z)}toNonIndexed(){function e(a,l){const c=a.array,u=a.itemSize,d=a.normalized,f=new c.constructor(l.length*u);let m=0,g=0;for(let p=0,h=l.length;p<h;p++){a.isInterleavedBufferAttribute?m=l[p]*a.data.stride+a.offset:m=l[p]*u;for(let x=0;x<u;x++)f[g++]=c[m++]}return new Dt(f,u,d)}if(this.index===null)return console.warn("THREE.BufferGeometry.toNonIndexed(): BufferGeometry is already non-indexed."),this;const t=new Ut,n=this.index.array,r=this.attributes;for(const a in r){const l=r[a],c=e(l,n);t.setAttribute(a,c)}const s=this.morphAttributes;for(const a in s){const l=[],c=s[a];for(let u=0,d=c.length;u<d;u++){const f=c[u],m=e(f,n);l.push(m)}t.morphAttributes[a]=l}t.morphTargetsRelative=this.morphTargetsRelative;const o=this.groups;for(let a=0,l=o.length;a<l;a++){const c=o[a];t.addGroup(c.start,c.count,c.materialIndex)}return t}toJSON(){const e={metadata:{version:4.5,type:"BufferGeometry",generator:"BufferGeometry.toJSON"}};if(e.uuid=this.uuid,e.type=this.type,this.name!==""&&(e.name=this.name),Object.keys(this.userData).length>0&&(e.userData=this.userData),this.parameters!==void 0){const l=this.parameters;for(const c in l)l[c]!==void 0&&(e[c]=l[c]);return e}e.data={attributes:{}};const t=this.index;t!==null&&(e.data.index={type:t.array.constructor.name,array:Array.prototype.slice.call(t.array)});const n=this.attributes;for(const l in n){const c=n[l];e.data.attributes[l]=c.toJSON(e.data)}const r={};let s=!1;for(const l in this.morphAttributes){const c=this.morphAttributes[l],u=[];for(let d=0,f=c.length;d<f;d++){const m=c[d];u.push(m.toJSON(e.data))}u.length>0&&(r[l]=u,s=!0)}s&&(e.data.morphAttributes=r,e.data.morphTargetsRelative=this.morphTargetsRelative);const o=this.groups;o.length>0&&(e.data.groups=JSON.parse(JSON.stringify(o)));const a=this.boundingSphere;return a!==null&&(e.data.boundingSphere={center:a.center.toArray(),radius:a.radius}),e}clone(){return new this.constructor().copy(this)}copy(e){this.index=null,this.attributes={},this.morphAttributes={},this.groups=[],this.boundingBox=null,this.boundingSphere=null;const t={};this.name=e.name;const n=e.index;n!==null&&this.setIndex(n.clone(t));const r=e.attributes;for(const c in r){const u=r[c];this.setAttribute(c,u.clone(t))}const s=e.morphAttributes;for(const c in s){const u=[],d=s[c];for(let f=0,m=d.length;f<m;f++)u.push(d[f].clone(t));this.morphAttributes[c]=u}this.morphTargetsRelative=e.morphTargetsRelative;const o=e.groups;for(let c=0,u=o.length;c<u;c++){const d=o[c];this.addGroup(d.start,d.count,d.materialIndex)}const a=e.boundingBox;a!==null&&(this.boundingBox=a.clone());const l=e.boundingSphere;return l!==null&&(this.boundingSphere=l.clone()),this.drawRange.start=e.drawRange.start,this.drawRange.count=e.drawRange.count,this.userData=e.userData,e.parameters!==void 0&&(this.parameters=Object.assign({},e.parameters)),this}dispose(){this.dispatchEvent({type:"dispose"})}}const Sa=new ke,Gn=new xr,Kr=new Di,on=new C,ln=new C,cn=new C,Qr=new C,Jr=new C,$r=new C,Xi=new C,ji=new C,Yi=new C,Ki=new Se,Qi=new Se,Ji=new Se,es=new C,$i=new C;class zt extends pt{constructor(e=new Ut,t=new Es){super(),this.isMesh=!0,this.type="Mesh",this.geometry=e,this.material=t,this.updateMorphTargets()}copy(e,t){return super.copy(e,t),e.morphTargetInfluences!==void 0&&(this.morphTargetInfluences=e.morphTargetInfluences.slice()),e.morphTargetDictionary!==void 0&&(this.morphTargetDictionary=Object.assign({},e.morphTargetDictionary)),this.material=e.material,this.geometry=e.geometry,this}updateMorphTargets(){const t=this.geometry.morphAttributes,n=Object.keys(t);if(n.length>0){const r=t[n[0]];if(r!==void 0){this.morphTargetInfluences=[],this.morphTargetDictionary={};for(let s=0,o=r.length;s<o;s++){const a=r[s].name||String(s);this.morphTargetInfluences.push(0),this.morphTargetDictionary[a]=s}}}}raycast(e,t){const n=this.geometry,r=this.material,s=this.matrixWorld;if(r===void 0||(n.boundingSphere===null&&n.computeBoundingSphere(),Kr.copy(n.boundingSphere),Kr.applyMatrix4(s),e.ray.intersectsSphere(Kr)===!1)||(Sa.copy(s).invert(),Gn.copy(e.ray).applyMatrix4(Sa),n.boundingBox!==null&&Gn.intersectsBox(n.boundingBox)===!1))return;let o;const a=n.index,l=n.attributes.position,c=n.morphAttributes.position,u=n.morphTargetsRelative,d=n.attributes.uv,f=n.attributes.uv2,m=n.groups,g=n.drawRange;if(a!==null)if(Array.isArray(r))for(let p=0,h=m.length;p<h;p++){const x=m[p],E=r[x.materialIndex],b=Math.max(x.start,g.start),S=Math.min(a.count,Math.min(x.start+x.count,g.start+g.count));for(let y=b,R=S;y<R;y+=3){const U=a.getX(y),v=a.getX(y+1),T=a.getX(y+2);o=er(this,E,e,Gn,l,c,u,d,f,U,v,T),o&&(o.faceIndex=Math.floor(y/3),o.face.materialIndex=x.materialIndex,t.push(o))}}else{const p=Math.max(0,g.start),h=Math.min(a.count,g.start+g.count);for(let x=p,E=h;x<E;x+=3){const b=a.getX(x),S=a.getX(x+1),y=a.getX(x+2);o=er(this,r,e,Gn,l,c,u,d,f,b,S,y),o&&(o.faceIndex=Math.floor(x/3),t.push(o))}}else if(l!==void 0)if(Array.isArray(r))for(let p=0,h=m.length;p<h;p++){const x=m[p],E=r[x.materialIndex],b=Math.max(x.start,g.start),S=Math.min(l.count,Math.min(x.start+x.count,g.start+g.count));for(let y=b,R=S;y<R;y+=3){const U=y,v=y+1,T=y+2;o=er(this,E,e,Gn,l,c,u,d,f,U,v,T),o&&(o.faceIndex=Math.floor(y/3),o.face.materialIndex=x.materialIndex,t.push(o))}}else{const p=Math.max(0,g.start),h=Math.min(l.count,g.start+g.count);for(let x=p,E=h;x<E;x+=3){const b=x,S=x+1,y=x+2;o=er(this,r,e,Gn,l,c,u,d,f,b,S,y),o&&(o.faceIndex=Math.floor(x/3),t.push(o))}}}}function dc(i,e,t,n,r,s,o,a){let l;if(e.side===Vt?l=n.intersectTriangle(o,s,r,!0,a):l=n.intersectTriangle(r,s,o,e.side!==dn,a),l===null)return null;$i.copy(a),$i.applyMatrix4(i.matrixWorld);const c=t.ray.origin.distanceTo($i);return c<t.near||c>t.far?null:{distance:c,point:$i.clone(),object:i}}function er(i,e,t,n,r,s,o,a,l,c,u,d){on.fromBufferAttribute(r,c),ln.fromBufferAttribute(r,u),cn.fromBufferAttribute(r,d);const f=i.morphTargetInfluences;if(s&&f){Xi.set(0,0,0),ji.set(0,0,0),Yi.set(0,0,0);for(let g=0,p=s.length;g<p;g++){const h=f[g],x=s[g];h!==0&&(Qr.fromBufferAttribute(x,c),Jr.fromBufferAttribute(x,u),$r.fromBufferAttribute(x,d),o?(Xi.addScaledVector(Qr,h),ji.addScaledVector(Jr,h),Yi.addScaledVector($r,h)):(Xi.addScaledVector(Qr.sub(on),h),ji.addScaledVector(Jr.sub(ln),h),Yi.addScaledVector($r.sub(cn),h)))}on.add(Xi),ln.add(ji),cn.add(Yi)}i.isSkinnedMesh&&(i.boneTransform(c,on),i.boneTransform(u,ln),i.boneTransform(d,cn));const m=dc(i,e,t,n,on,ln,cn,es);if(m){a&&(Ki.fromBufferAttribute(a,c),Qi.fromBufferAttribute(a,u),Ji.fromBufferAttribute(a,d),m.uv=en.getUV(es,on,ln,cn,Ki,Qi,Ji,new Se)),l&&(Ki.fromBufferAttribute(l,c),Qi.fromBufferAttribute(l,u),Ji.fromBufferAttribute(l,d),m.uv2=en.getUV(es,on,ln,cn,Ki,Qi,Ji,new Se));const g={a:c,b:u,c:d,normal:new C,materialIndex:0};en.getNormal(on,ln,cn,g.normal),m.face=g}return m}class ui extends Ut{constructor(e=1,t=1,n=1,r=1,s=1,o=1){super(),this.type="BoxGeometry",this.parameters={width:e,height:t,depth:n,widthSegments:r,heightSegments:s,depthSegments:o};const a=this;r=Math.floor(r),s=Math.floor(s),o=Math.floor(o);const l=[],c=[],u=[],d=[];let f=0,m=0;g("z","y","x",-1,-1,n,t,e,o,s,0),g("z","y","x",1,-1,n,t,-e,o,s,1),g("x","z","y",1,1,e,n,t,r,o,2),g("x","z","y",1,-1,e,n,-t,r,o,3),g("x","y","z",1,-1,e,t,n,r,s,4),g("x","y","z",-1,-1,e,t,-n,r,s,5),this.setIndex(l),this.setAttribute("position",new Qe(c,3)),this.setAttribute("normal",new Qe(u,3)),this.setAttribute("uv",new Qe(d,2));function g(p,h,x,E,b,S,y,R,U,v,T){const D=S/U,X=y/v,oe=S/2,N=y/2,z=R/2,W=U+1,K=v+1;let j=0,G=0;const te=new C;for(let J=0;J<K;J++){const F=J*X-N;for(let k=0;k<W;k++){const Q=k*D-oe;te[p]=Q*E,te[h]=F*b,te[x]=z,c.push(te.x,te.y,te.z),te[p]=0,te[h]=0,te[x]=R>0?1:-1,u.push(te.x,te.y,te.z),d.push(k/U),d.push(1-J/v),j+=1}}for(let J=0;J<v;J++)for(let F=0;F<U;F++){const k=f+F+W*J,Q=f+F+W*(J+1),ee=f+(F+1)+W*(J+1),re=f+(F+1)+W*J;l.push(k,Q,re),l.push(Q,ee,re),G+=6}a.addGroup(m,G,T),m+=G,f+=j}}static fromJSON(e){return new ui(e.width,e.height,e.depth,e.widthSegments,e.heightSegments,e.depthSegments)}}function si(i){const e={};for(const t in i){e[t]={};for(const n in i[t]){const r=i[t][n];r&&(r.isColor||r.isMatrix3||r.isMatrix4||r.isVector2||r.isVector3||r.isVector4||r.isTexture||r.isQuaternion)?e[t][n]=r.clone():Array.isArray(r)?e[t][n]=r.slice():e[t][n]=r}}return e}function ht(i){const e={};for(let t=0;t<i.length;t++){const n=si(i[t]);for(const r in n)e[r]=n[r]}return e}function hc(i){const e=[];for(let t=0;t<i.length;t++)e.push(i[t].clone());return e}function Co(i){return i.getRenderTarget()===null&&i.outputEncoding===Be?Zt:Ci}const fc={clone:si,merge:ht};var pc=`void main() {
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}`,mc=`void main() {
	gl_FragColor = vec4( 1.0, 0.0, 0.0, 1.0 );
}`;class Ht extends ci{constructor(e){super(),this.isShaderMaterial=!0,this.type="ShaderMaterial",this.defines={},this.uniforms={},this.uniformsGroups=[],this.vertexShader=pc,this.fragmentShader=mc,this.linewidth=1,this.wireframe=!1,this.wireframeLinewidth=1,this.fog=!1,this.lights=!1,this.clipping=!1,this.extensions={derivatives:!1,fragDepth:!1,drawBuffers:!1,shaderTextureLOD:!1},this.defaultAttributeValues={color:[1,1,1],uv:[0,0],uv2:[0,0]},this.index0AttributeName=void 0,this.uniformsNeedUpdate=!1,this.glslVersion=null,e!==void 0&&this.setValues(e)}copy(e){return super.copy(e),this.fragmentShader=e.fragmentShader,this.vertexShader=e.vertexShader,this.uniforms=si(e.uniforms),this.uniformsGroups=hc(e.uniformsGroups),this.defines=Object.assign({},e.defines),this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this.fog=e.fog,this.lights=e.lights,this.clipping=e.clipping,this.extensions=Object.assign({},e.extensions),this.glslVersion=e.glslVersion,this}toJSON(e){const t=super.toJSON(e);t.glslVersion=this.glslVersion,t.uniforms={};for(const r in this.uniforms){const o=this.uniforms[r].value;o&&o.isTexture?t.uniforms[r]={type:"t",value:o.toJSON(e).uuid}:o&&o.isColor?t.uniforms[r]={type:"c",value:o.getHex()}:o&&o.isVector2?t.uniforms[r]={type:"v2",value:o.toArray()}:o&&o.isVector3?t.uniforms[r]={type:"v3",value:o.toArray()}:o&&o.isVector4?t.uniforms[r]={type:"v4",value:o.toArray()}:o&&o.isMatrix3?t.uniforms[r]={type:"m3",value:o.toArray()}:o&&o.isMatrix4?t.uniforms[r]={type:"m4",value:o.toArray()}:t.uniforms[r]={value:o}}Object.keys(this.defines).length>0&&(t.defines=this.defines),t.vertexShader=this.vertexShader,t.fragmentShader=this.fragmentShader;const n={};for(const r in this.extensions)this.extensions[r]===!0&&(n[r]=!0);return Object.keys(n).length>0&&(t.extensions=n),t}}class Lo extends pt{constructor(){super(),this.isCamera=!0,this.type="Camera",this.matrixWorldInverse=new ke,this.projectionMatrix=new ke,this.projectionMatrixInverse=new ke}copy(e,t){return super.copy(e,t),this.matrixWorldInverse.copy(e.matrixWorldInverse),this.projectionMatrix.copy(e.projectionMatrix),this.projectionMatrixInverse.copy(e.projectionMatrixInverse),this}getWorldDirection(e){this.updateWorldMatrix(!0,!1);const t=this.matrixWorld.elements;return e.set(-t[8],-t[9],-t[10]).normalize()}updateMatrixWorld(e){super.updateMatrixWorld(e),this.matrixWorldInverse.copy(this.matrixWorld).invert()}updateWorldMatrix(e,t){super.updateWorldMatrix(e,t),this.matrixWorldInverse.copy(this.matrixWorld).invert()}clone(){return new this.constructor().copy(this)}}class Lt extends Lo{constructor(e=50,t=1,n=.1,r=2e3){super(),this.isPerspectiveCamera=!0,this.type="PerspectiveCamera",this.fov=e,this.zoom=1,this.near=n,this.far=r,this.focus=10,this.aspect=t,this.view=null,this.filmGauge=35,this.filmOffset=0,this.updateProjectionMatrix()}copy(e,t){return super.copy(e,t),this.fov=e.fov,this.zoom=e.zoom,this.near=e.near,this.far=e.far,this.focus=e.focus,this.aspect=e.aspect,this.view=e.view===null?null:Object.assign({},e.view),this.filmGauge=e.filmGauge,this.filmOffset=e.filmOffset,this}setFocalLength(e){const t=.5*this.getFilmHeight()/e;this.fov=fa*2*Math.atan(t),this.updateProjectionMatrix()}getFocalLength(){const e=Math.tan(Lr*.5*this.fov);return .5*this.getFilmHeight()/e}getEffectiveFOV(){return fa*2*Math.atan(Math.tan(Lr*.5*this.fov)/this.zoom)}getFilmWidth(){return this.filmGauge*Math.min(this.aspect,1)}getFilmHeight(){return this.filmGauge/Math.max(this.aspect,1)}setViewOffset(e,t,n,r,s,o){this.aspect=e/t,this.view===null&&(this.view={enabled:!0,fullWidth:1,fullHeight:1,offsetX:0,offsetY:0,width:1,height:1}),this.view.enabled=!0,this.view.fullWidth=e,this.view.fullHeight=t,this.view.offsetX=n,this.view.offsetY=r,this.view.width=s,this.view.height=o,this.updateProjectionMatrix()}clearViewOffset(){this.view!==null&&(this.view.enabled=!1),this.updateProjectionMatrix()}updateProjectionMatrix(){const e=this.near;let t=e*Math.tan(Lr*.5*this.fov)/this.zoom,n=2*t,r=this.aspect*n,s=-.5*r;const o=this.view;if(this.view!==null&&this.view.enabled){const l=o.fullWidth,c=o.fullHeight;s+=o.offsetX*r/l,t-=o.offsetY*n/c,r*=o.width/l,n*=o.height/c}const a=this.filmOffset;a!==0&&(s+=e*a/this.getFilmWidth()),this.projectionMatrix.makePerspective(s,s+r,t,t-n,e,this.far),this.projectionMatrixInverse.copy(this.projectionMatrix).invert()}toJSON(e){const t=super.toJSON(e);return t.object.fov=this.fov,t.object.zoom=this.zoom,t.object.near=this.near,t.object.far=this.far,t.object.focus=this.focus,t.object.aspect=this.aspect,this.view!==null&&(t.object.view=Object.assign({},this.view)),t.object.filmGauge=this.filmGauge,t.object.filmOffset=this.filmOffset,t}}const Wn=-90,Xn=1;class gc extends pt{constructor(e,t,n){super(),this.type="CubeCamera",this.renderTarget=n;const r=new Lt(Wn,Xn,e,t);r.layers=this.layers,r.up.set(0,1,0),r.lookAt(1,0,0),this.add(r);const s=new Lt(Wn,Xn,e,t);s.layers=this.layers,s.up.set(0,1,0),s.lookAt(-1,0,0),this.add(s);const o=new Lt(Wn,Xn,e,t);o.layers=this.layers,o.up.set(0,0,-1),o.lookAt(0,1,0),this.add(o);const a=new Lt(Wn,Xn,e,t);a.layers=this.layers,a.up.set(0,0,1),a.lookAt(0,-1,0),this.add(a);const l=new Lt(Wn,Xn,e,t);l.layers=this.layers,l.up.set(0,1,0),l.lookAt(0,0,1),this.add(l);const c=new Lt(Wn,Xn,e,t);c.layers=this.layers,c.up.set(0,1,0),c.lookAt(0,0,-1),this.add(c)}update(e,t){this.parent===null&&this.updateMatrixWorld();const n=this.renderTarget,[r,s,o,a,l,c]=this.children,u=e.getRenderTarget(),d=e.toneMapping,f=e.xr.enabled;e.toneMapping=tn,e.xr.enabled=!1;const m=n.texture.generateMipmaps;n.texture.generateMipmaps=!1,e.setRenderTarget(n,0),e.render(t,r),e.setRenderTarget(n,1),e.render(t,s),e.setRenderTarget(n,2),e.render(t,o),e.setRenderTarget(n,3),e.render(t,a),e.setRenderTarget(n,4),e.render(t,l),n.texture.generateMipmaps=m,e.setRenderTarget(n,5),e.render(t,c),e.setRenderTarget(u),e.toneMapping=d,e.xr.enabled=f,n.texture.needsPMREMUpdate=!0}}class Po extends ft{constructor(e,t,n,r,s,o,a,l,c,u){e=e!==void 0?e:[],t=t!==void 0?t:ni,super(e,t,n,r,s,o,a,l,c,u),this.isCubeTexture=!0,this.flipY=!1}get images(){return this.image}set images(e){this.image=e}}class vc extends Cn{constructor(e=1,t={}){super(e,e,t),this.isWebGLCubeRenderTarget=!0;const n={width:e,height:e,depth:1},r=[n,n,n,n,n,n];this.texture=new Po(r,t.mapping,t.wrapS,t.wrapT,t.magFilter,t.minFilter,t.format,t.type,t.anisotropy,t.encoding),this.texture.isRenderTargetTexture=!0,this.texture.generateMipmaps=t.generateMipmaps!==void 0?t.generateMipmaps:!1,this.texture.minFilter=t.minFilter!==void 0?t.minFilter:Ct}fromEquirectangularTexture(e,t){this.texture.type=t.type,this.texture.encoding=t.encoding,this.texture.generateMipmaps=t.generateMipmaps,this.texture.minFilter=t.minFilter,this.texture.magFilter=t.magFilter;const n={uniforms:{tEquirect:{value:null}},vertexShader:`

				varying vec3 vWorldDirection;

				vec3 transformDirection( in vec3 dir, in mat4 matrix ) {

					return normalize( ( matrix * vec4( dir, 0.0 ) ).xyz );

				}

				void main() {

					vWorldDirection = transformDirection( position, modelMatrix );

					#include <begin_vertex>
					#include <project_vertex>

				}
			`,fragmentShader:`

				uniform sampler2D tEquirect;

				varying vec3 vWorldDirection;

				#include <common>

				void main() {

					vec3 direction = normalize( vWorldDirection );

					vec2 sampleUV = equirectUv( direction );

					gl_FragColor = texture2D( tEquirect, sampleUV );

				}
			`},r=new ui(5,5,5),s=new Ht({name:"CubemapFromEquirect",uniforms:si(n.uniforms),vertexShader:n.vertexShader,fragmentShader:n.fragmentShader,side:Vt,blending:hn});s.uniforms.tEquirect.value=t;const o=new zt(r,s),a=t.minFilter;return t.minFilter===vr&&(t.minFilter=Ct),new gc(1,10,this).update(e,o),t.minFilter=a,o.geometry.dispose(),o.material.dispose(),this}clear(e,t,n,r){const s=e.getRenderTarget();for(let o=0;o<6;o++)e.setRenderTarget(this,o),e.clear(t,n,r);e.setRenderTarget(s)}}const ts=new C,xc=new C,Mc=new St;class An{constructor(e=new C(1,0,0),t=0){this.isPlane=!0,this.normal=e,this.constant=t}set(e,t){return this.normal.copy(e),this.constant=t,this}setComponents(e,t,n,r){return this.normal.set(e,t,n),this.constant=r,this}setFromNormalAndCoplanarPoint(e,t){return this.normal.copy(e),this.constant=-t.dot(this.normal),this}setFromCoplanarPoints(e,t,n){const r=ts.subVectors(n,t).cross(xc.subVectors(e,t)).normalize();return this.setFromNormalAndCoplanarPoint(r,e),this}copy(e){return this.normal.copy(e.normal),this.constant=e.constant,this}normalize(){const e=1/this.normal.length();return this.normal.multiplyScalar(e),this.constant*=e,this}negate(){return this.constant*=-1,this.normal.negate(),this}distanceToPoint(e){return this.normal.dot(e)+this.constant}distanceToSphere(e){return this.distanceToPoint(e.center)-e.radius}projectPoint(e,t){return t.copy(this.normal).multiplyScalar(-this.distanceToPoint(e)).add(e)}intersectLine(e,t){const n=e.delta(ts),r=this.normal.dot(n);if(r===0)return this.distanceToPoint(e.start)===0?t.copy(e.start):null;const s=-(e.start.dot(this.normal)+this.constant)/r;return s<0||s>1?null:t.copy(n).multiplyScalar(s).add(e.start)}intersectsLine(e){const t=this.distanceToPoint(e.start),n=this.distanceToPoint(e.end);return t<0&&n>0||n<0&&t>0}intersectsBox(e){return e.intersectsPlane(this)}intersectsSphere(e){return e.intersectsPlane(this)}coplanarPoint(e){return e.copy(this.normal).multiplyScalar(-this.constant)}applyMatrix4(e,t){const n=t||Mc.getNormalMatrix(e),r=this.coplanarPoint(ts).applyMatrix4(e),s=this.normal.applyMatrix3(n).normalize();return this.constant=-r.dot(s),this}translate(e){return this.constant-=e.dot(this.normal),this}equals(e){return e.normal.equals(this.normal)&&e.constant===this.constant}clone(){return new this.constructor().copy(this)}}const jn=new Di,tr=new C;class zo{constructor(e=new An,t=new An,n=new An,r=new An,s=new An,o=new An){this.planes=[e,t,n,r,s,o]}set(e,t,n,r,s,o){const a=this.planes;return a[0].copy(e),a[1].copy(t),a[2].copy(n),a[3].copy(r),a[4].copy(s),a[5].copy(o),this}copy(e){const t=this.planes;for(let n=0;n<6;n++)t[n].copy(e.planes[n]);return this}setFromProjectionMatrix(e){const t=this.planes,n=e.elements,r=n[0],s=n[1],o=n[2],a=n[3],l=n[4],c=n[5],u=n[6],d=n[7],f=n[8],m=n[9],g=n[10],p=n[11],h=n[12],x=n[13],E=n[14],b=n[15];return t[0].setComponents(a-r,d-l,p-f,b-h).normalize(),t[1].setComponents(a+r,d+l,p+f,b+h).normalize(),t[2].setComponents(a+s,d+c,p+m,b+x).normalize(),t[3].setComponents(a-s,d-c,p-m,b-x).normalize(),t[4].setComponents(a-o,d-u,p-g,b-E).normalize(),t[5].setComponents(a+o,d+u,p+g,b+E).normalize(),this}intersectsObject(e){const t=e.geometry;return t.boundingSphere===null&&t.computeBoundingSphere(),jn.copy(t.boundingSphere).applyMatrix4(e.matrixWorld),this.intersectsSphere(jn)}intersectsSprite(e){return jn.center.set(0,0,0),jn.radius=.7071067811865476,jn.applyMatrix4(e.matrixWorld),this.intersectsSphere(jn)}intersectsSphere(e){const t=this.planes,n=e.center,r=-e.radius;for(let s=0;s<6;s++)if(t[s].distanceToPoint(n)<r)return!1;return!0}intersectsBox(e){const t=this.planes;for(let n=0;n<6;n++){const r=t[n];if(tr.x=r.normal.x>0?e.max.x:e.min.x,tr.y=r.normal.y>0?e.max.y:e.min.y,tr.z=r.normal.z>0?e.max.z:e.min.z,r.distanceToPoint(tr)<0)return!1}return!0}containsPoint(e){const t=this.planes;for(let n=0;n<6;n++)if(t[n].distanceToPoint(e)<0)return!1;return!0}clone(){return new this.constructor().copy(this)}}function Vo(){let i=null,e=!1,t=null,n=null;function r(s,o){t(s,o),n=i.requestAnimationFrame(r)}return{start:function(){e!==!0&&t!==null&&(n=i.requestAnimationFrame(r),e=!0)},stop:function(){i.cancelAnimationFrame(n),e=!1},setAnimationLoop:function(s){t=s},setContext:function(s){i=s}}}function yc(i,e){const t=e.isWebGL2,n=new WeakMap;function r(c,u){const d=c.array,f=c.usage,m=i.createBuffer();i.bindBuffer(u,m),i.bufferData(u,d,f),c.onUploadCallback();let g;if(d instanceof Float32Array)g=5126;else if(d instanceof Uint16Array)if(c.isFloat16BufferAttribute)if(t)g=5131;else throw new Error("THREE.WebGLAttributes: Usage of Float16BufferAttribute requires WebGL2.");else g=5123;else if(d instanceof Int16Array)g=5122;else if(d instanceof Uint32Array)g=5125;else if(d instanceof Int32Array)g=5124;else if(d instanceof Int8Array)g=5120;else if(d instanceof Uint8Array)g=5121;else if(d instanceof Uint8ClampedArray)g=5121;else throw new Error("THREE.WebGLAttributes: Unsupported buffer data format: "+d);return{buffer:m,type:g,bytesPerElement:d.BYTES_PER_ELEMENT,version:c.version}}function s(c,u,d){const f=u.array,m=u.updateRange;i.bindBuffer(d,c),m.count===-1?i.bufferSubData(d,0,f):(t?i.bufferSubData(d,m.offset*f.BYTES_PER_ELEMENT,f,m.offset,m.count):i.bufferSubData(d,m.offset*f.BYTES_PER_ELEMENT,f.subarray(m.offset,m.offset+m.count)),m.count=-1),u.onUploadCallback()}function o(c){return c.isInterleavedBufferAttribute&&(c=c.data),n.get(c)}function a(c){c.isInterleavedBufferAttribute&&(c=c.data);const u=n.get(c);u&&(i.deleteBuffer(u.buffer),n.delete(c))}function l(c,u){if(c.isGLBufferAttribute){const f=n.get(c);(!f||f.version<c.version)&&n.set(c,{buffer:c.buffer,type:c.type,bytesPerElement:c.elementSize,version:c.version});return}c.isInterleavedBufferAttribute&&(c=c.data);const d=n.get(c);d===void 0?n.set(c,r(c,u)):d.version<c.version&&(s(d.buffer,c,u),d.version=c.version)}return{get:o,remove:a,update:l}}class Mr extends Ut{constructor(e=1,t=1,n=1,r=1){super(),this.type="PlaneGeometry",this.parameters={width:e,height:t,widthSegments:n,heightSegments:r};const s=e/2,o=t/2,a=Math.floor(n),l=Math.floor(r),c=a+1,u=l+1,d=e/a,f=t/l,m=[],g=[],p=[],h=[];for(let x=0;x<u;x++){const E=x*f-o;for(let b=0;b<c;b++){const S=b*d-s;g.push(S,-E,0),p.push(0,0,1),h.push(b/a),h.push(1-x/l)}}for(let x=0;x<l;x++)for(let E=0;E<a;E++){const b=E+c*x,S=E+c*(x+1),y=E+1+c*(x+1),R=E+1+c*x;m.push(b,S,R),m.push(S,y,R)}this.setIndex(m),this.setAttribute("position",new Qe(g,3)),this.setAttribute("normal",new Qe(p,3)),this.setAttribute("uv",new Qe(h,2))}static fromJSON(e){return new Mr(e.width,e.height,e.widthSegments,e.heightSegments)}}var Ac=`#ifdef USE_ALPHAMAP
	diffuseColor.a *= texture2D( alphaMap, vUv ).g;
#endif`,_c=`#ifdef USE_ALPHAMAP
	uniform sampler2D alphaMap;
#endif`,bc=`#ifdef USE_ALPHATEST
	if ( diffuseColor.a < alphaTest ) discard;
#endif`,Sc=`#ifdef USE_ALPHATEST
	uniform float alphaTest;
#endif`,wc=`#ifdef USE_AOMAP
	float ambientOcclusion = ( texture2D( aoMap, vUv2 ).r - 1.0 ) * aoMapIntensity + 1.0;
	reflectedLight.indirectDiffuse *= ambientOcclusion;
	#if defined( USE_ENVMAP ) && defined( STANDARD )
		float dotNV = saturate( dot( geometry.normal, geometry.viewDir ) );
		reflectedLight.indirectSpecular *= computeSpecularOcclusion( dotNV, ambientOcclusion, material.roughness );
	#endif
#endif`,Ec=`#ifdef USE_AOMAP
	uniform sampler2D aoMap;
	uniform float aoMapIntensity;
#endif`,Tc="vec3 transformed = vec3( position );",Ic=`vec3 objectNormal = vec3( normal );
#ifdef USE_TANGENT
	vec3 objectTangent = vec3( tangent.xyz );
#endif`,Rc=`vec3 BRDF_Lambert( const in vec3 diffuseColor ) {
	return RECIPROCAL_PI * diffuseColor;
}
vec3 F_Schlick( const in vec3 f0, const in float f90, const in float dotVH ) {
	float fresnel = exp2( ( - 5.55473 * dotVH - 6.98316 ) * dotVH );
	return f0 * ( 1.0 - fresnel ) + ( f90 * fresnel );
}
float F_Schlick( const in float f0, const in float f90, const in float dotVH ) {
	float fresnel = exp2( ( - 5.55473 * dotVH - 6.98316 ) * dotVH );
	return f0 * ( 1.0 - fresnel ) + ( f90 * fresnel );
}
vec3 Schlick_to_F0( const in vec3 f, const in float f90, const in float dotVH ) {
    float x = clamp( 1.0 - dotVH, 0.0, 1.0 );
    float x2 = x * x;
    float x5 = clamp( x * x2 * x2, 0.0, 0.9999 );
    return ( f - vec3( f90 ) * x5 ) / ( 1.0 - x5 );
}
float V_GGX_SmithCorrelated( const in float alpha, const in float dotNL, const in float dotNV ) {
	float a2 = pow2( alpha );
	float gv = dotNL * sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNV ) );
	float gl = dotNV * sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNL ) );
	return 0.5 / max( gv + gl, EPSILON );
}
float D_GGX( const in float alpha, const in float dotNH ) {
	float a2 = pow2( alpha );
	float denom = pow2( dotNH ) * ( a2 - 1.0 ) + 1.0;
	return RECIPROCAL_PI * a2 / pow2( denom );
}
vec3 BRDF_GGX( const in vec3 lightDir, const in vec3 viewDir, const in vec3 normal, const in vec3 f0, const in float f90, const in float roughness ) {
	float alpha = pow2( roughness );
	vec3 halfDir = normalize( lightDir + viewDir );
	float dotNL = saturate( dot( normal, lightDir ) );
	float dotNV = saturate( dot( normal, viewDir ) );
	float dotNH = saturate( dot( normal, halfDir ) );
	float dotVH = saturate( dot( viewDir, halfDir ) );
	vec3 F = F_Schlick( f0, f90, dotVH );
	float V = V_GGX_SmithCorrelated( alpha, dotNL, dotNV );
	float D = D_GGX( alpha, dotNH );
	return F * ( V * D );
}
#ifdef USE_IRIDESCENCE
	vec3 BRDF_GGX_Iridescence( const in vec3 lightDir, const in vec3 viewDir, const in vec3 normal, const in vec3 f0, const in float f90, const in float iridescence, const in vec3 iridescenceFresnel, const in float roughness ) {
		float alpha = pow2( roughness );
		vec3 halfDir = normalize( lightDir + viewDir );
		float dotNL = saturate( dot( normal, lightDir ) );
		float dotNV = saturate( dot( normal, viewDir ) );
		float dotNH = saturate( dot( normal, halfDir ) );
		float dotVH = saturate( dot( viewDir, halfDir ) );
		vec3 F = mix( F_Schlick( f0, f90, dotVH ), iridescenceFresnel, iridescence );
		float V = V_GGX_SmithCorrelated( alpha, dotNL, dotNV );
		float D = D_GGX( alpha, dotNH );
		return F * ( V * D );
	}
#endif
vec2 LTC_Uv( const in vec3 N, const in vec3 V, const in float roughness ) {
	const float LUT_SIZE = 64.0;
	const float LUT_SCALE = ( LUT_SIZE - 1.0 ) / LUT_SIZE;
	const float LUT_BIAS = 0.5 / LUT_SIZE;
	float dotNV = saturate( dot( N, V ) );
	vec2 uv = vec2( roughness, sqrt( 1.0 - dotNV ) );
	uv = uv * LUT_SCALE + LUT_BIAS;
	return uv;
}
float LTC_ClippedSphereFormFactor( const in vec3 f ) {
	float l = length( f );
	return max( ( l * l + f.z ) / ( l + 1.0 ), 0.0 );
}
vec3 LTC_EdgeVectorFormFactor( const in vec3 v1, const in vec3 v2 ) {
	float x = dot( v1, v2 );
	float y = abs( x );
	float a = 0.8543985 + ( 0.4965155 + 0.0145206 * y ) * y;
	float b = 3.4175940 + ( 4.1616724 + y ) * y;
	float v = a / b;
	float theta_sintheta = ( x > 0.0 ) ? v : 0.5 * inversesqrt( max( 1.0 - x * x, 1e-7 ) ) - v;
	return cross( v1, v2 ) * theta_sintheta;
}
vec3 LTC_Evaluate( const in vec3 N, const in vec3 V, const in vec3 P, const in mat3 mInv, const in vec3 rectCoords[ 4 ] ) {
	vec3 v1 = rectCoords[ 1 ] - rectCoords[ 0 ];
	vec3 v2 = rectCoords[ 3 ] - rectCoords[ 0 ];
	vec3 lightNormal = cross( v1, v2 );
	if( dot( lightNormal, P - rectCoords[ 0 ] ) < 0.0 ) return vec3( 0.0 );
	vec3 T1, T2;
	T1 = normalize( V - N * dot( V, N ) );
	T2 = - cross( N, T1 );
	mat3 mat = mInv * transposeMat3( mat3( T1, T2, N ) );
	vec3 coords[ 4 ];
	coords[ 0 ] = mat * ( rectCoords[ 0 ] - P );
	coords[ 1 ] = mat * ( rectCoords[ 1 ] - P );
	coords[ 2 ] = mat * ( rectCoords[ 2 ] - P );
	coords[ 3 ] = mat * ( rectCoords[ 3 ] - P );
	coords[ 0 ] = normalize( coords[ 0 ] );
	coords[ 1 ] = normalize( coords[ 1 ] );
	coords[ 2 ] = normalize( coords[ 2 ] );
	coords[ 3 ] = normalize( coords[ 3 ] );
	vec3 vectorFormFactor = vec3( 0.0 );
	vectorFormFactor += LTC_EdgeVectorFormFactor( coords[ 0 ], coords[ 1 ] );
	vectorFormFactor += LTC_EdgeVectorFormFactor( coords[ 1 ], coords[ 2 ] );
	vectorFormFactor += LTC_EdgeVectorFormFactor( coords[ 2 ], coords[ 3 ] );
	vectorFormFactor += LTC_EdgeVectorFormFactor( coords[ 3 ], coords[ 0 ] );
	float result = LTC_ClippedSphereFormFactor( vectorFormFactor );
	return vec3( result );
}
float G_BlinnPhong_Implicit( ) {
	return 0.25;
}
float D_BlinnPhong( const in float shininess, const in float dotNH ) {
	return RECIPROCAL_PI * ( shininess * 0.5 + 1.0 ) * pow( dotNH, shininess );
}
vec3 BRDF_BlinnPhong( const in vec3 lightDir, const in vec3 viewDir, const in vec3 normal, const in vec3 specularColor, const in float shininess ) {
	vec3 halfDir = normalize( lightDir + viewDir );
	float dotNH = saturate( dot( normal, halfDir ) );
	float dotVH = saturate( dot( viewDir, halfDir ) );
	vec3 F = F_Schlick( specularColor, 1.0, dotVH );
	float G = G_BlinnPhong_Implicit( );
	float D = D_BlinnPhong( shininess, dotNH );
	return F * ( G * D );
}
#if defined( USE_SHEEN )
float D_Charlie( float roughness, float dotNH ) {
	float alpha = pow2( roughness );
	float invAlpha = 1.0 / alpha;
	float cos2h = dotNH * dotNH;
	float sin2h = max( 1.0 - cos2h, 0.0078125 );
	return ( 2.0 + invAlpha ) * pow( sin2h, invAlpha * 0.5 ) / ( 2.0 * PI );
}
float V_Neubelt( float dotNV, float dotNL ) {
	return saturate( 1.0 / ( 4.0 * ( dotNL + dotNV - dotNL * dotNV ) ) );
}
vec3 BRDF_Sheen( const in vec3 lightDir, const in vec3 viewDir, const in vec3 normal, vec3 sheenColor, const in float sheenRoughness ) {
	vec3 halfDir = normalize( lightDir + viewDir );
	float dotNL = saturate( dot( normal, lightDir ) );
	float dotNV = saturate( dot( normal, viewDir ) );
	float dotNH = saturate( dot( normal, halfDir ) );
	float D = D_Charlie( sheenRoughness, dotNH );
	float V = V_Neubelt( dotNV, dotNL );
	return sheenColor * ( D * V );
}
#endif`,Cc=`#ifdef USE_IRIDESCENCE
	const mat3 XYZ_TO_REC709 = mat3(
		 3.2404542, -0.9692660,  0.0556434,
		-1.5371385,  1.8760108, -0.2040259,
		-0.4985314,  0.0415560,  1.0572252
	);
	vec3 Fresnel0ToIor( vec3 fresnel0 ) {
		vec3 sqrtF0 = sqrt( fresnel0 );
		return ( vec3( 1.0 ) + sqrtF0 ) / ( vec3( 1.0 ) - sqrtF0 );
	}
	vec3 IorToFresnel0( vec3 transmittedIor, float incidentIor ) {
		return pow2( ( transmittedIor - vec3( incidentIor ) ) / ( transmittedIor + vec3( incidentIor ) ) );
	}
	float IorToFresnel0( float transmittedIor, float incidentIor ) {
		return pow2( ( transmittedIor - incidentIor ) / ( transmittedIor + incidentIor ));
	}
	vec3 evalSensitivity( float OPD, vec3 shift ) {
		float phase = 2.0 * PI * OPD * 1.0e-9;
		vec3 val = vec3( 5.4856e-13, 4.4201e-13, 5.2481e-13 );
		vec3 pos = vec3( 1.6810e+06, 1.7953e+06, 2.2084e+06 );
		vec3 var = vec3( 4.3278e+09, 9.3046e+09, 6.6121e+09 );
		vec3 xyz = val * sqrt( 2.0 * PI * var ) * cos( pos * phase + shift ) * exp( - pow2( phase ) * var );
		xyz.x += 9.7470e-14 * sqrt( 2.0 * PI * 4.5282e+09 ) * cos( 2.2399e+06 * phase + shift[ 0 ] ) * exp( - 4.5282e+09 * pow2( phase ) );
		xyz /= 1.0685e-7;
		vec3 rgb = XYZ_TO_REC709 * xyz;
		return rgb;
	}
	vec3 evalIridescence( float outsideIOR, float eta2, float cosTheta1, float thinFilmThickness, vec3 baseF0 ) {
		vec3 I;
		float iridescenceIOR = mix( outsideIOR, eta2, smoothstep( 0.0, 0.03, thinFilmThickness ) );
		float sinTheta2Sq = pow2( outsideIOR / iridescenceIOR ) * ( 1.0 - pow2( cosTheta1 ) );
		float cosTheta2Sq = 1.0 - sinTheta2Sq;
		if ( cosTheta2Sq < 0.0 ) {
			 return vec3( 1.0 );
		}
		float cosTheta2 = sqrt( cosTheta2Sq );
		float R0 = IorToFresnel0( iridescenceIOR, outsideIOR );
		float R12 = F_Schlick( R0, 1.0, cosTheta1 );
		float R21 = R12;
		float T121 = 1.0 - R12;
		float phi12 = 0.0;
		if ( iridescenceIOR < outsideIOR ) phi12 = PI;
		float phi21 = PI - phi12;
		vec3 baseIOR = Fresnel0ToIor( clamp( baseF0, 0.0, 0.9999 ) );		vec3 R1 = IorToFresnel0( baseIOR, iridescenceIOR );
		vec3 R23 = F_Schlick( R1, 1.0, cosTheta2 );
		vec3 phi23 = vec3( 0.0 );
		if ( baseIOR[ 0 ] < iridescenceIOR ) phi23[ 0 ] = PI;
		if ( baseIOR[ 1 ] < iridescenceIOR ) phi23[ 1 ] = PI;
		if ( baseIOR[ 2 ] < iridescenceIOR ) phi23[ 2 ] = PI;
		float OPD = 2.0 * iridescenceIOR * thinFilmThickness * cosTheta2;
		vec3 phi = vec3( phi21 ) + phi23;
		vec3 R123 = clamp( R12 * R23, 1e-5, 0.9999 );
		vec3 r123 = sqrt( R123 );
		vec3 Rs = pow2( T121 ) * R23 / ( vec3( 1.0 ) - R123 );
		vec3 C0 = R12 + Rs;
		I = C0;
		vec3 Cm = Rs - T121;
		for ( int m = 1; m <= 2; ++ m ) {
			Cm *= r123;
			vec3 Sm = 2.0 * evalSensitivity( float( m ) * OPD, float( m ) * phi );
			I += Cm * Sm;
		}
		return max( I, vec3( 0.0 ) );
	}
#endif`,Lc=`#ifdef USE_BUMPMAP
	uniform sampler2D bumpMap;
	uniform float bumpScale;
	vec2 dHdxy_fwd() {
		vec2 dSTdx = dFdx( vUv );
		vec2 dSTdy = dFdy( vUv );
		float Hll = bumpScale * texture2D( bumpMap, vUv ).x;
		float dBx = bumpScale * texture2D( bumpMap, vUv + dSTdx ).x - Hll;
		float dBy = bumpScale * texture2D( bumpMap, vUv + dSTdy ).x - Hll;
		return vec2( dBx, dBy );
	}
	vec3 perturbNormalArb( vec3 surf_pos, vec3 surf_norm, vec2 dHdxy, float faceDirection ) {
		vec3 vSigmaX = dFdx( surf_pos.xyz );
		vec3 vSigmaY = dFdy( surf_pos.xyz );
		vec3 vN = surf_norm;
		vec3 R1 = cross( vSigmaY, vN );
		vec3 R2 = cross( vN, vSigmaX );
		float fDet = dot( vSigmaX, R1 ) * faceDirection;
		vec3 vGrad = sign( fDet ) * ( dHdxy.x * R1 + dHdxy.y * R2 );
		return normalize( abs( fDet ) * surf_norm - vGrad );
	}
#endif`,Pc=`#if NUM_CLIPPING_PLANES > 0
	vec4 plane;
	#pragma unroll_loop_start
	for ( int i = 0; i < UNION_CLIPPING_PLANES; i ++ ) {
		plane = clippingPlanes[ i ];
		if ( dot( vClipPosition, plane.xyz ) > plane.w ) discard;
	}
	#pragma unroll_loop_end
	#if UNION_CLIPPING_PLANES < NUM_CLIPPING_PLANES
		bool clipped = true;
		#pragma unroll_loop_start
		for ( int i = UNION_CLIPPING_PLANES; i < NUM_CLIPPING_PLANES; i ++ ) {
			plane = clippingPlanes[ i ];
			clipped = ( dot( vClipPosition, plane.xyz ) > plane.w ) && clipped;
		}
		#pragma unroll_loop_end
		if ( clipped ) discard;
	#endif
#endif`,zc=`#if NUM_CLIPPING_PLANES > 0
	varying vec3 vClipPosition;
	uniform vec4 clippingPlanes[ NUM_CLIPPING_PLANES ];
#endif`,Vc=`#if NUM_CLIPPING_PLANES > 0
	varying vec3 vClipPosition;
#endif`,Dc=`#if NUM_CLIPPING_PLANES > 0
	vClipPosition = - mvPosition.xyz;
#endif`,Uc=`#if defined( USE_COLOR_ALPHA )
	diffuseColor *= vColor;
#elif defined( USE_COLOR )
	diffuseColor.rgb *= vColor;
#endif`,Nc=`#if defined( USE_COLOR_ALPHA )
	varying vec4 vColor;
#elif defined( USE_COLOR )
	varying vec3 vColor;
#endif`,Fc=`#if defined( USE_COLOR_ALPHA )
	varying vec4 vColor;
#elif defined( USE_COLOR ) || defined( USE_INSTANCING_COLOR )
	varying vec3 vColor;
#endif`,qc=`#if defined( USE_COLOR_ALPHA )
	vColor = vec4( 1.0 );
#elif defined( USE_COLOR ) || defined( USE_INSTANCING_COLOR )
	vColor = vec3( 1.0 );
#endif
#ifdef USE_COLOR
	vColor *= color;
#endif
#ifdef USE_INSTANCING_COLOR
	vColor.xyz *= instanceColor.xyz;
#endif`,Oc=`#define PI 3.141592653589793
#define PI2 6.283185307179586
#define PI_HALF 1.5707963267948966
#define RECIPROCAL_PI 0.3183098861837907
#define RECIPROCAL_PI2 0.15915494309189535
#define EPSILON 1e-6
#ifndef saturate
#define saturate( a ) clamp( a, 0.0, 1.0 )
#endif
#define whiteComplement( a ) ( 1.0 - saturate( a ) )
float pow2( const in float x ) { return x*x; }
vec3 pow2( const in vec3 x ) { return x*x; }
float pow3( const in float x ) { return x*x*x; }
float pow4( const in float x ) { float x2 = x*x; return x2*x2; }
float max3( const in vec3 v ) { return max( max( v.x, v.y ), v.z ); }
float average( const in vec3 v ) { return dot( v, vec3( 0.3333333 ) ); }
highp float rand( const in vec2 uv ) {
	const highp float a = 12.9898, b = 78.233, c = 43758.5453;
	highp float dt = dot( uv.xy, vec2( a,b ) ), sn = mod( dt, PI );
	return fract( sin( sn ) * c );
}
#ifdef HIGH_PRECISION
	float precisionSafeLength( vec3 v ) { return length( v ); }
#else
	float precisionSafeLength( vec3 v ) {
		float maxComponent = max3( abs( v ) );
		return length( v / maxComponent ) * maxComponent;
	}
#endif
struct IncidentLight {
	vec3 color;
	vec3 direction;
	bool visible;
};
struct ReflectedLight {
	vec3 directDiffuse;
	vec3 directSpecular;
	vec3 indirectDiffuse;
	vec3 indirectSpecular;
};
struct GeometricContext {
	vec3 position;
	vec3 normal;
	vec3 viewDir;
#ifdef USE_CLEARCOAT
	vec3 clearcoatNormal;
#endif
};
vec3 transformDirection( in vec3 dir, in mat4 matrix ) {
	return normalize( ( matrix * vec4( dir, 0.0 ) ).xyz );
}
vec3 inverseTransformDirection( in vec3 dir, in mat4 matrix ) {
	return normalize( ( vec4( dir, 0.0 ) * matrix ).xyz );
}
mat3 transposeMat3( const in mat3 m ) {
	mat3 tmp;
	tmp[ 0 ] = vec3( m[ 0 ].x, m[ 1 ].x, m[ 2 ].x );
	tmp[ 1 ] = vec3( m[ 0 ].y, m[ 1 ].y, m[ 2 ].y );
	tmp[ 2 ] = vec3( m[ 0 ].z, m[ 1 ].z, m[ 2 ].z );
	return tmp;
}
float luminance( const in vec3 rgb ) {
	const vec3 weights = vec3( 0.2126729, 0.7151522, 0.0721750 );
	return dot( weights, rgb );
}
bool isPerspectiveMatrix( mat4 m ) {
	return m[ 2 ][ 3 ] == - 1.0;
}
vec2 equirectUv( in vec3 dir ) {
	float u = atan( dir.z, dir.x ) * RECIPROCAL_PI2 + 0.5;
	float v = asin( clamp( dir.y, - 1.0, 1.0 ) ) * RECIPROCAL_PI + 0.5;
	return vec2( u, v );
}`,Bc=`#ifdef ENVMAP_TYPE_CUBE_UV
	#define cubeUV_minMipLevel 4.0
	#define cubeUV_minTileSize 16.0
	float getFace( vec3 direction ) {
		vec3 absDirection = abs( direction );
		float face = - 1.0;
		if ( absDirection.x > absDirection.z ) {
			if ( absDirection.x > absDirection.y )
				face = direction.x > 0.0 ? 0.0 : 3.0;
			else
				face = direction.y > 0.0 ? 1.0 : 4.0;
		} else {
			if ( absDirection.z > absDirection.y )
				face = direction.z > 0.0 ? 2.0 : 5.0;
			else
				face = direction.y > 0.0 ? 1.0 : 4.0;
		}
		return face;
	}
	vec2 getUV( vec3 direction, float face ) {
		vec2 uv;
		if ( face == 0.0 ) {
			uv = vec2( direction.z, direction.y ) / abs( direction.x );
		} else if ( face == 1.0 ) {
			uv = vec2( - direction.x, - direction.z ) / abs( direction.y );
		} else if ( face == 2.0 ) {
			uv = vec2( - direction.x, direction.y ) / abs( direction.z );
		} else if ( face == 3.0 ) {
			uv = vec2( - direction.z, direction.y ) / abs( direction.x );
		} else if ( face == 4.0 ) {
			uv = vec2( - direction.x, direction.z ) / abs( direction.y );
		} else {
			uv = vec2( direction.x, direction.y ) / abs( direction.z );
		}
		return 0.5 * ( uv + 1.0 );
	}
	vec3 bilinearCubeUV( sampler2D envMap, vec3 direction, float mipInt ) {
		float face = getFace( direction );
		float filterInt = max( cubeUV_minMipLevel - mipInt, 0.0 );
		mipInt = max( mipInt, cubeUV_minMipLevel );
		float faceSize = exp2( mipInt );
		vec2 uv = getUV( direction, face ) * ( faceSize - 2.0 ) + 1.0;
		if ( face > 2.0 ) {
			uv.y += faceSize;
			face -= 3.0;
		}
		uv.x += face * faceSize;
		uv.x += filterInt * 3.0 * cubeUV_minTileSize;
		uv.y += 4.0 * ( exp2( CUBEUV_MAX_MIP ) - faceSize );
		uv.x *= CUBEUV_TEXEL_WIDTH;
		uv.y *= CUBEUV_TEXEL_HEIGHT;
		#ifdef texture2DGradEXT
			return texture2DGradEXT( envMap, uv, vec2( 0.0 ), vec2( 0.0 ) ).rgb;
		#else
			return texture2D( envMap, uv ).rgb;
		#endif
	}
	#define cubeUV_r0 1.0
	#define cubeUV_v0 0.339
	#define cubeUV_m0 - 2.0
	#define cubeUV_r1 0.8
	#define cubeUV_v1 0.276
	#define cubeUV_m1 - 1.0
	#define cubeUV_r4 0.4
	#define cubeUV_v4 0.046
	#define cubeUV_m4 2.0
	#define cubeUV_r5 0.305
	#define cubeUV_v5 0.016
	#define cubeUV_m5 3.0
	#define cubeUV_r6 0.21
	#define cubeUV_v6 0.0038
	#define cubeUV_m6 4.0
	float roughnessToMip( float roughness ) {
		float mip = 0.0;
		if ( roughness >= cubeUV_r1 ) {
			mip = ( cubeUV_r0 - roughness ) * ( cubeUV_m1 - cubeUV_m0 ) / ( cubeUV_r0 - cubeUV_r1 ) + cubeUV_m0;
		} else if ( roughness >= cubeUV_r4 ) {
			mip = ( cubeUV_r1 - roughness ) * ( cubeUV_m4 - cubeUV_m1 ) / ( cubeUV_r1 - cubeUV_r4 ) + cubeUV_m1;
		} else if ( roughness >= cubeUV_r5 ) {
			mip = ( cubeUV_r4 - roughness ) * ( cubeUV_m5 - cubeUV_m4 ) / ( cubeUV_r4 - cubeUV_r5 ) + cubeUV_m4;
		} else if ( roughness >= cubeUV_r6 ) {
			mip = ( cubeUV_r5 - roughness ) * ( cubeUV_m6 - cubeUV_m5 ) / ( cubeUV_r5 - cubeUV_r6 ) + cubeUV_m5;
		} else {
			mip = - 2.0 * log2( 1.16 * roughness );		}
		return mip;
	}
	vec4 textureCubeUV( sampler2D envMap, vec3 sampleDir, float roughness ) {
		float mip = clamp( roughnessToMip( roughness ), cubeUV_m0, CUBEUV_MAX_MIP );
		float mipF = fract( mip );
		float mipInt = floor( mip );
		vec3 color0 = bilinearCubeUV( envMap, sampleDir, mipInt );
		if ( mipF == 0.0 ) {
			return vec4( color0, 1.0 );
		} else {
			vec3 color1 = bilinearCubeUV( envMap, sampleDir, mipInt + 1.0 );
			return vec4( mix( color0, color1, mipF ), 1.0 );
		}
	}
#endif`,kc=`vec3 transformedNormal = objectNormal;
#ifdef USE_INSTANCING
	mat3 m = mat3( instanceMatrix );
	transformedNormal /= vec3( dot( m[ 0 ], m[ 0 ] ), dot( m[ 1 ], m[ 1 ] ), dot( m[ 2 ], m[ 2 ] ) );
	transformedNormal = m * transformedNormal;
#endif
transformedNormal = normalMatrix * transformedNormal;
#ifdef FLIP_SIDED
	transformedNormal = - transformedNormal;
#endif
#ifdef USE_TANGENT
	vec3 transformedTangent = ( modelViewMatrix * vec4( objectTangent, 0.0 ) ).xyz;
	#ifdef FLIP_SIDED
		transformedTangent = - transformedTangent;
	#endif
#endif`,Hc=`#ifdef USE_DISPLACEMENTMAP
	uniform sampler2D displacementMap;
	uniform float displacementScale;
	uniform float displacementBias;
#endif`,Zc=`#ifdef USE_DISPLACEMENTMAP
	transformed += normalize( objectNormal ) * ( texture2D( displacementMap, vUv ).x * displacementScale + displacementBias );
#endif`,Gc=`#ifdef USE_EMISSIVEMAP
	vec4 emissiveColor = texture2D( emissiveMap, vUv );
	totalEmissiveRadiance *= emissiveColor.rgb;
#endif`,Wc=`#ifdef USE_EMISSIVEMAP
	uniform sampler2D emissiveMap;
#endif`,Xc="gl_FragColor = linearToOutputTexel( gl_FragColor );",jc=`vec4 LinearToLinear( in vec4 value ) {
	return value;
}
vec4 LinearTosRGB( in vec4 value ) {
	return vec4( mix( pow( value.rgb, vec3( 0.41666 ) ) * 1.055 - vec3( 0.055 ), value.rgb * 12.92, vec3( lessThanEqual( value.rgb, vec3( 0.0031308 ) ) ) ), value.a );
}`,Yc=`#ifdef USE_ENVMAP
	#ifdef ENV_WORLDPOS
		vec3 cameraToFrag;
		if ( isOrthographic ) {
			cameraToFrag = normalize( vec3( - viewMatrix[ 0 ][ 2 ], - viewMatrix[ 1 ][ 2 ], - viewMatrix[ 2 ][ 2 ] ) );
		} else {
			cameraToFrag = normalize( vWorldPosition - cameraPosition );
		}
		vec3 worldNormal = inverseTransformDirection( normal, viewMatrix );
		#ifdef ENVMAP_MODE_REFLECTION
			vec3 reflectVec = reflect( cameraToFrag, worldNormal );
		#else
			vec3 reflectVec = refract( cameraToFrag, worldNormal, refractionRatio );
		#endif
	#else
		vec3 reflectVec = vReflect;
	#endif
	#ifdef ENVMAP_TYPE_CUBE
		vec4 envColor = textureCube( envMap, vec3( flipEnvMap * reflectVec.x, reflectVec.yz ) );
	#else
		vec4 envColor = vec4( 0.0 );
	#endif
	#ifdef ENVMAP_BLENDING_MULTIPLY
		outgoingLight = mix( outgoingLight, outgoingLight * envColor.xyz, specularStrength * reflectivity );
	#elif defined( ENVMAP_BLENDING_MIX )
		outgoingLight = mix( outgoingLight, envColor.xyz, specularStrength * reflectivity );
	#elif defined( ENVMAP_BLENDING_ADD )
		outgoingLight += envColor.xyz * specularStrength * reflectivity;
	#endif
#endif`,Kc=`#ifdef USE_ENVMAP
	uniform float envMapIntensity;
	uniform float flipEnvMap;
	#ifdef ENVMAP_TYPE_CUBE
		uniform samplerCube envMap;
	#else
		uniform sampler2D envMap;
	#endif
	
#endif`,Qc=`#ifdef USE_ENVMAP
	uniform float reflectivity;
	#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG ) || defined( LAMBERT )
		#define ENV_WORLDPOS
	#endif
	#ifdef ENV_WORLDPOS
		varying vec3 vWorldPosition;
		uniform float refractionRatio;
	#else
		varying vec3 vReflect;
	#endif
#endif`,Jc=`#ifdef USE_ENVMAP
	#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG ) || defined( LAMBERT )
		#define ENV_WORLDPOS
	#endif
	#ifdef ENV_WORLDPOS
		
		varying vec3 vWorldPosition;
	#else
		varying vec3 vReflect;
		uniform float refractionRatio;
	#endif
#endif`,$c=`#ifdef USE_ENVMAP
	#ifdef ENV_WORLDPOS
		vWorldPosition = worldPosition.xyz;
	#else
		vec3 cameraToVertex;
		if ( isOrthographic ) {
			cameraToVertex = normalize( vec3( - viewMatrix[ 0 ][ 2 ], - viewMatrix[ 1 ][ 2 ], - viewMatrix[ 2 ][ 2 ] ) );
		} else {
			cameraToVertex = normalize( worldPosition.xyz - cameraPosition );
		}
		vec3 worldNormal = inverseTransformDirection( transformedNormal, viewMatrix );
		#ifdef ENVMAP_MODE_REFLECTION
			vReflect = reflect( cameraToVertex, worldNormal );
		#else
			vReflect = refract( cameraToVertex, worldNormal, refractionRatio );
		#endif
	#endif
#endif`,eu=`#ifdef USE_FOG
	vFogDepth = - mvPosition.z;
#endif`,tu=`#ifdef USE_FOG
	varying float vFogDepth;
#endif`,nu=`#ifdef USE_FOG
	#ifdef FOG_EXP2
		float fogFactor = 1.0 - exp( - fogDensity * fogDensity * vFogDepth * vFogDepth );
	#else
		float fogFactor = smoothstep( fogNear, fogFar, vFogDepth );
	#endif
	gl_FragColor.rgb = mix( gl_FragColor.rgb, fogColor, fogFactor );
#endif`,iu=`#ifdef USE_FOG
	uniform vec3 fogColor;
	varying float vFogDepth;
	#ifdef FOG_EXP2
		uniform float fogDensity;
	#else
		uniform float fogNear;
		uniform float fogFar;
	#endif
#endif`,ru=`#ifdef USE_GRADIENTMAP
	uniform sampler2D gradientMap;
#endif
vec3 getGradientIrradiance( vec3 normal, vec3 lightDirection ) {
	float dotNL = dot( normal, lightDirection );
	vec2 coord = vec2( dotNL * 0.5 + 0.5, 0.0 );
	#ifdef USE_GRADIENTMAP
		return vec3( texture2D( gradientMap, coord ).r );
	#else
		vec2 fw = fwidth( coord ) * 0.5;
		return mix( vec3( 0.7 ), vec3( 1.0 ), smoothstep( 0.7 - fw.x, 0.7 + fw.x, coord.x ) );
	#endif
}`,su=`#ifdef USE_LIGHTMAP
	vec4 lightMapTexel = texture2D( lightMap, vUv2 );
	vec3 lightMapIrradiance = lightMapTexel.rgb * lightMapIntensity;
	reflectedLight.indirectDiffuse += lightMapIrradiance;
#endif`,au=`#ifdef USE_LIGHTMAP
	uniform sampler2D lightMap;
	uniform float lightMapIntensity;
#endif`,ou=`LambertMaterial material;
material.diffuseColor = diffuseColor.rgb;
material.specularStrength = specularStrength;`,lu=`varying vec3 vViewPosition;
struct LambertMaterial {
	vec3 diffuseColor;
	float specularStrength;
};
void RE_Direct_Lambert( const in IncidentLight directLight, const in GeometricContext geometry, const in LambertMaterial material, inout ReflectedLight reflectedLight ) {
	float dotNL = saturate( dot( geometry.normal, directLight.direction ) );
	vec3 irradiance = dotNL * directLight.color;
	reflectedLight.directDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
void RE_IndirectDiffuse_Lambert( const in vec3 irradiance, const in GeometricContext geometry, const in LambertMaterial material, inout ReflectedLight reflectedLight ) {
	reflectedLight.indirectDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
#define RE_Direct				RE_Direct_Lambert
#define RE_IndirectDiffuse		RE_IndirectDiffuse_Lambert`,cu=`uniform bool receiveShadow;
uniform vec3 ambientLightColor;
uniform vec3 lightProbe[ 9 ];
vec3 shGetIrradianceAt( in vec3 normal, in vec3 shCoefficients[ 9 ] ) {
	float x = normal.x, y = normal.y, z = normal.z;
	vec3 result = shCoefficients[ 0 ] * 0.886227;
	result += shCoefficients[ 1 ] * 2.0 * 0.511664 * y;
	result += shCoefficients[ 2 ] * 2.0 * 0.511664 * z;
	result += shCoefficients[ 3 ] * 2.0 * 0.511664 * x;
	result += shCoefficients[ 4 ] * 2.0 * 0.429043 * x * y;
	result += shCoefficients[ 5 ] * 2.0 * 0.429043 * y * z;
	result += shCoefficients[ 6 ] * ( 0.743125 * z * z - 0.247708 );
	result += shCoefficients[ 7 ] * 2.0 * 0.429043 * x * z;
	result += shCoefficients[ 8 ] * 0.429043 * ( x * x - y * y );
	return result;
}
vec3 getLightProbeIrradiance( const in vec3 lightProbe[ 9 ], const in vec3 normal ) {
	vec3 worldNormal = inverseTransformDirection( normal, viewMatrix );
	vec3 irradiance = shGetIrradianceAt( worldNormal, lightProbe );
	return irradiance;
}
vec3 getAmbientLightIrradiance( const in vec3 ambientLightColor ) {
	vec3 irradiance = ambientLightColor;
	return irradiance;
}
float getDistanceAttenuation( const in float lightDistance, const in float cutoffDistance, const in float decayExponent ) {
	#if defined ( PHYSICALLY_CORRECT_LIGHTS )
		float distanceFalloff = 1.0 / max( pow( lightDistance, decayExponent ), 0.01 );
		if ( cutoffDistance > 0.0 ) {
			distanceFalloff *= pow2( saturate( 1.0 - pow4( lightDistance / cutoffDistance ) ) );
		}
		return distanceFalloff;
	#else
		if ( cutoffDistance > 0.0 && decayExponent > 0.0 ) {
			return pow( saturate( - lightDistance / cutoffDistance + 1.0 ), decayExponent );
		}
		return 1.0;
	#endif
}
float getSpotAttenuation( const in float coneCosine, const in float penumbraCosine, const in float angleCosine ) {
	return smoothstep( coneCosine, penumbraCosine, angleCosine );
}
#if NUM_DIR_LIGHTS > 0
	struct DirectionalLight {
		vec3 direction;
		vec3 color;
	};
	uniform DirectionalLight directionalLights[ NUM_DIR_LIGHTS ];
	void getDirectionalLightInfo( const in DirectionalLight directionalLight, const in GeometricContext geometry, out IncidentLight light ) {
		light.color = directionalLight.color;
		light.direction = directionalLight.direction;
		light.visible = true;
	}
#endif
#if NUM_POINT_LIGHTS > 0
	struct PointLight {
		vec3 position;
		vec3 color;
		float distance;
		float decay;
	};
	uniform PointLight pointLights[ NUM_POINT_LIGHTS ];
	void getPointLightInfo( const in PointLight pointLight, const in GeometricContext geometry, out IncidentLight light ) {
		vec3 lVector = pointLight.position - geometry.position;
		light.direction = normalize( lVector );
		float lightDistance = length( lVector );
		light.color = pointLight.color;
		light.color *= getDistanceAttenuation( lightDistance, pointLight.distance, pointLight.decay );
		light.visible = ( light.color != vec3( 0.0 ) );
	}
#endif
#if NUM_SPOT_LIGHTS > 0
	struct SpotLight {
		vec3 position;
		vec3 direction;
		vec3 color;
		float distance;
		float decay;
		float coneCos;
		float penumbraCos;
	};
	uniform SpotLight spotLights[ NUM_SPOT_LIGHTS ];
	void getSpotLightInfo( const in SpotLight spotLight, const in GeometricContext geometry, out IncidentLight light ) {
		vec3 lVector = spotLight.position - geometry.position;
		light.direction = normalize( lVector );
		float angleCos = dot( light.direction, spotLight.direction );
		float spotAttenuation = getSpotAttenuation( spotLight.coneCos, spotLight.penumbraCos, angleCos );
		if ( spotAttenuation > 0.0 ) {
			float lightDistance = length( lVector );
			light.color = spotLight.color * spotAttenuation;
			light.color *= getDistanceAttenuation( lightDistance, spotLight.distance, spotLight.decay );
			light.visible = ( light.color != vec3( 0.0 ) );
		} else {
			light.color = vec3( 0.0 );
			light.visible = false;
		}
	}
#endif
#if NUM_RECT_AREA_LIGHTS > 0
	struct RectAreaLight {
		vec3 color;
		vec3 position;
		vec3 halfWidth;
		vec3 halfHeight;
	};
	uniform sampler2D ltc_1;	uniform sampler2D ltc_2;
	uniform RectAreaLight rectAreaLights[ NUM_RECT_AREA_LIGHTS ];
#endif
#if NUM_HEMI_LIGHTS > 0
	struct HemisphereLight {
		vec3 direction;
		vec3 skyColor;
		vec3 groundColor;
	};
	uniform HemisphereLight hemisphereLights[ NUM_HEMI_LIGHTS ];
	vec3 getHemisphereLightIrradiance( const in HemisphereLight hemiLight, const in vec3 normal ) {
		float dotNL = dot( normal, hemiLight.direction );
		float hemiDiffuseWeight = 0.5 * dotNL + 0.5;
		vec3 irradiance = mix( hemiLight.groundColor, hemiLight.skyColor, hemiDiffuseWeight );
		return irradiance;
	}
#endif`,uu=`#if defined( USE_ENVMAP )
	vec3 getIBLIrradiance( const in vec3 normal ) {
		#if defined( ENVMAP_TYPE_CUBE_UV )
			vec3 worldNormal = inverseTransformDirection( normal, viewMatrix );
			vec4 envMapColor = textureCubeUV( envMap, worldNormal, 1.0 );
			return PI * envMapColor.rgb * envMapIntensity;
		#else
			return vec3( 0.0 );
		#endif
	}
	vec3 getIBLRadiance( const in vec3 viewDir, const in vec3 normal, const in float roughness ) {
		#if defined( ENVMAP_TYPE_CUBE_UV )
			vec3 reflectVec = reflect( - viewDir, normal );
			reflectVec = normalize( mix( reflectVec, normal, roughness * roughness) );
			reflectVec = inverseTransformDirection( reflectVec, viewMatrix );
			vec4 envMapColor = textureCubeUV( envMap, reflectVec, roughness );
			return envMapColor.rgb * envMapIntensity;
		#else
			return vec3( 0.0 );
		#endif
	}
#endif`,du=`ToonMaterial material;
material.diffuseColor = diffuseColor.rgb;`,hu=`varying vec3 vViewPosition;
struct ToonMaterial {
	vec3 diffuseColor;
};
void RE_Direct_Toon( const in IncidentLight directLight, const in GeometricContext geometry, const in ToonMaterial material, inout ReflectedLight reflectedLight ) {
	vec3 irradiance = getGradientIrradiance( geometry.normal, directLight.direction ) * directLight.color;
	reflectedLight.directDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
void RE_IndirectDiffuse_Toon( const in vec3 irradiance, const in GeometricContext geometry, const in ToonMaterial material, inout ReflectedLight reflectedLight ) {
	reflectedLight.indirectDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
#define RE_Direct				RE_Direct_Toon
#define RE_IndirectDiffuse		RE_IndirectDiffuse_Toon`,fu=`BlinnPhongMaterial material;
material.diffuseColor = diffuseColor.rgb;
material.specularColor = specular;
material.specularShininess = shininess;
material.specularStrength = specularStrength;`,pu=`varying vec3 vViewPosition;
struct BlinnPhongMaterial {
	vec3 diffuseColor;
	vec3 specularColor;
	float specularShininess;
	float specularStrength;
};
void RE_Direct_BlinnPhong( const in IncidentLight directLight, const in GeometricContext geometry, const in BlinnPhongMaterial material, inout ReflectedLight reflectedLight ) {
	float dotNL = saturate( dot( geometry.normal, directLight.direction ) );
	vec3 irradiance = dotNL * directLight.color;
	reflectedLight.directDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
	reflectedLight.directSpecular += irradiance * BRDF_BlinnPhong( directLight.direction, geometry.viewDir, geometry.normal, material.specularColor, material.specularShininess ) * material.specularStrength;
}
void RE_IndirectDiffuse_BlinnPhong( const in vec3 irradiance, const in GeometricContext geometry, const in BlinnPhongMaterial material, inout ReflectedLight reflectedLight ) {
	reflectedLight.indirectDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
#define RE_Direct				RE_Direct_BlinnPhong
#define RE_IndirectDiffuse		RE_IndirectDiffuse_BlinnPhong`,mu=`PhysicalMaterial material;
material.diffuseColor = diffuseColor.rgb * ( 1.0 - metalnessFactor );
vec3 dxy = max( abs( dFdx( geometryNormal ) ), abs( dFdy( geometryNormal ) ) );
float geometryRoughness = max( max( dxy.x, dxy.y ), dxy.z );
material.roughness = max( roughnessFactor, 0.0525 );material.roughness += geometryRoughness;
material.roughness = min( material.roughness, 1.0 );
#ifdef IOR
	material.ior = ior;
	#ifdef SPECULAR
		float specularIntensityFactor = specularIntensity;
		vec3 specularColorFactor = specularColor;
		#ifdef USE_SPECULARINTENSITYMAP
			specularIntensityFactor *= texture2D( specularIntensityMap, vUv ).a;
		#endif
		#ifdef USE_SPECULARCOLORMAP
			specularColorFactor *= texture2D( specularColorMap, vUv ).rgb;
		#endif
		material.specularF90 = mix( specularIntensityFactor, 1.0, metalnessFactor );
	#else
		float specularIntensityFactor = 1.0;
		vec3 specularColorFactor = vec3( 1.0 );
		material.specularF90 = 1.0;
	#endif
	material.specularColor = mix( min( pow2( ( material.ior - 1.0 ) / ( material.ior + 1.0 ) ) * specularColorFactor, vec3( 1.0 ) ) * specularIntensityFactor, diffuseColor.rgb, metalnessFactor );
#else
	material.specularColor = mix( vec3( 0.04 ), diffuseColor.rgb, metalnessFactor );
	material.specularF90 = 1.0;
#endif
#ifdef USE_CLEARCOAT
	material.clearcoat = clearcoat;
	material.clearcoatRoughness = clearcoatRoughness;
	material.clearcoatF0 = vec3( 0.04 );
	material.clearcoatF90 = 1.0;
	#ifdef USE_CLEARCOATMAP
		material.clearcoat *= texture2D( clearcoatMap, vUv ).x;
	#endif
	#ifdef USE_CLEARCOAT_ROUGHNESSMAP
		material.clearcoatRoughness *= texture2D( clearcoatRoughnessMap, vUv ).y;
	#endif
	material.clearcoat = saturate( material.clearcoat );	material.clearcoatRoughness = max( material.clearcoatRoughness, 0.0525 );
	material.clearcoatRoughness += geometryRoughness;
	material.clearcoatRoughness = min( material.clearcoatRoughness, 1.0 );
#endif
#ifdef USE_IRIDESCENCE
	material.iridescence = iridescence;
	material.iridescenceIOR = iridescenceIOR;
	#ifdef USE_IRIDESCENCEMAP
		material.iridescence *= texture2D( iridescenceMap, vUv ).r;
	#endif
	#ifdef USE_IRIDESCENCE_THICKNESSMAP
		material.iridescenceThickness = (iridescenceThicknessMaximum - iridescenceThicknessMinimum) * texture2D( iridescenceThicknessMap, vUv ).g + iridescenceThicknessMinimum;
	#else
		material.iridescenceThickness = iridescenceThicknessMaximum;
	#endif
#endif
#ifdef USE_SHEEN
	material.sheenColor = sheenColor;
	#ifdef USE_SHEENCOLORMAP
		material.sheenColor *= texture2D( sheenColorMap, vUv ).rgb;
	#endif
	material.sheenRoughness = clamp( sheenRoughness, 0.07, 1.0 );
	#ifdef USE_SHEENROUGHNESSMAP
		material.sheenRoughness *= texture2D( sheenRoughnessMap, vUv ).a;
	#endif
#endif`,gu=`struct PhysicalMaterial {
	vec3 diffuseColor;
	float roughness;
	vec3 specularColor;
	float specularF90;
	#ifdef USE_CLEARCOAT
		float clearcoat;
		float clearcoatRoughness;
		vec3 clearcoatF0;
		float clearcoatF90;
	#endif
	#ifdef USE_IRIDESCENCE
		float iridescence;
		float iridescenceIOR;
		float iridescenceThickness;
		vec3 iridescenceFresnel;
		vec3 iridescenceF0;
	#endif
	#ifdef USE_SHEEN
		vec3 sheenColor;
		float sheenRoughness;
	#endif
	#ifdef IOR
		float ior;
	#endif
	#ifdef USE_TRANSMISSION
		float transmission;
		float transmissionAlpha;
		float thickness;
		float attenuationDistance;
		vec3 attenuationColor;
	#endif
};
vec3 clearcoatSpecular = vec3( 0.0 );
vec3 sheenSpecular = vec3( 0.0 );
float IBLSheenBRDF( const in vec3 normal, const in vec3 viewDir, const in float roughness ) {
	float dotNV = saturate( dot( normal, viewDir ) );
	float r2 = roughness * roughness;
	float a = roughness < 0.25 ? -339.2 * r2 + 161.4 * roughness - 25.9 : -8.48 * r2 + 14.3 * roughness - 9.95;
	float b = roughness < 0.25 ? 44.0 * r2 - 23.7 * roughness + 3.26 : 1.97 * r2 - 3.27 * roughness + 0.72;
	float DG = exp( a * dotNV + b ) + ( roughness < 0.25 ? 0.0 : 0.1 * ( roughness - 0.25 ) );
	return saturate( DG * RECIPROCAL_PI );
}
vec2 DFGApprox( const in vec3 normal, const in vec3 viewDir, const in float roughness ) {
	float dotNV = saturate( dot( normal, viewDir ) );
	const vec4 c0 = vec4( - 1, - 0.0275, - 0.572, 0.022 );
	const vec4 c1 = vec4( 1, 0.0425, 1.04, - 0.04 );
	vec4 r = roughness * c0 + c1;
	float a004 = min( r.x * r.x, exp2( - 9.28 * dotNV ) ) * r.x + r.y;
	vec2 fab = vec2( - 1.04, 1.04 ) * a004 + r.zw;
	return fab;
}
vec3 EnvironmentBRDF( const in vec3 normal, const in vec3 viewDir, const in vec3 specularColor, const in float specularF90, const in float roughness ) {
	vec2 fab = DFGApprox( normal, viewDir, roughness );
	return specularColor * fab.x + specularF90 * fab.y;
}
#ifdef USE_IRIDESCENCE
void computeMultiscatteringIridescence( const in vec3 normal, const in vec3 viewDir, const in vec3 specularColor, const in float specularF90, const in float iridescence, const in vec3 iridescenceF0, const in float roughness, inout vec3 singleScatter, inout vec3 multiScatter ) {
#else
void computeMultiscattering( const in vec3 normal, const in vec3 viewDir, const in vec3 specularColor, const in float specularF90, const in float roughness, inout vec3 singleScatter, inout vec3 multiScatter ) {
#endif
	vec2 fab = DFGApprox( normal, viewDir, roughness );
	#ifdef USE_IRIDESCENCE
		vec3 Fr = mix( specularColor, iridescenceF0, iridescence );
	#else
		vec3 Fr = specularColor;
	#endif
	vec3 FssEss = Fr * fab.x + specularF90 * fab.y;
	float Ess = fab.x + fab.y;
	float Ems = 1.0 - Ess;
	vec3 Favg = Fr + ( 1.0 - Fr ) * 0.047619;	vec3 Fms = FssEss * Favg / ( 1.0 - Ems * Favg );
	singleScatter += FssEss;
	multiScatter += Fms * Ems;
}
#if NUM_RECT_AREA_LIGHTS > 0
	void RE_Direct_RectArea_Physical( const in RectAreaLight rectAreaLight, const in GeometricContext geometry, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {
		vec3 normal = geometry.normal;
		vec3 viewDir = geometry.viewDir;
		vec3 position = geometry.position;
		vec3 lightPos = rectAreaLight.position;
		vec3 halfWidth = rectAreaLight.halfWidth;
		vec3 halfHeight = rectAreaLight.halfHeight;
		vec3 lightColor = rectAreaLight.color;
		float roughness = material.roughness;
		vec3 rectCoords[ 4 ];
		rectCoords[ 0 ] = lightPos + halfWidth - halfHeight;		rectCoords[ 1 ] = lightPos - halfWidth - halfHeight;
		rectCoords[ 2 ] = lightPos - halfWidth + halfHeight;
		rectCoords[ 3 ] = lightPos + halfWidth + halfHeight;
		vec2 uv = LTC_Uv( normal, viewDir, roughness );
		vec4 t1 = texture2D( ltc_1, uv );
		vec4 t2 = texture2D( ltc_2, uv );
		mat3 mInv = mat3(
			vec3( t1.x, 0, t1.y ),
			vec3(    0, 1,    0 ),
			vec3( t1.z, 0, t1.w )
		);
		vec3 fresnel = ( material.specularColor * t2.x + ( vec3( 1.0 ) - material.specularColor ) * t2.y );
		reflectedLight.directSpecular += lightColor * fresnel * LTC_Evaluate( normal, viewDir, position, mInv, rectCoords );
		reflectedLight.directDiffuse += lightColor * material.diffuseColor * LTC_Evaluate( normal, viewDir, position, mat3( 1.0 ), rectCoords );
	}
#endif
void RE_Direct_Physical( const in IncidentLight directLight, const in GeometricContext geometry, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {
	float dotNL = saturate( dot( geometry.normal, directLight.direction ) );
	vec3 irradiance = dotNL * directLight.color;
	#ifdef USE_CLEARCOAT
		float dotNLcc = saturate( dot( geometry.clearcoatNormal, directLight.direction ) );
		vec3 ccIrradiance = dotNLcc * directLight.color;
		clearcoatSpecular += ccIrradiance * BRDF_GGX( directLight.direction, geometry.viewDir, geometry.clearcoatNormal, material.clearcoatF0, material.clearcoatF90, material.clearcoatRoughness );
	#endif
	#ifdef USE_SHEEN
		sheenSpecular += irradiance * BRDF_Sheen( directLight.direction, geometry.viewDir, geometry.normal, material.sheenColor, material.sheenRoughness );
	#endif
	#ifdef USE_IRIDESCENCE
		reflectedLight.directSpecular += irradiance * BRDF_GGX_Iridescence( directLight.direction, geometry.viewDir, geometry.normal, material.specularColor, material.specularF90, material.iridescence, material.iridescenceFresnel, material.roughness );
	#else
		reflectedLight.directSpecular += irradiance * BRDF_GGX( directLight.direction, geometry.viewDir, geometry.normal, material.specularColor, material.specularF90, material.roughness );
	#endif
	reflectedLight.directDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
void RE_IndirectDiffuse_Physical( const in vec3 irradiance, const in GeometricContext geometry, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {
	reflectedLight.indirectDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
void RE_IndirectSpecular_Physical( const in vec3 radiance, const in vec3 irradiance, const in vec3 clearcoatRadiance, const in GeometricContext geometry, const in PhysicalMaterial material, inout ReflectedLight reflectedLight) {
	#ifdef USE_CLEARCOAT
		clearcoatSpecular += clearcoatRadiance * EnvironmentBRDF( geometry.clearcoatNormal, geometry.viewDir, material.clearcoatF0, material.clearcoatF90, material.clearcoatRoughness );
	#endif
	#ifdef USE_SHEEN
		sheenSpecular += irradiance * material.sheenColor * IBLSheenBRDF( geometry.normal, geometry.viewDir, material.sheenRoughness );
	#endif
	vec3 singleScattering = vec3( 0.0 );
	vec3 multiScattering = vec3( 0.0 );
	vec3 cosineWeightedIrradiance = irradiance * RECIPROCAL_PI;
	#ifdef USE_IRIDESCENCE
		computeMultiscatteringIridescence( geometry.normal, geometry.viewDir, material.specularColor, material.specularF90, material.iridescence, material.iridescenceFresnel, material.roughness, singleScattering, multiScattering );
	#else
		computeMultiscattering( geometry.normal, geometry.viewDir, material.specularColor, material.specularF90, material.roughness, singleScattering, multiScattering );
	#endif
	vec3 totalScattering = singleScattering + multiScattering;
	vec3 diffuse = material.diffuseColor * ( 1.0 - max( max( totalScattering.r, totalScattering.g ), totalScattering.b ) );
	reflectedLight.indirectSpecular += radiance * singleScattering;
	reflectedLight.indirectSpecular += multiScattering * cosineWeightedIrradiance;
	reflectedLight.indirectDiffuse += diffuse * cosineWeightedIrradiance;
}
#define RE_Direct				RE_Direct_Physical
#define RE_Direct_RectArea		RE_Direct_RectArea_Physical
#define RE_IndirectDiffuse		RE_IndirectDiffuse_Physical
#define RE_IndirectSpecular		RE_IndirectSpecular_Physical
float computeSpecularOcclusion( const in float dotNV, const in float ambientOcclusion, const in float roughness ) {
	return saturate( pow( dotNV + ambientOcclusion, exp2( - 16.0 * roughness - 1.0 ) ) - 1.0 + ambientOcclusion );
}`,vu=`
GeometricContext geometry;
geometry.position = - vViewPosition;
geometry.normal = normal;
geometry.viewDir = ( isOrthographic ) ? vec3( 0, 0, 1 ) : normalize( vViewPosition );
#ifdef USE_CLEARCOAT
	geometry.clearcoatNormal = clearcoatNormal;
#endif
#ifdef USE_IRIDESCENCE
	float dotNVi = saturate( dot( normal, geometry.viewDir ) );
	if ( material.iridescenceThickness == 0.0 ) {
		material.iridescence = 0.0;
	} else {
		material.iridescence = saturate( material.iridescence );
	}
	if ( material.iridescence > 0.0 ) {
		material.iridescenceFresnel = evalIridescence( 1.0, material.iridescenceIOR, dotNVi, material.iridescenceThickness, material.specularColor );
		material.iridescenceF0 = Schlick_to_F0( material.iridescenceFresnel, 1.0, dotNVi );
	}
#endif
IncidentLight directLight;
#if ( NUM_POINT_LIGHTS > 0 ) && defined( RE_Direct )
	PointLight pointLight;
	#if defined( USE_SHADOWMAP ) && NUM_POINT_LIGHT_SHADOWS > 0
	PointLightShadow pointLightShadow;
	#endif
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {
		pointLight = pointLights[ i ];
		getPointLightInfo( pointLight, geometry, directLight );
		#if defined( USE_SHADOWMAP ) && ( UNROLLED_LOOP_INDEX < NUM_POINT_LIGHT_SHADOWS )
		pointLightShadow = pointLightShadows[ i ];
		directLight.color *= all( bvec2( directLight.visible, receiveShadow ) ) ? getPointShadow( pointShadowMap[ i ], pointLightShadow.shadowMapSize, pointLightShadow.shadowBias, pointLightShadow.shadowRadius, vPointShadowCoord[ i ], pointLightShadow.shadowCameraNear, pointLightShadow.shadowCameraFar ) : 1.0;
		#endif
		RE_Direct( directLight, geometry, material, reflectedLight );
	}
	#pragma unroll_loop_end
#endif
#if ( NUM_SPOT_LIGHTS > 0 ) && defined( RE_Direct )
	SpotLight spotLight;
	vec4 spotColor;
	vec3 spotLightCoord;
	bool inSpotLightMap;
	#if defined( USE_SHADOWMAP ) && NUM_SPOT_LIGHT_SHADOWS > 0
	SpotLightShadow spotLightShadow;
	#endif
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_SPOT_LIGHTS; i ++ ) {
		spotLight = spotLights[ i ];
		getSpotLightInfo( spotLight, geometry, directLight );
		#if ( UNROLLED_LOOP_INDEX < NUM_SPOT_LIGHT_SHADOWS_WITH_MAPS )
		#define SPOT_LIGHT_MAP_INDEX UNROLLED_LOOP_INDEX
		#elif ( UNROLLED_LOOP_INDEX < NUM_SPOT_LIGHT_SHADOWS )
		#define SPOT_LIGHT_MAP_INDEX NUM_SPOT_LIGHT_MAPS
		#else
		#define SPOT_LIGHT_MAP_INDEX ( UNROLLED_LOOP_INDEX - NUM_SPOT_LIGHT_SHADOWS + NUM_SPOT_LIGHT_SHADOWS_WITH_MAPS )
		#endif
		#if ( SPOT_LIGHT_MAP_INDEX < NUM_SPOT_LIGHT_MAPS )
			spotLightCoord = vSpotLightCoord[ i ].xyz / vSpotLightCoord[ i ].w;
			inSpotLightMap = all( lessThan( abs( spotLightCoord * 2. - 1. ), vec3( 1.0 ) ) );
			spotColor = texture2D( spotLightMap[ SPOT_LIGHT_MAP_INDEX ], spotLightCoord.xy );
			directLight.color = inSpotLightMap ? directLight.color * spotColor.rgb : directLight.color;
		#endif
		#undef SPOT_LIGHT_MAP_INDEX
		#if defined( USE_SHADOWMAP ) && ( UNROLLED_LOOP_INDEX < NUM_SPOT_LIGHT_SHADOWS )
		spotLightShadow = spotLightShadows[ i ];
		directLight.color *= all( bvec2( directLight.visible, receiveShadow ) ) ? getShadow( spotShadowMap[ i ], spotLightShadow.shadowMapSize, spotLightShadow.shadowBias, spotLightShadow.shadowRadius, vSpotLightCoord[ i ] ) : 1.0;
		#endif
		RE_Direct( directLight, geometry, material, reflectedLight );
	}
	#pragma unroll_loop_end
#endif
#if ( NUM_DIR_LIGHTS > 0 ) && defined( RE_Direct )
	DirectionalLight directionalLight;
	#if defined( USE_SHADOWMAP ) && NUM_DIR_LIGHT_SHADOWS > 0
	DirectionalLightShadow directionalLightShadow;
	#endif
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {
		directionalLight = directionalLights[ i ];
		getDirectionalLightInfo( directionalLight, geometry, directLight );
		#if defined( USE_SHADOWMAP ) && ( UNROLLED_LOOP_INDEX < NUM_DIR_LIGHT_SHADOWS )
		directionalLightShadow = directionalLightShadows[ i ];
		directLight.color *= all( bvec2( directLight.visible, receiveShadow ) ) ? getShadow( directionalShadowMap[ i ], directionalLightShadow.shadowMapSize, directionalLightShadow.shadowBias, directionalLightShadow.shadowRadius, vDirectionalShadowCoord[ i ] ) : 1.0;
		#endif
		RE_Direct( directLight, geometry, material, reflectedLight );
	}
	#pragma unroll_loop_end
#endif
#if ( NUM_RECT_AREA_LIGHTS > 0 ) && defined( RE_Direct_RectArea )
	RectAreaLight rectAreaLight;
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_RECT_AREA_LIGHTS; i ++ ) {
		rectAreaLight = rectAreaLights[ i ];
		RE_Direct_RectArea( rectAreaLight, geometry, material, reflectedLight );
	}
	#pragma unroll_loop_end
#endif
#if defined( RE_IndirectDiffuse )
	vec3 iblIrradiance = vec3( 0.0 );
	vec3 irradiance = getAmbientLightIrradiance( ambientLightColor );
	irradiance += getLightProbeIrradiance( lightProbe, geometry.normal );
	#if ( NUM_HEMI_LIGHTS > 0 )
		#pragma unroll_loop_start
		for ( int i = 0; i < NUM_HEMI_LIGHTS; i ++ ) {
			irradiance += getHemisphereLightIrradiance( hemisphereLights[ i ], geometry.normal );
		}
		#pragma unroll_loop_end
	#endif
#endif
#if defined( RE_IndirectSpecular )
	vec3 radiance = vec3( 0.0 );
	vec3 clearcoatRadiance = vec3( 0.0 );
#endif`,xu=`#if defined( RE_IndirectDiffuse )
	#ifdef USE_LIGHTMAP
		vec4 lightMapTexel = texture2D( lightMap, vUv2 );
		vec3 lightMapIrradiance = lightMapTexel.rgb * lightMapIntensity;
		irradiance += lightMapIrradiance;
	#endif
	#if defined( USE_ENVMAP ) && defined( STANDARD ) && defined( ENVMAP_TYPE_CUBE_UV )
		iblIrradiance += getIBLIrradiance( geometry.normal );
	#endif
#endif
#if defined( USE_ENVMAP ) && defined( RE_IndirectSpecular )
	radiance += getIBLRadiance( geometry.viewDir, geometry.normal, material.roughness );
	#ifdef USE_CLEARCOAT
		clearcoatRadiance += getIBLRadiance( geometry.viewDir, geometry.clearcoatNormal, material.clearcoatRoughness );
	#endif
#endif`,Mu=`#if defined( RE_IndirectDiffuse )
	RE_IndirectDiffuse( irradiance, geometry, material, reflectedLight );
#endif
#if defined( RE_IndirectSpecular )
	RE_IndirectSpecular( radiance, iblIrradiance, clearcoatRadiance, geometry, material, reflectedLight );
#endif`,yu=`#if defined( USE_LOGDEPTHBUF ) && defined( USE_LOGDEPTHBUF_EXT )
	gl_FragDepthEXT = vIsPerspective == 0.0 ? gl_FragCoord.z : log2( vFragDepth ) * logDepthBufFC * 0.5;
#endif`,Au=`#if defined( USE_LOGDEPTHBUF ) && defined( USE_LOGDEPTHBUF_EXT )
	uniform float logDepthBufFC;
	varying float vFragDepth;
	varying float vIsPerspective;
#endif`,_u=`#ifdef USE_LOGDEPTHBUF
	#ifdef USE_LOGDEPTHBUF_EXT
		varying float vFragDepth;
		varying float vIsPerspective;
	#else
		uniform float logDepthBufFC;
	#endif
#endif`,bu=`#ifdef USE_LOGDEPTHBUF
	#ifdef USE_LOGDEPTHBUF_EXT
		vFragDepth = 1.0 + gl_Position.w;
		vIsPerspective = float( isPerspectiveMatrix( projectionMatrix ) );
	#else
		if ( isPerspectiveMatrix( projectionMatrix ) ) {
			gl_Position.z = log2( max( EPSILON, gl_Position.w + 1.0 ) ) * logDepthBufFC - 1.0;
			gl_Position.z *= gl_Position.w;
		}
	#endif
#endif`,Su=`#ifdef USE_MAP
	vec4 sampledDiffuseColor = texture2D( map, vUv );
	#ifdef DECODE_VIDEO_TEXTURE
		sampledDiffuseColor = vec4( mix( pow( sampledDiffuseColor.rgb * 0.9478672986 + vec3( 0.0521327014 ), vec3( 2.4 ) ), sampledDiffuseColor.rgb * 0.0773993808, vec3( lessThanEqual( sampledDiffuseColor.rgb, vec3( 0.04045 ) ) ) ), sampledDiffuseColor.w );
	#endif
	diffuseColor *= sampledDiffuseColor;
#endif`,wu=`#ifdef USE_MAP
	uniform sampler2D map;
#endif`,Eu=`#if defined( USE_MAP ) || defined( USE_ALPHAMAP )
	vec2 uv = ( uvTransform * vec3( gl_PointCoord.x, 1.0 - gl_PointCoord.y, 1 ) ).xy;
#endif
#ifdef USE_MAP
	diffuseColor *= texture2D( map, uv );
#endif
#ifdef USE_ALPHAMAP
	diffuseColor.a *= texture2D( alphaMap, uv ).g;
#endif`,Tu=`#if defined( USE_MAP ) || defined( USE_ALPHAMAP )
	uniform mat3 uvTransform;
#endif
#ifdef USE_MAP
	uniform sampler2D map;
#endif
#ifdef USE_ALPHAMAP
	uniform sampler2D alphaMap;
#endif`,Iu=`float metalnessFactor = metalness;
#ifdef USE_METALNESSMAP
	vec4 texelMetalness = texture2D( metalnessMap, vUv );
	metalnessFactor *= texelMetalness.b;
#endif`,Ru=`#ifdef USE_METALNESSMAP
	uniform sampler2D metalnessMap;
#endif`,Cu=`#if defined( USE_MORPHCOLORS ) && defined( MORPHTARGETS_TEXTURE )
	vColor *= morphTargetBaseInfluence;
	for ( int i = 0; i < MORPHTARGETS_COUNT; i ++ ) {
		#if defined( USE_COLOR_ALPHA )
			if ( morphTargetInfluences[ i ] != 0.0 ) vColor += getMorph( gl_VertexID, i, 2 ) * morphTargetInfluences[ i ];
		#elif defined( USE_COLOR )
			if ( morphTargetInfluences[ i ] != 0.0 ) vColor += getMorph( gl_VertexID, i, 2 ).rgb * morphTargetInfluences[ i ];
		#endif
	}
#endif`,Lu=`#ifdef USE_MORPHNORMALS
	objectNormal *= morphTargetBaseInfluence;
	#ifdef MORPHTARGETS_TEXTURE
		for ( int i = 0; i < MORPHTARGETS_COUNT; i ++ ) {
			if ( morphTargetInfluences[ i ] != 0.0 ) objectNormal += getMorph( gl_VertexID, i, 1 ).xyz * morphTargetInfluences[ i ];
		}
	#else
		objectNormal += morphNormal0 * morphTargetInfluences[ 0 ];
		objectNormal += morphNormal1 * morphTargetInfluences[ 1 ];
		objectNormal += morphNormal2 * morphTargetInfluences[ 2 ];
		objectNormal += morphNormal3 * morphTargetInfluences[ 3 ];
	#endif
#endif`,Pu=`#ifdef USE_MORPHTARGETS
	uniform float morphTargetBaseInfluence;
	#ifdef MORPHTARGETS_TEXTURE
		uniform float morphTargetInfluences[ MORPHTARGETS_COUNT ];
		uniform sampler2DArray morphTargetsTexture;
		uniform ivec2 morphTargetsTextureSize;
		vec4 getMorph( const in int vertexIndex, const in int morphTargetIndex, const in int offset ) {
			int texelIndex = vertexIndex * MORPHTARGETS_TEXTURE_STRIDE + offset;
			int y = texelIndex / morphTargetsTextureSize.x;
			int x = texelIndex - y * morphTargetsTextureSize.x;
			ivec3 morphUV = ivec3( x, y, morphTargetIndex );
			return texelFetch( morphTargetsTexture, morphUV, 0 );
		}
	#else
		#ifndef USE_MORPHNORMALS
			uniform float morphTargetInfluences[ 8 ];
		#else
			uniform float morphTargetInfluences[ 4 ];
		#endif
	#endif
#endif`,zu=`#ifdef USE_MORPHTARGETS
	transformed *= morphTargetBaseInfluence;
	#ifdef MORPHTARGETS_TEXTURE
		for ( int i = 0; i < MORPHTARGETS_COUNT; i ++ ) {
			if ( morphTargetInfluences[ i ] != 0.0 ) transformed += getMorph( gl_VertexID, i, 0 ).xyz * morphTargetInfluences[ i ];
		}
	#else
		transformed += morphTarget0 * morphTargetInfluences[ 0 ];
		transformed += morphTarget1 * morphTargetInfluences[ 1 ];
		transformed += morphTarget2 * morphTargetInfluences[ 2 ];
		transformed += morphTarget3 * morphTargetInfluences[ 3 ];
		#ifndef USE_MORPHNORMALS
			transformed += morphTarget4 * morphTargetInfluences[ 4 ];
			transformed += morphTarget5 * morphTargetInfluences[ 5 ];
			transformed += morphTarget6 * morphTargetInfluences[ 6 ];
			transformed += morphTarget7 * morphTargetInfluences[ 7 ];
		#endif
	#endif
#endif`,Vu=`float faceDirection = gl_FrontFacing ? 1.0 : - 1.0;
#ifdef FLAT_SHADED
	vec3 fdx = dFdx( vViewPosition );
	vec3 fdy = dFdy( vViewPosition );
	vec3 normal = normalize( cross( fdx, fdy ) );
#else
	vec3 normal = normalize( vNormal );
	#ifdef DOUBLE_SIDED
		normal = normal * faceDirection;
	#endif
	#ifdef USE_TANGENT
		vec3 tangent = normalize( vTangent );
		vec3 bitangent = normalize( vBitangent );
		#ifdef DOUBLE_SIDED
			tangent = tangent * faceDirection;
			bitangent = bitangent * faceDirection;
		#endif
		#if defined( TANGENTSPACE_NORMALMAP ) || defined( USE_CLEARCOAT_NORMALMAP )
			mat3 vTBN = mat3( tangent, bitangent, normal );
		#endif
	#endif
#endif
vec3 geometryNormal = normal;`,Du=`#ifdef OBJECTSPACE_NORMALMAP
	normal = texture2D( normalMap, vUv ).xyz * 2.0 - 1.0;
	#ifdef FLIP_SIDED
		normal = - normal;
	#endif
	#ifdef DOUBLE_SIDED
		normal = normal * faceDirection;
	#endif
	normal = normalize( normalMatrix * normal );
#elif defined( TANGENTSPACE_NORMALMAP )
	vec3 mapN = texture2D( normalMap, vUv ).xyz * 2.0 - 1.0;
	mapN.xy *= normalScale;
	#ifdef USE_TANGENT
		normal = normalize( vTBN * mapN );
	#else
		normal = perturbNormal2Arb( - vViewPosition, normal, mapN, faceDirection );
	#endif
#elif defined( USE_BUMPMAP )
	normal = perturbNormalArb( - vViewPosition, normal, dHdxy_fwd(), faceDirection );
#endif`,Uu=`#ifndef FLAT_SHADED
	varying vec3 vNormal;
	#ifdef USE_TANGENT
		varying vec3 vTangent;
		varying vec3 vBitangent;
	#endif
#endif`,Nu=`#ifndef FLAT_SHADED
	varying vec3 vNormal;
	#ifdef USE_TANGENT
		varying vec3 vTangent;
		varying vec3 vBitangent;
	#endif
#endif`,Fu=`#ifndef FLAT_SHADED
	vNormal = normalize( transformedNormal );
	#ifdef USE_TANGENT
		vTangent = normalize( transformedTangent );
		vBitangent = normalize( cross( vNormal, vTangent ) * tangent.w );
	#endif
#endif`,qu=`#ifdef USE_NORMALMAP
	uniform sampler2D normalMap;
	uniform vec2 normalScale;
#endif
#ifdef OBJECTSPACE_NORMALMAP
	uniform mat3 normalMatrix;
#endif
#if ! defined ( USE_TANGENT ) && ( defined ( TANGENTSPACE_NORMALMAP ) || defined ( USE_CLEARCOAT_NORMALMAP ) )
	vec3 perturbNormal2Arb( vec3 eye_pos, vec3 surf_norm, vec3 mapN, float faceDirection ) {
		vec3 q0 = dFdx( eye_pos.xyz );
		vec3 q1 = dFdy( eye_pos.xyz );
		vec2 st0 = dFdx( vUv.st );
		vec2 st1 = dFdy( vUv.st );
		vec3 N = surf_norm;
		vec3 q1perp = cross( q1, N );
		vec3 q0perp = cross( N, q0 );
		vec3 T = q1perp * st0.x + q0perp * st1.x;
		vec3 B = q1perp * st0.y + q0perp * st1.y;
		float det = max( dot( T, T ), dot( B, B ) );
		float scale = ( det == 0.0 ) ? 0.0 : faceDirection * inversesqrt( det );
		return normalize( T * ( mapN.x * scale ) + B * ( mapN.y * scale ) + N * mapN.z );
	}
#endif`,Ou=`#ifdef USE_CLEARCOAT
	vec3 clearcoatNormal = geometryNormal;
#endif`,Bu=`#ifdef USE_CLEARCOAT_NORMALMAP
	vec3 clearcoatMapN = texture2D( clearcoatNormalMap, vUv ).xyz * 2.0 - 1.0;
	clearcoatMapN.xy *= clearcoatNormalScale;
	#ifdef USE_TANGENT
		clearcoatNormal = normalize( vTBN * clearcoatMapN );
	#else
		clearcoatNormal = perturbNormal2Arb( - vViewPosition, clearcoatNormal, clearcoatMapN, faceDirection );
	#endif
#endif`,ku=`#ifdef USE_CLEARCOATMAP
	uniform sampler2D clearcoatMap;
#endif
#ifdef USE_CLEARCOAT_ROUGHNESSMAP
	uniform sampler2D clearcoatRoughnessMap;
#endif
#ifdef USE_CLEARCOAT_NORMALMAP
	uniform sampler2D clearcoatNormalMap;
	uniform vec2 clearcoatNormalScale;
#endif`,Hu=`#ifdef USE_IRIDESCENCEMAP
	uniform sampler2D iridescenceMap;
#endif
#ifdef USE_IRIDESCENCE_THICKNESSMAP
	uniform sampler2D iridescenceThicknessMap;
#endif`,Zu=`#ifdef OPAQUE
diffuseColor.a = 1.0;
#endif
#ifdef USE_TRANSMISSION
diffuseColor.a *= material.transmissionAlpha + 0.1;
#endif
gl_FragColor = vec4( outgoingLight, diffuseColor.a );`,Gu=`vec3 packNormalToRGB( const in vec3 normal ) {
	return normalize( normal ) * 0.5 + 0.5;
}
vec3 unpackRGBToNormal( const in vec3 rgb ) {
	return 2.0 * rgb.xyz - 1.0;
}
const float PackUpscale = 256. / 255.;const float UnpackDownscale = 255. / 256.;
const vec3 PackFactors = vec3( 256. * 256. * 256., 256. * 256., 256. );
const vec4 UnpackFactors = UnpackDownscale / vec4( PackFactors, 1. );
const float ShiftRight8 = 1. / 256.;
vec4 packDepthToRGBA( const in float v ) {
	vec4 r = vec4( fract( v * PackFactors ), v );
	r.yzw -= r.xyz * ShiftRight8;	return r * PackUpscale;
}
float unpackRGBAToDepth( const in vec4 v ) {
	return dot( v, UnpackFactors );
}
vec2 packDepthToRG( in highp float v ) {
	return packDepthToRGBA( v ).yx;
}
float unpackRGToDepth( const in highp vec2 v ) {
	return unpackRGBAToDepth( vec4( v.xy, 0.0, 0.0 ) );
}
vec4 pack2HalfToRGBA( vec2 v ) {
	vec4 r = vec4( v.x, fract( v.x * 255.0 ), v.y, fract( v.y * 255.0 ) );
	return vec4( r.x - r.y / 255.0, r.y, r.z - r.w / 255.0, r.w );
}
vec2 unpackRGBATo2Half( vec4 v ) {
	return vec2( v.x + ( v.y / 255.0 ), v.z + ( v.w / 255.0 ) );
}
float viewZToOrthographicDepth( const in float viewZ, const in float near, const in float far ) {
	return ( viewZ + near ) / ( near - far );
}
float orthographicDepthToViewZ( const in float linearClipZ, const in float near, const in float far ) {
	return linearClipZ * ( near - far ) - near;
}
float viewZToPerspectiveDepth( const in float viewZ, const in float near, const in float far ) {
	return ( ( near + viewZ ) * far ) / ( ( far - near ) * viewZ );
}
float perspectiveDepthToViewZ( const in float invClipZ, const in float near, const in float far ) {
	return ( near * far ) / ( ( far - near ) * invClipZ - far );
}`,Wu=`#ifdef PREMULTIPLIED_ALPHA
	gl_FragColor.rgb *= gl_FragColor.a;
#endif`,Xu=`vec4 mvPosition = vec4( transformed, 1.0 );
#ifdef USE_INSTANCING
	mvPosition = instanceMatrix * mvPosition;
#endif
mvPosition = modelViewMatrix * mvPosition;
gl_Position = projectionMatrix * mvPosition;`,ju=`#ifdef DITHERING
	gl_FragColor.rgb = dithering( gl_FragColor.rgb );
#endif`,Yu=`#ifdef DITHERING
	vec3 dithering( vec3 color ) {
		float grid_position = rand( gl_FragCoord.xy );
		vec3 dither_shift_RGB = vec3( 0.25 / 255.0, -0.25 / 255.0, 0.25 / 255.0 );
		dither_shift_RGB = mix( 2.0 * dither_shift_RGB, -2.0 * dither_shift_RGB, grid_position );
		return color + dither_shift_RGB;
	}
#endif`,Ku=`float roughnessFactor = roughness;
#ifdef USE_ROUGHNESSMAP
	vec4 texelRoughness = texture2D( roughnessMap, vUv );
	roughnessFactor *= texelRoughness.g;
#endif`,Qu=`#ifdef USE_ROUGHNESSMAP
	uniform sampler2D roughnessMap;
#endif`,Ju=`#if NUM_SPOT_LIGHT_COORDS > 0
  varying vec4 vSpotLightCoord[ NUM_SPOT_LIGHT_COORDS ];
#endif
#if NUM_SPOT_LIGHT_MAPS > 0
  uniform sampler2D spotLightMap[ NUM_SPOT_LIGHT_MAPS ];
#endif
#ifdef USE_SHADOWMAP
	#if NUM_DIR_LIGHT_SHADOWS > 0
		uniform sampler2D directionalShadowMap[ NUM_DIR_LIGHT_SHADOWS ];
		varying vec4 vDirectionalShadowCoord[ NUM_DIR_LIGHT_SHADOWS ];
		struct DirectionalLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
		};
		uniform DirectionalLightShadow directionalLightShadows[ NUM_DIR_LIGHT_SHADOWS ];
	#endif
	#if NUM_SPOT_LIGHT_SHADOWS > 0
		uniform sampler2D spotShadowMap[ NUM_SPOT_LIGHT_SHADOWS ];
		struct SpotLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
		};
		uniform SpotLightShadow spotLightShadows[ NUM_SPOT_LIGHT_SHADOWS ];
	#endif
	#if NUM_POINT_LIGHT_SHADOWS > 0
		uniform sampler2D pointShadowMap[ NUM_POINT_LIGHT_SHADOWS ];
		varying vec4 vPointShadowCoord[ NUM_POINT_LIGHT_SHADOWS ];
		struct PointLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
			float shadowCameraNear;
			float shadowCameraFar;
		};
		uniform PointLightShadow pointLightShadows[ NUM_POINT_LIGHT_SHADOWS ];
	#endif
	float texture2DCompare( sampler2D depths, vec2 uv, float compare ) {
		return step( compare, unpackRGBAToDepth( texture2D( depths, uv ) ) );
	}
	vec2 texture2DDistribution( sampler2D shadow, vec2 uv ) {
		return unpackRGBATo2Half( texture2D( shadow, uv ) );
	}
	float VSMShadow (sampler2D shadow, vec2 uv, float compare ){
		float occlusion = 1.0;
		vec2 distribution = texture2DDistribution( shadow, uv );
		float hard_shadow = step( compare , distribution.x );
		if (hard_shadow != 1.0 ) {
			float distance = compare - distribution.x ;
			float variance = max( 0.00000, distribution.y * distribution.y );
			float softness_probability = variance / (variance + distance * distance );			softness_probability = clamp( ( softness_probability - 0.3 ) / ( 0.95 - 0.3 ), 0.0, 1.0 );			occlusion = clamp( max( hard_shadow, softness_probability ), 0.0, 1.0 );
		}
		return occlusion;
	}
	float getShadow( sampler2D shadowMap, vec2 shadowMapSize, float shadowBias, float shadowRadius, vec4 shadowCoord ) {
		float shadow = 1.0;
		shadowCoord.xyz /= shadowCoord.w;
		shadowCoord.z += shadowBias;
		bvec4 inFrustumVec = bvec4 ( shadowCoord.x >= 0.0, shadowCoord.x <= 1.0, shadowCoord.y >= 0.0, shadowCoord.y <= 1.0 );
		bool inFrustum = all( inFrustumVec );
		bvec2 frustumTestVec = bvec2( inFrustum, shadowCoord.z <= 1.0 );
		bool frustumTest = all( frustumTestVec );
		if ( frustumTest ) {
		#if defined( SHADOWMAP_TYPE_PCF )
			vec2 texelSize = vec2( 1.0 ) / shadowMapSize;
			float dx0 = - texelSize.x * shadowRadius;
			float dy0 = - texelSize.y * shadowRadius;
			float dx1 = + texelSize.x * shadowRadius;
			float dy1 = + texelSize.y * shadowRadius;
			float dx2 = dx0 / 2.0;
			float dy2 = dy0 / 2.0;
			float dx3 = dx1 / 2.0;
			float dy3 = dy1 / 2.0;
			shadow = (
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, dy0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, dy0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx2, dy2 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy2 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx3, dy2 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, 0.0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx2, 0.0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy, shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx3, 0.0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, 0.0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx2, dy3 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy3 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx3, dy3 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, dy1 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy1 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, dy1 ), shadowCoord.z )
			) * ( 1.0 / 17.0 );
		#elif defined( SHADOWMAP_TYPE_PCF_SOFT )
			vec2 texelSize = vec2( 1.0 ) / shadowMapSize;
			float dx = texelSize.x;
			float dy = texelSize.y;
			vec2 uv = shadowCoord.xy;
			vec2 f = fract( uv * shadowMapSize + 0.5 );
			uv -= f * texelSize;
			shadow = (
				texture2DCompare( shadowMap, uv, shadowCoord.z ) +
				texture2DCompare( shadowMap, uv + vec2( dx, 0.0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, uv + vec2( 0.0, dy ), shadowCoord.z ) +
				texture2DCompare( shadowMap, uv + texelSize, shadowCoord.z ) +
				mix( texture2DCompare( shadowMap, uv + vec2( -dx, 0.0 ), shadowCoord.z ),
					 texture2DCompare( shadowMap, uv + vec2( 2.0 * dx, 0.0 ), shadowCoord.z ),
					 f.x ) +
				mix( texture2DCompare( shadowMap, uv + vec2( -dx, dy ), shadowCoord.z ),
					 texture2DCompare( shadowMap, uv + vec2( 2.0 * dx, dy ), shadowCoord.z ),
					 f.x ) +
				mix( texture2DCompare( shadowMap, uv + vec2( 0.0, -dy ), shadowCoord.z ),
					 texture2DCompare( shadowMap, uv + vec2( 0.0, 2.0 * dy ), shadowCoord.z ),
					 f.y ) +
				mix( texture2DCompare( shadowMap, uv + vec2( dx, -dy ), shadowCoord.z ),
					 texture2DCompare( shadowMap, uv + vec2( dx, 2.0 * dy ), shadowCoord.z ),
					 f.y ) +
				mix( mix( texture2DCompare( shadowMap, uv + vec2( -dx, -dy ), shadowCoord.z ),
						  texture2DCompare( shadowMap, uv + vec2( 2.0 * dx, -dy ), shadowCoord.z ),
						  f.x ),
					 mix( texture2DCompare( shadowMap, uv + vec2( -dx, 2.0 * dy ), shadowCoord.z ),
						  texture2DCompare( shadowMap, uv + vec2( 2.0 * dx, 2.0 * dy ), shadowCoord.z ),
						  f.x ),
					 f.y )
			) * ( 1.0 / 9.0 );
		#elif defined( SHADOWMAP_TYPE_VSM )
			shadow = VSMShadow( shadowMap, shadowCoord.xy, shadowCoord.z );
		#else
			shadow = texture2DCompare( shadowMap, shadowCoord.xy, shadowCoord.z );
		#endif
		}
		return shadow;
	}
	vec2 cubeToUV( vec3 v, float texelSizeY ) {
		vec3 absV = abs( v );
		float scaleToCube = 1.0 / max( absV.x, max( absV.y, absV.z ) );
		absV *= scaleToCube;
		v *= scaleToCube * ( 1.0 - 2.0 * texelSizeY );
		vec2 planar = v.xy;
		float almostATexel = 1.5 * texelSizeY;
		float almostOne = 1.0 - almostATexel;
		if ( absV.z >= almostOne ) {
			if ( v.z > 0.0 )
				planar.x = 4.0 - v.x;
		} else if ( absV.x >= almostOne ) {
			float signX = sign( v.x );
			planar.x = v.z * signX + 2.0 * signX;
		} else if ( absV.y >= almostOne ) {
			float signY = sign( v.y );
			planar.x = v.x + 2.0 * signY + 2.0;
			planar.y = v.z * signY - 2.0;
		}
		return vec2( 0.125, 0.25 ) * planar + vec2( 0.375, 0.75 );
	}
	float getPointShadow( sampler2D shadowMap, vec2 shadowMapSize, float shadowBias, float shadowRadius, vec4 shadowCoord, float shadowCameraNear, float shadowCameraFar ) {
		vec2 texelSize = vec2( 1.0 ) / ( shadowMapSize * vec2( 4.0, 2.0 ) );
		vec3 lightToPosition = shadowCoord.xyz;
		float dp = ( length( lightToPosition ) - shadowCameraNear ) / ( shadowCameraFar - shadowCameraNear );		dp += shadowBias;
		vec3 bd3D = normalize( lightToPosition );
		#if defined( SHADOWMAP_TYPE_PCF ) || defined( SHADOWMAP_TYPE_PCF_SOFT ) || defined( SHADOWMAP_TYPE_VSM )
			vec2 offset = vec2( - 1, 1 ) * shadowRadius * texelSize.y;
			return (
				texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xyy, texelSize.y ), dp ) +
				texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yyy, texelSize.y ), dp ) +
				texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xyx, texelSize.y ), dp ) +
				texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yyx, texelSize.y ), dp ) +
				texture2DCompare( shadowMap, cubeToUV( bd3D, texelSize.y ), dp ) +
				texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xxy, texelSize.y ), dp ) +
				texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yxy, texelSize.y ), dp ) +
				texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xxx, texelSize.y ), dp ) +
				texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yxx, texelSize.y ), dp )
			) * ( 1.0 / 9.0 );
		#else
			return texture2DCompare( shadowMap, cubeToUV( bd3D, texelSize.y ), dp );
		#endif
	}
#endif`,$u=`#if NUM_SPOT_LIGHT_COORDS > 0
  uniform mat4 spotLightMatrix[ NUM_SPOT_LIGHT_COORDS ];
  varying vec4 vSpotLightCoord[ NUM_SPOT_LIGHT_COORDS ];
#endif
#ifdef USE_SHADOWMAP
	#if NUM_DIR_LIGHT_SHADOWS > 0
		uniform mat4 directionalShadowMatrix[ NUM_DIR_LIGHT_SHADOWS ];
		varying vec4 vDirectionalShadowCoord[ NUM_DIR_LIGHT_SHADOWS ];
		struct DirectionalLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
		};
		uniform DirectionalLightShadow directionalLightShadows[ NUM_DIR_LIGHT_SHADOWS ];
	#endif
	#if NUM_SPOT_LIGHT_SHADOWS > 0
		struct SpotLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
		};
		uniform SpotLightShadow spotLightShadows[ NUM_SPOT_LIGHT_SHADOWS ];
	#endif
	#if NUM_POINT_LIGHT_SHADOWS > 0
		uniform mat4 pointShadowMatrix[ NUM_POINT_LIGHT_SHADOWS ];
		varying vec4 vPointShadowCoord[ NUM_POINT_LIGHT_SHADOWS ];
		struct PointLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
			float shadowCameraNear;
			float shadowCameraFar;
		};
		uniform PointLightShadow pointLightShadows[ NUM_POINT_LIGHT_SHADOWS ];
	#endif
#endif`,ed=`#if defined( USE_SHADOWMAP ) || ( NUM_SPOT_LIGHT_COORDS > 0 )
	#if NUM_DIR_LIGHT_SHADOWS > 0 || NUM_SPOT_LIGHT_COORDS > 0 || NUM_POINT_LIGHT_SHADOWS > 0
		vec3 shadowWorldNormal = inverseTransformDirection( transformedNormal, viewMatrix );
		vec4 shadowWorldPosition;
	#endif
	#if NUM_DIR_LIGHT_SHADOWS > 0
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_DIR_LIGHT_SHADOWS; i ++ ) {
		shadowWorldPosition = worldPosition + vec4( shadowWorldNormal * directionalLightShadows[ i ].shadowNormalBias, 0 );
		vDirectionalShadowCoord[ i ] = directionalShadowMatrix[ i ] * shadowWorldPosition;
	}
	#pragma unroll_loop_end
	#endif
	#if NUM_SPOT_LIGHT_COORDS > 0
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_SPOT_LIGHT_COORDS; i ++ ) {
		shadowWorldPosition = worldPosition;
		#if ( defined( USE_SHADOWMAP ) && UNROLLED_LOOP_INDEX < NUM_SPOT_LIGHT_SHADOWS )
			shadowWorldPosition.xyz += shadowWorldNormal * spotLightShadows[ i ].shadowNormalBias;
		#endif
		vSpotLightCoord[ i ] = spotLightMatrix[ i ] * shadowWorldPosition;
	}
	#pragma unroll_loop_end
	#endif
	#if NUM_POINT_LIGHT_SHADOWS > 0
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_POINT_LIGHT_SHADOWS; i ++ ) {
		shadowWorldPosition = worldPosition + vec4( shadowWorldNormal * pointLightShadows[ i ].shadowNormalBias, 0 );
		vPointShadowCoord[ i ] = pointShadowMatrix[ i ] * shadowWorldPosition;
	}
	#pragma unroll_loop_end
	#endif
#endif`,td=`float getShadowMask() {
	float shadow = 1.0;
	#ifdef USE_SHADOWMAP
	#if NUM_DIR_LIGHT_SHADOWS > 0
	DirectionalLightShadow directionalLight;
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_DIR_LIGHT_SHADOWS; i ++ ) {
		directionalLight = directionalLightShadows[ i ];
		shadow *= receiveShadow ? getShadow( directionalShadowMap[ i ], directionalLight.shadowMapSize, directionalLight.shadowBias, directionalLight.shadowRadius, vDirectionalShadowCoord[ i ] ) : 1.0;
	}
	#pragma unroll_loop_end
	#endif
	#if NUM_SPOT_LIGHT_SHADOWS > 0
	SpotLightShadow spotLight;
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_SPOT_LIGHT_SHADOWS; i ++ ) {
		spotLight = spotLightShadows[ i ];
		shadow *= receiveShadow ? getShadow( spotShadowMap[ i ], spotLight.shadowMapSize, spotLight.shadowBias, spotLight.shadowRadius, vSpotLightCoord[ i ] ) : 1.0;
	}
	#pragma unroll_loop_end
	#endif
	#if NUM_POINT_LIGHT_SHADOWS > 0
	PointLightShadow pointLight;
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_POINT_LIGHT_SHADOWS; i ++ ) {
		pointLight = pointLightShadows[ i ];
		shadow *= receiveShadow ? getPointShadow( pointShadowMap[ i ], pointLight.shadowMapSize, pointLight.shadowBias, pointLight.shadowRadius, vPointShadowCoord[ i ], pointLight.shadowCameraNear, pointLight.shadowCameraFar ) : 1.0;
	}
	#pragma unroll_loop_end
	#endif
	#endif
	return shadow;
}`,nd=`#ifdef USE_SKINNING
	mat4 boneMatX = getBoneMatrix( skinIndex.x );
	mat4 boneMatY = getBoneMatrix( skinIndex.y );
	mat4 boneMatZ = getBoneMatrix( skinIndex.z );
	mat4 boneMatW = getBoneMatrix( skinIndex.w );
#endif`,id=`#ifdef USE_SKINNING
	uniform mat4 bindMatrix;
	uniform mat4 bindMatrixInverse;
	uniform highp sampler2D boneTexture;
	uniform int boneTextureSize;
	mat4 getBoneMatrix( const in float i ) {
		float j = i * 4.0;
		float x = mod( j, float( boneTextureSize ) );
		float y = floor( j / float( boneTextureSize ) );
		float dx = 1.0 / float( boneTextureSize );
		float dy = 1.0 / float( boneTextureSize );
		y = dy * ( y + 0.5 );
		vec4 v1 = texture2D( boneTexture, vec2( dx * ( x + 0.5 ), y ) );
		vec4 v2 = texture2D( boneTexture, vec2( dx * ( x + 1.5 ), y ) );
		vec4 v3 = texture2D( boneTexture, vec2( dx * ( x + 2.5 ), y ) );
		vec4 v4 = texture2D( boneTexture, vec2( dx * ( x + 3.5 ), y ) );
		mat4 bone = mat4( v1, v2, v3, v4 );
		return bone;
	}
#endif`,rd=`#ifdef USE_SKINNING
	vec4 skinVertex = bindMatrix * vec4( transformed, 1.0 );
	vec4 skinned = vec4( 0.0 );
	skinned += boneMatX * skinVertex * skinWeight.x;
	skinned += boneMatY * skinVertex * skinWeight.y;
	skinned += boneMatZ * skinVertex * skinWeight.z;
	skinned += boneMatW * skinVertex * skinWeight.w;
	transformed = ( bindMatrixInverse * skinned ).xyz;
#endif`,sd=`#ifdef USE_SKINNING
	mat4 skinMatrix = mat4( 0.0 );
	skinMatrix += skinWeight.x * boneMatX;
	skinMatrix += skinWeight.y * boneMatY;
	skinMatrix += skinWeight.z * boneMatZ;
	skinMatrix += skinWeight.w * boneMatW;
	skinMatrix = bindMatrixInverse * skinMatrix * bindMatrix;
	objectNormal = vec4( skinMatrix * vec4( objectNormal, 0.0 ) ).xyz;
	#ifdef USE_TANGENT
		objectTangent = vec4( skinMatrix * vec4( objectTangent, 0.0 ) ).xyz;
	#endif
#endif`,ad=`float specularStrength;
#ifdef USE_SPECULARMAP
	vec4 texelSpecular = texture2D( specularMap, vUv );
	specularStrength = texelSpecular.r;
#else
	specularStrength = 1.0;
#endif`,od=`#ifdef USE_SPECULARMAP
	uniform sampler2D specularMap;
#endif`,ld=`#if defined( TONE_MAPPING )
	gl_FragColor.rgb = toneMapping( gl_FragColor.rgb );
#endif`,cd=`#ifndef saturate
#define saturate( a ) clamp( a, 0.0, 1.0 )
#endif
uniform float toneMappingExposure;
vec3 LinearToneMapping( vec3 color ) {
	return toneMappingExposure * color;
}
vec3 ReinhardToneMapping( vec3 color ) {
	color *= toneMappingExposure;
	return saturate( color / ( vec3( 1.0 ) + color ) );
}
vec3 OptimizedCineonToneMapping( vec3 color ) {
	color *= toneMappingExposure;
	color = max( vec3( 0.0 ), color - 0.004 );
	return pow( ( color * ( 6.2 * color + 0.5 ) ) / ( color * ( 6.2 * color + 1.7 ) + 0.06 ), vec3( 2.2 ) );
}
vec3 RRTAndODTFit( vec3 v ) {
	vec3 a = v * ( v + 0.0245786 ) - 0.000090537;
	vec3 b = v * ( 0.983729 * v + 0.4329510 ) + 0.238081;
	return a / b;
}
vec3 ACESFilmicToneMapping( vec3 color ) {
	const mat3 ACESInputMat = mat3(
		vec3( 0.59719, 0.07600, 0.02840 ),		vec3( 0.35458, 0.90834, 0.13383 ),
		vec3( 0.04823, 0.01566, 0.83777 )
	);
	const mat3 ACESOutputMat = mat3(
		vec3(  1.60475, -0.10208, -0.00327 ),		vec3( -0.53108,  1.10813, -0.07276 ),
		vec3( -0.07367, -0.00605,  1.07602 )
	);
	color *= toneMappingExposure / 0.6;
	color = ACESInputMat * color;
	color = RRTAndODTFit( color );
	color = ACESOutputMat * color;
	return saturate( color );
}
vec3 CustomToneMapping( vec3 color ) { return color; }`,ud=`#ifdef USE_TRANSMISSION
	material.transmission = transmission;
	material.transmissionAlpha = 1.0;
	material.thickness = thickness;
	material.attenuationDistance = attenuationDistance;
	material.attenuationColor = attenuationColor;
	#ifdef USE_TRANSMISSIONMAP
		material.transmission *= texture2D( transmissionMap, vUv ).r;
	#endif
	#ifdef USE_THICKNESSMAP
		material.thickness *= texture2D( thicknessMap, vUv ).g;
	#endif
	vec3 pos = vWorldPosition;
	vec3 v = normalize( cameraPosition - pos );
	vec3 n = inverseTransformDirection( normal, viewMatrix );
	vec4 transmission = getIBLVolumeRefraction(
		n, v, material.roughness, material.diffuseColor, material.specularColor, material.specularF90,
		pos, modelMatrix, viewMatrix, projectionMatrix, material.ior, material.thickness,
		material.attenuationColor, material.attenuationDistance );
	material.transmissionAlpha = mix( material.transmissionAlpha, transmission.a, material.transmission );
	totalDiffuse = mix( totalDiffuse, transmission.rgb, material.transmission );
#endif`,dd=`#ifdef USE_TRANSMISSION
	uniform float transmission;
	uniform float thickness;
	uniform float attenuationDistance;
	uniform vec3 attenuationColor;
	#ifdef USE_TRANSMISSIONMAP
		uniform sampler2D transmissionMap;
	#endif
	#ifdef USE_THICKNESSMAP
		uniform sampler2D thicknessMap;
	#endif
	uniform vec2 transmissionSamplerSize;
	uniform sampler2D transmissionSamplerMap;
	uniform mat4 modelMatrix;
	uniform mat4 projectionMatrix;
	varying vec3 vWorldPosition;
	vec3 getVolumeTransmissionRay( const in vec3 n, const in vec3 v, const in float thickness, const in float ior, const in mat4 modelMatrix ) {
		vec3 refractionVector = refract( - v, normalize( n ), 1.0 / ior );
		vec3 modelScale;
		modelScale.x = length( vec3( modelMatrix[ 0 ].xyz ) );
		modelScale.y = length( vec3( modelMatrix[ 1 ].xyz ) );
		modelScale.z = length( vec3( modelMatrix[ 2 ].xyz ) );
		return normalize( refractionVector ) * thickness * modelScale;
	}
	float applyIorToRoughness( const in float roughness, const in float ior ) {
		return roughness * clamp( ior * 2.0 - 2.0, 0.0, 1.0 );
	}
	vec4 getTransmissionSample( const in vec2 fragCoord, const in float roughness, const in float ior ) {
		float framebufferLod = log2( transmissionSamplerSize.x ) * applyIorToRoughness( roughness, ior );
		#ifdef texture2DLodEXT
			return texture2DLodEXT( transmissionSamplerMap, fragCoord.xy, framebufferLod );
		#else
			return texture2D( transmissionSamplerMap, fragCoord.xy, framebufferLod );
		#endif
	}
	vec3 applyVolumeAttenuation( const in vec3 radiance, const in float transmissionDistance, const in vec3 attenuationColor, const in float attenuationDistance ) {
		if ( isinf( attenuationDistance ) ) {
			return radiance;
		} else {
			vec3 attenuationCoefficient = -log( attenuationColor ) / attenuationDistance;
			vec3 transmittance = exp( - attenuationCoefficient * transmissionDistance );			return transmittance * radiance;
		}
	}
	vec4 getIBLVolumeRefraction( const in vec3 n, const in vec3 v, const in float roughness, const in vec3 diffuseColor,
		const in vec3 specularColor, const in float specularF90, const in vec3 position, const in mat4 modelMatrix,
		const in mat4 viewMatrix, const in mat4 projMatrix, const in float ior, const in float thickness,
		const in vec3 attenuationColor, const in float attenuationDistance ) {
		vec3 transmissionRay = getVolumeTransmissionRay( n, v, thickness, ior, modelMatrix );
		vec3 refractedRayExit = position + transmissionRay;
		vec4 ndcPos = projMatrix * viewMatrix * vec4( refractedRayExit, 1.0 );
		vec2 refractionCoords = ndcPos.xy / ndcPos.w;
		refractionCoords += 1.0;
		refractionCoords /= 2.0;
		vec4 transmittedLight = getTransmissionSample( refractionCoords, roughness, ior );
		vec3 attenuatedColor = applyVolumeAttenuation( transmittedLight.rgb, length( transmissionRay ), attenuationColor, attenuationDistance );
		vec3 F = EnvironmentBRDF( n, v, specularColor, specularF90, roughness );
		return vec4( ( 1.0 - F ) * attenuatedColor * diffuseColor, transmittedLight.a );
	}
#endif`,hd=`#if ( defined( USE_UV ) && ! defined( UVS_VERTEX_ONLY ) )
	varying vec2 vUv;
#endif`,fd=`#ifdef USE_UV
	#ifdef UVS_VERTEX_ONLY
		vec2 vUv;
	#else
		varying vec2 vUv;
	#endif
	uniform mat3 uvTransform;
#endif`,pd=`#ifdef USE_UV
	vUv = ( uvTransform * vec3( uv, 1 ) ).xy;
#endif`,md=`#if defined( USE_LIGHTMAP ) || defined( USE_AOMAP )
	varying vec2 vUv2;
#endif`,gd=`#if defined( USE_LIGHTMAP ) || defined( USE_AOMAP )
	attribute vec2 uv2;
	varying vec2 vUv2;
	uniform mat3 uv2Transform;
#endif`,vd=`#if defined( USE_LIGHTMAP ) || defined( USE_AOMAP )
	vUv2 = ( uv2Transform * vec3( uv2, 1 ) ).xy;
#endif`,xd=`#if defined( USE_ENVMAP ) || defined( DISTANCE ) || defined ( USE_SHADOWMAP ) || defined ( USE_TRANSMISSION ) || NUM_SPOT_LIGHT_COORDS > 0
	vec4 worldPosition = vec4( transformed, 1.0 );
	#ifdef USE_INSTANCING
		worldPosition = instanceMatrix * worldPosition;
	#endif
	worldPosition = modelMatrix * worldPosition;
#endif`;const Md=`varying vec2 vUv;
uniform mat3 uvTransform;
void main() {
	vUv = ( uvTransform * vec3( uv, 1 ) ).xy;
	gl_Position = vec4( position.xy, 1.0, 1.0 );
}`,yd=`uniform sampler2D t2D;
uniform float backgroundIntensity;
varying vec2 vUv;
void main() {
	vec4 texColor = texture2D( t2D, vUv );
	#ifdef DECODE_VIDEO_TEXTURE
		texColor = vec4( mix( pow( texColor.rgb * 0.9478672986 + vec3( 0.0521327014 ), vec3( 2.4 ) ), texColor.rgb * 0.0773993808, vec3( lessThanEqual( texColor.rgb, vec3( 0.04045 ) ) ) ), texColor.w );
	#endif
	texColor.rgb *= backgroundIntensity;
	gl_FragColor = texColor;
	#include <tonemapping_fragment>
	#include <encodings_fragment>
}`,Ad=`varying vec3 vWorldDirection;
#include <common>
void main() {
	vWorldDirection = transformDirection( position, modelMatrix );
	#include <begin_vertex>
	#include <project_vertex>
	gl_Position.z = gl_Position.w;
}`,_d=`#ifdef ENVMAP_TYPE_CUBE
	uniform samplerCube envMap;
#elif defined( ENVMAP_TYPE_CUBE_UV )
	uniform sampler2D envMap;
#endif
uniform float flipEnvMap;
uniform float backgroundBlurriness;
uniform float backgroundIntensity;
varying vec3 vWorldDirection;
#include <cube_uv_reflection_fragment>
void main() {
	#ifdef ENVMAP_TYPE_CUBE
		vec4 texColor = textureCube( envMap, vec3( flipEnvMap * vWorldDirection.x, vWorldDirection.yz ) );
	#elif defined( ENVMAP_TYPE_CUBE_UV )
		vec4 texColor = textureCubeUV( envMap, vWorldDirection, backgroundBlurriness );
	#else
		vec4 texColor = vec4( 0.0, 0.0, 0.0, 1.0 );
	#endif
	texColor.rgb *= backgroundIntensity;
	gl_FragColor = texColor;
	#include <tonemapping_fragment>
	#include <encodings_fragment>
}`,bd=`varying vec3 vWorldDirection;
#include <common>
void main() {
	vWorldDirection = transformDirection( position, modelMatrix );
	#include <begin_vertex>
	#include <project_vertex>
	gl_Position.z = gl_Position.w;
}`,Sd=`uniform samplerCube tCube;
uniform float tFlip;
uniform float opacity;
varying vec3 vWorldDirection;
void main() {
	vec4 texColor = textureCube( tCube, vec3( tFlip * vWorldDirection.x, vWorldDirection.yz ) );
	gl_FragColor = texColor;
	gl_FragColor.a *= opacity;
	#include <tonemapping_fragment>
	#include <encodings_fragment>
}`,wd=`#include <common>
#include <uv_pars_vertex>
#include <displacementmap_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
varying vec2 vHighPrecisionZW;
void main() {
	#include <uv_vertex>
	#include <skinbase_vertex>
	#ifdef USE_DISPLACEMENTMAP
		#include <beginnormal_vertex>
		#include <morphnormal_vertex>
		#include <skinnormal_vertex>
	#endif
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	vHighPrecisionZW = gl_Position.zw;
}`,Ed=`#if DEPTH_PACKING == 3200
	uniform float opacity;
#endif
#include <common>
#include <packing>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
varying vec2 vHighPrecisionZW;
void main() {
	#include <clipping_planes_fragment>
	vec4 diffuseColor = vec4( 1.0 );
	#if DEPTH_PACKING == 3200
		diffuseColor.a = opacity;
	#endif
	#include <map_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <logdepthbuf_fragment>
	float fragCoordZ = 0.5 * vHighPrecisionZW[0] / vHighPrecisionZW[1] + 0.5;
	#if DEPTH_PACKING == 3200
		gl_FragColor = vec4( vec3( 1.0 - fragCoordZ ), opacity );
	#elif DEPTH_PACKING == 3201
		gl_FragColor = packDepthToRGBA( fragCoordZ );
	#endif
}`,Td=`#define DISTANCE
varying vec3 vWorldPosition;
#include <common>
#include <uv_pars_vertex>
#include <displacementmap_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <skinbase_vertex>
	#ifdef USE_DISPLACEMENTMAP
		#include <beginnormal_vertex>
		#include <morphnormal_vertex>
		#include <skinnormal_vertex>
	#endif
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <worldpos_vertex>
	#include <clipping_planes_vertex>
	vWorldPosition = worldPosition.xyz;
}`,Id=`#define DISTANCE
uniform vec3 referencePosition;
uniform float nearDistance;
uniform float farDistance;
varying vec3 vWorldPosition;
#include <common>
#include <packing>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <clipping_planes_pars_fragment>
void main () {
	#include <clipping_planes_fragment>
	vec4 diffuseColor = vec4( 1.0 );
	#include <map_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	float dist = length( vWorldPosition - referencePosition );
	dist = ( dist - nearDistance ) / ( farDistance - nearDistance );
	dist = saturate( dist );
	gl_FragColor = packDepthToRGBA( dist );
}`,Rd=`varying vec3 vWorldDirection;
#include <common>
void main() {
	vWorldDirection = transformDirection( position, modelMatrix );
	#include <begin_vertex>
	#include <project_vertex>
}`,Cd=`uniform sampler2D tEquirect;
varying vec3 vWorldDirection;
#include <common>
void main() {
	vec3 direction = normalize( vWorldDirection );
	vec2 sampleUV = equirectUv( direction );
	gl_FragColor = texture2D( tEquirect, sampleUV );
	#include <tonemapping_fragment>
	#include <encodings_fragment>
}`,Ld=`uniform float scale;
attribute float lineDistance;
varying float vLineDistance;
#include <common>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	vLineDistance = scale * lineDistance;
	#include <color_vertex>
	#include <morphcolor_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <fog_vertex>
}`,Pd=`uniform vec3 diffuse;
uniform float opacity;
uniform float dashSize;
uniform float totalSize;
varying float vLineDistance;
#include <common>
#include <color_pars_fragment>
#include <fog_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	if ( mod( vLineDistance, totalSize ) > dashSize ) {
		discard;
	}
	vec3 outgoingLight = vec3( 0.0 );
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <logdepthbuf_fragment>
	#include <color_fragment>
	outgoingLight = diffuseColor.rgb;
	#include <output_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
}`,zd=`#include <common>
#include <uv_pars_vertex>
#include <uv2_pars_vertex>
#include <envmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <uv2_vertex>
	#include <color_vertex>
	#include <morphcolor_vertex>
	#if defined ( USE_ENVMAP ) || defined ( USE_SKINNING )
		#include <beginnormal_vertex>
		#include <morphnormal_vertex>
		#include <skinbase_vertex>
		#include <skinnormal_vertex>
		#include <defaultnormal_vertex>
	#endif
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <worldpos_vertex>
	#include <envmap_vertex>
	#include <fog_vertex>
}`,Vd=`uniform vec3 diffuse;
uniform float opacity;
#ifndef FLAT_SHADED
	varying vec3 vNormal;
#endif
#include <common>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <uv2_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <envmap_common_pars_fragment>
#include <envmap_pars_fragment>
#include <fog_pars_fragment>
#include <specularmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <specularmap_fragment>
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	#ifdef USE_LIGHTMAP
		vec4 lightMapTexel = texture2D( lightMap, vUv2 );
		reflectedLight.indirectDiffuse += lightMapTexel.rgb * lightMapIntensity * RECIPROCAL_PI;
	#else
		reflectedLight.indirectDiffuse += vec3( 1.0 );
	#endif
	#include <aomap_fragment>
	reflectedLight.indirectDiffuse *= diffuseColor.rgb;
	vec3 outgoingLight = reflectedLight.indirectDiffuse;
	#include <envmap_fragment>
	#include <output_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,Dd=`#define LAMBERT
varying vec3 vViewPosition;
#include <common>
#include <uv_pars_vertex>
#include <uv2_pars_vertex>
#include <displacementmap_pars_vertex>
#include <envmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <uv2_vertex>
	#include <color_vertex>
	#include <morphcolor_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	vViewPosition = - mvPosition.xyz;
	#include <worldpos_vertex>
	#include <envmap_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>
}`,Ud=`#define LAMBERT
uniform vec3 diffuse;
uniform vec3 emissive;
uniform float opacity;
#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <uv2_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <envmap_common_pars_fragment>
#include <envmap_pars_fragment>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars_begin>
#include <normal_pars_fragment>
#include <lights_lambert_pars_fragment>
#include <shadowmap_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <specularmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	vec4 diffuseColor = vec4( diffuse, opacity );
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	vec3 totalEmissiveRadiance = emissive;
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <specularmap_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	#include <emissivemap_fragment>
	#include <lights_lambert_fragment>
	#include <lights_fragment_begin>
	#include <lights_fragment_maps>
	#include <lights_fragment_end>
	#include <aomap_fragment>
	vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + totalEmissiveRadiance;
	#include <envmap_fragment>
	#include <output_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,Nd=`#define MATCAP
varying vec3 vViewPosition;
#include <common>
#include <uv_pars_vertex>
#include <color_pars_vertex>
#include <displacementmap_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <color_vertex>
	#include <morphcolor_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <fog_vertex>
	vViewPosition = - mvPosition.xyz;
}`,Fd=`#define MATCAP
uniform vec3 diffuse;
uniform float opacity;
uniform sampler2D matcap;
varying vec3 vViewPosition;
#include <common>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <fog_pars_fragment>
#include <normal_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	vec3 viewDir = normalize( vViewPosition );
	vec3 x = normalize( vec3( viewDir.z, 0.0, - viewDir.x ) );
	vec3 y = cross( viewDir, x );
	vec2 uv = vec2( dot( x, normal ), dot( y, normal ) ) * 0.495 + 0.5;
	#ifdef USE_MATCAP
		vec4 matcapColor = texture2D( matcap, uv );
	#else
		vec4 matcapColor = vec4( vec3( mix( 0.2, 0.8, uv.y ) ), 1.0 );
	#endif
	vec3 outgoingLight = diffuseColor.rgb * matcapColor.rgb;
	#include <output_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,qd=`#define NORMAL
#if defined( FLAT_SHADED ) || defined( USE_BUMPMAP ) || defined( TANGENTSPACE_NORMALMAP )
	varying vec3 vViewPosition;
#endif
#include <common>
#include <uv_pars_vertex>
#include <displacementmap_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
#if defined( FLAT_SHADED ) || defined( USE_BUMPMAP ) || defined( TANGENTSPACE_NORMALMAP )
	vViewPosition = - mvPosition.xyz;
#endif
}`,Od=`#define NORMAL
uniform float opacity;
#if defined( FLAT_SHADED ) || defined( USE_BUMPMAP ) || defined( TANGENTSPACE_NORMALMAP )
	varying vec3 vViewPosition;
#endif
#include <packing>
#include <uv_pars_fragment>
#include <normal_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	#include <logdepthbuf_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	gl_FragColor = vec4( packNormalToRGB( normal ), opacity );
	#ifdef OPAQUE
		gl_FragColor.a = 1.0;
	#endif
}`,Bd=`#define PHONG
varying vec3 vViewPosition;
#include <common>
#include <uv_pars_vertex>
#include <uv2_pars_vertex>
#include <displacementmap_pars_vertex>
#include <envmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <uv2_vertex>
	#include <color_vertex>
	#include <morphcolor_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	vViewPosition = - mvPosition.xyz;
	#include <worldpos_vertex>
	#include <envmap_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>
}`,kd=`#define PHONG
uniform vec3 diffuse;
uniform vec3 emissive;
uniform vec3 specular;
uniform float shininess;
uniform float opacity;
#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <uv2_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <envmap_common_pars_fragment>
#include <envmap_pars_fragment>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars_begin>
#include <normal_pars_fragment>
#include <lights_phong_pars_fragment>
#include <shadowmap_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <specularmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	vec4 diffuseColor = vec4( diffuse, opacity );
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	vec3 totalEmissiveRadiance = emissive;
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <specularmap_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	#include <emissivemap_fragment>
	#include <lights_phong_fragment>
	#include <lights_fragment_begin>
	#include <lights_fragment_maps>
	#include <lights_fragment_end>
	#include <aomap_fragment>
	vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;
	#include <envmap_fragment>
	#include <output_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,Hd=`#define STANDARD
varying vec3 vViewPosition;
#ifdef USE_TRANSMISSION
	varying vec3 vWorldPosition;
#endif
#include <common>
#include <uv_pars_vertex>
#include <uv2_pars_vertex>
#include <displacementmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <uv2_vertex>
	#include <color_vertex>
	#include <morphcolor_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	vViewPosition = - mvPosition.xyz;
	#include <worldpos_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>
#ifdef USE_TRANSMISSION
	vWorldPosition = worldPosition.xyz;
#endif
}`,Zd=`#define STANDARD
#ifdef PHYSICAL
	#define IOR
	#define SPECULAR
#endif
uniform vec3 diffuse;
uniform vec3 emissive;
uniform float roughness;
uniform float metalness;
uniform float opacity;
#ifdef IOR
	uniform float ior;
#endif
#ifdef SPECULAR
	uniform float specularIntensity;
	uniform vec3 specularColor;
	#ifdef USE_SPECULARINTENSITYMAP
		uniform sampler2D specularIntensityMap;
	#endif
	#ifdef USE_SPECULARCOLORMAP
		uniform sampler2D specularColorMap;
	#endif
#endif
#ifdef USE_CLEARCOAT
	uniform float clearcoat;
	uniform float clearcoatRoughness;
#endif
#ifdef USE_IRIDESCENCE
	uniform float iridescence;
	uniform float iridescenceIOR;
	uniform float iridescenceThicknessMinimum;
	uniform float iridescenceThicknessMaximum;
#endif
#ifdef USE_SHEEN
	uniform vec3 sheenColor;
	uniform float sheenRoughness;
	#ifdef USE_SHEENCOLORMAP
		uniform sampler2D sheenColorMap;
	#endif
	#ifdef USE_SHEENROUGHNESSMAP
		uniform sampler2D sheenRoughnessMap;
	#endif
#endif
varying vec3 vViewPosition;
#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <uv2_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <bsdfs>
#include <iridescence_fragment>
#include <cube_uv_reflection_fragment>
#include <envmap_common_pars_fragment>
#include <envmap_physical_pars_fragment>
#include <fog_pars_fragment>
#include <lights_pars_begin>
#include <normal_pars_fragment>
#include <lights_physical_pars_fragment>
#include <transmission_pars_fragment>
#include <shadowmap_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <clearcoat_pars_fragment>
#include <iridescence_pars_fragment>
#include <roughnessmap_pars_fragment>
#include <metalnessmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	vec4 diffuseColor = vec4( diffuse, opacity );
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	vec3 totalEmissiveRadiance = emissive;
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <roughnessmap_fragment>
	#include <metalnessmap_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	#include <clearcoat_normal_fragment_begin>
	#include <clearcoat_normal_fragment_maps>
	#include <emissivemap_fragment>
	#include <lights_physical_fragment>
	#include <lights_fragment_begin>
	#include <lights_fragment_maps>
	#include <lights_fragment_end>
	#include <aomap_fragment>
	vec3 totalDiffuse = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse;
	vec3 totalSpecular = reflectedLight.directSpecular + reflectedLight.indirectSpecular;
	#include <transmission_fragment>
	vec3 outgoingLight = totalDiffuse + totalSpecular + totalEmissiveRadiance;
	#ifdef USE_SHEEN
		float sheenEnergyComp = 1.0 - 0.157 * max3( material.sheenColor );
		outgoingLight = outgoingLight * sheenEnergyComp + sheenSpecular;
	#endif
	#ifdef USE_CLEARCOAT
		float dotNVcc = saturate( dot( geometry.clearcoatNormal, geometry.viewDir ) );
		vec3 Fcc = F_Schlick( material.clearcoatF0, material.clearcoatF90, dotNVcc );
		outgoingLight = outgoingLight * ( 1.0 - material.clearcoat * Fcc ) + clearcoatSpecular * material.clearcoat;
	#endif
	#include <output_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,Gd=`#define TOON
varying vec3 vViewPosition;
#include <common>
#include <uv_pars_vertex>
#include <uv2_pars_vertex>
#include <displacementmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <uv2_vertex>
	#include <color_vertex>
	#include <morphcolor_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	vViewPosition = - mvPosition.xyz;
	#include <worldpos_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>
}`,Wd=`#define TOON
uniform vec3 diffuse;
uniform vec3 emissive;
uniform float opacity;
#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <uv2_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <gradientmap_pars_fragment>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars_begin>
#include <normal_pars_fragment>
#include <lights_toon_pars_fragment>
#include <shadowmap_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	vec4 diffuseColor = vec4( diffuse, opacity );
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	vec3 totalEmissiveRadiance = emissive;
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	#include <emissivemap_fragment>
	#include <lights_toon_fragment>
	#include <lights_fragment_begin>
	#include <lights_fragment_maps>
	#include <lights_fragment_end>
	#include <aomap_fragment>
	vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + totalEmissiveRadiance;
	#include <output_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,Xd=`uniform float size;
uniform float scale;
#include <common>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <color_vertex>
	#include <morphcolor_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <project_vertex>
	gl_PointSize = size;
	#ifdef USE_SIZEATTENUATION
		bool isPerspective = isPerspectiveMatrix( projectionMatrix );
		if ( isPerspective ) gl_PointSize *= ( scale / - mvPosition.z );
	#endif
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <worldpos_vertex>
	#include <fog_vertex>
}`,jd=`uniform vec3 diffuse;
uniform float opacity;
#include <common>
#include <color_pars_fragment>
#include <map_particle_pars_fragment>
#include <alphatest_pars_fragment>
#include <fog_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	vec3 outgoingLight = vec3( 0.0 );
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <logdepthbuf_fragment>
	#include <map_particle_fragment>
	#include <color_fragment>
	#include <alphatest_fragment>
	outgoingLight = diffuseColor.rgb;
	#include <output_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
}`,Yd=`#include <common>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
void main() {
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <project_vertex>
	#include <worldpos_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>
}`,Kd=`uniform vec3 color;
uniform float opacity;
#include <common>
#include <packing>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars_begin>
#include <shadowmap_pars_fragment>
#include <shadowmask_pars_fragment>
void main() {
	gl_FragColor = vec4( color, opacity * ( 1.0 - getShadowMask() ) );
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
}`,Qd=`uniform float rotation;
uniform vec2 center;
#include <common>
#include <uv_pars_vertex>
#include <fog_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	vec4 mvPosition = modelViewMatrix * vec4( 0.0, 0.0, 0.0, 1.0 );
	vec2 scale;
	scale.x = length( vec3( modelMatrix[ 0 ].x, modelMatrix[ 0 ].y, modelMatrix[ 0 ].z ) );
	scale.y = length( vec3( modelMatrix[ 1 ].x, modelMatrix[ 1 ].y, modelMatrix[ 1 ].z ) );
	#ifndef USE_SIZEATTENUATION
		bool isPerspective = isPerspectiveMatrix( projectionMatrix );
		if ( isPerspective ) scale *= - mvPosition.z;
	#endif
	vec2 alignedPosition = ( position.xy - ( center - vec2( 0.5 ) ) ) * scale;
	vec2 rotatedPosition;
	rotatedPosition.x = cos( rotation ) * alignedPosition.x - sin( rotation ) * alignedPosition.y;
	rotatedPosition.y = sin( rotation ) * alignedPosition.x + cos( rotation ) * alignedPosition.y;
	mvPosition.xy += rotatedPosition;
	gl_Position = projectionMatrix * mvPosition;
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <fog_vertex>
}`,Jd=`uniform vec3 diffuse;
uniform float opacity;
#include <common>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <fog_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	vec3 outgoingLight = vec3( 0.0 );
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	outgoingLight = diffuseColor.rgb;
	#include <output_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
}`,_e={alphamap_fragment:Ac,alphamap_pars_fragment:_c,alphatest_fragment:bc,alphatest_pars_fragment:Sc,aomap_fragment:wc,aomap_pars_fragment:Ec,begin_vertex:Tc,beginnormal_vertex:Ic,bsdfs:Rc,iridescence_fragment:Cc,bumpmap_pars_fragment:Lc,clipping_planes_fragment:Pc,clipping_planes_pars_fragment:zc,clipping_planes_pars_vertex:Vc,clipping_planes_vertex:Dc,color_fragment:Uc,color_pars_fragment:Nc,color_pars_vertex:Fc,color_vertex:qc,common:Oc,cube_uv_reflection_fragment:Bc,defaultnormal_vertex:kc,displacementmap_pars_vertex:Hc,displacementmap_vertex:Zc,emissivemap_fragment:Gc,emissivemap_pars_fragment:Wc,encodings_fragment:Xc,encodings_pars_fragment:jc,envmap_fragment:Yc,envmap_common_pars_fragment:Kc,envmap_pars_fragment:Qc,envmap_pars_vertex:Jc,envmap_physical_pars_fragment:uu,envmap_vertex:$c,fog_vertex:eu,fog_pars_vertex:tu,fog_fragment:nu,fog_pars_fragment:iu,gradientmap_pars_fragment:ru,lightmap_fragment:su,lightmap_pars_fragment:au,lights_lambert_fragment:ou,lights_lambert_pars_fragment:lu,lights_pars_begin:cu,lights_toon_fragment:du,lights_toon_pars_fragment:hu,lights_phong_fragment:fu,lights_phong_pars_fragment:pu,lights_physical_fragment:mu,lights_physical_pars_fragment:gu,lights_fragment_begin:vu,lights_fragment_maps:xu,lights_fragment_end:Mu,logdepthbuf_fragment:yu,logdepthbuf_pars_fragment:Au,logdepthbuf_pars_vertex:_u,logdepthbuf_vertex:bu,map_fragment:Su,map_pars_fragment:wu,map_particle_fragment:Eu,map_particle_pars_fragment:Tu,metalnessmap_fragment:Iu,metalnessmap_pars_fragment:Ru,morphcolor_vertex:Cu,morphnormal_vertex:Lu,morphtarget_pars_vertex:Pu,morphtarget_vertex:zu,normal_fragment_begin:Vu,normal_fragment_maps:Du,normal_pars_fragment:Uu,normal_pars_vertex:Nu,normal_vertex:Fu,normalmap_pars_fragment:qu,clearcoat_normal_fragment_begin:Ou,clearcoat_normal_fragment_maps:Bu,clearcoat_pars_fragment:ku,iridescence_pars_fragment:Hu,output_fragment:Zu,packing:Gu,premultiplied_alpha_fragment:Wu,project_vertex:Xu,dithering_fragment:ju,dithering_pars_fragment:Yu,roughnessmap_fragment:Ku,roughnessmap_pars_fragment:Qu,shadowmap_pars_fragment:Ju,shadowmap_pars_vertex:$u,shadowmap_vertex:ed,shadowmask_pars_fragment:td,skinbase_vertex:nd,skinning_pars_vertex:id,skinning_vertex:rd,skinnormal_vertex:sd,specularmap_fragment:ad,specularmap_pars_fragment:od,tonemapping_fragment:ld,tonemapping_pars_fragment:cd,transmission_fragment:ud,transmission_pars_fragment:dd,uv_pars_fragment:hd,uv_pars_vertex:fd,uv_vertex:pd,uv2_pars_fragment:md,uv2_pars_vertex:gd,uv2_vertex:vd,worldpos_vertex:xd,background_vert:Md,background_frag:yd,backgroundCube_vert:Ad,backgroundCube_frag:_d,cube_vert:bd,cube_frag:Sd,depth_vert:wd,depth_frag:Ed,distanceRGBA_vert:Td,distanceRGBA_frag:Id,equirect_vert:Rd,equirect_frag:Cd,linedashed_vert:Ld,linedashed_frag:Pd,meshbasic_vert:zd,meshbasic_frag:Vd,meshlambert_vert:Dd,meshlambert_frag:Ud,meshmatcap_vert:Nd,meshmatcap_frag:Fd,meshnormal_vert:qd,meshnormal_frag:Od,meshphong_vert:Bd,meshphong_frag:kd,meshphysical_vert:Hd,meshphysical_frag:Zd,meshtoon_vert:Gd,meshtoon_frag:Wd,points_vert:Xd,points_frag:jd,shadow_vert:Yd,shadow_frag:Kd,sprite_vert:Qd,sprite_frag:Jd},ie={common:{diffuse:{value:new qe(16777215)},opacity:{value:1},map:{value:null},uvTransform:{value:new St},uv2Transform:{value:new St},alphaMap:{value:null},alphaTest:{value:0}},specularmap:{specularMap:{value:null}},envmap:{envMap:{value:null},flipEnvMap:{value:-1},reflectivity:{value:1},ior:{value:1.5},refractionRatio:{value:.98}},aomap:{aoMap:{value:null},aoMapIntensity:{value:1}},lightmap:{lightMap:{value:null},lightMapIntensity:{value:1}},emissivemap:{emissiveMap:{value:null}},bumpmap:{bumpMap:{value:null},bumpScale:{value:1}},normalmap:{normalMap:{value:null},normalScale:{value:new Se(1,1)}},displacementmap:{displacementMap:{value:null},displacementScale:{value:1},displacementBias:{value:0}},roughnessmap:{roughnessMap:{value:null}},metalnessmap:{metalnessMap:{value:null}},gradientmap:{gradientMap:{value:null}},fog:{fogDensity:{value:25e-5},fogNear:{value:1},fogFar:{value:2e3},fogColor:{value:new qe(16777215)}},lights:{ambientLightColor:{value:[]},lightProbe:{value:[]},directionalLights:{value:[],properties:{direction:{},color:{}}},directionalLightShadows:{value:[],properties:{shadowBias:{},shadowNormalBias:{},shadowRadius:{},shadowMapSize:{}}},directionalShadowMap:{value:[]},directionalShadowMatrix:{value:[]},spotLights:{value:[],properties:{color:{},position:{},direction:{},distance:{},coneCos:{},penumbraCos:{},decay:{}}},spotLightShadows:{value:[],properties:{shadowBias:{},shadowNormalBias:{},shadowRadius:{},shadowMapSize:{}}},spotLightMap:{value:[]},spotShadowMap:{value:[]},spotLightMatrix:{value:[]},pointLights:{value:[],properties:{color:{},position:{},decay:{},distance:{}}},pointLightShadows:{value:[],properties:{shadowBias:{},shadowNormalBias:{},shadowRadius:{},shadowMapSize:{},shadowCameraNear:{},shadowCameraFar:{}}},pointShadowMap:{value:[]},pointShadowMatrix:{value:[]},hemisphereLights:{value:[],properties:{direction:{},skyColor:{},groundColor:{}}},rectAreaLights:{value:[],properties:{color:{},position:{},width:{},height:{}}},ltc_1:{value:null},ltc_2:{value:null}},points:{diffuse:{value:new qe(16777215)},opacity:{value:1},size:{value:1},scale:{value:1},map:{value:null},alphaMap:{value:null},alphaTest:{value:0},uvTransform:{value:new St}},sprite:{diffuse:{value:new qe(16777215)},opacity:{value:1},center:{value:new Se(.5,.5)},rotation:{value:0},map:{value:null},alphaMap:{value:null},alphaTest:{value:0},uvTransform:{value:new St}}},Gt={basic:{uniforms:ht([ie.common,ie.specularmap,ie.envmap,ie.aomap,ie.lightmap,ie.fog]),vertexShader:_e.meshbasic_vert,fragmentShader:_e.meshbasic_frag},lambert:{uniforms:ht([ie.common,ie.specularmap,ie.envmap,ie.aomap,ie.lightmap,ie.emissivemap,ie.bumpmap,ie.normalmap,ie.displacementmap,ie.fog,ie.lights,{emissive:{value:new qe(0)}}]),vertexShader:_e.meshlambert_vert,fragmentShader:_e.meshlambert_frag},phong:{uniforms:ht([ie.common,ie.specularmap,ie.envmap,ie.aomap,ie.lightmap,ie.emissivemap,ie.bumpmap,ie.normalmap,ie.displacementmap,ie.fog,ie.lights,{emissive:{value:new qe(0)},specular:{value:new qe(1118481)},shininess:{value:30}}]),vertexShader:_e.meshphong_vert,fragmentShader:_e.meshphong_frag},standard:{uniforms:ht([ie.common,ie.envmap,ie.aomap,ie.lightmap,ie.emissivemap,ie.bumpmap,ie.normalmap,ie.displacementmap,ie.roughnessmap,ie.metalnessmap,ie.fog,ie.lights,{emissive:{value:new qe(0)},roughness:{value:1},metalness:{value:0},envMapIntensity:{value:1}}]),vertexShader:_e.meshphysical_vert,fragmentShader:_e.meshphysical_frag},toon:{uniforms:ht([ie.common,ie.aomap,ie.lightmap,ie.emissivemap,ie.bumpmap,ie.normalmap,ie.displacementmap,ie.gradientmap,ie.fog,ie.lights,{emissive:{value:new qe(0)}}]),vertexShader:_e.meshtoon_vert,fragmentShader:_e.meshtoon_frag},matcap:{uniforms:ht([ie.common,ie.bumpmap,ie.normalmap,ie.displacementmap,ie.fog,{matcap:{value:null}}]),vertexShader:_e.meshmatcap_vert,fragmentShader:_e.meshmatcap_frag},points:{uniforms:ht([ie.points,ie.fog]),vertexShader:_e.points_vert,fragmentShader:_e.points_frag},dashed:{uniforms:ht([ie.common,ie.fog,{scale:{value:1},dashSize:{value:1},totalSize:{value:2}}]),vertexShader:_e.linedashed_vert,fragmentShader:_e.linedashed_frag},depth:{uniforms:ht([ie.common,ie.displacementmap]),vertexShader:_e.depth_vert,fragmentShader:_e.depth_frag},normal:{uniforms:ht([ie.common,ie.bumpmap,ie.normalmap,ie.displacementmap,{opacity:{value:1}}]),vertexShader:_e.meshnormal_vert,fragmentShader:_e.meshnormal_frag},sprite:{uniforms:ht([ie.sprite,ie.fog]),vertexShader:_e.sprite_vert,fragmentShader:_e.sprite_frag},background:{uniforms:{uvTransform:{value:new St},t2D:{value:null},backgroundIntensity:{value:1}},vertexShader:_e.background_vert,fragmentShader:_e.background_frag},backgroundCube:{uniforms:{envMap:{value:null},flipEnvMap:{value:-1},backgroundBlurriness:{value:0},backgroundIntensity:{value:1}},vertexShader:_e.backgroundCube_vert,fragmentShader:_e.backgroundCube_frag},cube:{uniforms:{tCube:{value:null},tFlip:{value:-1},opacity:{value:1}},vertexShader:_e.cube_vert,fragmentShader:_e.cube_frag},equirect:{uniforms:{tEquirect:{value:null}},vertexShader:_e.equirect_vert,fragmentShader:_e.equirect_frag},distanceRGBA:{uniforms:ht([ie.common,ie.displacementmap,{referencePosition:{value:new C},nearDistance:{value:1},farDistance:{value:1e3}}]),vertexShader:_e.distanceRGBA_vert,fragmentShader:_e.distanceRGBA_frag},shadow:{uniforms:ht([ie.lights,ie.fog,{color:{value:new qe(0)},opacity:{value:1}}]),vertexShader:_e.shadow_vert,fragmentShader:_e.shadow_frag}};Gt.physical={uniforms:ht([Gt.standard.uniforms,{clearcoat:{value:0},clearcoatMap:{value:null},clearcoatRoughness:{value:0},clearcoatRoughnessMap:{value:null},clearcoatNormalScale:{value:new Se(1,1)},clearcoatNormalMap:{value:null},iridescence:{value:0},iridescenceMap:{value:null},iridescenceIOR:{value:1.3},iridescenceThicknessMinimum:{value:100},iridescenceThicknessMaximum:{value:400},iridescenceThicknessMap:{value:null},sheen:{value:0},sheenColor:{value:new qe(0)},sheenColorMap:{value:null},sheenRoughness:{value:1},sheenRoughnessMap:{value:null},transmission:{value:0},transmissionMap:{value:null},transmissionSamplerSize:{value:new Se},transmissionSamplerMap:{value:null},thickness:{value:0},thicknessMap:{value:null},attenuationDistance:{value:0},attenuationColor:{value:new qe(0)},specularIntensity:{value:1},specularIntensityMap:{value:null},specularColor:{value:new qe(1,1,1)},specularColorMap:{value:null}}]),vertexShader:_e.meshphysical_vert,fragmentShader:_e.meshphysical_frag};const nr={r:0,b:0,g:0};function $d(i,e,t,n,r,s,o){const a=new qe(0);let l=s===!0?0:1,c,u,d=null,f=0,m=null;function g(h,x){let E=!1,b=x.isScene===!0?x.background:null;b&&b.isTexture&&(b=(x.backgroundBlurriness>0?t:e).get(b));const S=i.xr,y=S.getSession&&S.getSession();y&&y.environmentBlendMode==="additive"&&(b=null),b===null?p(a,l):b&&b.isColor&&(p(b,1),E=!0),(i.autoClear||E)&&i.clear(i.autoClearColor,i.autoClearDepth,i.autoClearStencil),b&&(b.isCubeTexture||b.mapping===gr)?(u===void 0&&(u=new zt(new ui(1,1,1),new Ht({name:"BackgroundCubeMaterial",uniforms:si(Gt.backgroundCube.uniforms),vertexShader:Gt.backgroundCube.vertexShader,fragmentShader:Gt.backgroundCube.fragmentShader,side:Vt,depthTest:!1,depthWrite:!1,fog:!1})),u.geometry.deleteAttribute("normal"),u.geometry.deleteAttribute("uv"),u.onBeforeRender=function(R,U,v){this.matrixWorld.copyPosition(v.matrixWorld)},Object.defineProperty(u.material,"envMap",{get:function(){return this.uniforms.envMap.value}}),r.update(u)),u.material.uniforms.envMap.value=b,u.material.uniforms.flipEnvMap.value=b.isCubeTexture&&b.isRenderTargetTexture===!1?-1:1,u.material.uniforms.backgroundBlurriness.value=x.backgroundBlurriness,u.material.uniforms.backgroundIntensity.value=x.backgroundIntensity,(d!==b||f!==b.version||m!==i.toneMapping)&&(u.material.needsUpdate=!0,d=b,f=b.version,m=i.toneMapping),u.layers.enableAll(),h.unshift(u,u.geometry,u.material,0,0,null)):b&&b.isTexture&&(c===void 0&&(c=new zt(new Mr(2,2),new Ht({name:"BackgroundMaterial",uniforms:si(Gt.background.uniforms),vertexShader:Gt.background.vertexShader,fragmentShader:Gt.background.fragmentShader,side:ti,depthTest:!1,depthWrite:!1,fog:!1})),c.geometry.deleteAttribute("normal"),Object.defineProperty(c.material,"map",{get:function(){return this.uniforms.t2D.value}}),r.update(c)),c.material.uniforms.t2D.value=b,c.material.uniforms.backgroundIntensity.value=x.backgroundIntensity,b.matrixAutoUpdate===!0&&b.updateMatrix(),c.material.uniforms.uvTransform.value.copy(b.matrix),(d!==b||f!==b.version||m!==i.toneMapping)&&(c.material.needsUpdate=!0,d=b,f=b.version,m=i.toneMapping),c.layers.enableAll(),h.unshift(c,c.geometry,c.material,0,0,null))}function p(h,x){h.getRGB(nr,Co(i)),n.buffers.color.setClear(nr.r,nr.g,nr.b,x,o)}return{getClearColor:function(){return a},setClearColor:function(h,x=1){a.set(h),l=x,p(a,l)},getClearAlpha:function(){return l},setClearAlpha:function(h){l=h,p(a,l)},render:g}}function eh(i,e,t,n){const r=i.getParameter(34921),s=n.isWebGL2?null:e.get("OES_vertex_array_object"),o=n.isWebGL2||s!==null,a={},l=h(null);let c=l,u=!1;function d(z,W,K,j,G){let te=!1;if(o){const J=p(j,K,W);c!==J&&(c=J,m(c.object)),te=x(z,j,K,G),te&&E(z,j,K,G)}else{const J=W.wireframe===!0;(c.geometry!==j.id||c.program!==K.id||c.wireframe!==J)&&(c.geometry=j.id,c.program=K.id,c.wireframe=J,te=!0)}G!==null&&t.update(G,34963),(te||u)&&(u=!1,v(z,W,K,j),G!==null&&i.bindBuffer(34963,t.get(G).buffer))}function f(){return n.isWebGL2?i.createVertexArray():s.createVertexArrayOES()}function m(z){return n.isWebGL2?i.bindVertexArray(z):s.bindVertexArrayOES(z)}function g(z){return n.isWebGL2?i.deleteVertexArray(z):s.deleteVertexArrayOES(z)}function p(z,W,K){const j=K.wireframe===!0;let G=a[z.id];G===void 0&&(G={},a[z.id]=G);let te=G[W.id];te===void 0&&(te={},G[W.id]=te);let J=te[j];return J===void 0&&(J=h(f()),te[j]=J),J}function h(z){const W=[],K=[],j=[];for(let G=0;G<r;G++)W[G]=0,K[G]=0,j[G]=0;return{geometry:null,program:null,wireframe:!1,newAttributes:W,enabledAttributes:K,attributeDivisors:j,object:z,attributes:{},index:null}}function x(z,W,K,j){const G=c.attributes,te=W.attributes;let J=0;const F=K.getAttributes();for(const k in F)if(F[k].location>=0){const ee=G[k];let re=te[k];if(re===void 0&&(k==="instanceMatrix"&&z.instanceMatrix&&(re=z.instanceMatrix),k==="instanceColor"&&z.instanceColor&&(re=z.instanceColor)),ee===void 0||ee.attribute!==re||re&&ee.data!==re.data)return!0;J++}return c.attributesNum!==J||c.index!==j}function E(z,W,K,j){const G={},te=W.attributes;let J=0;const F=K.getAttributes();for(const k in F)if(F[k].location>=0){let ee=te[k];ee===void 0&&(k==="instanceMatrix"&&z.instanceMatrix&&(ee=z.instanceMatrix),k==="instanceColor"&&z.instanceColor&&(ee=z.instanceColor));const re={};re.attribute=ee,ee&&ee.data&&(re.data=ee.data),G[k]=re,J++}c.attributes=G,c.attributesNum=J,c.index=j}function b(){const z=c.newAttributes;for(let W=0,K=z.length;W<K;W++)z[W]=0}function S(z){y(z,0)}function y(z,W){const K=c.newAttributes,j=c.enabledAttributes,G=c.attributeDivisors;K[z]=1,j[z]===0&&(i.enableVertexAttribArray(z),j[z]=1),G[z]!==W&&((n.isWebGL2?i:e.get("ANGLE_instanced_arrays"))[n.isWebGL2?"vertexAttribDivisor":"vertexAttribDivisorANGLE"](z,W),G[z]=W)}function R(){const z=c.newAttributes,W=c.enabledAttributes;for(let K=0,j=W.length;K<j;K++)W[K]!==z[K]&&(i.disableVertexAttribArray(K),W[K]=0)}function U(z,W,K,j,G,te){n.isWebGL2===!0&&(K===5124||K===5125)?i.vertexAttribIPointer(z,W,K,G,te):i.vertexAttribPointer(z,W,K,j,G,te)}function v(z,W,K,j){if(n.isWebGL2===!1&&(z.isInstancedMesh||j.isInstancedBufferGeometry)&&e.get("ANGLE_instanced_arrays")===null)return;b();const G=j.attributes,te=K.getAttributes(),J=W.defaultAttributeValues;for(const F in te){const k=te[F];if(k.location>=0){let Q=G[F];if(Q===void 0&&(F==="instanceMatrix"&&z.instanceMatrix&&(Q=z.instanceMatrix),F==="instanceColor"&&z.instanceColor&&(Q=z.instanceColor)),Q!==void 0){const ee=Q.normalized,re=Q.itemSize,H=t.get(Q);if(H===void 0)continue;const Ee=H.buffer,he=H.type,ve=H.bytesPerElement;if(Q.isInterleavedBufferAttribute){const de=Q.data,ze=de.stride,Ae=Q.offset;if(de.isInstancedInterleavedBuffer){for(let xe=0;xe<k.locationSize;xe++)y(k.location+xe,de.meshPerAttribute);z.isInstancedMesh!==!0&&j._maxInstanceCount===void 0&&(j._maxInstanceCount=de.meshPerAttribute*de.count)}else for(let xe=0;xe<k.locationSize;xe++)S(k.location+xe);i.bindBuffer(34962,Ee);for(let xe=0;xe<k.locationSize;xe++)U(k.location+xe,re/k.locationSize,he,ee,ze*ve,(Ae+re/k.locationSize*xe)*ve)}else{if(Q.isInstancedBufferAttribute){for(let de=0;de<k.locationSize;de++)y(k.location+de,Q.meshPerAttribute);z.isInstancedMesh!==!0&&j._maxInstanceCount===void 0&&(j._maxInstanceCount=Q.meshPerAttribute*Q.count)}else for(let de=0;de<k.locationSize;de++)S(k.location+de);i.bindBuffer(34962,Ee);for(let de=0;de<k.locationSize;de++)U(k.location+de,re/k.locationSize,he,ee,re*ve,re/k.locationSize*de*ve)}}else if(J!==void 0){const ee=J[F];if(ee!==void 0)switch(ee.length){case 2:i.vertexAttrib2fv(k.location,ee);break;case 3:i.vertexAttrib3fv(k.location,ee);break;case 4:i.vertexAttrib4fv(k.location,ee);break;default:i.vertexAttrib1fv(k.location,ee)}}}}R()}function T(){oe();for(const z in a){const W=a[z];for(const K in W){const j=W[K];for(const G in j)g(j[G].object),delete j[G];delete W[K]}delete a[z]}}function D(z){if(a[z.id]===void 0)return;const W=a[z.id];for(const K in W){const j=W[K];for(const G in j)g(j[G].object),delete j[G];delete W[K]}delete a[z.id]}function X(z){for(const W in a){const K=a[W];if(K[z.id]===void 0)continue;const j=K[z.id];for(const G in j)g(j[G].object),delete j[G];delete K[z.id]}}function oe(){N(),u=!0,c!==l&&(c=l,m(c.object))}function N(){l.geometry=null,l.program=null,l.wireframe=!1}return{setup:d,reset:oe,resetDefaultState:N,dispose:T,releaseStatesOfGeometry:D,releaseStatesOfProgram:X,initAttributes:b,enableAttribute:S,disableUnusedAttributes:R}}function th(i,e,t,n){const r=n.isWebGL2;let s;function o(c){s=c}function a(c,u){i.drawArrays(s,c,u),t.update(u,s,1)}function l(c,u,d){if(d===0)return;let f,m;if(r)f=i,m="drawArraysInstanced";else if(f=e.get("ANGLE_instanced_arrays"),m="drawArraysInstancedANGLE",f===null){console.error("THREE.WebGLBufferRenderer: using THREE.InstancedBufferGeometry but hardware does not support extension ANGLE_instanced_arrays.");return}f[m](s,c,u,d),t.update(u,s,d)}this.setMode=o,this.render=a,this.renderInstances=l}function nh(i,e,t){let n;function r(){if(n!==void 0)return n;if(e.has("EXT_texture_filter_anisotropic")===!0){const U=e.get("EXT_texture_filter_anisotropic");n=i.getParameter(U.MAX_TEXTURE_MAX_ANISOTROPY_EXT)}else n=0;return n}function s(U){if(U==="highp"){if(i.getShaderPrecisionFormat(35633,36338).precision>0&&i.getShaderPrecisionFormat(35632,36338).precision>0)return"highp";U="mediump"}return U==="mediump"&&i.getShaderPrecisionFormat(35633,36337).precision>0&&i.getShaderPrecisionFormat(35632,36337).precision>0?"mediump":"lowp"}const o=typeof WebGL2RenderingContext<"u"&&i instanceof WebGL2RenderingContext||typeof WebGL2ComputeRenderingContext<"u"&&i instanceof WebGL2ComputeRenderingContext;let a=t.precision!==void 0?t.precision:"highp";const l=s(a);l!==a&&(console.warn("THREE.WebGLRenderer:",a,"not supported, using",l,"instead."),a=l);const c=o||e.has("WEBGL_draw_buffers"),u=t.logarithmicDepthBuffer===!0,d=i.getParameter(34930),f=i.getParameter(35660),m=i.getParameter(3379),g=i.getParameter(34076),p=i.getParameter(34921),h=i.getParameter(36347),x=i.getParameter(36348),E=i.getParameter(36349),b=f>0,S=o||e.has("OES_texture_float"),y=b&&S,R=o?i.getParameter(36183):0;return{isWebGL2:o,drawBuffers:c,getMaxAnisotropy:r,getMaxPrecision:s,precision:a,logarithmicDepthBuffer:u,maxTextures:d,maxVertexTextures:f,maxTextureSize:m,maxCubemapSize:g,maxAttributes:p,maxVertexUniforms:h,maxVaryings:x,maxFragmentUniforms:E,vertexTextures:b,floatFragmentTextures:S,floatVertexTextures:y,maxSamples:R}}function ih(i){const e=this;let t=null,n=0,r=!1,s=!1;const o=new An,a=new St,l={value:null,needsUpdate:!1};this.uniform=l,this.numPlanes=0,this.numIntersection=0,this.init=function(d,f,m){const g=d.length!==0||f||n!==0||r;return r=f,t=u(d,m,0),n=d.length,g},this.beginShadows=function(){s=!0,u(null)},this.endShadows=function(){s=!1,c()},this.setState=function(d,f,m){const g=d.clippingPlanes,p=d.clipIntersection,h=d.clipShadows,x=i.get(d);if(!r||g===null||g.length===0||s&&!h)s?u(null):c();else{const E=s?0:n,b=E*4;let S=x.clippingState||null;l.value=S,S=u(g,f,b,m);for(let y=0;y!==b;++y)S[y]=t[y];x.clippingState=S,this.numIntersection=p?this.numPlanes:0,this.numPlanes+=E}};function c(){l.value!==t&&(l.value=t,l.needsUpdate=n>0),e.numPlanes=n,e.numIntersection=0}function u(d,f,m,g){const p=d!==null?d.length:0;let h=null;if(p!==0){if(h=l.value,g!==!0||h===null){const x=m+p*4,E=f.matrixWorldInverse;a.getNormalMatrix(E),(h===null||h.length<x)&&(h=new Float32Array(x));for(let b=0,S=m;b!==p;++b,S+=4)o.copy(d[b]).applyMatrix4(E,a),o.normal.toArray(h,S),h[S+3]=o.constant}l.value=h,l.needsUpdate=!0}return e.numPlanes=p,e.numIntersection=0,h}}function rh(i){let e=new WeakMap;function t(o,a){return a===hs?o.mapping=ni:a===fs&&(o.mapping=ii),o}function n(o){if(o&&o.isTexture&&o.isRenderTargetTexture===!1){const a=o.mapping;if(a===hs||a===fs)if(e.has(o)){const l=e.get(o).texture;return t(l,o.mapping)}else{const l=o.image;if(l&&l.height>0){const c=new vc(l.height/2);return c.fromEquirectangularTexture(i,o),e.set(o,c),o.addEventListener("dispose",r),t(c.texture,o.mapping)}else return null}}return o}function r(o){const a=o.target;a.removeEventListener("dispose",r);const l=e.get(a);l!==void 0&&(e.delete(a),l.dispose())}function s(){e=new WeakMap}return{get:n,dispose:s}}class sh extends Lo{constructor(e=-1,t=1,n=1,r=-1,s=.1,o=2e3){super(),this.isOrthographicCamera=!0,this.type="OrthographicCamera",this.zoom=1,this.view=null,this.left=e,this.right=t,this.top=n,this.bottom=r,this.near=s,this.far=o,this.updateProjectionMatrix()}copy(e,t){return super.copy(e,t),this.left=e.left,this.right=e.right,this.top=e.top,this.bottom=e.bottom,this.near=e.near,this.far=e.far,this.zoom=e.zoom,this.view=e.view===null?null:Object.assign({},e.view),this}setViewOffset(e,t,n,r,s,o){this.view===null&&(this.view={enabled:!0,fullWidth:1,fullHeight:1,offsetX:0,offsetY:0,width:1,height:1}),this.view.enabled=!0,this.view.fullWidth=e,this.view.fullHeight=t,this.view.offsetX=n,this.view.offsetY=r,this.view.width=s,this.view.height=o,this.updateProjectionMatrix()}clearViewOffset(){this.view!==null&&(this.view.enabled=!1),this.updateProjectionMatrix()}updateProjectionMatrix(){const e=(this.right-this.left)/(2*this.zoom),t=(this.top-this.bottom)/(2*this.zoom),n=(this.right+this.left)/2,r=(this.top+this.bottom)/2;let s=n-e,o=n+e,a=r+t,l=r-t;if(this.view!==null&&this.view.enabled){const c=(this.right-this.left)/this.view.fullWidth/this.zoom,u=(this.top-this.bottom)/this.view.fullHeight/this.zoom;s+=c*this.view.offsetX,o=s+c*this.view.width,a-=u*this.view.offsetY,l=a-u*this.view.height}this.projectionMatrix.makeOrthographic(s,o,a,l,this.near,this.far),this.projectionMatrixInverse.copy(this.projectionMatrix).invert()}toJSON(e){const t=super.toJSON(e);return t.object.zoom=this.zoom,t.object.left=this.left,t.object.right=this.right,t.object.top=this.top,t.object.bottom=this.bottom,t.object.near=this.near,t.object.far=this.far,this.view!==null&&(t.object.view=Object.assign({},this.view)),t}}const Jn=4,wa=[.125,.215,.35,.446,.526,.582],bn=20,ns=new sh,Ea=new qe;let is=null;const _n=(1+Math.sqrt(5))/2,Yn=1/_n,Ta=[new C(1,1,1),new C(-1,1,1),new C(1,1,-1),new C(-1,1,-1),new C(0,_n,Yn),new C(0,_n,-Yn),new C(Yn,0,_n),new C(-Yn,0,_n),new C(_n,Yn,0),new C(-_n,Yn,0)];class Ia{constructor(e){this._renderer=e,this._pingPongRenderTarget=null,this._lodMax=0,this._cubeSize=0,this._lodPlanes=[],this._sizeLods=[],this._sigmas=[],this._blurMaterial=null,this._cubemapMaterial=null,this._equirectMaterial=null,this._compileMaterial(this._blurMaterial)}fromScene(e,t=0,n=.1,r=100){is=this._renderer.getRenderTarget(),this._setSize(256);const s=this._allocateTargets();return s.depthBuffer=!0,this._sceneToCubeUV(e,n,r,s),t>0&&this._blur(s,0,0,t),this._applyPMREM(s),this._cleanup(s),s}fromEquirectangular(e,t=null){return this._fromTexture(e,t)}fromCubemap(e,t=null){return this._fromTexture(e,t)}compileCubemapShader(){this._cubemapMaterial===null&&(this._cubemapMaterial=La(),this._compileMaterial(this._cubemapMaterial))}compileEquirectangularShader(){this._equirectMaterial===null&&(this._equirectMaterial=Ca(),this._compileMaterial(this._equirectMaterial))}dispose(){this._dispose(),this._cubemapMaterial!==null&&this._cubemapMaterial.dispose(),this._equirectMaterial!==null&&this._equirectMaterial.dispose()}_setSize(e){this._lodMax=Math.floor(Math.log2(e)),this._cubeSize=Math.pow(2,this._lodMax)}_dispose(){this._blurMaterial!==null&&this._blurMaterial.dispose(),this._pingPongRenderTarget!==null&&this._pingPongRenderTarget.dispose();for(let e=0;e<this._lodPlanes.length;e++)this._lodPlanes[e].dispose()}_cleanup(e){this._renderer.setRenderTarget(is),e.scissorTest=!1,ir(e,0,0,e.width,e.height)}_fromTexture(e,t){e.mapping===ni||e.mapping===ii?this._setSize(e.image.length===0?16:e.image[0].width||e.image[0].image.width):this._setSize(e.image.width/4),is=this._renderer.getRenderTarget();const n=t||this._allocateTargets();return this._textureToCubeUV(e,n),this._applyPMREM(n),this._cleanup(n),n}_allocateTargets(){const e=3*Math.max(this._cubeSize,112),t=4*this._cubeSize,n={magFilter:Ct,minFilter:Ct,generateMipmaps:!1,type:Ri,format:kt,encoding:Rn,depthBuffer:!1},r=Ra(e,t,n);if(this._pingPongRenderTarget===null||this._pingPongRenderTarget.width!==e){this._pingPongRenderTarget!==null&&this._dispose(),this._pingPongRenderTarget=Ra(e,t,n);const{_lodMax:s}=this;({sizeLods:this._sizeLods,lodPlanes:this._lodPlanes,sigmas:this._sigmas}=ah(s)),this._blurMaterial=oh(s,e,t)}return r}_compileMaterial(e){const t=new zt(this._lodPlanes[0],e);this._renderer.compile(t,ns)}_sceneToCubeUV(e,t,n,r){const a=new Lt(90,1,t,n),l=[1,-1,1,1,1,1],c=[1,1,1,-1,-1,-1],u=this._renderer,d=u.autoClear,f=u.toneMapping;u.getClearColor(Ea),u.toneMapping=tn,u.autoClear=!1;const m=new Es({name:"PMREM.Background",side:Vt,depthWrite:!1,depthTest:!1}),g=new zt(new ui,m);let p=!1;const h=e.background;h?h.isColor&&(m.color.copy(h),e.background=null,p=!0):(m.color.copy(Ea),p=!0);for(let x=0;x<6;x++){const E=x%3;E===0?(a.up.set(0,l[x],0),a.lookAt(c[x],0,0)):E===1?(a.up.set(0,0,l[x]),a.lookAt(0,c[x],0)):(a.up.set(0,l[x],0),a.lookAt(0,0,c[x]));const b=this._cubeSize;ir(r,E*b,x>2?b:0,b,b),u.setRenderTarget(r),p&&u.render(g,a),u.render(e,a)}g.geometry.dispose(),g.material.dispose(),u.toneMapping=f,u.autoClear=d,e.background=h}_textureToCubeUV(e,t){const n=this._renderer,r=e.mapping===ni||e.mapping===ii;r?(this._cubemapMaterial===null&&(this._cubemapMaterial=La()),this._cubemapMaterial.uniforms.flipEnvMap.value=e.isRenderTargetTexture===!1?-1:1):this._equirectMaterial===null&&(this._equirectMaterial=Ca());const s=r?this._cubemapMaterial:this._equirectMaterial,o=new zt(this._lodPlanes[0],s),a=s.uniforms;a.envMap.value=e;const l=this._cubeSize;ir(t,0,0,3*l,2*l),n.setRenderTarget(t),n.render(o,ns)}_applyPMREM(e){const t=this._renderer,n=t.autoClear;t.autoClear=!1;for(let r=1;r<this._lodPlanes.length;r++){const s=Math.sqrt(this._sigmas[r]*this._sigmas[r]-this._sigmas[r-1]*this._sigmas[r-1]),o=Ta[(r-1)%Ta.length];this._blur(e,r-1,r,s,o)}t.autoClear=n}_blur(e,t,n,r,s){const o=this._pingPongRenderTarget;this._halfBlur(e,o,t,n,r,"latitudinal",s),this._halfBlur(o,e,n,n,r,"longitudinal",s)}_halfBlur(e,t,n,r,s,o,a){const l=this._renderer,c=this._blurMaterial;o!=="latitudinal"&&o!=="longitudinal"&&console.error("blur direction must be either latitudinal or longitudinal!");const u=3,d=new zt(this._lodPlanes[r],c),f=c.uniforms,m=this._sizeLods[n]-1,g=isFinite(s)?Math.PI/(2*m):2*Math.PI/(2*bn-1),p=s/g,h=isFinite(s)?1+Math.floor(u*p):bn;h>bn&&console.warn(`sigmaRadians, ${s}, is too large and will clip, as it requested ${h} samples when the maximum is set to ${bn}`);const x=[];let E=0;for(let U=0;U<bn;++U){const v=U/p,T=Math.exp(-v*v/2);x.push(T),U===0?E+=T:U<h&&(E+=2*T)}for(let U=0;U<x.length;U++)x[U]=x[U]/E;f.envMap.value=e.texture,f.samples.value=h,f.weights.value=x,f.latitudinal.value=o==="latitudinal",a&&(f.poleAxis.value=a);const{_lodMax:b}=this;f.dTheta.value=g,f.mipInt.value=b-n;const S=this._sizeLods[r],y=3*S*(r>b-Jn?r-b+Jn:0),R=4*(this._cubeSize-S);ir(t,y,R,3*S,2*S),l.setRenderTarget(t),l.render(d,ns)}}function ah(i){const e=[],t=[],n=[];let r=i;const s=i-Jn+1+wa.length;for(let o=0;o<s;o++){const a=Math.pow(2,r);t.push(a);let l=1/a;o>i-Jn?l=wa[o-i+Jn-1]:o===0&&(l=0),n.push(l);const c=1/(a-2),u=-c,d=1+c,f=[u,u,d,u,d,d,u,u,d,d,u,d],m=6,g=6,p=3,h=2,x=1,E=new Float32Array(p*g*m),b=new Float32Array(h*g*m),S=new Float32Array(x*g*m);for(let R=0;R<m;R++){const U=R%3*2/3-1,v=R>2?0:-1,T=[U,v,0,U+2/3,v,0,U+2/3,v+1,0,U,v,0,U+2/3,v+1,0,U,v+1,0];E.set(T,p*g*R),b.set(f,h*g*R);const D=[R,R,R,R,R,R];S.set(D,x*g*R)}const y=new Ut;y.setAttribute("position",new Dt(E,p)),y.setAttribute("uv",new Dt(b,h)),y.setAttribute("faceIndex",new Dt(S,x)),e.push(y),r>Jn&&r--}return{lodPlanes:e,sizeLods:t,sigmas:n}}function Ra(i,e,t){const n=new Cn(i,e,t);return n.texture.mapping=gr,n.texture.name="PMREM.cubeUv",n.scissorTest=!0,n}function ir(i,e,t,n,r){i.viewport.set(e,t,n,r),i.scissor.set(e,t,n,r)}function oh(i,e,t){const n=new Float32Array(bn),r=new C(0,1,0);return new Ht({name:"SphericalGaussianBlur",defines:{n:bn,CUBEUV_TEXEL_WIDTH:1/e,CUBEUV_TEXEL_HEIGHT:1/t,CUBEUV_MAX_MIP:`${i}.0`},uniforms:{envMap:{value:null},samples:{value:1},weights:{value:n},latitudinal:{value:!1},dTheta:{value:0},mipInt:{value:0},poleAxis:{value:r}},vertexShader:Is(),fragmentShader:`

			precision mediump float;
			precision mediump int;

			varying vec3 vOutputDirection;

			uniform sampler2D envMap;
			uniform int samples;
			uniform float weights[ n ];
			uniform bool latitudinal;
			uniform float dTheta;
			uniform float mipInt;
			uniform vec3 poleAxis;

			#define ENVMAP_TYPE_CUBE_UV
			#include <cube_uv_reflection_fragment>

			vec3 getSample( float theta, vec3 axis ) {

				float cosTheta = cos( theta );
				// Rodrigues' axis-angle rotation
				vec3 sampleDirection = vOutputDirection * cosTheta
					+ cross( axis, vOutputDirection ) * sin( theta )
					+ axis * dot( axis, vOutputDirection ) * ( 1.0 - cosTheta );

				return bilinearCubeUV( envMap, sampleDirection, mipInt );

			}

			void main() {

				vec3 axis = latitudinal ? poleAxis : cross( poleAxis, vOutputDirection );

				if ( all( equal( axis, vec3( 0.0 ) ) ) ) {

					axis = vec3( vOutputDirection.z, 0.0, - vOutputDirection.x );

				}

				axis = normalize( axis );

				gl_FragColor = vec4( 0.0, 0.0, 0.0, 1.0 );
				gl_FragColor.rgb += weights[ 0 ] * getSample( 0.0, axis );

				for ( int i = 1; i < n; i++ ) {

					if ( i >= samples ) {

						break;

					}

					float theta = dTheta * float( i );
					gl_FragColor.rgb += weights[ i ] * getSample( -1.0 * theta, axis );
					gl_FragColor.rgb += weights[ i ] * getSample( theta, axis );

				}

			}
		`,blending:hn,depthTest:!1,depthWrite:!1})}function Ca(){return new Ht({name:"EquirectangularToCubeUV",uniforms:{envMap:{value:null}},vertexShader:Is(),fragmentShader:`

			precision mediump float;
			precision mediump int;

			varying vec3 vOutputDirection;

			uniform sampler2D envMap;

			#include <common>

			void main() {

				vec3 outputDirection = normalize( vOutputDirection );
				vec2 uv = equirectUv( outputDirection );

				gl_FragColor = vec4( texture2D ( envMap, uv ).rgb, 1.0 );

			}
		`,blending:hn,depthTest:!1,depthWrite:!1})}function La(){return new Ht({name:"CubemapToCubeUV",uniforms:{envMap:{value:null},flipEnvMap:{value:-1}},vertexShader:Is(),fragmentShader:`

			precision mediump float;
			precision mediump int;

			uniform float flipEnvMap;

			varying vec3 vOutputDirection;

			uniform samplerCube envMap;

			void main() {

				gl_FragColor = textureCube( envMap, vec3( flipEnvMap * vOutputDirection.x, vOutputDirection.yz ) );

			}
		`,blending:hn,depthTest:!1,depthWrite:!1})}function Is(){return`

		precision mediump float;
		precision mediump int;

		attribute float faceIndex;

		varying vec3 vOutputDirection;

		// RH coordinate system; PMREM face-indexing convention
		vec3 getDirection( vec2 uv, float face ) {

			uv = 2.0 * uv - 1.0;

			vec3 direction = vec3( uv, 1.0 );

			if ( face == 0.0 ) {

				direction = direction.zyx; // ( 1, v, u ) pos x

			} else if ( face == 1.0 ) {

				direction = direction.xzy;
				direction.xz *= -1.0; // ( -u, 1, -v ) pos y

			} else if ( face == 2.0 ) {

				direction.x *= -1.0; // ( -u, v, 1 ) pos z

			} else if ( face == 3.0 ) {

				direction = direction.zyx;
				direction.xz *= -1.0; // ( -1, v, -u ) neg x

			} else if ( face == 4.0 ) {

				direction = direction.xzy;
				direction.xy *= -1.0; // ( -u, -1, v ) neg y

			} else if ( face == 5.0 ) {

				direction.z *= -1.0; // ( u, v, -1 ) neg z

			}

			return direction;

		}

		void main() {

			vOutputDirection = getDirection( uv, faceIndex );
			gl_Position = vec4( position, 1.0 );

		}
	`}function lh(i){let e=new WeakMap,t=null;function n(a){if(a&&a.isTexture){const l=a.mapping,c=l===hs||l===fs,u=l===ni||l===ii;if(c||u)if(a.isRenderTargetTexture&&a.needsPMREMUpdate===!0){a.needsPMREMUpdate=!1;let d=e.get(a);return t===null&&(t=new Ia(i)),d=c?t.fromEquirectangular(a,d):t.fromCubemap(a,d),e.set(a,d),d.texture}else{if(e.has(a))return e.get(a).texture;{const d=a.image;if(c&&d&&d.height>0||u&&d&&r(d)){t===null&&(t=new Ia(i));const f=c?t.fromEquirectangular(a):t.fromCubemap(a);return e.set(a,f),a.addEventListener("dispose",s),f.texture}else return null}}}return a}function r(a){let l=0;const c=6;for(let u=0;u<c;u++)a[u]!==void 0&&l++;return l===c}function s(a){const l=a.target;l.removeEventListener("dispose",s);const c=e.get(l);c!==void 0&&(e.delete(l),c.dispose())}function o(){e=new WeakMap,t!==null&&(t.dispose(),t=null)}return{get:n,dispose:o}}function ch(i){const e={};function t(n){if(e[n]!==void 0)return e[n];let r;switch(n){case"WEBGL_depth_texture":r=i.getExtension("WEBGL_depth_texture")||i.getExtension("MOZ_WEBGL_depth_texture")||i.getExtension("WEBKIT_WEBGL_depth_texture");break;case"EXT_texture_filter_anisotropic":r=i.getExtension("EXT_texture_filter_anisotropic")||i.getExtension("MOZ_EXT_texture_filter_anisotropic")||i.getExtension("WEBKIT_EXT_texture_filter_anisotropic");break;case"WEBGL_compressed_texture_s3tc":r=i.getExtension("WEBGL_compressed_texture_s3tc")||i.getExtension("MOZ_WEBGL_compressed_texture_s3tc")||i.getExtension("WEBKIT_WEBGL_compressed_texture_s3tc");break;case"WEBGL_compressed_texture_pvrtc":r=i.getExtension("WEBGL_compressed_texture_pvrtc")||i.getExtension("WEBKIT_WEBGL_compressed_texture_pvrtc");break;default:r=i.getExtension(n)}return e[n]=r,r}return{has:function(n){return t(n)!==null},init:function(n){n.isWebGL2?t("EXT_color_buffer_float"):(t("WEBGL_depth_texture"),t("OES_texture_float"),t("OES_texture_half_float"),t("OES_texture_half_float_linear"),t("OES_standard_derivatives"),t("OES_element_index_uint"),t("OES_vertex_array_object"),t("ANGLE_instanced_arrays")),t("OES_texture_float_linear"),t("EXT_color_buffer_half_float"),t("WEBGL_multisampled_render_to_texture")},get:function(n){const r=t(n);return r===null&&console.warn("THREE.WebGLRenderer: "+n+" extension not supported."),r}}}function uh(i,e,t,n){const r={},s=new WeakMap;function o(d){const f=d.target;f.index!==null&&e.remove(f.index);for(const g in f.attributes)e.remove(f.attributes[g]);f.removeEventListener("dispose",o),delete r[f.id];const m=s.get(f);m&&(e.remove(m),s.delete(f)),n.releaseStatesOfGeometry(f),f.isInstancedBufferGeometry===!0&&delete f._maxInstanceCount,t.memory.geometries--}function a(d,f){return r[f.id]===!0||(f.addEventListener("dispose",o),r[f.id]=!0,t.memory.geometries++),f}function l(d){const f=d.attributes;for(const g in f)e.update(f[g],34962);const m=d.morphAttributes;for(const g in m){const p=m[g];for(let h=0,x=p.length;h<x;h++)e.update(p[h],34962)}}function c(d){const f=[],m=d.index,g=d.attributes.position;let p=0;if(m!==null){const E=m.array;p=m.version;for(let b=0,S=E.length;b<S;b+=3){const y=E[b+0],R=E[b+1],U=E[b+2];f.push(y,R,R,U,U,y)}}else{const E=g.array;p=g.version;for(let b=0,S=E.length/3-1;b<S;b+=3){const y=b+0,R=b+1,U=b+2;f.push(y,R,R,U,U,y)}}const h=new(So(f)?Ts:Ro)(f,1);h.version=p;const x=s.get(d);x&&e.remove(x),s.set(d,h)}function u(d){const f=s.get(d);if(f){const m=d.index;m!==null&&f.version<m.version&&c(d)}else c(d);return s.get(d)}return{get:a,update:l,getWireframeAttribute:u}}function dh(i,e,t,n){const r=n.isWebGL2;let s;function o(f){s=f}let a,l;function c(f){a=f.type,l=f.bytesPerElement}function u(f,m){i.drawElements(s,m,a,f*l),t.update(m,s,1)}function d(f,m,g){if(g===0)return;let p,h;if(r)p=i,h="drawElementsInstanced";else if(p=e.get("ANGLE_instanced_arrays"),h="drawElementsInstancedANGLE",p===null){console.error("THREE.WebGLIndexedBufferRenderer: using THREE.InstancedBufferGeometry but hardware does not support extension ANGLE_instanced_arrays.");return}p[h](s,m,a,f*l,g),t.update(m,s,g)}this.setMode=o,this.setIndex=c,this.render=u,this.renderInstances=d}function hh(i){const e={geometries:0,textures:0},t={frame:0,calls:0,triangles:0,points:0,lines:0};function n(s,o,a){switch(t.calls++,o){case 4:t.triangles+=a*(s/3);break;case 1:t.lines+=a*(s/2);break;case 3:t.lines+=a*(s-1);break;case 2:t.lines+=a*s;break;case 0:t.points+=a*s;break;default:console.error("THREE.WebGLInfo: Unknown draw mode:",o);break}}function r(){t.frame++,t.calls=0,t.triangles=0,t.points=0,t.lines=0}return{memory:e,render:t,programs:null,autoReset:!0,reset:r,update:n}}function fh(i,e){return i[0]-e[0]}function ph(i,e){return Math.abs(e[1])-Math.abs(i[1])}function mh(i,e,t){const n={},r=new Float32Array(8),s=new WeakMap,o=new Xe,a=[];for(let c=0;c<8;c++)a[c]=[c,0];function l(c,u,d,f){const m=c.morphTargetInfluences;if(e.isWebGL2===!0){const p=u.morphAttributes.position||u.morphAttributes.normal||u.morphAttributes.color,h=p!==void 0?p.length:0;let x=s.get(u);if(x===void 0||x.count!==h){let K=function(){z.dispose(),s.delete(u),u.removeEventListener("dispose",K)};var g=K;x!==void 0&&x.texture.dispose();const S=u.morphAttributes.position!==void 0,y=u.morphAttributes.normal!==void 0,R=u.morphAttributes.color!==void 0,U=u.morphAttributes.position||[],v=u.morphAttributes.normal||[],T=u.morphAttributes.color||[];let D=0;S===!0&&(D=1),y===!0&&(D=2),R===!0&&(D=3);let X=u.attributes.position.count*D,oe=1;X>e.maxTextureSize&&(oe=Math.ceil(X/e.maxTextureSize),X=e.maxTextureSize);const N=new Float32Array(X*oe*4*h),z=new Io(N,X,oe,h);z.type=wn,z.needsUpdate=!0;const W=D*4;for(let j=0;j<h;j++){const G=U[j],te=v[j],J=T[j],F=X*oe*4*j;for(let k=0;k<G.count;k++){const Q=k*W;S===!0&&(o.fromBufferAttribute(G,k),N[F+Q+0]=o.x,N[F+Q+1]=o.y,N[F+Q+2]=o.z,N[F+Q+3]=0),y===!0&&(o.fromBufferAttribute(te,k),N[F+Q+4]=o.x,N[F+Q+5]=o.y,N[F+Q+6]=o.z,N[F+Q+7]=0),R===!0&&(o.fromBufferAttribute(J,k),N[F+Q+8]=o.x,N[F+Q+9]=o.y,N[F+Q+10]=o.z,N[F+Q+11]=J.itemSize===4?o.w:1)}}x={count:h,texture:z,size:new Se(X,oe)},s.set(u,x),u.addEventListener("dispose",K)}let E=0;for(let S=0;S<m.length;S++)E+=m[S];const b=u.morphTargetsRelative?1:1-E;f.getUniforms().setValue(i,"morphTargetBaseInfluence",b),f.getUniforms().setValue(i,"morphTargetInfluences",m),f.getUniforms().setValue(i,"morphTargetsTexture",x.texture,t),f.getUniforms().setValue(i,"morphTargetsTextureSize",x.size)}else{const p=m===void 0?0:m.length;let h=n[u.id];if(h===void 0||h.length!==p){h=[];for(let y=0;y<p;y++)h[y]=[y,0];n[u.id]=h}for(let y=0;y<p;y++){const R=h[y];R[0]=y,R[1]=m[y]}h.sort(ph);for(let y=0;y<8;y++)y<p&&h[y][1]?(a[y][0]=h[y][0],a[y][1]=h[y][1]):(a[y][0]=Number.MAX_SAFE_INTEGER,a[y][1]=0);a.sort(fh);const x=u.morphAttributes.position,E=u.morphAttributes.normal;let b=0;for(let y=0;y<8;y++){const R=a[y],U=R[0],v=R[1];U!==Number.MAX_SAFE_INTEGER&&v?(x&&u.getAttribute("morphTarget"+y)!==x[U]&&u.setAttribute("morphTarget"+y,x[U]),E&&u.getAttribute("morphNormal"+y)!==E[U]&&u.setAttribute("morphNormal"+y,E[U]),r[y]=v,b+=v):(x&&u.hasAttribute("morphTarget"+y)===!0&&u.deleteAttribute("morphTarget"+y),E&&u.hasAttribute("morphNormal"+y)===!0&&u.deleteAttribute("morphNormal"+y),r[y]=0)}const S=u.morphTargetsRelative?1:1-b;f.getUniforms().setValue(i,"morphTargetBaseInfluence",S),f.getUniforms().setValue(i,"morphTargetInfluences",r)}}return{update:l}}function gh(i,e,t,n){let r=new WeakMap;function s(l){const c=n.render.frame,u=l.geometry,d=e.get(l,u);return r.get(d)!==c&&(e.update(d),r.set(d,c)),l.isInstancedMesh&&(l.hasEventListener("dispose",a)===!1&&l.addEventListener("dispose",a),t.update(l.instanceMatrix,34962),l.instanceColor!==null&&t.update(l.instanceColor,34962)),d}function o(){r=new WeakMap}function a(l){const c=l.target;c.removeEventListener("dispose",a),t.remove(c.instanceMatrix),c.instanceColor!==null&&t.remove(c.instanceColor)}return{update:s,dispose:o}}const Do=new ft,Uo=new Io,No=new tc,Fo=new Po,Pa=[],za=[],Va=new Float32Array(16),Da=new Float32Array(9),Ua=new Float32Array(4);function di(i,e,t){const n=i[0];if(n<=0||n>0)return i;const r=e*t;let s=Pa[r];if(s===void 0&&(s=new Float32Array(r),Pa[r]=s),e!==0){n.toArray(s,0);for(let o=1,a=0;o!==e;++o)a+=t,i[o].toArray(s,a)}return s}function Je(i,e){if(i.length!==e.length)return!1;for(let t=0,n=i.length;t<n;t++)if(i[t]!==e[t])return!1;return!0}function $e(i,e){for(let t=0,n=e.length;t<n;t++)i[t]=e[t]}function yr(i,e){let t=za[e];t===void 0&&(t=new Int32Array(e),za[e]=t);for(let n=0;n!==e;++n)t[n]=i.allocateTextureUnit();return t}function vh(i,e){const t=this.cache;t[0]!==e&&(i.uniform1f(this.addr,e),t[0]=e)}function xh(i,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y)&&(i.uniform2f(this.addr,e.x,e.y),t[0]=e.x,t[1]=e.y);else{if(Je(t,e))return;i.uniform2fv(this.addr,e),$e(t,e)}}function Mh(i,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z)&&(i.uniform3f(this.addr,e.x,e.y,e.z),t[0]=e.x,t[1]=e.y,t[2]=e.z);else if(e.r!==void 0)(t[0]!==e.r||t[1]!==e.g||t[2]!==e.b)&&(i.uniform3f(this.addr,e.r,e.g,e.b),t[0]=e.r,t[1]=e.g,t[2]=e.b);else{if(Je(t,e))return;i.uniform3fv(this.addr,e),$e(t,e)}}function yh(i,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z||t[3]!==e.w)&&(i.uniform4f(this.addr,e.x,e.y,e.z,e.w),t[0]=e.x,t[1]=e.y,t[2]=e.z,t[3]=e.w);else{if(Je(t,e))return;i.uniform4fv(this.addr,e),$e(t,e)}}function Ah(i,e){const t=this.cache,n=e.elements;if(n===void 0){if(Je(t,e))return;i.uniformMatrix2fv(this.addr,!1,e),$e(t,e)}else{if(Je(t,n))return;Ua.set(n),i.uniformMatrix2fv(this.addr,!1,Ua),$e(t,n)}}function _h(i,e){const t=this.cache,n=e.elements;if(n===void 0){if(Je(t,e))return;i.uniformMatrix3fv(this.addr,!1,e),$e(t,e)}else{if(Je(t,n))return;Da.set(n),i.uniformMatrix3fv(this.addr,!1,Da),$e(t,n)}}function bh(i,e){const t=this.cache,n=e.elements;if(n===void 0){if(Je(t,e))return;i.uniformMatrix4fv(this.addr,!1,e),$e(t,e)}else{if(Je(t,n))return;Va.set(n),i.uniformMatrix4fv(this.addr,!1,Va),$e(t,n)}}function Sh(i,e){const t=this.cache;t[0]!==e&&(i.uniform1i(this.addr,e),t[0]=e)}function wh(i,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y)&&(i.uniform2i(this.addr,e.x,e.y),t[0]=e.x,t[1]=e.y);else{if(Je(t,e))return;i.uniform2iv(this.addr,e),$e(t,e)}}function Eh(i,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z)&&(i.uniform3i(this.addr,e.x,e.y,e.z),t[0]=e.x,t[1]=e.y,t[2]=e.z);else{if(Je(t,e))return;i.uniform3iv(this.addr,e),$e(t,e)}}function Th(i,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z||t[3]!==e.w)&&(i.uniform4i(this.addr,e.x,e.y,e.z,e.w),t[0]=e.x,t[1]=e.y,t[2]=e.z,t[3]=e.w);else{if(Je(t,e))return;i.uniform4iv(this.addr,e),$e(t,e)}}function Ih(i,e){const t=this.cache;t[0]!==e&&(i.uniform1ui(this.addr,e),t[0]=e)}function Rh(i,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y)&&(i.uniform2ui(this.addr,e.x,e.y),t[0]=e.x,t[1]=e.y);else{if(Je(t,e))return;i.uniform2uiv(this.addr,e),$e(t,e)}}function Ch(i,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z)&&(i.uniform3ui(this.addr,e.x,e.y,e.z),t[0]=e.x,t[1]=e.y,t[2]=e.z);else{if(Je(t,e))return;i.uniform3uiv(this.addr,e),$e(t,e)}}function Lh(i,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z||t[3]!==e.w)&&(i.uniform4ui(this.addr,e.x,e.y,e.z,e.w),t[0]=e.x,t[1]=e.y,t[2]=e.z,t[3]=e.w);else{if(Je(t,e))return;i.uniform4uiv(this.addr,e),$e(t,e)}}function Ph(i,e,t){const n=this.cache,r=t.allocateTextureUnit();n[0]!==r&&(i.uniform1i(this.addr,r),n[0]=r),t.setTexture2D(e||Do,r)}function zh(i,e,t){const n=this.cache,r=t.allocateTextureUnit();n[0]!==r&&(i.uniform1i(this.addr,r),n[0]=r),t.setTexture3D(e||No,r)}function Vh(i,e,t){const n=this.cache,r=t.allocateTextureUnit();n[0]!==r&&(i.uniform1i(this.addr,r),n[0]=r),t.setTextureCube(e||Fo,r)}function Dh(i,e,t){const n=this.cache,r=t.allocateTextureUnit();n[0]!==r&&(i.uniform1i(this.addr,r),n[0]=r),t.setTexture2DArray(e||Uo,r)}function Uh(i){switch(i){case 5126:return vh;case 35664:return xh;case 35665:return Mh;case 35666:return yh;case 35674:return Ah;case 35675:return _h;case 35676:return bh;case 5124:case 35670:return Sh;case 35667:case 35671:return wh;case 35668:case 35672:return Eh;case 35669:case 35673:return Th;case 5125:return Ih;case 36294:return Rh;case 36295:return Ch;case 36296:return Lh;case 35678:case 36198:case 36298:case 36306:case 35682:return Ph;case 35679:case 36299:case 36307:return zh;case 35680:case 36300:case 36308:case 36293:return Vh;case 36289:case 36303:case 36311:case 36292:return Dh}}function Nh(i,e){i.uniform1fv(this.addr,e)}function Fh(i,e){const t=di(e,this.size,2);i.uniform2fv(this.addr,t)}function qh(i,e){const t=di(e,this.size,3);i.uniform3fv(this.addr,t)}function Oh(i,e){const t=di(e,this.size,4);i.uniform4fv(this.addr,t)}function Bh(i,e){const t=di(e,this.size,4);i.uniformMatrix2fv(this.addr,!1,t)}function kh(i,e){const t=di(e,this.size,9);i.uniformMatrix3fv(this.addr,!1,t)}function Hh(i,e){const t=di(e,this.size,16);i.uniformMatrix4fv(this.addr,!1,t)}function Zh(i,e){i.uniform1iv(this.addr,e)}function Gh(i,e){i.uniform2iv(this.addr,e)}function Wh(i,e){i.uniform3iv(this.addr,e)}function Xh(i,e){i.uniform4iv(this.addr,e)}function jh(i,e){i.uniform1uiv(this.addr,e)}function Yh(i,e){i.uniform2uiv(this.addr,e)}function Kh(i,e){i.uniform3uiv(this.addr,e)}function Qh(i,e){i.uniform4uiv(this.addr,e)}function Jh(i,e,t){const n=this.cache,r=e.length,s=yr(t,r);Je(n,s)||(i.uniform1iv(this.addr,s),$e(n,s));for(let o=0;o!==r;++o)t.setTexture2D(e[o]||Do,s[o])}function $h(i,e,t){const n=this.cache,r=e.length,s=yr(t,r);Je(n,s)||(i.uniform1iv(this.addr,s),$e(n,s));for(let o=0;o!==r;++o)t.setTexture3D(e[o]||No,s[o])}function ef(i,e,t){const n=this.cache,r=e.length,s=yr(t,r);Je(n,s)||(i.uniform1iv(this.addr,s),$e(n,s));for(let o=0;o!==r;++o)t.setTextureCube(e[o]||Fo,s[o])}function tf(i,e,t){const n=this.cache,r=e.length,s=yr(t,r);Je(n,s)||(i.uniform1iv(this.addr,s),$e(n,s));for(let o=0;o!==r;++o)t.setTexture2DArray(e[o]||Uo,s[o])}function nf(i){switch(i){case 5126:return Nh;case 35664:return Fh;case 35665:return qh;case 35666:return Oh;case 35674:return Bh;case 35675:return kh;case 35676:return Hh;case 5124:case 35670:return Zh;case 35667:case 35671:return Gh;case 35668:case 35672:return Wh;case 35669:case 35673:return Xh;case 5125:return jh;case 36294:return Yh;case 36295:return Kh;case 36296:return Qh;case 35678:case 36198:case 36298:case 36306:case 35682:return Jh;case 35679:case 36299:case 36307:return $h;case 35680:case 36300:case 36308:case 36293:return ef;case 36289:case 36303:case 36311:case 36292:return tf}}class rf{constructor(e,t,n){this.id=e,this.addr=n,this.cache=[],this.setValue=Uh(t.type)}}class sf{constructor(e,t,n){this.id=e,this.addr=n,this.cache=[],this.size=t.size,this.setValue=nf(t.type)}}class af{constructor(e){this.id=e,this.seq=[],this.map={}}setValue(e,t,n){const r=this.seq;for(let s=0,o=r.length;s!==o;++s){const a=r[s];a.setValue(e,t[a.id],n)}}}const rs=/(\w+)(\])?(\[|\.)?/g;function Na(i,e){i.seq.push(e),i.map[e.id]=e}function of(i,e,t){const n=i.name,r=n.length;for(rs.lastIndex=0;;){const s=rs.exec(n),o=rs.lastIndex;let a=s[1];const l=s[2]==="]",c=s[3];if(l&&(a=a|0),c===void 0||c==="["&&o+2===r){Na(t,c===void 0?new rf(a,i,e):new sf(a,i,e));break}else{let d=t.map[a];d===void 0&&(d=new af(a),Na(t,d)),t=d}}}class dr{constructor(e,t){this.seq=[],this.map={};const n=e.getProgramParameter(t,35718);for(let r=0;r<n;++r){const s=e.getActiveUniform(t,r),o=e.getUniformLocation(t,s.name);of(s,o,this)}}setValue(e,t,n,r){const s=this.map[t];s!==void 0&&s.setValue(e,n,r)}setOptional(e,t,n){const r=t[n];r!==void 0&&this.setValue(e,n,r)}static upload(e,t,n,r){for(let s=0,o=t.length;s!==o;++s){const a=t[s],l=n[a.id];l.needsUpdate!==!1&&a.setValue(e,l.value,r)}}static seqWithValue(e,t){const n=[];for(let r=0,s=e.length;r!==s;++r){const o=e[r];o.id in t&&n.push(o)}return n}}function Fa(i,e,t){const n=i.createShader(e);return i.shaderSource(n,t),i.compileShader(n),n}let lf=0;function cf(i,e){const t=i.split(`
`),n=[],r=Math.max(e-6,0),s=Math.min(e+6,t.length);for(let o=r;o<s;o++){const a=o+1;n.push(`${a===e?">":" "} ${a}: ${t[o]}`)}return n.join(`
`)}function uf(i){switch(i){case Rn:return["Linear","( value )"];case Be:return["sRGB","( value )"];default:return console.warn("THREE.WebGLProgram: Unsupported encoding:",i),["Linear","( value )"]}}function qa(i,e,t){const n=i.getShaderParameter(e,35713),r=i.getShaderInfoLog(e).trim();if(n&&r==="")return"";const s=/ERROR: 0:(\d+)/.exec(r);if(s){const o=parseInt(s[1]);return t.toUpperCase()+`

`+r+`

`+cf(i.getShaderSource(e),o)}else return r}function df(i,e){const t=uf(e);return"vec4 "+i+"( vec4 value ) { return LinearTo"+t[0]+t[1]+"; }"}function hf(i,e){let t;switch(e){case Tl:t="Linear";break;case Il:t="Reinhard";break;case Rl:t="OptimizedCineon";break;case Cl:t="ACESFilmic";break;case Ll:t="Custom";break;default:console.warn("THREE.WebGLProgram: Unsupported toneMapping:",e),t="Linear"}return"vec3 "+i+"( vec3 color ) { return "+t+"ToneMapping( color ); }"}function ff(i){return[i.extensionDerivatives||!!i.envMapCubeUVHeight||i.bumpMap||i.tangentSpaceNormalMap||i.clearcoatNormalMap||i.flatShading||i.shaderID==="physical"?"#extension GL_OES_standard_derivatives : enable":"",(i.extensionFragDepth||i.logarithmicDepthBuffer)&&i.rendererExtensionFragDepth?"#extension GL_EXT_frag_depth : enable":"",i.extensionDrawBuffers&&i.rendererExtensionDrawBuffers?"#extension GL_EXT_draw_buffers : require":"",(i.extensionShaderTextureLOD||i.envMap||i.transmission)&&i.rendererExtensionShaderTextureLod?"#extension GL_EXT_shader_texture_lod : enable":""].filter(Si).join(`
`)}function pf(i){const e=[];for(const t in i){const n=i[t];n!==!1&&e.push("#define "+t+" "+n)}return e.join(`
`)}function mf(i,e){const t={},n=i.getProgramParameter(e,35721);for(let r=0;r<n;r++){const s=i.getActiveAttrib(e,r),o=s.name;let a=1;s.type===35674&&(a=2),s.type===35675&&(a=3),s.type===35676&&(a=4),t[o]={type:s.type,location:i.getAttribLocation(e,o),locationSize:a}}return t}function Si(i){return i!==""}function Oa(i,e){const t=e.numSpotLightShadows+e.numSpotLightMaps-e.numSpotLightShadowsWithMaps;return i.replace(/NUM_DIR_LIGHTS/g,e.numDirLights).replace(/NUM_SPOT_LIGHTS/g,e.numSpotLights).replace(/NUM_SPOT_LIGHT_MAPS/g,e.numSpotLightMaps).replace(/NUM_SPOT_LIGHT_COORDS/g,t).replace(/NUM_RECT_AREA_LIGHTS/g,e.numRectAreaLights).replace(/NUM_POINT_LIGHTS/g,e.numPointLights).replace(/NUM_HEMI_LIGHTS/g,e.numHemiLights).replace(/NUM_DIR_LIGHT_SHADOWS/g,e.numDirLightShadows).replace(/NUM_SPOT_LIGHT_SHADOWS_WITH_MAPS/g,e.numSpotLightShadowsWithMaps).replace(/NUM_SPOT_LIGHT_SHADOWS/g,e.numSpotLightShadows).replace(/NUM_POINT_LIGHT_SHADOWS/g,e.numPointLightShadows)}function Ba(i,e){return i.replace(/NUM_CLIPPING_PLANES/g,e.numClippingPlanes).replace(/UNION_CLIPPING_PLANES/g,e.numClippingPlanes-e.numClipIntersection)}const gf=/^[ \t]*#include +<([\w\d./]+)>/gm;function xs(i){return i.replace(gf,vf)}function vf(i,e){const t=_e[e];if(t===void 0)throw new Error("Can not resolve #include <"+e+">");return xs(t)}const xf=/#pragma unroll_loop_start\s+for\s*\(\s*int\s+i\s*=\s*(\d+)\s*;\s*i\s*<\s*(\d+)\s*;\s*i\s*\+\+\s*\)\s*{([\s\S]+?)}\s+#pragma unroll_loop_end/g;function ka(i){return i.replace(xf,Mf)}function Mf(i,e,t,n){let r="";for(let s=parseInt(e);s<parseInt(t);s++)r+=n.replace(/\[\s*i\s*\]/g,"[ "+s+" ]").replace(/UNROLLED_LOOP_INDEX/g,s);return r}function Ha(i){let e="precision "+i.precision+` float;
precision `+i.precision+" int;";return i.precision==="highp"?e+=`
#define HIGH_PRECISION`:i.precision==="mediump"?e+=`
#define MEDIUM_PRECISION`:i.precision==="lowp"&&(e+=`
#define LOW_PRECISION`),e}function yf(i){let e="SHADOWMAP_TYPE_BASIC";return i.shadowMapType===xo?e="SHADOWMAP_TYPE_PCF":i.shadowMapType===sl?e="SHADOWMAP_TYPE_PCF_SOFT":i.shadowMapType===bi&&(e="SHADOWMAP_TYPE_VSM"),e}function Af(i){let e="ENVMAP_TYPE_CUBE";if(i.envMap)switch(i.envMapMode){case ni:case ii:e="ENVMAP_TYPE_CUBE";break;case gr:e="ENVMAP_TYPE_CUBE_UV";break}return e}function _f(i){let e="ENVMAP_MODE_REFLECTION";if(i.envMap)switch(i.envMapMode){case ii:e="ENVMAP_MODE_REFRACTION";break}return e}function bf(i){let e="ENVMAP_BLENDING_NONE";if(i.envMap)switch(i.combine){case Ao:e="ENVMAP_BLENDING_MULTIPLY";break;case wl:e="ENVMAP_BLENDING_MIX";break;case El:e="ENVMAP_BLENDING_ADD";break}return e}function Sf(i){const e=i.envMapCubeUVHeight;if(e===null)return null;const t=Math.log2(e)-2,n=1/e;return{texelWidth:1/(3*Math.max(Math.pow(2,t),7*16)),texelHeight:n,maxMip:t}}function wf(i,e,t,n){const r=i.getContext(),s=t.defines;let o=t.vertexShader,a=t.fragmentShader;const l=yf(t),c=Af(t),u=_f(t),d=bf(t),f=Sf(t),m=t.isWebGL2?"":ff(t),g=pf(s),p=r.createProgram();let h,x,E=t.glslVersion?"#version "+t.glslVersion+`
`:"";t.isRawShaderMaterial?(h=[g].filter(Si).join(`
`),h.length>0&&(h+=`
`),x=[m,g].filter(Si).join(`
`),x.length>0&&(x+=`
`)):(h=[Ha(t),"#define SHADER_NAME "+t.shaderName,g,t.instancing?"#define USE_INSTANCING":"",t.instancingColor?"#define USE_INSTANCING_COLOR":"",t.supportsVertexTextures?"#define VERTEX_TEXTURES":"",t.useFog&&t.fog?"#define USE_FOG":"",t.useFog&&t.fogExp2?"#define FOG_EXP2":"",t.map?"#define USE_MAP":"",t.envMap?"#define USE_ENVMAP":"",t.envMap?"#define "+u:"",t.lightMap?"#define USE_LIGHTMAP":"",t.aoMap?"#define USE_AOMAP":"",t.emissiveMap?"#define USE_EMISSIVEMAP":"",t.bumpMap?"#define USE_BUMPMAP":"",t.normalMap?"#define USE_NORMALMAP":"",t.normalMap&&t.objectSpaceNormalMap?"#define OBJECTSPACE_NORMALMAP":"",t.normalMap&&t.tangentSpaceNormalMap?"#define TANGENTSPACE_NORMALMAP":"",t.clearcoatMap?"#define USE_CLEARCOATMAP":"",t.clearcoatRoughnessMap?"#define USE_CLEARCOAT_ROUGHNESSMAP":"",t.clearcoatNormalMap?"#define USE_CLEARCOAT_NORMALMAP":"",t.iridescenceMap?"#define USE_IRIDESCENCEMAP":"",t.iridescenceThicknessMap?"#define USE_IRIDESCENCE_THICKNESSMAP":"",t.displacementMap&&t.supportsVertexTextures?"#define USE_DISPLACEMENTMAP":"",t.specularMap?"#define USE_SPECULARMAP":"",t.specularIntensityMap?"#define USE_SPECULARINTENSITYMAP":"",t.specularColorMap?"#define USE_SPECULARCOLORMAP":"",t.roughnessMap?"#define USE_ROUGHNESSMAP":"",t.metalnessMap?"#define USE_METALNESSMAP":"",t.alphaMap?"#define USE_ALPHAMAP":"",t.transmission?"#define USE_TRANSMISSION":"",t.transmissionMap?"#define USE_TRANSMISSIONMAP":"",t.thicknessMap?"#define USE_THICKNESSMAP":"",t.sheenColorMap?"#define USE_SHEENCOLORMAP":"",t.sheenRoughnessMap?"#define USE_SHEENROUGHNESSMAP":"",t.vertexTangents?"#define USE_TANGENT":"",t.vertexColors?"#define USE_COLOR":"",t.vertexAlphas?"#define USE_COLOR_ALPHA":"",t.vertexUvs?"#define USE_UV":"",t.uvsVertexOnly?"#define UVS_VERTEX_ONLY":"",t.flatShading?"#define FLAT_SHADED":"",t.skinning?"#define USE_SKINNING":"",t.morphTargets?"#define USE_MORPHTARGETS":"",t.morphNormals&&t.flatShading===!1?"#define USE_MORPHNORMALS":"",t.morphColors&&t.isWebGL2?"#define USE_MORPHCOLORS":"",t.morphTargetsCount>0&&t.isWebGL2?"#define MORPHTARGETS_TEXTURE":"",t.morphTargetsCount>0&&t.isWebGL2?"#define MORPHTARGETS_TEXTURE_STRIDE "+t.morphTextureStride:"",t.morphTargetsCount>0&&t.isWebGL2?"#define MORPHTARGETS_COUNT "+t.morphTargetsCount:"",t.doubleSided?"#define DOUBLE_SIDED":"",t.flipSided?"#define FLIP_SIDED":"",t.shadowMapEnabled?"#define USE_SHADOWMAP":"",t.shadowMapEnabled?"#define "+l:"",t.sizeAttenuation?"#define USE_SIZEATTENUATION":"",t.logarithmicDepthBuffer?"#define USE_LOGDEPTHBUF":"",t.logarithmicDepthBuffer&&t.rendererExtensionFragDepth?"#define USE_LOGDEPTHBUF_EXT":"","uniform mat4 modelMatrix;","uniform mat4 modelViewMatrix;","uniform mat4 projectionMatrix;","uniform mat4 viewMatrix;","uniform mat3 normalMatrix;","uniform vec3 cameraPosition;","uniform bool isOrthographic;","#ifdef USE_INSTANCING","	attribute mat4 instanceMatrix;","#endif","#ifdef USE_INSTANCING_COLOR","	attribute vec3 instanceColor;","#endif","attribute vec3 position;","attribute vec3 normal;","attribute vec2 uv;","#ifdef USE_TANGENT","	attribute vec4 tangent;","#endif","#if defined( USE_COLOR_ALPHA )","	attribute vec4 color;","#elif defined( USE_COLOR )","	attribute vec3 color;","#endif","#if ( defined( USE_MORPHTARGETS ) && ! defined( MORPHTARGETS_TEXTURE ) )","	attribute vec3 morphTarget0;","	attribute vec3 morphTarget1;","	attribute vec3 morphTarget2;","	attribute vec3 morphTarget3;","	#ifdef USE_MORPHNORMALS","		attribute vec3 morphNormal0;","		attribute vec3 morphNormal1;","		attribute vec3 morphNormal2;","		attribute vec3 morphNormal3;","	#else","		attribute vec3 morphTarget4;","		attribute vec3 morphTarget5;","		attribute vec3 morphTarget6;","		attribute vec3 morphTarget7;","	#endif","#endif","#ifdef USE_SKINNING","	attribute vec4 skinIndex;","	attribute vec4 skinWeight;","#endif",`
`].filter(Si).join(`
`),x=[m,Ha(t),"#define SHADER_NAME "+t.shaderName,g,t.useFog&&t.fog?"#define USE_FOG":"",t.useFog&&t.fogExp2?"#define FOG_EXP2":"",t.map?"#define USE_MAP":"",t.matcap?"#define USE_MATCAP":"",t.envMap?"#define USE_ENVMAP":"",t.envMap?"#define "+c:"",t.envMap?"#define "+u:"",t.envMap?"#define "+d:"",f?"#define CUBEUV_TEXEL_WIDTH "+f.texelWidth:"",f?"#define CUBEUV_TEXEL_HEIGHT "+f.texelHeight:"",f?"#define CUBEUV_MAX_MIP "+f.maxMip+".0":"",t.lightMap?"#define USE_LIGHTMAP":"",t.aoMap?"#define USE_AOMAP":"",t.emissiveMap?"#define USE_EMISSIVEMAP":"",t.bumpMap?"#define USE_BUMPMAP":"",t.normalMap?"#define USE_NORMALMAP":"",t.normalMap&&t.objectSpaceNormalMap?"#define OBJECTSPACE_NORMALMAP":"",t.normalMap&&t.tangentSpaceNormalMap?"#define TANGENTSPACE_NORMALMAP":"",t.clearcoat?"#define USE_CLEARCOAT":"",t.clearcoatMap?"#define USE_CLEARCOATMAP":"",t.clearcoatRoughnessMap?"#define USE_CLEARCOAT_ROUGHNESSMAP":"",t.clearcoatNormalMap?"#define USE_CLEARCOAT_NORMALMAP":"",t.iridescence?"#define USE_IRIDESCENCE":"",t.iridescenceMap?"#define USE_IRIDESCENCEMAP":"",t.iridescenceThicknessMap?"#define USE_IRIDESCENCE_THICKNESSMAP":"",t.specularMap?"#define USE_SPECULARMAP":"",t.specularIntensityMap?"#define USE_SPECULARINTENSITYMAP":"",t.specularColorMap?"#define USE_SPECULARCOLORMAP":"",t.roughnessMap?"#define USE_ROUGHNESSMAP":"",t.metalnessMap?"#define USE_METALNESSMAP":"",t.alphaMap?"#define USE_ALPHAMAP":"",t.alphaTest?"#define USE_ALPHATEST":"",t.sheen?"#define USE_SHEEN":"",t.sheenColorMap?"#define USE_SHEENCOLORMAP":"",t.sheenRoughnessMap?"#define USE_SHEENROUGHNESSMAP":"",t.transmission?"#define USE_TRANSMISSION":"",t.transmissionMap?"#define USE_TRANSMISSIONMAP":"",t.thicknessMap?"#define USE_THICKNESSMAP":"",t.decodeVideoTexture?"#define DECODE_VIDEO_TEXTURE":"",t.vertexTangents?"#define USE_TANGENT":"",t.vertexColors||t.instancingColor?"#define USE_COLOR":"",t.vertexAlphas?"#define USE_COLOR_ALPHA":"",t.vertexUvs?"#define USE_UV":"",t.uvsVertexOnly?"#define UVS_VERTEX_ONLY":"",t.gradientMap?"#define USE_GRADIENTMAP":"",t.flatShading?"#define FLAT_SHADED":"",t.doubleSided?"#define DOUBLE_SIDED":"",t.flipSided?"#define FLIP_SIDED":"",t.shadowMapEnabled?"#define USE_SHADOWMAP":"",t.shadowMapEnabled?"#define "+l:"",t.premultipliedAlpha?"#define PREMULTIPLIED_ALPHA":"",t.physicallyCorrectLights?"#define PHYSICALLY_CORRECT_LIGHTS":"",t.logarithmicDepthBuffer?"#define USE_LOGDEPTHBUF":"",t.logarithmicDepthBuffer&&t.rendererExtensionFragDepth?"#define USE_LOGDEPTHBUF_EXT":"","uniform mat4 viewMatrix;","uniform vec3 cameraPosition;","uniform bool isOrthographic;",t.toneMapping!==tn?"#define TONE_MAPPING":"",t.toneMapping!==tn?_e.tonemapping_pars_fragment:"",t.toneMapping!==tn?hf("toneMapping",t.toneMapping):"",t.dithering?"#define DITHERING":"",t.opaque?"#define OPAQUE":"",_e.encodings_pars_fragment,df("linearToOutputTexel",t.outputEncoding),t.useDepthPacking?"#define DEPTH_PACKING "+t.depthPacking:"",`
`].filter(Si).join(`
`)),o=xs(o),o=Oa(o,t),o=Ba(o,t),a=xs(a),a=Oa(a,t),a=Ba(a,t),o=ka(o),a=ka(a),t.isWebGL2&&t.isRawShaderMaterial!==!0&&(E=`#version 300 es
`,h=["precision mediump sampler2DArray;","#define attribute in","#define varying out","#define texture2D texture"].join(`
`)+`
`+h,x=["#define varying in",t.glslVersion===ha?"":"layout(location = 0) out highp vec4 pc_fragColor;",t.glslVersion===ha?"":"#define gl_FragColor pc_fragColor","#define gl_FragDepthEXT gl_FragDepth","#define texture2D texture","#define textureCube texture","#define texture2DProj textureProj","#define texture2DLodEXT textureLod","#define texture2DProjLodEXT textureProjLod","#define textureCubeLodEXT textureLod","#define texture2DGradEXT textureGrad","#define texture2DProjGradEXT textureProjGrad","#define textureCubeGradEXT textureGrad"].join(`
`)+`
`+x);const b=E+h+o,S=E+x+a,y=Fa(r,35633,b),R=Fa(r,35632,S);if(r.attachShader(p,y),r.attachShader(p,R),t.index0AttributeName!==void 0?r.bindAttribLocation(p,0,t.index0AttributeName):t.morphTargets===!0&&r.bindAttribLocation(p,0,"position"),r.linkProgram(p),i.debug.checkShaderErrors){const T=r.getProgramInfoLog(p).trim(),D=r.getShaderInfoLog(y).trim(),X=r.getShaderInfoLog(R).trim();let oe=!0,N=!0;if(r.getProgramParameter(p,35714)===!1){oe=!1;const z=qa(r,y,"vertex"),W=qa(r,R,"fragment");console.error("THREE.WebGLProgram: Shader Error "+r.getError()+" - VALIDATE_STATUS "+r.getProgramParameter(p,35715)+`

Program Info Log: `+T+`
`+z+`
`+W)}else T!==""?console.warn("THREE.WebGLProgram: Program Info Log:",T):(D===""||X==="")&&(N=!1);N&&(this.diagnostics={runnable:oe,programLog:T,vertexShader:{log:D,prefix:h},fragmentShader:{log:X,prefix:x}})}r.deleteShader(y),r.deleteShader(R);let U;this.getUniforms=function(){return U===void 0&&(U=new dr(r,p)),U};let v;return this.getAttributes=function(){return v===void 0&&(v=mf(r,p)),v},this.destroy=function(){n.releaseStatesOfProgram(this),r.deleteProgram(p),this.program=void 0},this.name=t.shaderName,this.id=lf++,this.cacheKey=e,this.usedTimes=1,this.program=p,this.vertexShader=y,this.fragmentShader=R,this}let Ef=0;class Tf{constructor(){this.shaderCache=new Map,this.materialCache=new Map}update(e){const t=e.vertexShader,n=e.fragmentShader,r=this._getShaderStage(t),s=this._getShaderStage(n),o=this._getShaderCacheForMaterial(e);return o.has(r)===!1&&(o.add(r),r.usedTimes++),o.has(s)===!1&&(o.add(s),s.usedTimes++),this}remove(e){const t=this.materialCache.get(e);for(const n of t)n.usedTimes--,n.usedTimes===0&&this.shaderCache.delete(n.code);return this.materialCache.delete(e),this}getVertexShaderID(e){return this._getShaderStage(e.vertexShader).id}getFragmentShaderID(e){return this._getShaderStage(e.fragmentShader).id}dispose(){this.shaderCache.clear(),this.materialCache.clear()}_getShaderCacheForMaterial(e){const t=this.materialCache;let n=t.get(e);return n===void 0&&(n=new Set,t.set(e,n)),n}_getShaderStage(e){const t=this.shaderCache;let n=t.get(e);return n===void 0&&(n=new If(e),t.set(e,n)),n}}class If{constructor(e){this.id=Ef++,this.code=e,this.usedTimes=0}}function Rf(i,e,t,n,r,s,o){const a=new ws,l=new Tf,c=[],u=r.isWebGL2,d=r.logarithmicDepthBuffer,f=r.vertexTextures;let m=r.precision;const g={MeshDepthMaterial:"depth",MeshDistanceMaterial:"distanceRGBA",MeshNormalMaterial:"normal",MeshBasicMaterial:"basic",MeshLambertMaterial:"lambert",MeshPhongMaterial:"phong",MeshToonMaterial:"toon",MeshStandardMaterial:"physical",MeshPhysicalMaterial:"physical",MeshMatcapMaterial:"matcap",LineBasicMaterial:"basic",LineDashedMaterial:"dashed",PointsMaterial:"points",ShadowMaterial:"shadow",SpriteMaterial:"sprite"};function p(v,T,D,X,oe){const N=X.fog,z=oe.geometry,W=v.isMeshStandardMaterial?X.environment:null,K=(v.isMeshStandardMaterial?t:e).get(v.envMap||W),j=!!K&&K.mapping===gr?K.image.height:null,G=g[v.type];v.precision!==null&&(m=r.getMaxPrecision(v.precision),m!==v.precision&&console.warn("THREE.WebGLProgram.getParameters:",v.precision,"not supported, using",m,"instead."));const te=z.morphAttributes.position||z.morphAttributes.normal||z.morphAttributes.color,J=te!==void 0?te.length:0;let F=0;z.morphAttributes.position!==void 0&&(F=1),z.morphAttributes.normal!==void 0&&(F=2),z.morphAttributes.color!==void 0&&(F=3);let k,Q,ee,re;if(G){const ze=Gt[G];k=ze.vertexShader,Q=ze.fragmentShader}else k=v.vertexShader,Q=v.fragmentShader,l.update(v),ee=l.getVertexShaderID(v),re=l.getFragmentShaderID(v);const H=i.getRenderTarget(),Ee=v.alphaTest>0,he=v.clearcoat>0,ve=v.iridescence>0;return{isWebGL2:u,shaderID:G,shaderName:v.type,vertexShader:k,fragmentShader:Q,defines:v.defines,customVertexShaderID:ee,customFragmentShaderID:re,isRawShaderMaterial:v.isRawShaderMaterial===!0,glslVersion:v.glslVersion,precision:m,instancing:oe.isInstancedMesh===!0,instancingColor:oe.isInstancedMesh===!0&&oe.instanceColor!==null,supportsVertexTextures:f,outputEncoding:H===null?i.outputEncoding:H.isXRRenderTarget===!0?H.texture.encoding:Rn,map:!!v.map,matcap:!!v.matcap,envMap:!!K,envMapMode:K&&K.mapping,envMapCubeUVHeight:j,lightMap:!!v.lightMap,aoMap:!!v.aoMap,emissiveMap:!!v.emissiveMap,bumpMap:!!v.bumpMap,normalMap:!!v.normalMap,objectSpaceNormalMap:v.normalMapType===Ql,tangentSpaceNormalMap:v.normalMapType===Kl,decodeVideoTexture:!!v.map&&v.map.isVideoTexture===!0&&v.map.encoding===Be,clearcoat:he,clearcoatMap:he&&!!v.clearcoatMap,clearcoatRoughnessMap:he&&!!v.clearcoatRoughnessMap,clearcoatNormalMap:he&&!!v.clearcoatNormalMap,iridescence:ve,iridescenceMap:ve&&!!v.iridescenceMap,iridescenceThicknessMap:ve&&!!v.iridescenceThicknessMap,displacementMap:!!v.displacementMap,roughnessMap:!!v.roughnessMap,metalnessMap:!!v.metalnessMap,specularMap:!!v.specularMap,specularIntensityMap:!!v.specularIntensityMap,specularColorMap:!!v.specularColorMap,opaque:v.transparent===!1&&v.blending===$n,alphaMap:!!v.alphaMap,alphaTest:Ee,gradientMap:!!v.gradientMap,sheen:v.sheen>0,sheenColorMap:!!v.sheenColorMap,sheenRoughnessMap:!!v.sheenRoughnessMap,transmission:v.transmission>0,transmissionMap:!!v.transmissionMap,thicknessMap:!!v.thicknessMap,combine:v.combine,vertexTangents:!!v.normalMap&&!!z.attributes.tangent,vertexColors:v.vertexColors,vertexAlphas:v.vertexColors===!0&&!!z.attributes.color&&z.attributes.color.itemSize===4,vertexUvs:!!v.map||!!v.bumpMap||!!v.normalMap||!!v.specularMap||!!v.alphaMap||!!v.emissiveMap||!!v.roughnessMap||!!v.metalnessMap||!!v.clearcoatMap||!!v.clearcoatRoughnessMap||!!v.clearcoatNormalMap||!!v.iridescenceMap||!!v.iridescenceThicknessMap||!!v.displacementMap||!!v.transmissionMap||!!v.thicknessMap||!!v.specularIntensityMap||!!v.specularColorMap||!!v.sheenColorMap||!!v.sheenRoughnessMap,uvsVertexOnly:!(!!v.map||!!v.bumpMap||!!v.normalMap||!!v.specularMap||!!v.alphaMap||!!v.emissiveMap||!!v.roughnessMap||!!v.metalnessMap||!!v.clearcoatNormalMap||!!v.iridescenceMap||!!v.iridescenceThicknessMap||v.transmission>0||!!v.transmissionMap||!!v.thicknessMap||!!v.specularIntensityMap||!!v.specularColorMap||v.sheen>0||!!v.sheenColorMap||!!v.sheenRoughnessMap)&&!!v.displacementMap,fog:!!N,useFog:v.fog===!0,fogExp2:N&&N.isFogExp2,flatShading:!!v.flatShading,sizeAttenuation:v.sizeAttenuation,logarithmicDepthBuffer:d,skinning:oe.isSkinnedMesh===!0,morphTargets:z.morphAttributes.position!==void 0,morphNormals:z.morphAttributes.normal!==void 0,morphColors:z.morphAttributes.color!==void 0,morphTargetsCount:J,morphTextureStride:F,numDirLights:T.directional.length,numPointLights:T.point.length,numSpotLights:T.spot.length,numSpotLightMaps:T.spotLightMap.length,numRectAreaLights:T.rectArea.length,numHemiLights:T.hemi.length,numDirLightShadows:T.directionalShadowMap.length,numPointLightShadows:T.pointShadowMap.length,numSpotLightShadows:T.spotShadowMap.length,numSpotLightShadowsWithMaps:T.numSpotLightShadowsWithMaps,numClippingPlanes:o.numPlanes,numClipIntersection:o.numIntersection,dithering:v.dithering,shadowMapEnabled:i.shadowMap.enabled&&D.length>0,shadowMapType:i.shadowMap.type,toneMapping:v.toneMapped?i.toneMapping:tn,physicallyCorrectLights:i.physicallyCorrectLights,premultipliedAlpha:v.premultipliedAlpha,doubleSided:v.side===dn,flipSided:v.side===Vt,useDepthPacking:!!v.depthPacking,depthPacking:v.depthPacking||0,index0AttributeName:v.index0AttributeName,extensionDerivatives:v.extensions&&v.extensions.derivatives,extensionFragDepth:v.extensions&&v.extensions.fragDepth,extensionDrawBuffers:v.extensions&&v.extensions.drawBuffers,extensionShaderTextureLOD:v.extensions&&v.extensions.shaderTextureLOD,rendererExtensionFragDepth:u||n.has("EXT_frag_depth"),rendererExtensionDrawBuffers:u||n.has("WEBGL_draw_buffers"),rendererExtensionShaderTextureLod:u||n.has("EXT_shader_texture_lod"),customProgramCacheKey:v.customProgramCacheKey()}}function h(v){const T=[];if(v.shaderID?T.push(v.shaderID):(T.push(v.customVertexShaderID),T.push(v.customFragmentShaderID)),v.defines!==void 0)for(const D in v.defines)T.push(D),T.push(v.defines[D]);return v.isRawShaderMaterial===!1&&(x(T,v),E(T,v),T.push(i.outputEncoding)),T.push(v.customProgramCacheKey),T.join()}function x(v,T){v.push(T.precision),v.push(T.outputEncoding),v.push(T.envMapMode),v.push(T.envMapCubeUVHeight),v.push(T.combine),v.push(T.vertexUvs),v.push(T.fogExp2),v.push(T.sizeAttenuation),v.push(T.morphTargetsCount),v.push(T.morphAttributeCount),v.push(T.numDirLights),v.push(T.numPointLights),v.push(T.numSpotLights),v.push(T.numSpotLightMaps),v.push(T.numHemiLights),v.push(T.numRectAreaLights),v.push(T.numDirLightShadows),v.push(T.numPointLightShadows),v.push(T.numSpotLightShadows),v.push(T.numSpotLightShadowsWithMaps),v.push(T.shadowMapType),v.push(T.toneMapping),v.push(T.numClippingPlanes),v.push(T.numClipIntersection),v.push(T.depthPacking)}function E(v,T){a.disableAll(),T.isWebGL2&&a.enable(0),T.supportsVertexTextures&&a.enable(1),T.instancing&&a.enable(2),T.instancingColor&&a.enable(3),T.map&&a.enable(4),T.matcap&&a.enable(5),T.envMap&&a.enable(6),T.lightMap&&a.enable(7),T.aoMap&&a.enable(8),T.emissiveMap&&a.enable(9),T.bumpMap&&a.enable(10),T.normalMap&&a.enable(11),T.objectSpaceNormalMap&&a.enable(12),T.tangentSpaceNormalMap&&a.enable(13),T.clearcoat&&a.enable(14),T.clearcoatMap&&a.enable(15),T.clearcoatRoughnessMap&&a.enable(16),T.clearcoatNormalMap&&a.enable(17),T.iridescence&&a.enable(18),T.iridescenceMap&&a.enable(19),T.iridescenceThicknessMap&&a.enable(20),T.displacementMap&&a.enable(21),T.specularMap&&a.enable(22),T.roughnessMap&&a.enable(23),T.metalnessMap&&a.enable(24),T.gradientMap&&a.enable(25),T.alphaMap&&a.enable(26),T.alphaTest&&a.enable(27),T.vertexColors&&a.enable(28),T.vertexAlphas&&a.enable(29),T.vertexUvs&&a.enable(30),T.vertexTangents&&a.enable(31),T.uvsVertexOnly&&a.enable(32),v.push(a.mask),a.disableAll(),T.fog&&a.enable(0),T.useFog&&a.enable(1),T.flatShading&&a.enable(2),T.logarithmicDepthBuffer&&a.enable(3),T.skinning&&a.enable(4),T.morphTargets&&a.enable(5),T.morphNormals&&a.enable(6),T.morphColors&&a.enable(7),T.premultipliedAlpha&&a.enable(8),T.shadowMapEnabled&&a.enable(9),T.physicallyCorrectLights&&a.enable(10),T.doubleSided&&a.enable(11),T.flipSided&&a.enable(12),T.useDepthPacking&&a.enable(13),T.dithering&&a.enable(14),T.specularIntensityMap&&a.enable(15),T.specularColorMap&&a.enable(16),T.transmission&&a.enable(17),T.transmissionMap&&a.enable(18),T.thicknessMap&&a.enable(19),T.sheen&&a.enable(20),T.sheenColorMap&&a.enable(21),T.sheenRoughnessMap&&a.enable(22),T.decodeVideoTexture&&a.enable(23),T.opaque&&a.enable(24),v.push(a.mask)}function b(v){const T=g[v.type];let D;if(T){const X=Gt[T];D=fc.clone(X.uniforms)}else D=v.uniforms;return D}function S(v,T){let D;for(let X=0,oe=c.length;X<oe;X++){const N=c[X];if(N.cacheKey===T){D=N,++D.usedTimes;break}}return D===void 0&&(D=new wf(i,T,v,s),c.push(D)),D}function y(v){if(--v.usedTimes===0){const T=c.indexOf(v);c[T]=c[c.length-1],c.pop(),v.destroy()}}function R(v){l.remove(v)}function U(){l.dispose()}return{getParameters:p,getProgramCacheKey:h,getUniforms:b,acquireProgram:S,releaseProgram:y,releaseShaderCache:R,programs:c,dispose:U}}function Cf(){let i=new WeakMap;function e(s){let o=i.get(s);return o===void 0&&(o={},i.set(s,o)),o}function t(s){i.delete(s)}function n(s,o,a){i.get(s)[o]=a}function r(){i=new WeakMap}return{get:e,remove:t,update:n,dispose:r}}function Lf(i,e){return i.groupOrder!==e.groupOrder?i.groupOrder-e.groupOrder:i.renderOrder!==e.renderOrder?i.renderOrder-e.renderOrder:i.material.id!==e.material.id?i.material.id-e.material.id:i.z!==e.z?i.z-e.z:i.id-e.id}function Za(i,e){return i.groupOrder!==e.groupOrder?i.groupOrder-e.groupOrder:i.renderOrder!==e.renderOrder?i.renderOrder-e.renderOrder:i.z!==e.z?e.z-i.z:i.id-e.id}function Ga(){const i=[];let e=0;const t=[],n=[],r=[];function s(){e=0,t.length=0,n.length=0,r.length=0}function o(d,f,m,g,p,h){let x=i[e];return x===void 0?(x={id:d.id,object:d,geometry:f,material:m,groupOrder:g,renderOrder:d.renderOrder,z:p,group:h},i[e]=x):(x.id=d.id,x.object=d,x.geometry=f,x.material=m,x.groupOrder=g,x.renderOrder=d.renderOrder,x.z=p,x.group=h),e++,x}function a(d,f,m,g,p,h){const x=o(d,f,m,g,p,h);m.transmission>0?n.push(x):m.transparent===!0?r.push(x):t.push(x)}function l(d,f,m,g,p,h){const x=o(d,f,m,g,p,h);m.transmission>0?n.unshift(x):m.transparent===!0?r.unshift(x):t.unshift(x)}function c(d,f){t.length>1&&t.sort(d||Lf),n.length>1&&n.sort(f||Za),r.length>1&&r.sort(f||Za)}function u(){for(let d=e,f=i.length;d<f;d++){const m=i[d];if(m.id===null)break;m.id=null,m.object=null,m.geometry=null,m.material=null,m.group=null}}return{opaque:t,transmissive:n,transparent:r,init:s,push:a,unshift:l,finish:u,sort:c}}function Pf(){let i=new WeakMap;function e(n,r){const s=i.get(n);let o;return s===void 0?(o=new Ga,i.set(n,[o])):r>=s.length?(o=new Ga,s.push(o)):o=s[r],o}function t(){i=new WeakMap}return{get:e,dispose:t}}function zf(){const i={};return{get:function(e){if(i[e.id]!==void 0)return i[e.id];let t;switch(e.type){case"DirectionalLight":t={direction:new C,color:new qe};break;case"SpotLight":t={position:new C,direction:new C,color:new qe,distance:0,coneCos:0,penumbraCos:0,decay:0};break;case"PointLight":t={position:new C,color:new qe,distance:0,decay:0};break;case"HemisphereLight":t={direction:new C,skyColor:new qe,groundColor:new qe};break;case"RectAreaLight":t={color:new qe,position:new C,halfWidth:new C,halfHeight:new C};break}return i[e.id]=t,t}}}function Vf(){const i={};return{get:function(e){if(i[e.id]!==void 0)return i[e.id];let t;switch(e.type){case"DirectionalLight":t={shadowBias:0,shadowNormalBias:0,shadowRadius:1,shadowMapSize:new Se};break;case"SpotLight":t={shadowBias:0,shadowNormalBias:0,shadowRadius:1,shadowMapSize:new Se};break;case"PointLight":t={shadowBias:0,shadowNormalBias:0,shadowRadius:1,shadowMapSize:new Se,shadowCameraNear:1,shadowCameraFar:1e3};break}return i[e.id]=t,t}}}let Df=0;function Uf(i,e){return(e.castShadow?2:0)-(i.castShadow?2:0)+(e.map?1:0)-(i.map?1:0)}function Nf(i,e){const t=new zf,n=Vf(),r={version:0,hash:{directionalLength:-1,pointLength:-1,spotLength:-1,rectAreaLength:-1,hemiLength:-1,numDirectionalShadows:-1,numPointShadows:-1,numSpotShadows:-1,numSpotMaps:-1},ambient:[0,0,0],probe:[],directional:[],directionalShadow:[],directionalShadowMap:[],directionalShadowMatrix:[],spot:[],spotLightMap:[],spotShadow:[],spotShadowMap:[],spotLightMatrix:[],rectArea:[],rectAreaLTC1:null,rectAreaLTC2:null,point:[],pointShadow:[],pointShadowMap:[],pointShadowMatrix:[],hemi:[],numSpotLightShadowsWithMaps:0};for(let u=0;u<9;u++)r.probe.push(new C);const s=new C,o=new ke,a=new ke;function l(u,d){let f=0,m=0,g=0;for(let X=0;X<9;X++)r.probe[X].set(0,0,0);let p=0,h=0,x=0,E=0,b=0,S=0,y=0,R=0,U=0,v=0;u.sort(Uf);const T=d!==!0?Math.PI:1;for(let X=0,oe=u.length;X<oe;X++){const N=u[X],z=N.color,W=N.intensity,K=N.distance,j=N.shadow&&N.shadow.map?N.shadow.map.texture:null;if(N.isAmbientLight)f+=z.r*W*T,m+=z.g*W*T,g+=z.b*W*T;else if(N.isLightProbe)for(let G=0;G<9;G++)r.probe[G].addScaledVector(N.sh.coefficients[G],W);else if(N.isDirectionalLight){const G=t.get(N);if(G.color.copy(N.color).multiplyScalar(N.intensity*T),N.castShadow){const te=N.shadow,J=n.get(N);J.shadowBias=te.bias,J.shadowNormalBias=te.normalBias,J.shadowRadius=te.radius,J.shadowMapSize=te.mapSize,r.directionalShadow[p]=J,r.directionalShadowMap[p]=j,r.directionalShadowMatrix[p]=N.shadow.matrix,S++}r.directional[p]=G,p++}else if(N.isSpotLight){const G=t.get(N);G.position.setFromMatrixPosition(N.matrixWorld),G.color.copy(z).multiplyScalar(W*T),G.distance=K,G.coneCos=Math.cos(N.angle),G.penumbraCos=Math.cos(N.angle*(1-N.penumbra)),G.decay=N.decay,r.spot[x]=G;const te=N.shadow;if(N.map&&(r.spotLightMap[U]=N.map,U++,te.updateMatrices(N),N.castShadow&&v++),r.spotLightMatrix[x]=te.matrix,N.castShadow){const J=n.get(N);J.shadowBias=te.bias,J.shadowNormalBias=te.normalBias,J.shadowRadius=te.radius,J.shadowMapSize=te.mapSize,r.spotShadow[x]=J,r.spotShadowMap[x]=j,R++}x++}else if(N.isRectAreaLight){const G=t.get(N);G.color.copy(z).multiplyScalar(W),G.halfWidth.set(N.width*.5,0,0),G.halfHeight.set(0,N.height*.5,0),r.rectArea[E]=G,E++}else if(N.isPointLight){const G=t.get(N);if(G.color.copy(N.color).multiplyScalar(N.intensity*T),G.distance=N.distance,G.decay=N.decay,N.castShadow){const te=N.shadow,J=n.get(N);J.shadowBias=te.bias,J.shadowNormalBias=te.normalBias,J.shadowRadius=te.radius,J.shadowMapSize=te.mapSize,J.shadowCameraNear=te.camera.near,J.shadowCameraFar=te.camera.far,r.pointShadow[h]=J,r.pointShadowMap[h]=j,r.pointShadowMatrix[h]=N.shadow.matrix,y++}r.point[h]=G,h++}else if(N.isHemisphereLight){const G=t.get(N);G.skyColor.copy(N.color).multiplyScalar(W*T),G.groundColor.copy(N.groundColor).multiplyScalar(W*T),r.hemi[b]=G,b++}}E>0&&(e.isWebGL2||i.has("OES_texture_float_linear")===!0?(r.rectAreaLTC1=ie.LTC_FLOAT_1,r.rectAreaLTC2=ie.LTC_FLOAT_2):i.has("OES_texture_half_float_linear")===!0?(r.rectAreaLTC1=ie.LTC_HALF_1,r.rectAreaLTC2=ie.LTC_HALF_2):console.error("THREE.WebGLRenderer: Unable to use RectAreaLight. Missing WebGL extensions.")),r.ambient[0]=f,r.ambient[1]=m,r.ambient[2]=g;const D=r.hash;(D.directionalLength!==p||D.pointLength!==h||D.spotLength!==x||D.rectAreaLength!==E||D.hemiLength!==b||D.numDirectionalShadows!==S||D.numPointShadows!==y||D.numSpotShadows!==R||D.numSpotMaps!==U)&&(r.directional.length=p,r.spot.length=x,r.rectArea.length=E,r.point.length=h,r.hemi.length=b,r.directionalShadow.length=S,r.directionalShadowMap.length=S,r.pointShadow.length=y,r.pointShadowMap.length=y,r.spotShadow.length=R,r.spotShadowMap.length=R,r.directionalShadowMatrix.length=S,r.pointShadowMatrix.length=y,r.spotLightMatrix.length=R+U-v,r.spotLightMap.length=U,r.numSpotLightShadowsWithMaps=v,D.directionalLength=p,D.pointLength=h,D.spotLength=x,D.rectAreaLength=E,D.hemiLength=b,D.numDirectionalShadows=S,D.numPointShadows=y,D.numSpotShadows=R,D.numSpotMaps=U,r.version=Df++)}function c(u,d){let f=0,m=0,g=0,p=0,h=0;const x=d.matrixWorldInverse;for(let E=0,b=u.length;E<b;E++){const S=u[E];if(S.isDirectionalLight){const y=r.directional[f];y.direction.setFromMatrixPosition(S.matrixWorld),s.setFromMatrixPosition(S.target.matrixWorld),y.direction.sub(s),y.direction.transformDirection(x),f++}else if(S.isSpotLight){const y=r.spot[g];y.position.setFromMatrixPosition(S.matrixWorld),y.position.applyMatrix4(x),y.direction.setFromMatrixPosition(S.matrixWorld),s.setFromMatrixPosition(S.target.matrixWorld),y.direction.sub(s),y.direction.transformDirection(x),g++}else if(S.isRectAreaLight){const y=r.rectArea[p];y.position.setFromMatrixPosition(S.matrixWorld),y.position.applyMatrix4(x),a.identity(),o.copy(S.matrixWorld),o.premultiply(x),a.extractRotation(o),y.halfWidth.set(S.width*.5,0,0),y.halfHeight.set(0,S.height*.5,0),y.halfWidth.applyMatrix4(a),y.halfHeight.applyMatrix4(a),p++}else if(S.isPointLight){const y=r.point[m];y.position.setFromMatrixPosition(S.matrixWorld),y.position.applyMatrix4(x),m++}else if(S.isHemisphereLight){const y=r.hemi[h];y.direction.setFromMatrixPosition(S.matrixWorld),y.direction.transformDirection(x),h++}}}return{setup:l,setupView:c,state:r}}function Wa(i,e){const t=new Nf(i,e),n=[],r=[];function s(){n.length=0,r.length=0}function o(d){n.push(d)}function a(d){r.push(d)}function l(d){t.setup(n,d)}function c(d){t.setupView(n,d)}return{init:s,state:{lightsArray:n,shadowsArray:r,lights:t},setupLights:l,setupLightsView:c,pushLight:o,pushShadow:a}}function Ff(i,e){let t=new WeakMap;function n(s,o=0){const a=t.get(s);let l;return a===void 0?(l=new Wa(i,e),t.set(s,[l])):o>=a.length?(l=new Wa(i,e),a.push(l)):l=a[o],l}function r(){t=new WeakMap}return{get:n,dispose:r}}class qf extends ci{constructor(e){super(),this.isMeshDepthMaterial=!0,this.type="MeshDepthMaterial",this.depthPacking=jl,this.map=null,this.alphaMap=null,this.displacementMap=null,this.displacementScale=1,this.displacementBias=0,this.wireframe=!1,this.wireframeLinewidth=1,this.setValues(e)}copy(e){return super.copy(e),this.depthPacking=e.depthPacking,this.map=e.map,this.alphaMap=e.alphaMap,this.displacementMap=e.displacementMap,this.displacementScale=e.displacementScale,this.displacementBias=e.displacementBias,this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this}}class Of extends ci{constructor(e){super(),this.isMeshDistanceMaterial=!0,this.type="MeshDistanceMaterial",this.referencePosition=new C,this.nearDistance=1,this.farDistance=1e3,this.map=null,this.alphaMap=null,this.displacementMap=null,this.displacementScale=1,this.displacementBias=0,this.setValues(e)}copy(e){return super.copy(e),this.referencePosition.copy(e.referencePosition),this.nearDistance=e.nearDistance,this.farDistance=e.farDistance,this.map=e.map,this.alphaMap=e.alphaMap,this.displacementMap=e.displacementMap,this.displacementScale=e.displacementScale,this.displacementBias=e.displacementBias,this}}const Bf=`void main() {
	gl_Position = vec4( position, 1.0 );
}`,kf=`uniform sampler2D shadow_pass;
uniform vec2 resolution;
uniform float radius;
#include <packing>
void main() {
	const float samples = float( VSM_SAMPLES );
	float mean = 0.0;
	float squared_mean = 0.0;
	float uvStride = samples <= 1.0 ? 0.0 : 2.0 / ( samples - 1.0 );
	float uvStart = samples <= 1.0 ? 0.0 : - 1.0;
	for ( float i = 0.0; i < samples; i ++ ) {
		float uvOffset = uvStart + i * uvStride;
		#ifdef HORIZONTAL_PASS
			vec2 distribution = unpackRGBATo2Half( texture2D( shadow_pass, ( gl_FragCoord.xy + vec2( uvOffset, 0.0 ) * radius ) / resolution ) );
			mean += distribution.x;
			squared_mean += distribution.y * distribution.y + distribution.x * distribution.x;
		#else
			float depth = unpackRGBAToDepth( texture2D( shadow_pass, ( gl_FragCoord.xy + vec2( 0.0, uvOffset ) * radius ) / resolution ) );
			mean += depth;
			squared_mean += depth * depth;
		#endif
	}
	mean = mean / samples;
	squared_mean = squared_mean / samples;
	float std_dev = sqrt( squared_mean - mean * mean );
	gl_FragColor = pack2HalfToRGBA( vec2( mean, std_dev ) );
}`;function Hf(i,e,t){let n=new zo;const r=new Se,s=new Se,o=new Xe,a=new qf({depthPacking:Yl}),l=new Of,c={},u=t.maxTextureSize,d={0:Vt,1:ti,2:dn},f=new Ht({defines:{VSM_SAMPLES:8},uniforms:{shadow_pass:{value:null},resolution:{value:new Se},radius:{value:4}},vertexShader:Bf,fragmentShader:kf}),m=f.clone();m.defines.HORIZONTAL_PASS=1;const g=new Ut;g.setAttribute("position",new Dt(new Float32Array([-1,-1,.5,3,-1,.5,-1,3,.5]),3));const p=new zt(g,f),h=this;this.enabled=!1,this.autoUpdate=!0,this.needsUpdate=!1,this.type=xo,this.render=function(S,y,R){if(h.enabled===!1||h.autoUpdate===!1&&h.needsUpdate===!1||S.length===0)return;const U=i.getRenderTarget(),v=i.getActiveCubeFace(),T=i.getActiveMipmapLevel(),D=i.state;D.setBlending(hn),D.buffers.color.setClear(1,1,1,1),D.buffers.depth.setTest(!0),D.setScissorTest(!1);for(let X=0,oe=S.length;X<oe;X++){const N=S[X],z=N.shadow;if(z===void 0){console.warn("THREE.WebGLShadowMap:",N,"has no shadow.");continue}if(z.autoUpdate===!1&&z.needsUpdate===!1)continue;r.copy(z.mapSize);const W=z.getFrameExtents();if(r.multiply(W),s.copy(z.mapSize),(r.x>u||r.y>u)&&(r.x>u&&(s.x=Math.floor(u/W.x),r.x=s.x*W.x,z.mapSize.x=s.x),r.y>u&&(s.y=Math.floor(u/W.y),r.y=s.y*W.y,z.mapSize.y=s.y)),z.map===null){const j=this.type!==bi?{minFilter:gt,magFilter:gt}:{};z.map=new Cn(r.x,r.y,j),z.map.texture.name=N.name+".shadowMap",z.camera.updateProjectionMatrix()}i.setRenderTarget(z.map),i.clear();const K=z.getViewportCount();for(let j=0;j<K;j++){const G=z.getViewport(j);o.set(s.x*G.x,s.y*G.y,s.x*G.z,s.y*G.w),D.viewport(o),z.updateMatrices(N,j),n=z.getFrustum(),b(y,R,z.camera,N,this.type)}z.isPointLightShadow!==!0&&this.type===bi&&x(z,R),z.needsUpdate=!1}h.needsUpdate=!1,i.setRenderTarget(U,v,T)};function x(S,y){const R=e.update(p);f.defines.VSM_SAMPLES!==S.blurSamples&&(f.defines.VSM_SAMPLES=S.blurSamples,m.defines.VSM_SAMPLES=S.blurSamples,f.needsUpdate=!0,m.needsUpdate=!0),S.mapPass===null&&(S.mapPass=new Cn(r.x,r.y)),f.uniforms.shadow_pass.value=S.map.texture,f.uniforms.resolution.value=S.mapSize,f.uniforms.radius.value=S.radius,i.setRenderTarget(S.mapPass),i.clear(),i.renderBufferDirect(y,null,R,f,p,null),m.uniforms.shadow_pass.value=S.mapPass.texture,m.uniforms.resolution.value=S.mapSize,m.uniforms.radius.value=S.radius,i.setRenderTarget(S.map),i.clear(),i.renderBufferDirect(y,null,R,m,p,null)}function E(S,y,R,U,v,T){let D=null;const X=R.isPointLight===!0?S.customDistanceMaterial:S.customDepthMaterial;if(X!==void 0?D=X:D=R.isPointLight===!0?l:a,i.localClippingEnabled&&y.clipShadows===!0&&Array.isArray(y.clippingPlanes)&&y.clippingPlanes.length!==0||y.displacementMap&&y.displacementScale!==0||y.alphaMap&&y.alphaTest>0||y.map&&y.alphaTest>0){const oe=D.uuid,N=y.uuid;let z=c[oe];z===void 0&&(z={},c[oe]=z);let W=z[N];W===void 0&&(W=D.clone(),z[N]=W),D=W}return D.visible=y.visible,D.wireframe=y.wireframe,T===bi?D.side=y.shadowSide!==null?y.shadowSide:y.side:D.side=y.shadowSide!==null?y.shadowSide:d[y.side],D.alphaMap=y.alphaMap,D.alphaTest=y.alphaTest,D.map=y.map,D.clipShadows=y.clipShadows,D.clippingPlanes=y.clippingPlanes,D.clipIntersection=y.clipIntersection,D.displacementMap=y.displacementMap,D.displacementScale=y.displacementScale,D.displacementBias=y.displacementBias,D.wireframeLinewidth=y.wireframeLinewidth,D.linewidth=y.linewidth,R.isPointLight===!0&&D.isMeshDistanceMaterial===!0&&(D.referencePosition.setFromMatrixPosition(R.matrixWorld),D.nearDistance=U,D.farDistance=v),D}function b(S,y,R,U,v){if(S.visible===!1)return;if(S.layers.test(y.layers)&&(S.isMesh||S.isLine||S.isPoints)&&(S.castShadow||S.receiveShadow&&v===bi)&&(!S.frustumCulled||n.intersectsObject(S))){S.modelViewMatrix.multiplyMatrices(R.matrixWorldInverse,S.matrixWorld);const X=e.update(S),oe=S.material;if(Array.isArray(oe)){const N=X.groups;for(let z=0,W=N.length;z<W;z++){const K=N[z],j=oe[K.materialIndex];if(j&&j.visible){const G=E(S,j,U,R.near,R.far,v);i.renderBufferDirect(R,null,X,G,S,K)}}}else if(oe.visible){const N=E(S,oe,U,R.near,R.far,v);i.renderBufferDirect(R,null,X,N,S,null)}}const D=S.children;for(let X=0,oe=D.length;X<oe;X++)b(D[X],y,R,U,v)}}function Zf(i,e,t){const n=t.isWebGL2;function r(){let I=!1;const B=new Xe;let Y=null;const ce=new Xe(0,0,0,0);return{setMask:function(me){Y!==me&&!I&&(i.colorMask(me,me,me,me),Y=me)},setLocked:function(me){I=me},setClear:function(me,Ve,tt,at,pn){pn===!0&&(me*=at,Ve*=at,tt*=at),B.set(me,Ve,tt,at),ce.equals(B)===!1&&(i.clearColor(me,Ve,tt,at),ce.copy(B))},reset:function(){I=!1,Y=null,ce.set(-1,0,0,0)}}}function s(){let I=!1,B=null,Y=null,ce=null;return{setTest:function(me){me?Ee(2929):he(2929)},setMask:function(me){B!==me&&!I&&(i.depthMask(me),B=me)},setFunc:function(me){if(Y!==me){switch(me){case xl:i.depthFunc(512);break;case Ml:i.depthFunc(519);break;case yl:i.depthFunc(513);break;case ds:i.depthFunc(515);break;case Al:i.depthFunc(514);break;case _l:i.depthFunc(518);break;case bl:i.depthFunc(516);break;case Sl:i.depthFunc(517);break;default:i.depthFunc(515)}Y=me}},setLocked:function(me){I=me},setClear:function(me){ce!==me&&(i.clearDepth(me),ce=me)},reset:function(){I=!1,B=null,Y=null,ce=null}}}function o(){let I=!1,B=null,Y=null,ce=null,me=null,Ve=null,tt=null,at=null,pn=null;return{setTest:function(Oe){I||(Oe?Ee(2960):he(2960))},setMask:function(Oe){B!==Oe&&!I&&(i.stencilMask(Oe),B=Oe)},setFunc:function(Oe,Xt,Tt){(Y!==Oe||ce!==Xt||me!==Tt)&&(i.stencilFunc(Oe,Xt,Tt),Y=Oe,ce=Xt,me=Tt)},setOp:function(Oe,Xt,Tt){(Ve!==Oe||tt!==Xt||at!==Tt)&&(i.stencilOp(Oe,Xt,Tt),Ve=Oe,tt=Xt,at=Tt)},setLocked:function(Oe){I=Oe},setClear:function(Oe){pn!==Oe&&(i.clearStencil(Oe),pn=Oe)},reset:function(){I=!1,B=null,Y=null,ce=null,me=null,Ve=null,tt=null,at=null,pn=null}}}const a=new r,l=new s,c=new o,u=new WeakMap,d=new WeakMap;let f={},m={},g=new WeakMap,p=[],h=null,x=!1,E=null,b=null,S=null,y=null,R=null,U=null,v=null,T=!1,D=null,X=null,oe=null,N=null,z=null;const W=i.getParameter(35661);let K=!1,j=0;const G=i.getParameter(7938);G.indexOf("WebGL")!==-1?(j=parseFloat(/^WebGL (\d)/.exec(G)[1]),K=j>=1):G.indexOf("OpenGL ES")!==-1&&(j=parseFloat(/^OpenGL ES (\d)/.exec(G)[1]),K=j>=2);let te=null,J={};const F=i.getParameter(3088),k=i.getParameter(2978),Q=new Xe().fromArray(F),ee=new Xe().fromArray(k);function re(I,B,Y){const ce=new Uint8Array(4),me=i.createTexture();i.bindTexture(I,me),i.texParameteri(I,10241,9728),i.texParameteri(I,10240,9728);for(let Ve=0;Ve<Y;Ve++)i.texImage2D(B+Ve,0,6408,1,1,0,6408,5121,ce);return me}const H={};H[3553]=re(3553,3553,1),H[34067]=re(34067,34069,6),a.setClear(0,0,0,1),l.setClear(1),c.setClear(0),Ee(2929),l.setFunc(ds),Ye(!1),lt(Us),Ee(2884),et(hn);function Ee(I){f[I]!==!0&&(i.enable(I),f[I]=!0)}function he(I){f[I]!==!1&&(i.disable(I),f[I]=!1)}function ve(I,B){return m[I]!==B?(i.bindFramebuffer(I,B),m[I]=B,n&&(I===36009&&(m[36160]=B),I===36160&&(m[36009]=B)),!0):!1}function de(I,B){let Y=p,ce=!1;if(I)if(Y=g.get(B),Y===void 0&&(Y=[],g.set(B,Y)),I.isWebGLMultipleRenderTargets){const me=I.texture;if(Y.length!==me.length||Y[0]!==36064){for(let Ve=0,tt=me.length;Ve<tt;Ve++)Y[Ve]=36064+Ve;Y.length=me.length,ce=!0}}else Y[0]!==36064&&(Y[0]=36064,ce=!0);else Y[0]!==1029&&(Y[0]=1029,ce=!0);ce&&(t.isWebGL2?i.drawBuffers(Y):e.get("WEBGL_draw_buffers").drawBuffersWEBGL(Y))}function ze(I){return h!==I?(i.useProgram(I),h=I,!0):!1}const Ae={[Qn]:32774,[ol]:32778,[ll]:32779};if(n)Ae[Os]=32775,Ae[Bs]=32776;else{const I=e.get("EXT_blend_minmax");I!==null&&(Ae[Os]=I.MIN_EXT,Ae[Bs]=I.MAX_EXT)}const xe={[cl]:0,[ul]:1,[dl]:768,[Mo]:770,[vl]:776,[ml]:774,[fl]:772,[hl]:769,[yo]:771,[gl]:775,[pl]:773};function et(I,B,Y,ce,me,Ve,tt,at){if(I===hn){x===!0&&(he(3042),x=!1);return}if(x===!1&&(Ee(3042),x=!0),I!==al){if(I!==E||at!==T){if((b!==Qn||R!==Qn)&&(i.blendEquation(32774),b=Qn,R=Qn),at)switch(I){case $n:i.blendFuncSeparate(1,771,1,771);break;case Ns:i.blendFunc(1,1);break;case Fs:i.blendFuncSeparate(0,769,0,1);break;case qs:i.blendFuncSeparate(0,768,0,770);break;default:console.error("THREE.WebGLState: Invalid blending: ",I);break}else switch(I){case $n:i.blendFuncSeparate(770,771,1,771);break;case Ns:i.blendFunc(770,1);break;case Fs:i.blendFuncSeparate(0,769,0,1);break;case qs:i.blendFunc(0,768);break;default:console.error("THREE.WebGLState: Invalid blending: ",I);break}S=null,y=null,U=null,v=null,E=I,T=at}return}me=me||B,Ve=Ve||Y,tt=tt||ce,(B!==b||me!==R)&&(i.blendEquationSeparate(Ae[B],Ae[me]),b=B,R=me),(Y!==S||ce!==y||Ve!==U||tt!==v)&&(i.blendFuncSeparate(xe[Y],xe[ce],xe[Ve],xe[tt]),S=Y,y=ce,U=Ve,v=tt),E=I,T=!1}function je(I,B){I.side===dn?he(2884):Ee(2884);let Y=I.side===Vt;B&&(Y=!Y),Ye(Y),I.blending===$n&&I.transparent===!1?et(hn):et(I.blending,I.blendEquation,I.blendSrc,I.blendDst,I.blendEquationAlpha,I.blendSrcAlpha,I.blendDstAlpha,I.premultipliedAlpha),l.setFunc(I.depthFunc),l.setTest(I.depthTest),l.setMask(I.depthWrite),a.setMask(I.colorWrite);const ce=I.stencilWrite;c.setTest(ce),ce&&(c.setMask(I.stencilWriteMask),c.setFunc(I.stencilFunc,I.stencilRef,I.stencilFuncMask),c.setOp(I.stencilFail,I.stencilZFail,I.stencilZPass)),Ue(I.polygonOffset,I.polygonOffsetFactor,I.polygonOffsetUnits),I.alphaToCoverage===!0?Ee(32926):he(32926)}function Ye(I){D!==I&&(I?i.frontFace(2304):i.frontFace(2305),D=I)}function lt(I){I!==il?(Ee(2884),I!==X&&(I===Us?i.cullFace(1029):I===rl?i.cullFace(1028):i.cullFace(1032))):he(2884),X=I}function He(I){I!==oe&&(K&&i.lineWidth(I),oe=I)}function Ue(I,B,Y){I?(Ee(32823),(N!==B||z!==Y)&&(i.polygonOffset(B,Y),N=B,z=Y)):he(32823)}function Et(I){I?Ee(3089):he(3089)}function mt(I){I===void 0&&(I=33984+W-1),te!==I&&(i.activeTexture(I),te=I)}function _(I,B,Y){Y===void 0&&(te===null?Y=33984+W-1:Y=te);let ce=J[Y];ce===void 0&&(ce={type:void 0,texture:void 0},J[Y]=ce),(ce.type!==I||ce.texture!==B)&&(te!==Y&&(i.activeTexture(Y),te=Y),i.bindTexture(I,B||H[I]),ce.type=I,ce.texture=B)}function M(){const I=J[te];I!==void 0&&I.type!==void 0&&(i.bindTexture(I.type,null),I.type=void 0,I.texture=void 0)}function q(){try{i.compressedTexImage2D.apply(i,arguments)}catch(I){console.error("THREE.WebGLState:",I)}}function $(){try{i.compressedTexImage3D.apply(i,arguments)}catch(I){console.error("THREE.WebGLState:",I)}}function ne(){try{i.texSubImage2D.apply(i,arguments)}catch(I){console.error("THREE.WebGLState:",I)}}function se(){try{i.texSubImage3D.apply(i,arguments)}catch(I){console.error("THREE.WebGLState:",I)}}function Me(){try{i.compressedTexSubImage2D.apply(i,arguments)}catch(I){console.error("THREE.WebGLState:",I)}}function w(){try{i.compressedTexSubImage3D.apply(i,arguments)}catch(I){console.error("THREE.WebGLState:",I)}}function L(){try{i.texStorage2D.apply(i,arguments)}catch(I){console.error("THREE.WebGLState:",I)}}function le(){try{i.texStorage3D.apply(i,arguments)}catch(I){console.error("THREE.WebGLState:",I)}}function ue(){try{i.texImage2D.apply(i,arguments)}catch(I){console.error("THREE.WebGLState:",I)}}function ae(){try{i.texImage3D.apply(i,arguments)}catch(I){console.error("THREE.WebGLState:",I)}}function pe(I){Q.equals(I)===!1&&(i.scissor(I.x,I.y,I.z,I.w),Q.copy(I))}function fe(I){ee.equals(I)===!1&&(i.viewport(I.x,I.y,I.z,I.w),ee.copy(I))}function Te(I,B){let Y=d.get(B);Y===void 0&&(Y=new WeakMap,d.set(B,Y));let ce=Y.get(I);ce===void 0&&(ce=i.getUniformBlockIndex(B,I.name),Y.set(I,ce))}function Ie(I,B){const ce=d.get(B).get(I);u.get(I)!==ce&&(i.uniformBlockBinding(B,ce,I.__bindingPointIndex),u.set(I,ce))}function Ne(){i.disable(3042),i.disable(2884),i.disable(2929),i.disable(32823),i.disable(3089),i.disable(2960),i.disable(32926),i.blendEquation(32774),i.blendFunc(1,0),i.blendFuncSeparate(1,0,1,0),i.colorMask(!0,!0,!0,!0),i.clearColor(0,0,0,0),i.depthMask(!0),i.depthFunc(513),i.clearDepth(1),i.stencilMask(4294967295),i.stencilFunc(519,0,4294967295),i.stencilOp(7680,7680,7680),i.clearStencil(0),i.cullFace(1029),i.frontFace(2305),i.polygonOffset(0,0),i.activeTexture(33984),i.bindFramebuffer(36160,null),n===!0&&(i.bindFramebuffer(36009,null),i.bindFramebuffer(36008,null)),i.useProgram(null),i.lineWidth(1),i.scissor(0,0,i.canvas.width,i.canvas.height),i.viewport(0,0,i.canvas.width,i.canvas.height),f={},te=null,J={},m={},g=new WeakMap,p=[],h=null,x=!1,E=null,b=null,S=null,y=null,R=null,U=null,v=null,T=!1,D=null,X=null,oe=null,N=null,z=null,Q.set(0,0,i.canvas.width,i.canvas.height),ee.set(0,0,i.canvas.width,i.canvas.height),a.reset(),l.reset(),c.reset()}return{buffers:{color:a,depth:l,stencil:c},enable:Ee,disable:he,bindFramebuffer:ve,drawBuffers:de,useProgram:ze,setBlending:et,setMaterial:je,setFlipSided:Ye,setCullFace:lt,setLineWidth:He,setPolygonOffset:Ue,setScissorTest:Et,activeTexture:mt,bindTexture:_,unbindTexture:M,compressedTexImage2D:q,compressedTexImage3D:$,texImage2D:ue,texImage3D:ae,updateUBOMapping:Te,uniformBlockBinding:Ie,texStorage2D:L,texStorage3D:le,texSubImage2D:ne,texSubImage3D:se,compressedTexSubImage2D:Me,compressedTexSubImage3D:w,scissor:pe,viewport:fe,reset:Ne}}function Gf(i,e,t,n,r,s,o){const a=r.isWebGL2,l=r.maxTextures,c=r.maxCubemapSize,u=r.maxTextureSize,d=r.maxSamples,f=e.has("WEBGL_multisampled_render_to_texture")?e.get("WEBGL_multisampled_render_to_texture"):null,m=typeof navigator>"u"?!1:/OculusBrowser/g.test(navigator.userAgent),g=new WeakMap;let p;const h=new WeakMap;let x=!1;try{x=typeof OffscreenCanvas<"u"&&new OffscreenCanvas(1,1).getContext("2d")!==null}catch{}function E(_,M){return x?new OffscreenCanvas(_,M):Li("canvas")}function b(_,M,q,$){let ne=1;if((_.width>$||_.height>$)&&(ne=$/Math.max(_.width,_.height)),ne<1||M===!0)if(typeof HTMLImageElement<"u"&&_ instanceof HTMLImageElement||typeof HTMLCanvasElement<"u"&&_ instanceof HTMLCanvasElement||typeof ImageBitmap<"u"&&_ instanceof ImageBitmap){const se=M?vs:Math.floor,Me=se(ne*_.width),w=se(ne*_.height);p===void 0&&(p=E(Me,w));const L=q?E(Me,w):p;return L.width=Me,L.height=w,L.getContext("2d").drawImage(_,0,0,Me,w),console.warn("THREE.WebGLRenderer: Texture has been resized from ("+_.width+"x"+_.height+") to ("+Me+"x"+w+")."),L}else return"data"in _&&console.warn("THREE.WebGLRenderer: Image in DataTexture is too big ("+_.width+"x"+_.height+")."),_;return _}function S(_){return pa(_.width)&&pa(_.height)}function y(_){return a?!1:_.wrapS!==Bt||_.wrapT!==Bt||_.minFilter!==gt&&_.minFilter!==Ct}function R(_,M){return _.generateMipmaps&&M&&_.minFilter!==gt&&_.minFilter!==Ct}function U(_){i.generateMipmap(_)}function v(_,M,q,$,ne=!1){if(a===!1)return M;if(_!==null){if(i[_]!==void 0)return i[_];console.warn("THREE.WebGLRenderer: Attempt to use non-existing WebGL internal format '"+_+"'")}let se=M;return M===6403&&(q===5126&&(se=33326),q===5131&&(se=33325),q===5121&&(se=33321)),M===33319&&(q===5126&&(se=33328),q===5131&&(se=33327),q===5121&&(se=33323)),M===6408&&(q===5126&&(se=34836),q===5131&&(se=34842),q===5121&&(se=$===Be&&ne===!1?35907:32856),q===32819&&(se=32854),q===32820&&(se=32855)),(se===33325||se===33326||se===33327||se===33328||se===34842||se===34836)&&e.get("EXT_color_buffer_float"),se}function T(_,M,q){return R(_,q)===!0||_.isFramebufferTexture&&_.minFilter!==gt&&_.minFilter!==Ct?Math.log2(Math.max(M.width,M.height))+1:_.mipmaps!==void 0&&_.mipmaps.length>0?_.mipmaps.length:_.isCompressedTexture&&Array.isArray(_.image)?M.mipmaps.length:1}function D(_){return _===gt||_===ks||_===Hs?9728:9729}function X(_){const M=_.target;M.removeEventListener("dispose",X),N(M),M.isVideoTexture&&g.delete(M)}function oe(_){const M=_.target;M.removeEventListener("dispose",oe),W(M)}function N(_){const M=n.get(_);if(M.__webglInit===void 0)return;const q=_.source,$=h.get(q);if($){const ne=$[M.__cacheKey];ne.usedTimes--,ne.usedTimes===0&&z(_),Object.keys($).length===0&&h.delete(q)}n.remove(_)}function z(_){const M=n.get(_);i.deleteTexture(M.__webglTexture);const q=_.source,$=h.get(q);delete $[M.__cacheKey],o.memory.textures--}function W(_){const M=_.texture,q=n.get(_),$=n.get(M);if($.__webglTexture!==void 0&&(i.deleteTexture($.__webglTexture),o.memory.textures--),_.depthTexture&&_.depthTexture.dispose(),_.isWebGLCubeRenderTarget)for(let ne=0;ne<6;ne++)i.deleteFramebuffer(q.__webglFramebuffer[ne]),q.__webglDepthbuffer&&i.deleteRenderbuffer(q.__webglDepthbuffer[ne]);else{if(i.deleteFramebuffer(q.__webglFramebuffer),q.__webglDepthbuffer&&i.deleteRenderbuffer(q.__webglDepthbuffer),q.__webglMultisampledFramebuffer&&i.deleteFramebuffer(q.__webglMultisampledFramebuffer),q.__webglColorRenderbuffer)for(let ne=0;ne<q.__webglColorRenderbuffer.length;ne++)q.__webglColorRenderbuffer[ne]&&i.deleteRenderbuffer(q.__webglColorRenderbuffer[ne]);q.__webglDepthRenderbuffer&&i.deleteRenderbuffer(q.__webglDepthRenderbuffer)}if(_.isWebGLMultipleRenderTargets)for(let ne=0,se=M.length;ne<se;ne++){const Me=n.get(M[ne]);Me.__webglTexture&&(i.deleteTexture(Me.__webglTexture),o.memory.textures--),n.remove(M[ne])}n.remove(M),n.remove(_)}let K=0;function j(){K=0}function G(){const _=K;return _>=l&&console.warn("THREE.WebGLTextures: Trying to use "+_+" texture units while this GPU supports only "+l),K+=1,_}function te(_){const M=[];return M.push(_.wrapS),M.push(_.wrapT),M.push(_.wrapR||0),M.push(_.magFilter),M.push(_.minFilter),M.push(_.anisotropy),M.push(_.internalFormat),M.push(_.format),M.push(_.type),M.push(_.generateMipmaps),M.push(_.premultiplyAlpha),M.push(_.flipY),M.push(_.unpackAlignment),M.push(_.encoding),M.join()}function J(_,M){const q=n.get(_);if(_.isVideoTexture&&Et(_),_.isRenderTargetTexture===!1&&_.version>0&&q.__version!==_.version){const $=_.image;if($===null)console.warn("THREE.WebGLRenderer: Texture marked for update but no image data found.");else if($.complete===!1)console.warn("THREE.WebGLRenderer: Texture marked for update but image is incomplete");else{he(q,_,M);return}}t.bindTexture(3553,q.__webglTexture,33984+M)}function F(_,M){const q=n.get(_);if(_.version>0&&q.__version!==_.version){he(q,_,M);return}t.bindTexture(35866,q.__webglTexture,33984+M)}function k(_,M){const q=n.get(_);if(_.version>0&&q.__version!==_.version){he(q,_,M);return}t.bindTexture(32879,q.__webglTexture,33984+M)}function Q(_,M){const q=n.get(_);if(_.version>0&&q.__version!==_.version){ve(q,_,M);return}t.bindTexture(34067,q.__webglTexture,33984+M)}const ee={[ps]:10497,[Bt]:33071,[ms]:33648},re={[gt]:9728,[ks]:9984,[Hs]:9986,[Ct]:9729,[Pl]:9985,[vr]:9987};function H(_,M,q){if(q?(i.texParameteri(_,10242,ee[M.wrapS]),i.texParameteri(_,10243,ee[M.wrapT]),(_===32879||_===35866)&&i.texParameteri(_,32882,ee[M.wrapR]),i.texParameteri(_,10240,re[M.magFilter]),i.texParameteri(_,10241,re[M.minFilter])):(i.texParameteri(_,10242,33071),i.texParameteri(_,10243,33071),(_===32879||_===35866)&&i.texParameteri(_,32882,33071),(M.wrapS!==Bt||M.wrapT!==Bt)&&console.warn("THREE.WebGLRenderer: Texture is not power of two. Texture.wrapS and Texture.wrapT should be set to THREE.ClampToEdgeWrapping."),i.texParameteri(_,10240,D(M.magFilter)),i.texParameteri(_,10241,D(M.minFilter)),M.minFilter!==gt&&M.minFilter!==Ct&&console.warn("THREE.WebGLRenderer: Texture is not power of two. Texture.minFilter should be set to THREE.NearestFilter or THREE.LinearFilter.")),e.has("EXT_texture_filter_anisotropic")===!0){const $=e.get("EXT_texture_filter_anisotropic");if(M.type===wn&&e.has("OES_texture_float_linear")===!1||a===!1&&M.type===Ri&&e.has("OES_texture_half_float_linear")===!1)return;(M.anisotropy>1||n.get(M).__currentAnisotropy)&&(i.texParameterf(_,$.TEXTURE_MAX_ANISOTROPY_EXT,Math.min(M.anisotropy,r.getMaxAnisotropy())),n.get(M).__currentAnisotropy=M.anisotropy)}}function Ee(_,M){let q=!1;_.__webglInit===void 0&&(_.__webglInit=!0,M.addEventListener("dispose",X));const $=M.source;let ne=h.get($);ne===void 0&&(ne={},h.set($,ne));const se=te(M);if(se!==_.__cacheKey){ne[se]===void 0&&(ne[se]={texture:i.createTexture(),usedTimes:0},o.memory.textures++,q=!0),ne[se].usedTimes++;const Me=ne[_.__cacheKey];Me!==void 0&&(ne[_.__cacheKey].usedTimes--,Me.usedTimes===0&&z(M)),_.__cacheKey=se,_.__webglTexture=ne[se].texture}return q}function he(_,M,q){let $=3553;(M.isDataArrayTexture||M.isCompressedArrayTexture)&&($=35866),M.isData3DTexture&&($=32879);const ne=Ee(_,M),se=M.source;t.bindTexture($,_.__webglTexture,33984+q);const Me=n.get(se);if(se.version!==Me.__version||ne===!0){t.activeTexture(33984+q),i.pixelStorei(37440,M.flipY),i.pixelStorei(37441,M.premultiplyAlpha),i.pixelStorei(3317,M.unpackAlignment),i.pixelStorei(37443,0);const w=y(M)&&S(M.image)===!1;let L=b(M.image,w,!1,u);L=mt(M,L);const le=S(L)||a,ue=s.convert(M.format,M.encoding);let ae=s.convert(M.type),pe=v(M.internalFormat,ue,ae,M.encoding,M.isVideoTexture);H($,M,le);let fe;const Te=M.mipmaps,Ie=a&&M.isVideoTexture!==!0,Ne=Me.__version===void 0||ne===!0,I=T(M,L,le);if(M.isDepthTexture)pe=6402,a?M.type===wn?pe=36012:M.type===Sn?pe=33190:M.type===ei?pe=35056:pe=33189:M.type===wn&&console.error("WebGLRenderer: Floating point depth texture requires WebGL2."),M.format===En&&pe===6402&&M.type!==bo&&M.type!==Sn&&(console.warn("THREE.WebGLRenderer: Use UnsignedShortType or UnsignedIntType for DepthFormat DepthTexture."),M.type=Sn,ae=s.convert(M.type)),M.format===ri&&pe===6402&&(pe=34041,M.type!==ei&&(console.warn("THREE.WebGLRenderer: Use UnsignedInt248Type for DepthStencilFormat DepthTexture."),M.type=ei,ae=s.convert(M.type))),Ne&&(Ie?t.texStorage2D(3553,1,pe,L.width,L.height):t.texImage2D(3553,0,pe,L.width,L.height,0,ue,ae,null));else if(M.isDataTexture)if(Te.length>0&&le){Ie&&Ne&&t.texStorage2D(3553,I,pe,Te[0].width,Te[0].height);for(let B=0,Y=Te.length;B<Y;B++)fe=Te[B],Ie?t.texSubImage2D(3553,B,0,0,fe.width,fe.height,ue,ae,fe.data):t.texImage2D(3553,B,pe,fe.width,fe.height,0,ue,ae,fe.data);M.generateMipmaps=!1}else Ie?(Ne&&t.texStorage2D(3553,I,pe,L.width,L.height),t.texSubImage2D(3553,0,0,0,L.width,L.height,ue,ae,L.data)):t.texImage2D(3553,0,pe,L.width,L.height,0,ue,ae,L.data);else if(M.isCompressedTexture)if(M.isCompressedArrayTexture){Ie&&Ne&&t.texStorage3D(35866,I,pe,Te[0].width,Te[0].height,L.depth);for(let B=0,Y=Te.length;B<Y;B++)fe=Te[B],M.format!==kt?ue!==null?Ie?t.compressedTexSubImage3D(35866,B,0,0,0,fe.width,fe.height,L.depth,ue,fe.data,0,0):t.compressedTexImage3D(35866,B,pe,fe.width,fe.height,L.depth,0,fe.data,0,0):console.warn("THREE.WebGLRenderer: Attempt to load unsupported compressed texture format in .uploadTexture()"):Ie?t.texSubImage3D(35866,B,0,0,0,fe.width,fe.height,L.depth,ue,ae,fe.data):t.texImage3D(35866,B,pe,fe.width,fe.height,L.depth,0,ue,ae,fe.data)}else{Ie&&Ne&&t.texStorage2D(3553,I,pe,Te[0].width,Te[0].height);for(let B=0,Y=Te.length;B<Y;B++)fe=Te[B],M.format!==kt?ue!==null?Ie?t.compressedTexSubImage2D(3553,B,0,0,fe.width,fe.height,ue,fe.data):t.compressedTexImage2D(3553,B,pe,fe.width,fe.height,0,fe.data):console.warn("THREE.WebGLRenderer: Attempt to load unsupported compressed texture format in .uploadTexture()"):Ie?t.texSubImage2D(3553,B,0,0,fe.width,fe.height,ue,ae,fe.data):t.texImage2D(3553,B,pe,fe.width,fe.height,0,ue,ae,fe.data)}else if(M.isDataArrayTexture)Ie?(Ne&&t.texStorage3D(35866,I,pe,L.width,L.height,L.depth),t.texSubImage3D(35866,0,0,0,0,L.width,L.height,L.depth,ue,ae,L.data)):t.texImage3D(35866,0,pe,L.width,L.height,L.depth,0,ue,ae,L.data);else if(M.isData3DTexture)Ie?(Ne&&t.texStorage3D(32879,I,pe,L.width,L.height,L.depth),t.texSubImage3D(32879,0,0,0,0,L.width,L.height,L.depth,ue,ae,L.data)):t.texImage3D(32879,0,pe,L.width,L.height,L.depth,0,ue,ae,L.data);else if(M.isFramebufferTexture){if(Ne)if(Ie)t.texStorage2D(3553,I,pe,L.width,L.height);else{let B=L.width,Y=L.height;for(let ce=0;ce<I;ce++)t.texImage2D(3553,ce,pe,B,Y,0,ue,ae,null),B>>=1,Y>>=1}}else if(Te.length>0&&le){Ie&&Ne&&t.texStorage2D(3553,I,pe,Te[0].width,Te[0].height);for(let B=0,Y=Te.length;B<Y;B++)fe=Te[B],Ie?t.texSubImage2D(3553,B,0,0,ue,ae,fe):t.texImage2D(3553,B,pe,ue,ae,fe);M.generateMipmaps=!1}else Ie?(Ne&&t.texStorage2D(3553,I,pe,L.width,L.height),t.texSubImage2D(3553,0,0,0,ue,ae,L)):t.texImage2D(3553,0,pe,ue,ae,L);R(M,le)&&U($),Me.__version=se.version,M.onUpdate&&M.onUpdate(M)}_.__version=M.version}function ve(_,M,q){if(M.image.length!==6)return;const $=Ee(_,M),ne=M.source;t.bindTexture(34067,_.__webglTexture,33984+q);const se=n.get(ne);if(ne.version!==se.__version||$===!0){t.activeTexture(33984+q),i.pixelStorei(37440,M.flipY),i.pixelStorei(37441,M.premultiplyAlpha),i.pixelStorei(3317,M.unpackAlignment),i.pixelStorei(37443,0);const Me=M.isCompressedTexture||M.image[0].isCompressedTexture,w=M.image[0]&&M.image[0].isDataTexture,L=[];for(let B=0;B<6;B++)!Me&&!w?L[B]=b(M.image[B],!1,!0,c):L[B]=w?M.image[B].image:M.image[B],L[B]=mt(M,L[B]);const le=L[0],ue=S(le)||a,ae=s.convert(M.format,M.encoding),pe=s.convert(M.type),fe=v(M.internalFormat,ae,pe,M.encoding),Te=a&&M.isVideoTexture!==!0,Ie=se.__version===void 0||$===!0;let Ne=T(M,le,ue);H(34067,M,ue);let I;if(Me){Te&&Ie&&t.texStorage2D(34067,Ne,fe,le.width,le.height);for(let B=0;B<6;B++){I=L[B].mipmaps;for(let Y=0;Y<I.length;Y++){const ce=I[Y];M.format!==kt?ae!==null?Te?t.compressedTexSubImage2D(34069+B,Y,0,0,ce.width,ce.height,ae,ce.data):t.compressedTexImage2D(34069+B,Y,fe,ce.width,ce.height,0,ce.data):console.warn("THREE.WebGLRenderer: Attempt to load unsupported compressed texture format in .setTextureCube()"):Te?t.texSubImage2D(34069+B,Y,0,0,ce.width,ce.height,ae,pe,ce.data):t.texImage2D(34069+B,Y,fe,ce.width,ce.height,0,ae,pe,ce.data)}}}else{I=M.mipmaps,Te&&Ie&&(I.length>0&&Ne++,t.texStorage2D(34067,Ne,fe,L[0].width,L[0].height));for(let B=0;B<6;B++)if(w){Te?t.texSubImage2D(34069+B,0,0,0,L[B].width,L[B].height,ae,pe,L[B].data):t.texImage2D(34069+B,0,fe,L[B].width,L[B].height,0,ae,pe,L[B].data);for(let Y=0;Y<I.length;Y++){const me=I[Y].image[B].image;Te?t.texSubImage2D(34069+B,Y+1,0,0,me.width,me.height,ae,pe,me.data):t.texImage2D(34069+B,Y+1,fe,me.width,me.height,0,ae,pe,me.data)}}else{Te?t.texSubImage2D(34069+B,0,0,0,ae,pe,L[B]):t.texImage2D(34069+B,0,fe,ae,pe,L[B]);for(let Y=0;Y<I.length;Y++){const ce=I[Y];Te?t.texSubImage2D(34069+B,Y+1,0,0,ae,pe,ce.image[B]):t.texImage2D(34069+B,Y+1,fe,ae,pe,ce.image[B])}}}R(M,ue)&&U(34067),se.__version=ne.version,M.onUpdate&&M.onUpdate(M)}_.__version=M.version}function de(_,M,q,$,ne){const se=s.convert(q.format,q.encoding),Me=s.convert(q.type),w=v(q.internalFormat,se,Me,q.encoding);n.get(M).__hasExternalTextures||(ne===32879||ne===35866?t.texImage3D(ne,0,w,M.width,M.height,M.depth,0,se,Me,null):t.texImage2D(ne,0,w,M.width,M.height,0,se,Me,null)),t.bindFramebuffer(36160,_),Ue(M)?f.framebufferTexture2DMultisampleEXT(36160,$,ne,n.get(q).__webglTexture,0,He(M)):(ne===3553||ne>=34069&&ne<=34074)&&i.framebufferTexture2D(36160,$,ne,n.get(q).__webglTexture,0),t.bindFramebuffer(36160,null)}function ze(_,M,q){if(i.bindRenderbuffer(36161,_),M.depthBuffer&&!M.stencilBuffer){let $=33189;if(q||Ue(M)){const ne=M.depthTexture;ne&&ne.isDepthTexture&&(ne.type===wn?$=36012:ne.type===Sn&&($=33190));const se=He(M);Ue(M)?f.renderbufferStorageMultisampleEXT(36161,se,$,M.width,M.height):i.renderbufferStorageMultisample(36161,se,$,M.width,M.height)}else i.renderbufferStorage(36161,$,M.width,M.height);i.framebufferRenderbuffer(36160,36096,36161,_)}else if(M.depthBuffer&&M.stencilBuffer){const $=He(M);q&&Ue(M)===!1?i.renderbufferStorageMultisample(36161,$,35056,M.width,M.height):Ue(M)?f.renderbufferStorageMultisampleEXT(36161,$,35056,M.width,M.height):i.renderbufferStorage(36161,34041,M.width,M.height),i.framebufferRenderbuffer(36160,33306,36161,_)}else{const $=M.isWebGLMultipleRenderTargets===!0?M.texture:[M.texture];for(let ne=0;ne<$.length;ne++){const se=$[ne],Me=s.convert(se.format,se.encoding),w=s.convert(se.type),L=v(se.internalFormat,Me,w,se.encoding),le=He(M);q&&Ue(M)===!1?i.renderbufferStorageMultisample(36161,le,L,M.width,M.height):Ue(M)?f.renderbufferStorageMultisampleEXT(36161,le,L,M.width,M.height):i.renderbufferStorage(36161,L,M.width,M.height)}}i.bindRenderbuffer(36161,null)}function Ae(_,M){if(M&&M.isWebGLCubeRenderTarget)throw new Error("Depth Texture with cube render targets is not supported");if(t.bindFramebuffer(36160,_),!(M.depthTexture&&M.depthTexture.isDepthTexture))throw new Error("renderTarget.depthTexture must be an instance of THREE.DepthTexture");(!n.get(M.depthTexture).__webglTexture||M.depthTexture.image.width!==M.width||M.depthTexture.image.height!==M.height)&&(M.depthTexture.image.width=M.width,M.depthTexture.image.height=M.height,M.depthTexture.needsUpdate=!0),J(M.depthTexture,0);const $=n.get(M.depthTexture).__webglTexture,ne=He(M);if(M.depthTexture.format===En)Ue(M)?f.framebufferTexture2DMultisampleEXT(36160,36096,3553,$,0,ne):i.framebufferTexture2D(36160,36096,3553,$,0);else if(M.depthTexture.format===ri)Ue(M)?f.framebufferTexture2DMultisampleEXT(36160,33306,3553,$,0,ne):i.framebufferTexture2D(36160,33306,3553,$,0);else throw new Error("Unknown depthTexture format")}function xe(_){const M=n.get(_),q=_.isWebGLCubeRenderTarget===!0;if(_.depthTexture&&!M.__autoAllocateDepthBuffer){if(q)throw new Error("target.depthTexture not supported in Cube render targets");Ae(M.__webglFramebuffer,_)}else if(q){M.__webglDepthbuffer=[];for(let $=0;$<6;$++)t.bindFramebuffer(36160,M.__webglFramebuffer[$]),M.__webglDepthbuffer[$]=i.createRenderbuffer(),ze(M.__webglDepthbuffer[$],_,!1)}else t.bindFramebuffer(36160,M.__webglFramebuffer),M.__webglDepthbuffer=i.createRenderbuffer(),ze(M.__webglDepthbuffer,_,!1);t.bindFramebuffer(36160,null)}function et(_,M,q){const $=n.get(_);M!==void 0&&de($.__webglFramebuffer,_,_.texture,36064,3553),q!==void 0&&xe(_)}function je(_){const M=_.texture,q=n.get(_),$=n.get(M);_.addEventListener("dispose",oe),_.isWebGLMultipleRenderTargets!==!0&&($.__webglTexture===void 0&&($.__webglTexture=i.createTexture()),$.__version=M.version,o.memory.textures++);const ne=_.isWebGLCubeRenderTarget===!0,se=_.isWebGLMultipleRenderTargets===!0,Me=S(_)||a;if(ne){q.__webglFramebuffer=[];for(let w=0;w<6;w++)q.__webglFramebuffer[w]=i.createFramebuffer()}else{if(q.__webglFramebuffer=i.createFramebuffer(),se)if(r.drawBuffers){const w=_.texture;for(let L=0,le=w.length;L<le;L++){const ue=n.get(w[L]);ue.__webglTexture===void 0&&(ue.__webglTexture=i.createTexture(),o.memory.textures++)}}else console.warn("THREE.WebGLRenderer: WebGLMultipleRenderTargets can only be used with WebGL2 or WEBGL_draw_buffers extension.");if(a&&_.samples>0&&Ue(_)===!1){const w=se?M:[M];q.__webglMultisampledFramebuffer=i.createFramebuffer(),q.__webglColorRenderbuffer=[],t.bindFramebuffer(36160,q.__webglMultisampledFramebuffer);for(let L=0;L<w.length;L++){const le=w[L];q.__webglColorRenderbuffer[L]=i.createRenderbuffer(),i.bindRenderbuffer(36161,q.__webglColorRenderbuffer[L]);const ue=s.convert(le.format,le.encoding),ae=s.convert(le.type),pe=v(le.internalFormat,ue,ae,le.encoding,_.isXRRenderTarget===!0),fe=He(_);i.renderbufferStorageMultisample(36161,fe,pe,_.width,_.height),i.framebufferRenderbuffer(36160,36064+L,36161,q.__webglColorRenderbuffer[L])}i.bindRenderbuffer(36161,null),_.depthBuffer&&(q.__webglDepthRenderbuffer=i.createRenderbuffer(),ze(q.__webglDepthRenderbuffer,_,!0)),t.bindFramebuffer(36160,null)}}if(ne){t.bindTexture(34067,$.__webglTexture),H(34067,M,Me);for(let w=0;w<6;w++)de(q.__webglFramebuffer[w],_,M,36064,34069+w);R(M,Me)&&U(34067),t.unbindTexture()}else if(se){const w=_.texture;for(let L=0,le=w.length;L<le;L++){const ue=w[L],ae=n.get(ue);t.bindTexture(3553,ae.__webglTexture),H(3553,ue,Me),de(q.__webglFramebuffer,_,ue,36064+L,3553),R(ue,Me)&&U(3553)}t.unbindTexture()}else{let w=3553;(_.isWebGL3DRenderTarget||_.isWebGLArrayRenderTarget)&&(a?w=_.isWebGL3DRenderTarget?32879:35866:console.error("THREE.WebGLTextures: THREE.Data3DTexture and THREE.DataArrayTexture only supported with WebGL2.")),t.bindTexture(w,$.__webglTexture),H(w,M,Me),de(q.__webglFramebuffer,_,M,36064,w),R(M,Me)&&U(w),t.unbindTexture()}_.depthBuffer&&xe(_)}function Ye(_){const M=S(_)||a,q=_.isWebGLMultipleRenderTargets===!0?_.texture:[_.texture];for(let $=0,ne=q.length;$<ne;$++){const se=q[$];if(R(se,M)){const Me=_.isWebGLCubeRenderTarget?34067:3553,w=n.get(se).__webglTexture;t.bindTexture(Me,w),U(Me),t.unbindTexture()}}}function lt(_){if(a&&_.samples>0&&Ue(_)===!1){const M=_.isWebGLMultipleRenderTargets?_.texture:[_.texture],q=_.width,$=_.height;let ne=16384;const se=[],Me=_.stencilBuffer?33306:36096,w=n.get(_),L=_.isWebGLMultipleRenderTargets===!0;if(L)for(let le=0;le<M.length;le++)t.bindFramebuffer(36160,w.__webglMultisampledFramebuffer),i.framebufferRenderbuffer(36160,36064+le,36161,null),t.bindFramebuffer(36160,w.__webglFramebuffer),i.framebufferTexture2D(36009,36064+le,3553,null,0);t.bindFramebuffer(36008,w.__webglMultisampledFramebuffer),t.bindFramebuffer(36009,w.__webglFramebuffer);for(let le=0;le<M.length;le++){se.push(36064+le),_.depthBuffer&&se.push(Me);const ue=w.__ignoreDepthValues!==void 0?w.__ignoreDepthValues:!1;if(ue===!1&&(_.depthBuffer&&(ne|=256),_.stencilBuffer&&(ne|=1024)),L&&i.framebufferRenderbuffer(36008,36064,36161,w.__webglColorRenderbuffer[le]),ue===!0&&(i.invalidateFramebuffer(36008,[Me]),i.invalidateFramebuffer(36009,[Me])),L){const ae=n.get(M[le]).__webglTexture;i.framebufferTexture2D(36009,36064,3553,ae,0)}i.blitFramebuffer(0,0,q,$,0,0,q,$,ne,9728),m&&i.invalidateFramebuffer(36008,se)}if(t.bindFramebuffer(36008,null),t.bindFramebuffer(36009,null),L)for(let le=0;le<M.length;le++){t.bindFramebuffer(36160,w.__webglMultisampledFramebuffer),i.framebufferRenderbuffer(36160,36064+le,36161,w.__webglColorRenderbuffer[le]);const ue=n.get(M[le]).__webglTexture;t.bindFramebuffer(36160,w.__webglFramebuffer),i.framebufferTexture2D(36009,36064+le,3553,ue,0)}t.bindFramebuffer(36009,w.__webglMultisampledFramebuffer)}}function He(_){return Math.min(d,_.samples)}function Ue(_){const M=n.get(_);return a&&_.samples>0&&e.has("WEBGL_multisampled_render_to_texture")===!0&&M.__useRenderToTexture!==!1}function Et(_){const M=o.render.frame;g.get(_)!==M&&(g.set(_,M),_.update())}function mt(_,M){const q=_.encoding,$=_.format,ne=_.type;return _.isCompressedTexture===!0||_.isVideoTexture===!0||_.format===gs||q!==Rn&&(q===Be?a===!1?e.has("EXT_sRGB")===!0&&$===kt?(_.format=gs,_.minFilter=Ct,_.generateMipmaps=!1):M=Eo.sRGBToLinear(M):($!==kt||ne!==In)&&console.warn("THREE.WebGLTextures: sRGB encoded textures have to use RGBAFormat and UnsignedByteType."):console.error("THREE.WebGLTextures: Unsupported texture encoding:",q)),M}this.allocateTextureUnit=G,this.resetTextureUnits=j,this.setTexture2D=J,this.setTexture2DArray=F,this.setTexture3D=k,this.setTextureCube=Q,this.rebindTextures=et,this.setupRenderTarget=je,this.updateRenderTargetMipmap=Ye,this.updateMultisampleRenderTarget=lt,this.setupDepthRenderbuffer=xe,this.setupFrameBufferTexture=de,this.useMultisampledRTT=Ue}function Wf(i,e,t){const n=t.isWebGL2;function r(s,o=null){let a;if(s===In)return 5121;if(s===Ul)return 32819;if(s===Nl)return 32820;if(s===zl)return 5120;if(s===Vl)return 5122;if(s===bo)return 5123;if(s===Dl)return 5124;if(s===Sn)return 5125;if(s===wn)return 5126;if(s===Ri)return n?5131:(a=e.get("OES_texture_half_float"),a!==null?a.HALF_FLOAT_OES:null);if(s===Fl)return 6406;if(s===kt)return 6408;if(s===Ol)return 6409;if(s===Bl)return 6410;if(s===En)return 6402;if(s===ri)return 34041;if(s===ql)return console.warn("THREE.WebGLRenderer: THREE.RGBFormat has been removed. Use THREE.RGBAFormat instead. https://github.com/mrdoob/three.js/pull/23228"),6408;if(s===gs)return a=e.get("EXT_sRGB"),a!==null?a.SRGB_ALPHA_EXT:null;if(s===kl)return 6403;if(s===Hl)return 36244;if(s===Zl)return 33319;if(s===Gl)return 33320;if(s===Wl)return 36249;if(s===Er||s===Tr||s===Ir||s===Rr)if(o===Be)if(a=e.get("WEBGL_compressed_texture_s3tc_srgb"),a!==null){if(s===Er)return a.COMPRESSED_SRGB_S3TC_DXT1_EXT;if(s===Tr)return a.COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT;if(s===Ir)return a.COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT;if(s===Rr)return a.COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT}else return null;else if(a=e.get("WEBGL_compressed_texture_s3tc"),a!==null){if(s===Er)return a.COMPRESSED_RGB_S3TC_DXT1_EXT;if(s===Tr)return a.COMPRESSED_RGBA_S3TC_DXT1_EXT;if(s===Ir)return a.COMPRESSED_RGBA_S3TC_DXT3_EXT;if(s===Rr)return a.COMPRESSED_RGBA_S3TC_DXT5_EXT}else return null;if(s===Zs||s===Gs||s===Ws||s===Xs)if(a=e.get("WEBGL_compressed_texture_pvrtc"),a!==null){if(s===Zs)return a.COMPRESSED_RGB_PVRTC_4BPPV1_IMG;if(s===Gs)return a.COMPRESSED_RGB_PVRTC_2BPPV1_IMG;if(s===Ws)return a.COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;if(s===Xs)return a.COMPRESSED_RGBA_PVRTC_2BPPV1_IMG}else return null;if(s===Xl)return a=e.get("WEBGL_compressed_texture_etc1"),a!==null?a.COMPRESSED_RGB_ETC1_WEBGL:null;if(s===js||s===Ys)if(a=e.get("WEBGL_compressed_texture_etc"),a!==null){if(s===js)return o===Be?a.COMPRESSED_SRGB8_ETC2:a.COMPRESSED_RGB8_ETC2;if(s===Ys)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ETC2_EAC:a.COMPRESSED_RGBA8_ETC2_EAC}else return null;if(s===Ks||s===Qs||s===Js||s===$s||s===ea||s===ta||s===na||s===ia||s===ra||s===sa||s===aa||s===oa||s===la||s===ca)if(a=e.get("WEBGL_compressed_texture_astc"),a!==null){if(s===Ks)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_4x4_KHR:a.COMPRESSED_RGBA_ASTC_4x4_KHR;if(s===Qs)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_5x4_KHR:a.COMPRESSED_RGBA_ASTC_5x4_KHR;if(s===Js)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_5x5_KHR:a.COMPRESSED_RGBA_ASTC_5x5_KHR;if(s===$s)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_6x5_KHR:a.COMPRESSED_RGBA_ASTC_6x5_KHR;if(s===ea)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_6x6_KHR:a.COMPRESSED_RGBA_ASTC_6x6_KHR;if(s===ta)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_8x5_KHR:a.COMPRESSED_RGBA_ASTC_8x5_KHR;if(s===na)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_8x6_KHR:a.COMPRESSED_RGBA_ASTC_8x6_KHR;if(s===ia)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_8x8_KHR:a.COMPRESSED_RGBA_ASTC_8x8_KHR;if(s===ra)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_10x5_KHR:a.COMPRESSED_RGBA_ASTC_10x5_KHR;if(s===sa)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_10x6_KHR:a.COMPRESSED_RGBA_ASTC_10x6_KHR;if(s===aa)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_10x8_KHR:a.COMPRESSED_RGBA_ASTC_10x8_KHR;if(s===oa)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_10x10_KHR:a.COMPRESSED_RGBA_ASTC_10x10_KHR;if(s===la)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_12x10_KHR:a.COMPRESSED_RGBA_ASTC_12x10_KHR;if(s===ca)return o===Be?a.COMPRESSED_SRGB8_ALPHA8_ASTC_12x12_KHR:a.COMPRESSED_RGBA_ASTC_12x12_KHR}else return null;if(s===ua)if(a=e.get("EXT_texture_compression_bptc"),a!==null){if(s===ua)return o===Be?a.COMPRESSED_SRGB_ALPHA_BPTC_UNORM_EXT:a.COMPRESSED_RGBA_BPTC_UNORM_EXT}else return null;return s===ei?n?34042:(a=e.get("WEBGL_depth_texture"),a!==null?a.UNSIGNED_INT_24_8_WEBGL:null):i[s]!==void 0?i[s]:null}return{convert:r}}class Xf extends Lt{constructor(e=[]){super(),this.isArrayCamera=!0,this.cameras=e}}class rr extends pt{constructor(){super(),this.isGroup=!0,this.type="Group"}}const jf={type:"move"};class ss{constructor(){this._targetRay=null,this._grip=null,this._hand=null}getHandSpace(){return this._hand===null&&(this._hand=new rr,this._hand.matrixAutoUpdate=!1,this._hand.visible=!1,this._hand.joints={},this._hand.inputState={pinching:!1}),this._hand}getTargetRaySpace(){return this._targetRay===null&&(this._targetRay=new rr,this._targetRay.matrixAutoUpdate=!1,this._targetRay.visible=!1,this._targetRay.hasLinearVelocity=!1,this._targetRay.linearVelocity=new C,this._targetRay.hasAngularVelocity=!1,this._targetRay.angularVelocity=new C),this._targetRay}getGripSpace(){return this._grip===null&&(this._grip=new rr,this._grip.matrixAutoUpdate=!1,this._grip.visible=!1,this._grip.hasLinearVelocity=!1,this._grip.linearVelocity=new C,this._grip.hasAngularVelocity=!1,this._grip.angularVelocity=new C),this._grip}dispatchEvent(e){return this._targetRay!==null&&this._targetRay.dispatchEvent(e),this._grip!==null&&this._grip.dispatchEvent(e),this._hand!==null&&this._hand.dispatchEvent(e),this}connect(e){if(e&&e.hand){const t=this._hand;if(t)for(const n of e.hand.values())this._getHandJoint(t,n)}return this.dispatchEvent({type:"connected",data:e}),this}disconnect(e){return this.dispatchEvent({type:"disconnected",data:e}),this._targetRay!==null&&(this._targetRay.visible=!1),this._grip!==null&&(this._grip.visible=!1),this._hand!==null&&(this._hand.visible=!1),this}update(e,t,n){let r=null,s=null,o=null;const a=this._targetRay,l=this._grip,c=this._hand;if(e&&t.session.visibilityState!=="visible-blurred"){if(c&&e.hand){o=!0;for(const p of e.hand.values()){const h=t.getJointPose(p,n),x=this._getHandJoint(c,p);h!==null&&(x.matrix.fromArray(h.transform.matrix),x.matrix.decompose(x.position,x.rotation,x.scale),x.jointRadius=h.radius),x.visible=h!==null}const u=c.joints["index-finger-tip"],d=c.joints["thumb-tip"],f=u.position.distanceTo(d.position),m=.02,g=.005;c.inputState.pinching&&f>m+g?(c.inputState.pinching=!1,this.dispatchEvent({type:"pinchend",handedness:e.handedness,target:this})):!c.inputState.pinching&&f<=m-g&&(c.inputState.pinching=!0,this.dispatchEvent({type:"pinchstart",handedness:e.handedness,target:this}))}else l!==null&&e.gripSpace&&(s=t.getPose(e.gripSpace,n),s!==null&&(l.matrix.fromArray(s.transform.matrix),l.matrix.decompose(l.position,l.rotation,l.scale),s.linearVelocity?(l.hasLinearVelocity=!0,l.linearVelocity.copy(s.linearVelocity)):l.hasLinearVelocity=!1,s.angularVelocity?(l.hasAngularVelocity=!0,l.angularVelocity.copy(s.angularVelocity)):l.hasAngularVelocity=!1));a!==null&&(r=t.getPose(e.targetRaySpace,n),r===null&&s!==null&&(r=s),r!==null&&(a.matrix.fromArray(r.transform.matrix),a.matrix.decompose(a.position,a.rotation,a.scale),r.linearVelocity?(a.hasLinearVelocity=!0,a.linearVelocity.copy(r.linearVelocity)):a.hasLinearVelocity=!1,r.angularVelocity?(a.hasAngularVelocity=!0,a.angularVelocity.copy(r.angularVelocity)):a.hasAngularVelocity=!1,this.dispatchEvent(jf)))}return a!==null&&(a.visible=r!==null),l!==null&&(l.visible=s!==null),c!==null&&(c.visible=o!==null),this}_getHandJoint(e,t){if(e.joints[t.jointName]===void 0){const n=new rr;n.matrixAutoUpdate=!1,n.visible=!1,e.joints[t.jointName]=n,e.add(n)}return e.joints[t.jointName]}}class Yf extends ft{constructor(e,t,n,r,s,o,a,l,c,u){if(u=u!==void 0?u:En,u!==En&&u!==ri)throw new Error("DepthTexture format must be either THREE.DepthFormat or THREE.DepthStencilFormat");n===void 0&&u===En&&(n=Sn),n===void 0&&u===ri&&(n=ei),super(null,r,s,o,a,l,u,n,c),this.isDepthTexture=!0,this.image={width:e,height:t},this.magFilter=a!==void 0?a:gt,this.minFilter=l!==void 0?l:gt,this.flipY=!1,this.generateMipmaps=!1}}class Kf extends Pn{constructor(e,t){super();const n=this;let r=null,s=1,o=null,a="local-floor",l=null,c=null,u=null,d=null,f=null,m=null;const g=t.getContextAttributes();let p=null,h=null;const x=[],E=[],b=new Set,S=new Map,y=new Lt;y.layers.enable(1),y.viewport=new Xe;const R=new Lt;R.layers.enable(2),R.viewport=new Xe;const U=[y,R],v=new Xf;v.layers.enable(1),v.layers.enable(2);let T=null,D=null;this.cameraAutoUpdate=!0,this.enabled=!1,this.isPresenting=!1,this.getController=function(F){let k=x[F];return k===void 0&&(k=new ss,x[F]=k),k.getTargetRaySpace()},this.getControllerGrip=function(F){let k=x[F];return k===void 0&&(k=new ss,x[F]=k),k.getGripSpace()},this.getHand=function(F){let k=x[F];return k===void 0&&(k=new ss,x[F]=k),k.getHandSpace()};function X(F){const k=E.indexOf(F.inputSource);if(k===-1)return;const Q=x[k];Q!==void 0&&Q.dispatchEvent({type:F.type,data:F.inputSource})}function oe(){r.removeEventListener("select",X),r.removeEventListener("selectstart",X),r.removeEventListener("selectend",X),r.removeEventListener("squeeze",X),r.removeEventListener("squeezestart",X),r.removeEventListener("squeezeend",X),r.removeEventListener("end",oe),r.removeEventListener("inputsourceschange",N);for(let F=0;F<x.length;F++){const k=E[F];k!==null&&(E[F]=null,x[F].disconnect(k))}T=null,D=null,e.setRenderTarget(p),f=null,d=null,u=null,r=null,h=null,J.stop(),n.isPresenting=!1,n.dispatchEvent({type:"sessionend"})}this.setFramebufferScaleFactor=function(F){s=F,n.isPresenting===!0&&console.warn("THREE.WebXRManager: Cannot change framebuffer scale while presenting.")},this.setReferenceSpaceType=function(F){a=F,n.isPresenting===!0&&console.warn("THREE.WebXRManager: Cannot change reference space type while presenting.")},this.getReferenceSpace=function(){return l||o},this.setReferenceSpace=function(F){l=F},this.getBaseLayer=function(){return d!==null?d:f},this.getBinding=function(){return u},this.getFrame=function(){return m},this.getSession=function(){return r},this.setSession=async function(F){if(r=F,r!==null){if(p=e.getRenderTarget(),r.addEventListener("select",X),r.addEventListener("selectstart",X),r.addEventListener("selectend",X),r.addEventListener("squeeze",X),r.addEventListener("squeezestart",X),r.addEventListener("squeezeend",X),r.addEventListener("end",oe),r.addEventListener("inputsourceschange",N),g.xrCompatible!==!0&&await t.makeXRCompatible(),r.renderState.layers===void 0||e.capabilities.isWebGL2===!1){const k={antialias:r.renderState.layers===void 0?g.antialias:!0,alpha:g.alpha,depth:g.depth,stencil:g.stencil,framebufferScaleFactor:s};f=new XRWebGLLayer(r,t,k),r.updateRenderState({baseLayer:f}),h=new Cn(f.framebufferWidth,f.framebufferHeight,{format:kt,type:In,encoding:e.outputEncoding,stencilBuffer:g.stencil})}else{let k=null,Q=null,ee=null;g.depth&&(ee=g.stencil?35056:33190,k=g.stencil?ri:En,Q=g.stencil?ei:Sn);const re={colorFormat:32856,depthFormat:ee,scaleFactor:s};u=new XRWebGLBinding(r,t),d=u.createProjectionLayer(re),r.updateRenderState({layers:[d]}),h=new Cn(d.textureWidth,d.textureHeight,{format:kt,type:In,depthTexture:new Yf(d.textureWidth,d.textureHeight,Q,void 0,void 0,void 0,void 0,void 0,void 0,k),stencilBuffer:g.stencil,encoding:e.outputEncoding,samples:g.antialias?4:0});const H=e.properties.get(h);H.__ignoreDepthValues=d.ignoreDepthValues}h.isXRRenderTarget=!0,this.setFoveation(1),l=null,o=await r.requestReferenceSpace(a),J.setContext(r),J.start(),n.isPresenting=!0,n.dispatchEvent({type:"sessionstart"})}};function N(F){for(let k=0;k<F.removed.length;k++){const Q=F.removed[k],ee=E.indexOf(Q);ee>=0&&(E[ee]=null,x[ee].disconnect(Q))}for(let k=0;k<F.added.length;k++){const Q=F.added[k];let ee=E.indexOf(Q);if(ee===-1){for(let H=0;H<x.length;H++)if(H>=E.length){E.push(Q),ee=H;break}else if(E[H]===null){E[H]=Q,ee=H;break}if(ee===-1)break}const re=x[ee];re&&re.connect(Q)}}const z=new C,W=new C;function K(F,k,Q){z.setFromMatrixPosition(k.matrixWorld),W.setFromMatrixPosition(Q.matrixWorld);const ee=z.distanceTo(W),re=k.projectionMatrix.elements,H=Q.projectionMatrix.elements,Ee=re[14]/(re[10]-1),he=re[14]/(re[10]+1),ve=(re[9]+1)/re[5],de=(re[9]-1)/re[5],ze=(re[8]-1)/re[0],Ae=(H[8]+1)/H[0],xe=Ee*ze,et=Ee*Ae,je=ee/(-ze+Ae),Ye=je*-ze;k.matrixWorld.decompose(F.position,F.quaternion,F.scale),F.translateX(Ye),F.translateZ(je),F.matrixWorld.compose(F.position,F.quaternion,F.scale),F.matrixWorldInverse.copy(F.matrixWorld).invert();const lt=Ee+je,He=he+je,Ue=xe-Ye,Et=et+(ee-Ye),mt=ve*he/He*lt,_=de*he/He*lt;F.projectionMatrix.makePerspective(Ue,Et,mt,_,lt,He)}function j(F,k){k===null?F.matrixWorld.copy(F.matrix):F.matrixWorld.multiplyMatrices(k.matrixWorld,F.matrix),F.matrixWorldInverse.copy(F.matrixWorld).invert()}this.updateCamera=function(F){if(r===null)return;v.near=R.near=y.near=F.near,v.far=R.far=y.far=F.far,(T!==v.near||D!==v.far)&&(r.updateRenderState({depthNear:v.near,depthFar:v.far}),T=v.near,D=v.far);const k=F.parent,Q=v.cameras;j(v,k);for(let re=0;re<Q.length;re++)j(Q[re],k);v.matrixWorld.decompose(v.position,v.quaternion,v.scale),F.matrix.copy(v.matrix),F.matrix.decompose(F.position,F.quaternion,F.scale);const ee=F.children;for(let re=0,H=ee.length;re<H;re++)ee[re].updateMatrixWorld(!0);Q.length===2?K(v,y,R):v.projectionMatrix.copy(y.projectionMatrix)},this.getCamera=function(){return v},this.getFoveation=function(){if(d!==null)return d.fixedFoveation;if(f!==null)return f.fixedFoveation},this.setFoveation=function(F){d!==null&&(d.fixedFoveation=F),f!==null&&f.fixedFoveation!==void 0&&(f.fixedFoveation=F)},this.getPlanes=function(){return b};let G=null;function te(F,k){if(c=k.getViewerPose(l||o),m=k,c!==null){const Q=c.views;f!==null&&(e.setRenderTargetFramebuffer(h,f.framebuffer),e.setRenderTarget(h));let ee=!1;Q.length!==v.cameras.length&&(v.cameras.length=0,ee=!0);for(let re=0;re<Q.length;re++){const H=Q[re];let Ee=null;if(f!==null)Ee=f.getViewport(H);else{const ve=u.getViewSubImage(d,H);Ee=ve.viewport,re===0&&(e.setRenderTargetTextures(h,ve.colorTexture,d.ignoreDepthValues?void 0:ve.depthStencilTexture),e.setRenderTarget(h))}let he=U[re];he===void 0&&(he=new Lt,he.layers.enable(re),he.viewport=new Xe,U[re]=he),he.matrix.fromArray(H.transform.matrix),he.projectionMatrix.fromArray(H.projectionMatrix),he.viewport.set(Ee.x,Ee.y,Ee.width,Ee.height),re===0&&v.matrix.copy(he.matrix),ee===!0&&v.cameras.push(he)}}for(let Q=0;Q<x.length;Q++){const ee=E[Q],re=x[Q];ee!==null&&re!==void 0&&re.update(ee,k,l||o)}if(G&&G(F,k),k.detectedPlanes){n.dispatchEvent({type:"planesdetected",data:k.detectedPlanes});let Q=null;for(const ee of b)k.detectedPlanes.has(ee)||(Q===null&&(Q=[]),Q.push(ee));if(Q!==null)for(const ee of Q)b.delete(ee),S.delete(ee),n.dispatchEvent({type:"planeremoved",data:ee});for(const ee of k.detectedPlanes)if(!b.has(ee))b.add(ee),S.set(ee,k.lastChangedTime),n.dispatchEvent({type:"planeadded",data:ee});else{const re=S.get(ee);ee.lastChangedTime>re&&(S.set(ee,ee.lastChangedTime),n.dispatchEvent({type:"planechanged",data:ee}))}}m=null}const J=new Vo;J.setAnimationLoop(te),this.setAnimationLoop=function(F){G=F},this.dispose=function(){}}}function Qf(i,e){function t(p,h){h.color.getRGB(p.fogColor.value,Co(i)),h.isFog?(p.fogNear.value=h.near,p.fogFar.value=h.far):h.isFogExp2&&(p.fogDensity.value=h.density)}function n(p,h,x,E,b){h.isMeshBasicMaterial||h.isMeshLambertMaterial?r(p,h):h.isMeshToonMaterial?(r(p,h),u(p,h)):h.isMeshPhongMaterial?(r(p,h),c(p,h)):h.isMeshStandardMaterial?(r(p,h),d(p,h),h.isMeshPhysicalMaterial&&f(p,h,b)):h.isMeshMatcapMaterial?(r(p,h),m(p,h)):h.isMeshDepthMaterial?r(p,h):h.isMeshDistanceMaterial?(r(p,h),g(p,h)):h.isMeshNormalMaterial?r(p,h):h.isLineBasicMaterial?(s(p,h),h.isLineDashedMaterial&&o(p,h)):h.isPointsMaterial?a(p,h,x,E):h.isSpriteMaterial?l(p,h):h.isShadowMaterial?(p.color.value.copy(h.color),p.opacity.value=h.opacity):h.isShaderMaterial&&(h.uniformsNeedUpdate=!1)}function r(p,h){p.opacity.value=h.opacity,h.color&&p.diffuse.value.copy(h.color),h.emissive&&p.emissive.value.copy(h.emissive).multiplyScalar(h.emissiveIntensity),h.map&&(p.map.value=h.map),h.alphaMap&&(p.alphaMap.value=h.alphaMap),h.bumpMap&&(p.bumpMap.value=h.bumpMap,p.bumpScale.value=h.bumpScale,h.side===Vt&&(p.bumpScale.value*=-1)),h.displacementMap&&(p.displacementMap.value=h.displacementMap,p.displacementScale.value=h.displacementScale,p.displacementBias.value=h.displacementBias),h.emissiveMap&&(p.emissiveMap.value=h.emissiveMap),h.normalMap&&(p.normalMap.value=h.normalMap,p.normalScale.value.copy(h.normalScale),h.side===Vt&&p.normalScale.value.negate()),h.specularMap&&(p.specularMap.value=h.specularMap),h.alphaTest>0&&(p.alphaTest.value=h.alphaTest);const x=e.get(h).envMap;if(x&&(p.envMap.value=x,p.flipEnvMap.value=x.isCubeTexture&&x.isRenderTargetTexture===!1?-1:1,p.reflectivity.value=h.reflectivity,p.ior.value=h.ior,p.refractionRatio.value=h.refractionRatio),h.lightMap){p.lightMap.value=h.lightMap;const S=i.physicallyCorrectLights!==!0?Math.PI:1;p.lightMapIntensity.value=h.lightMapIntensity*S}h.aoMap&&(p.aoMap.value=h.aoMap,p.aoMapIntensity.value=h.aoMapIntensity);let E;h.map?E=h.map:h.specularMap?E=h.specularMap:h.displacementMap?E=h.displacementMap:h.normalMap?E=h.normalMap:h.bumpMap?E=h.bumpMap:h.roughnessMap?E=h.roughnessMap:h.metalnessMap?E=h.metalnessMap:h.alphaMap?E=h.alphaMap:h.emissiveMap?E=h.emissiveMap:h.clearcoatMap?E=h.clearcoatMap:h.clearcoatNormalMap?E=h.clearcoatNormalMap:h.clearcoatRoughnessMap?E=h.clearcoatRoughnessMap:h.iridescenceMap?E=h.iridescenceMap:h.iridescenceThicknessMap?E=h.iridescenceThicknessMap:h.specularIntensityMap?E=h.specularIntensityMap:h.specularColorMap?E=h.specularColorMap:h.transmissionMap?E=h.transmissionMap:h.thicknessMap?E=h.thicknessMap:h.sheenColorMap?E=h.sheenColorMap:h.sheenRoughnessMap&&(E=h.sheenRoughnessMap),E!==void 0&&(E.isWebGLRenderTarget&&(E=E.texture),E.matrixAutoUpdate===!0&&E.updateMatrix(),p.uvTransform.value.copy(E.matrix));let b;h.aoMap?b=h.aoMap:h.lightMap&&(b=h.lightMap),b!==void 0&&(b.isWebGLRenderTarget&&(b=b.texture),b.matrixAutoUpdate===!0&&b.updateMatrix(),p.uv2Transform.value.copy(b.matrix))}function s(p,h){p.diffuse.value.copy(h.color),p.opacity.value=h.opacity}function o(p,h){p.dashSize.value=h.dashSize,p.totalSize.value=h.dashSize+h.gapSize,p.scale.value=h.scale}function a(p,h,x,E){p.diffuse.value.copy(h.color),p.opacity.value=h.opacity,p.size.value=h.size*x,p.scale.value=E*.5,h.map&&(p.map.value=h.map),h.alphaMap&&(p.alphaMap.value=h.alphaMap),h.alphaTest>0&&(p.alphaTest.value=h.alphaTest);let b;h.map?b=h.map:h.alphaMap&&(b=h.alphaMap),b!==void 0&&(b.matrixAutoUpdate===!0&&b.updateMatrix(),p.uvTransform.value.copy(b.matrix))}function l(p,h){p.diffuse.value.copy(h.color),p.opacity.value=h.opacity,p.rotation.value=h.rotation,h.map&&(p.map.value=h.map),h.alphaMap&&(p.alphaMap.value=h.alphaMap),h.alphaTest>0&&(p.alphaTest.value=h.alphaTest);let x;h.map?x=h.map:h.alphaMap&&(x=h.alphaMap),x!==void 0&&(x.matrixAutoUpdate===!0&&x.updateMatrix(),p.uvTransform.value.copy(x.matrix))}function c(p,h){p.specular.value.copy(h.specular),p.shininess.value=Math.max(h.shininess,1e-4)}function u(p,h){h.gradientMap&&(p.gradientMap.value=h.gradientMap)}function d(p,h){p.roughness.value=h.roughness,p.metalness.value=h.metalness,h.roughnessMap&&(p.roughnessMap.value=h.roughnessMap),h.metalnessMap&&(p.metalnessMap.value=h.metalnessMap),e.get(h).envMap&&(p.envMapIntensity.value=h.envMapIntensity)}function f(p,h,x){p.ior.value=h.ior,h.sheen>0&&(p.sheenColor.value.copy(h.sheenColor).multiplyScalar(h.sheen),p.sheenRoughness.value=h.sheenRoughness,h.sheenColorMap&&(p.sheenColorMap.value=h.sheenColorMap),h.sheenRoughnessMap&&(p.sheenRoughnessMap.value=h.sheenRoughnessMap)),h.clearcoat>0&&(p.clearcoat.value=h.clearcoat,p.clearcoatRoughness.value=h.clearcoatRoughness,h.clearcoatMap&&(p.clearcoatMap.value=h.clearcoatMap),h.clearcoatRoughnessMap&&(p.clearcoatRoughnessMap.value=h.clearcoatRoughnessMap),h.clearcoatNormalMap&&(p.clearcoatNormalScale.value.copy(h.clearcoatNormalScale),p.clearcoatNormalMap.value=h.clearcoatNormalMap,h.side===Vt&&p.clearcoatNormalScale.value.negate())),h.iridescence>0&&(p.iridescence.value=h.iridescence,p.iridescenceIOR.value=h.iridescenceIOR,p.iridescenceThicknessMinimum.value=h.iridescenceThicknessRange[0],p.iridescenceThicknessMaximum.value=h.iridescenceThicknessRange[1],h.iridescenceMap&&(p.iridescenceMap.value=h.iridescenceMap),h.iridescenceThicknessMap&&(p.iridescenceThicknessMap.value=h.iridescenceThicknessMap)),h.transmission>0&&(p.transmission.value=h.transmission,p.transmissionSamplerMap.value=x.texture,p.transmissionSamplerSize.value.set(x.width,x.height),h.transmissionMap&&(p.transmissionMap.value=h.transmissionMap),p.thickness.value=h.thickness,h.thicknessMap&&(p.thicknessMap.value=h.thicknessMap),p.attenuationDistance.value=h.attenuationDistance,p.attenuationColor.value.copy(h.attenuationColor)),p.specularIntensity.value=h.specularIntensity,p.specularColor.value.copy(h.specularColor),h.specularIntensityMap&&(p.specularIntensityMap.value=h.specularIntensityMap),h.specularColorMap&&(p.specularColorMap.value=h.specularColorMap)}function m(p,h){h.matcap&&(p.matcap.value=h.matcap)}function g(p,h){p.referencePosition.value.copy(h.referencePosition),p.nearDistance.value=h.nearDistance,p.farDistance.value=h.farDistance}return{refreshFogUniforms:t,refreshMaterialUniforms:n}}function Jf(i,e,t,n){let r={},s={},o=[];const a=t.isWebGL2?i.getParameter(35375):0;function l(E,b){const S=b.program;n.uniformBlockBinding(E,S)}function c(E,b){let S=r[E.id];S===void 0&&(g(E),S=u(E),r[E.id]=S,E.addEventListener("dispose",h));const y=b.program;n.updateUBOMapping(E,y);const R=e.render.frame;s[E.id]!==R&&(f(E),s[E.id]=R)}function u(E){const b=d();E.__bindingPointIndex=b;const S=i.createBuffer(),y=E.__size,R=E.usage;return i.bindBuffer(35345,S),i.bufferData(35345,y,R),i.bindBuffer(35345,null),i.bindBufferBase(35345,b,S),S}function d(){for(let E=0;E<a;E++)if(o.indexOf(E)===-1)return o.push(E),E;return console.error("THREE.WebGLRenderer: Maximum number of simultaneously usable uniforms groups reached."),0}function f(E){const b=r[E.id],S=E.uniforms,y=E.__cache;i.bindBuffer(35345,b);for(let R=0,U=S.length;R<U;R++){const v=S[R];if(m(v,R,y)===!0){const T=v.value,D=v.__offset;typeof T=="number"?(v.__data[0]=T,i.bufferSubData(35345,D,v.__data)):(v.value.isMatrix3?(v.__data[0]=v.value.elements[0],v.__data[1]=v.value.elements[1],v.__data[2]=v.value.elements[2],v.__data[3]=v.value.elements[0],v.__data[4]=v.value.elements[3],v.__data[5]=v.value.elements[4],v.__data[6]=v.value.elements[5],v.__data[7]=v.value.elements[0],v.__data[8]=v.value.elements[6],v.__data[9]=v.value.elements[7],v.__data[10]=v.value.elements[8],v.__data[11]=v.value.elements[0]):T.toArray(v.__data),i.bufferSubData(35345,D,v.__data))}}i.bindBuffer(35345,null)}function m(E,b,S){const y=E.value;if(S[b]===void 0)return typeof y=="number"?S[b]=y:S[b]=y.clone(),!0;if(typeof y=="number"){if(S[b]!==y)return S[b]=y,!0}else{const R=S[b];if(R.equals(y)===!1)return R.copy(y),!0}return!1}function g(E){const b=E.uniforms;let S=0;const y=16;let R=0;for(let U=0,v=b.length;U<v;U++){const T=b[U],D=p(T);if(T.__data=new Float32Array(D.storage/Float32Array.BYTES_PER_ELEMENT),T.__offset=S,U>0){R=S%y;const X=y-R;R!==0&&X-D.boundary<0&&(S+=y-R,T.__offset=S)}S+=D.storage}return R=S%y,R>0&&(S+=y-R),E.__size=S,E.__cache={},this}function p(E){const b=E.value,S={boundary:0,storage:0};return typeof b=="number"?(S.boundary=4,S.storage=4):b.isVector2?(S.boundary=8,S.storage=8):b.isVector3||b.isColor?(S.boundary=16,S.storage=12):b.isVector4?(S.boundary=16,S.storage=16):b.isMatrix3?(S.boundary=48,S.storage=48):b.isMatrix4?(S.boundary=64,S.storage=64):b.isTexture?console.warn("THREE.WebGLRenderer: Texture samplers can not be part of an uniforms group."):console.warn("THREE.WebGLRenderer: Unsupported uniform value type.",b),S}function h(E){const b=E.target;b.removeEventListener("dispose",h);const S=o.indexOf(b.__bindingPointIndex);o.splice(S,1),i.deleteBuffer(r[b.id]),delete r[b.id],delete s[b.id]}function x(){for(const E in r)i.deleteBuffer(r[E]);o=[],r={},s={}}return{bind:l,update:c,dispose:x}}function $f(){const i=Li("canvas");return i.style.display="block",i}function qo(i={}){this.isWebGLRenderer=!0;const e=i.canvas!==void 0?i.canvas:$f(),t=i.context!==void 0?i.context:null,n=i.depth!==void 0?i.depth:!0,r=i.stencil!==void 0?i.stencil:!0,s=i.antialias!==void 0?i.antialias:!1,o=i.premultipliedAlpha!==void 0?i.premultipliedAlpha:!0,a=i.preserveDrawingBuffer!==void 0?i.preserveDrawingBuffer:!1,l=i.powerPreference!==void 0?i.powerPreference:"default",c=i.failIfMajorPerformanceCaveat!==void 0?i.failIfMajorPerformanceCaveat:!1;let u;t!==null?u=t.getContextAttributes().alpha:u=i.alpha!==void 0?i.alpha:!1;let d=null,f=null;const m=[],g=[];this.domElement=e,this.debug={checkShaderErrors:!0},this.autoClear=!0,this.autoClearColor=!0,this.autoClearDepth=!0,this.autoClearStencil=!0,this.sortObjects=!0,this.clippingPlanes=[],this.localClippingEnabled=!1,this.outputEncoding=Rn,this.physicallyCorrectLights=!1,this.toneMapping=tn,this.toneMappingExposure=1;const p=this;let h=!1,x=0,E=0,b=null,S=-1,y=null;const R=new Xe,U=new Xe;let v=null,T=e.width,D=e.height,X=1,oe=null,N=null;const z=new Xe(0,0,T,D),W=new Xe(0,0,T,D);let K=!1;const j=new zo;let G=!1,te=!1,J=null;const F=new ke,k=new Se,Q=new C,ee={background:null,fog:null,environment:null,overrideMaterial:null,isScene:!0};function re(){return b===null?X:1}let H=t;function Ee(A,V){for(let O=0;O<A.length;O++){const P=A[O],Z=e.getContext(P,V);if(Z!==null)return Z}return null}try{const A={alpha:!0,depth:n,stencil:r,antialias:s,premultipliedAlpha:o,preserveDrawingBuffer:a,powerPreference:l,failIfMajorPerformanceCaveat:c};if("setAttribute"in e&&e.setAttribute("data-engine",`three.js r${Ss}`),e.addEventListener("webglcontextlost",pe,!1),e.addEventListener("webglcontextrestored",fe,!1),e.addEventListener("webglcontextcreationerror",Te,!1),H===null){const V=["webgl2","webgl","experimental-webgl"];if(p.isWebGL1Renderer===!0&&V.shift(),H=Ee(V,A),H===null)throw Ee(V)?new Error("Error creating WebGL context with your selected attributes."):new Error("Error creating WebGL context.")}H.getShaderPrecisionFormat===void 0&&(H.getShaderPrecisionFormat=function(){return{rangeMin:1,rangeMax:1,precision:1}})}catch(A){throw console.error("THREE.WebGLRenderer: "+A.message),A}let he,ve,de,ze,Ae,xe,et,je,Ye,lt,He,Ue,Et,mt,_,M,q,$,ne,se,Me,w,L,le;function ue(){he=new ch(H),ve=new nh(H,he,i),he.init(ve),w=new Wf(H,he,ve),de=new Zf(H,he,ve),ze=new hh,Ae=new Cf,xe=new Gf(H,he,de,Ae,ve,w,ze),et=new rh(p),je=new lh(p),Ye=new yc(H,ve),L=new eh(H,he,Ye,ve),lt=new uh(H,Ye,ze,L),He=new gh(H,lt,Ye,ze),ne=new mh(H,ve,xe),M=new ih(Ae),Ue=new Rf(p,et,je,he,ve,L,M),Et=new Qf(p,Ae),mt=new Pf,_=new Ff(he,ve),$=new $d(p,et,je,de,He,u,o),q=new Hf(p,He,ve),le=new Jf(H,ze,ve,de),se=new th(H,he,ze,ve),Me=new dh(H,he,ze,ve),ze.programs=Ue.programs,p.capabilities=ve,p.extensions=he,p.properties=Ae,p.renderLists=mt,p.shadowMap=q,p.state=de,p.info=ze}ue();const ae=new Kf(p,H);this.xr=ae,this.getContext=function(){return H},this.getContextAttributes=function(){return H.getContextAttributes()},this.forceContextLoss=function(){const A=he.get("WEBGL_lose_context");A&&A.loseContext()},this.forceContextRestore=function(){const A=he.get("WEBGL_lose_context");A&&A.restoreContext()},this.getPixelRatio=function(){return X},this.setPixelRatio=function(A){A!==void 0&&(X=A,this.setSize(T,D,!1))},this.getSize=function(A){return A.set(T,D)},this.setSize=function(A,V,O){if(ae.isPresenting){console.warn("THREE.WebGLRenderer: Can't change size while VR device is presenting.");return}T=A,D=V,e.width=Math.floor(A*X),e.height=Math.floor(V*X),O!==!1&&(e.style.width=A+"px",e.style.height=V+"px"),this.setViewport(0,0,A,V)},this.getDrawingBufferSize=function(A){return A.set(T*X,D*X).floor()},this.setDrawingBufferSize=function(A,V,O){T=A,D=V,X=O,e.width=Math.floor(A*O),e.height=Math.floor(V*O),this.setViewport(0,0,A,V)},this.getCurrentViewport=function(A){return A.copy(R)},this.getViewport=function(A){return A.copy(z)},this.setViewport=function(A,V,O,P){A.isVector4?z.set(A.x,A.y,A.z,A.w):z.set(A,V,O,P),de.viewport(R.copy(z).multiplyScalar(X).floor())},this.getScissor=function(A){return A.copy(W)},this.setScissor=function(A,V,O,P){A.isVector4?W.set(A.x,A.y,A.z,A.w):W.set(A,V,O,P),de.scissor(U.copy(W).multiplyScalar(X).floor())},this.getScissorTest=function(){return K},this.setScissorTest=function(A){de.setScissorTest(K=A)},this.setOpaqueSort=function(A){oe=A},this.setTransparentSort=function(A){N=A},this.getClearColor=function(A){return A.copy($.getClearColor())},this.setClearColor=function(){$.setClearColor.apply($,arguments)},this.getClearAlpha=function(){return $.getClearAlpha()},this.setClearAlpha=function(){$.setClearAlpha.apply($,arguments)},this.clear=function(A=!0,V=!0,O=!0){let P=0;A&&(P|=16384),V&&(P|=256),O&&(P|=1024),H.clear(P)},this.clearColor=function(){this.clear(!0,!1,!1)},this.clearDepth=function(){this.clear(!1,!0,!1)},this.clearStencil=function(){this.clear(!1,!1,!0)},this.dispose=function(){e.removeEventListener("webglcontextlost",pe,!1),e.removeEventListener("webglcontextrestored",fe,!1),e.removeEventListener("webglcontextcreationerror",Te,!1),mt.dispose(),_.dispose(),Ae.dispose(),et.dispose(),je.dispose(),He.dispose(),L.dispose(),le.dispose(),Ue.dispose(),ae.dispose(),ae.removeEventListener("sessionstart",ce),ae.removeEventListener("sessionend",me),J&&(J.dispose(),J=null),Ve.stop()};function pe(A){A.preventDefault(),console.log("THREE.WebGLRenderer: Context Lost."),h=!0}function fe(){console.log("THREE.WebGLRenderer: Context Restored."),h=!1;const A=ze.autoReset,V=q.enabled,O=q.autoUpdate,P=q.needsUpdate,Z=q.type;ue(),ze.autoReset=A,q.enabled=V,q.autoUpdate=O,q.needsUpdate=P,q.type=Z}function Te(A){console.error("THREE.WebGLRenderer: A WebGL context could not be created. Reason: ",A.statusMessage)}function Ie(A){const V=A.target;V.removeEventListener("dispose",Ie),Ne(V)}function Ne(A){I(A),Ae.remove(A)}function I(A){const V=Ae.get(A).programs;V!==void 0&&(V.forEach(function(O){Ue.releaseProgram(O)}),A.isShaderMaterial&&Ue.releaseShaderCache(A))}this.renderBufferDirect=function(A,V,O,P,Z,ge){V===null&&(V=ee);const ye=Z.isMesh&&Z.matrixWorld.determinant()<0,be=$o(A,V,O,P,Z);de.setMaterial(P,ye);let we=O.index,Pe=1;P.wireframe===!0&&(we=lt.getWireframeAttribute(O),Pe=2);const Re=O.drawRange,Ce=O.attributes.position;let Ze=Re.start*Pe,xt=(Re.start+Re.count)*Pe;ge!==null&&(Ze=Math.max(Ze,ge.start*Pe),xt=Math.min(xt,(ge.start+ge.count)*Pe)),we!==null?(Ze=Math.max(Ze,0),xt=Math.min(xt,we.count)):Ce!=null&&(Ze=Math.max(Ze,0),xt=Math.min(xt,Ce.count));const jt=xt-Ze;if(jt<0||jt===1/0)return;L.setup(Z,P,be,O,we);let mn,Ge=se;if(we!==null&&(mn=Ye.get(we),Ge=Me,Ge.setIndex(mn)),Z.isMesh)P.wireframe===!0?(de.setLineWidth(P.wireframeLinewidth*re()),Ge.setMode(1)):Ge.setMode(4);else if(Z.isLine){let Le=P.linewidth;Le===void 0&&(Le=1),de.setLineWidth(Le*re()),Z.isLineSegments?Ge.setMode(1):Z.isLineLoop?Ge.setMode(2):Ge.setMode(3)}else Z.isPoints?Ge.setMode(0):Z.isSprite&&Ge.setMode(4);if(Z.isInstancedMesh)Ge.renderInstances(Ze,jt,Z.count);else if(O.isInstancedBufferGeometry){const Le=O._maxInstanceCount!==void 0?O._maxInstanceCount:1/0,_r=Math.min(O.instanceCount,Le);Ge.renderInstances(Ze,jt,_r)}else Ge.render(Ze,jt)},this.compile=function(A,V){function O(P,Z,ge){P.transparent===!0&&P.side===dn?(P.side=Vt,P.needsUpdate=!0,Tt(P,Z,ge),P.side=ti,P.needsUpdate=!0,Tt(P,Z,ge),P.side=dn):Tt(P,Z,ge)}f=_.get(A),f.init(),g.push(f),A.traverseVisible(function(P){P.isLight&&P.layers.test(V.layers)&&(f.pushLight(P),P.castShadow&&f.pushShadow(P))}),f.setupLights(p.physicallyCorrectLights),A.traverse(function(P){const Z=P.material;if(Z)if(Array.isArray(Z))for(let ge=0;ge<Z.length;ge++){const ye=Z[ge];O(ye,A,P)}else O(Z,A,P)}),g.pop(),f=null};let B=null;function Y(A){B&&B(A)}function ce(){Ve.stop()}function me(){Ve.start()}const Ve=new Vo;Ve.setAnimationLoop(Y),typeof self<"u"&&Ve.setContext(self),this.setAnimationLoop=function(A){B=A,ae.setAnimationLoop(A),A===null?Ve.stop():Ve.start()},ae.addEventListener("sessionstart",ce),ae.addEventListener("sessionend",me),this.render=function(A,V){if(V!==void 0&&V.isCamera!==!0){console.error("THREE.WebGLRenderer.render: camera is not an instance of THREE.Camera.");return}if(h===!0)return;A.matrixWorldAutoUpdate===!0&&A.updateMatrixWorld(),V.parent===null&&V.matrixWorldAutoUpdate===!0&&V.updateMatrixWorld(),ae.enabled===!0&&ae.isPresenting===!0&&(ae.cameraAutoUpdate===!0&&ae.updateCamera(V),V=ae.getCamera()),A.isScene===!0&&A.onBeforeRender(p,A,V,b),f=_.get(A,g.length),f.init(),g.push(f),F.multiplyMatrices(V.projectionMatrix,V.matrixWorldInverse),j.setFromProjectionMatrix(F),te=this.localClippingEnabled,G=M.init(this.clippingPlanes,te,V),d=mt.get(A,m.length),d.init(),m.push(d),tt(A,V,0,p.sortObjects),d.finish(),p.sortObjects===!0&&d.sort(oe,N),G===!0&&M.beginShadows();const O=f.state.shadowsArray;if(q.render(O,A,V),G===!0&&M.endShadows(),this.info.autoReset===!0&&this.info.reset(),$.render(d,A),f.setupLights(p.physicallyCorrectLights),V.isArrayCamera){const P=V.cameras;for(let Z=0,ge=P.length;Z<ge;Z++){const ye=P[Z];at(d,A,ye,ye.viewport)}}else at(d,A,V);b!==null&&(xe.updateMultisampleRenderTarget(b),xe.updateRenderTargetMipmap(b)),A.isScene===!0&&A.onAfterRender(p,A,V),L.resetDefaultState(),S=-1,y=null,g.pop(),g.length>0?f=g[g.length-1]:f=null,m.pop(),m.length>0?d=m[m.length-1]:d=null};function tt(A,V,O,P){if(A.visible===!1)return;if(A.layers.test(V.layers)){if(A.isGroup)O=A.renderOrder;else if(A.isLOD)A.autoUpdate===!0&&A.update(V);else if(A.isLight)f.pushLight(A),A.castShadow&&f.pushShadow(A);else if(A.isSprite){if(!A.frustumCulled||j.intersectsSprite(A)){P&&Q.setFromMatrixPosition(A.matrixWorld).applyMatrix4(F);const ye=He.update(A),be=A.material;be.visible&&d.push(A,ye,be,O,Q.z,null)}}else if((A.isMesh||A.isLine||A.isPoints)&&(A.isSkinnedMesh&&A.skeleton.frame!==ze.render.frame&&(A.skeleton.update(),A.skeleton.frame=ze.render.frame),!A.frustumCulled||j.intersectsObject(A))){P&&Q.setFromMatrixPosition(A.matrixWorld).applyMatrix4(F);const ye=He.update(A),be=A.material;if(Array.isArray(be)){const we=ye.groups;for(let Pe=0,Re=we.length;Pe<Re;Pe++){const Ce=we[Pe],Ze=be[Ce.materialIndex];Ze&&Ze.visible&&d.push(A,ye,Ze,O,Q.z,Ce)}}else be.visible&&d.push(A,ye,be,O,Q.z,null)}}const ge=A.children;for(let ye=0,be=ge.length;ye<be;ye++)tt(ge[ye],V,O,P)}function at(A,V,O,P){const Z=A.opaque,ge=A.transmissive,ye=A.transparent;f.setupLightsView(O),ge.length>0&&pn(Z,V,O),P&&de.viewport(R.copy(P)),Z.length>0&&Oe(Z,V,O),ge.length>0&&Oe(ge,V,O),ye.length>0&&Oe(ye,V,O),de.buffers.depth.setTest(!0),de.buffers.depth.setMask(!0),de.buffers.color.setMask(!0),de.setPolygonOffset(!1)}function pn(A,V,O){const P=ve.isWebGL2;J===null&&(J=new Cn(1,1,{generateMipmaps:!0,type:he.has("EXT_color_buffer_half_float")?Ri:In,minFilter:vr,samples:P&&s===!0?4:0})),p.getDrawingBufferSize(k),P?J.setSize(k.x,k.y):J.setSize(vs(k.x),vs(k.y));const Z=p.getRenderTarget();p.setRenderTarget(J),p.clear();const ge=p.toneMapping;p.toneMapping=tn,Oe(A,V,O),p.toneMapping=ge,xe.updateMultisampleRenderTarget(J),xe.updateRenderTargetMipmap(J),p.setRenderTarget(Z)}function Oe(A,V,O){const P=V.isScene===!0?V.overrideMaterial:null;for(let Z=0,ge=A.length;Z<ge;Z++){const ye=A[Z],be=ye.object,we=ye.geometry,Pe=P===null?ye.material:P,Re=ye.group;be.layers.test(O.layers)&&Xt(be,V,O,we,Pe,Re)}}function Xt(A,V,O,P,Z,ge){A.onBeforeRender(p,V,O,P,Z,ge),A.modelViewMatrix.multiplyMatrices(O.matrixWorldInverse,A.matrixWorld),A.normalMatrix.getNormalMatrix(A.modelViewMatrix),Z.onBeforeRender(p,V,O,P,A,ge),Z.transparent===!0&&Z.side===dn?(Z.side=Vt,Z.needsUpdate=!0,p.renderBufferDirect(O,V,P,Z,A,ge),Z.side=ti,Z.needsUpdate=!0,p.renderBufferDirect(O,V,P,Z,A,ge),Z.side=dn):p.renderBufferDirect(O,V,P,Z,A,ge),A.onAfterRender(p,V,O,P,Z,ge)}function Tt(A,V,O){V.isScene!==!0&&(V=ee);const P=Ae.get(A),Z=f.state.lights,ge=f.state.shadowsArray,ye=Z.state.version,be=Ue.getParameters(A,Z.state,ge,V,O),we=Ue.getProgramCacheKey(be);let Pe=P.programs;P.environment=A.isMeshStandardMaterial?V.environment:null,P.fog=V.fog,P.envMap=(A.isMeshStandardMaterial?je:et).get(A.envMap||P.environment),Pe===void 0&&(A.addEventListener("dispose",Ie),Pe=new Map,P.programs=Pe);let Re=Pe.get(we);if(Re!==void 0){if(P.currentProgram===Re&&P.lightsStateVersion===ye)return zs(A,be),Re}else be.uniforms=Ue.getUniforms(A),A.onBuild(O,be,p),A.onBeforeCompile(be,p),Re=Ue.acquireProgram(be,we),Pe.set(we,Re),P.uniforms=be.uniforms;const Ce=P.uniforms;(!A.isShaderMaterial&&!A.isRawShaderMaterial||A.clipping===!0)&&(Ce.clippingPlanes=M.uniform),zs(A,be),P.needsLights=tl(A),P.lightsStateVersion=ye,P.needsLights&&(Ce.ambientLightColor.value=Z.state.ambient,Ce.lightProbe.value=Z.state.probe,Ce.directionalLights.value=Z.state.directional,Ce.directionalLightShadows.value=Z.state.directionalShadow,Ce.spotLights.value=Z.state.spot,Ce.spotLightShadows.value=Z.state.spotShadow,Ce.rectAreaLights.value=Z.state.rectArea,Ce.ltc_1.value=Z.state.rectAreaLTC1,Ce.ltc_2.value=Z.state.rectAreaLTC2,Ce.pointLights.value=Z.state.point,Ce.pointLightShadows.value=Z.state.pointShadow,Ce.hemisphereLights.value=Z.state.hemi,Ce.directionalShadowMap.value=Z.state.directionalShadowMap,Ce.directionalShadowMatrix.value=Z.state.directionalShadowMatrix,Ce.spotShadowMap.value=Z.state.spotShadowMap,Ce.spotLightMatrix.value=Z.state.spotLightMatrix,Ce.spotLightMap.value=Z.state.spotLightMap,Ce.pointShadowMap.value=Z.state.pointShadowMap,Ce.pointShadowMatrix.value=Z.state.pointShadowMatrix);const Ze=Re.getUniforms(),xt=dr.seqWithValue(Ze.seq,Ce);return P.currentProgram=Re,P.uniformsList=xt,Re}function zs(A,V){const O=Ae.get(A);O.outputEncoding=V.outputEncoding,O.instancing=V.instancing,O.skinning=V.skinning,O.morphTargets=V.morphTargets,O.morphNormals=V.morphNormals,O.morphColors=V.morphColors,O.morphTargetsCount=V.morphTargetsCount,O.numClippingPlanes=V.numClippingPlanes,O.numIntersection=V.numClipIntersection,O.vertexAlphas=V.vertexAlphas,O.vertexTangents=V.vertexTangents,O.toneMapping=V.toneMapping}function $o(A,V,O,P,Z){V.isScene!==!0&&(V=ee),xe.resetTextureUnits();const ge=V.fog,ye=P.isMeshStandardMaterial?V.environment:null,be=b===null?p.outputEncoding:b.isXRRenderTarget===!0?b.texture.encoding:Rn,we=(P.isMeshStandardMaterial?je:et).get(P.envMap||ye),Pe=P.vertexColors===!0&&!!O.attributes.color&&O.attributes.color.itemSize===4,Re=!!P.normalMap&&!!O.attributes.tangent,Ce=!!O.morphAttributes.position,Ze=!!O.morphAttributes.normal,xt=!!O.morphAttributes.color,jt=P.toneMapped?p.toneMapping:tn,mn=O.morphAttributes.position||O.morphAttributes.normal||O.morphAttributes.color,Ge=mn!==void 0?mn.length:0,Le=Ae.get(P),_r=f.state.lights;if(G===!0&&(te===!0||A!==y)){const Mt=A===y&&P.id===S;M.setState(P,A,Mt)}let nt=!1;P.version===Le.__version?(Le.needsLights&&Le.lightsStateVersion!==_r.state.version||Le.outputEncoding!==be||Z.isInstancedMesh&&Le.instancing===!1||!Z.isInstancedMesh&&Le.instancing===!0||Z.isSkinnedMesh&&Le.skinning===!1||!Z.isSkinnedMesh&&Le.skinning===!0||Le.envMap!==we||P.fog===!0&&Le.fog!==ge||Le.numClippingPlanes!==void 0&&(Le.numClippingPlanes!==M.numPlanes||Le.numIntersection!==M.numIntersection)||Le.vertexAlphas!==Pe||Le.vertexTangents!==Re||Le.morphTargets!==Ce||Le.morphNormals!==Ze||Le.morphColors!==xt||Le.toneMapping!==jt||ve.isWebGL2===!0&&Le.morphTargetsCount!==Ge)&&(nt=!0):(nt=!0,Le.__version=P.version);let gn=Le.currentProgram;nt===!0&&(gn=Tt(P,V,Z));let Vs=!1,hi=!1,br=!1;const ct=gn.getUniforms(),vn=Le.uniforms;if(de.useProgram(gn.program)&&(Vs=!0,hi=!0,br=!0),P.id!==S&&(S=P.id,hi=!0),Vs||y!==A){if(ct.setValue(H,"projectionMatrix",A.projectionMatrix),ve.logarithmicDepthBuffer&&ct.setValue(H,"logDepthBufFC",2/(Math.log(A.far+1)/Math.LN2)),y!==A&&(y=A,hi=!0,br=!0),P.isShaderMaterial||P.isMeshPhongMaterial||P.isMeshToonMaterial||P.isMeshStandardMaterial||P.envMap){const Mt=ct.map.cameraPosition;Mt!==void 0&&Mt.setValue(H,Q.setFromMatrixPosition(A.matrixWorld))}(P.isMeshPhongMaterial||P.isMeshToonMaterial||P.isMeshLambertMaterial||P.isMeshBasicMaterial||P.isMeshStandardMaterial||P.isShaderMaterial)&&ct.setValue(H,"isOrthographic",A.isOrthographicCamera===!0),(P.isMeshPhongMaterial||P.isMeshToonMaterial||P.isMeshLambertMaterial||P.isMeshBasicMaterial||P.isMeshStandardMaterial||P.isShaderMaterial||P.isShadowMaterial||Z.isSkinnedMesh)&&ct.setValue(H,"viewMatrix",A.matrixWorldInverse)}if(Z.isSkinnedMesh){ct.setOptional(H,Z,"bindMatrix"),ct.setOptional(H,Z,"bindMatrixInverse");const Mt=Z.skeleton;Mt&&(ve.floatVertexTextures?(Mt.boneTexture===null&&Mt.computeBoneTexture(),ct.setValue(H,"boneTexture",Mt.boneTexture,xe),ct.setValue(H,"boneTextureSize",Mt.boneTextureSize)):console.warn("THREE.WebGLRenderer: SkinnedMesh can only be used with WebGL 2. With WebGL 1 OES_texture_float and vertex textures support is required."))}const Sr=O.morphAttributes;if((Sr.position!==void 0||Sr.normal!==void 0||Sr.color!==void 0&&ve.isWebGL2===!0)&&ne.update(Z,O,P,gn),(hi||Le.receiveShadow!==Z.receiveShadow)&&(Le.receiveShadow=Z.receiveShadow,ct.setValue(H,"receiveShadow",Z.receiveShadow)),P.isMeshGouraudMaterial&&P.envMap!==null&&(vn.envMap.value=we,vn.flipEnvMap.value=we.isCubeTexture&&we.isRenderTargetTexture===!1?-1:1),hi&&(ct.setValue(H,"toneMappingExposure",p.toneMappingExposure),Le.needsLights&&el(vn,br),ge&&P.fog===!0&&Et.refreshFogUniforms(vn,ge),Et.refreshMaterialUniforms(vn,P,X,D,J),dr.upload(H,Le.uniformsList,vn,xe)),P.isShaderMaterial&&P.uniformsNeedUpdate===!0&&(dr.upload(H,Le.uniformsList,vn,xe),P.uniformsNeedUpdate=!1),P.isSpriteMaterial&&ct.setValue(H,"center",Z.center),ct.setValue(H,"modelViewMatrix",Z.modelViewMatrix),ct.setValue(H,"normalMatrix",Z.normalMatrix),ct.setValue(H,"modelMatrix",Z.matrixWorld),P.isShaderMaterial||P.isRawShaderMaterial){const Mt=P.uniformsGroups;for(let wr=0,nl=Mt.length;wr<nl;wr++)if(ve.isWebGL2){const Ds=Mt[wr];le.update(Ds,gn),le.bind(Ds,gn)}else console.warn("THREE.WebGLRenderer: Uniform Buffer Objects can only be used with WebGL 2.")}return gn}function el(A,V){A.ambientLightColor.needsUpdate=V,A.lightProbe.needsUpdate=V,A.directionalLights.needsUpdate=V,A.directionalLightShadows.needsUpdate=V,A.pointLights.needsUpdate=V,A.pointLightShadows.needsUpdate=V,A.spotLights.needsUpdate=V,A.spotLightShadows.needsUpdate=V,A.rectAreaLights.needsUpdate=V,A.hemisphereLights.needsUpdate=V}function tl(A){return A.isMeshLambertMaterial||A.isMeshToonMaterial||A.isMeshPhongMaterial||A.isMeshStandardMaterial||A.isShadowMaterial||A.isShaderMaterial&&A.lights===!0}this.getActiveCubeFace=function(){return x},this.getActiveMipmapLevel=function(){return E},this.getRenderTarget=function(){return b},this.setRenderTargetTextures=function(A,V,O){Ae.get(A.texture).__webglTexture=V,Ae.get(A.depthTexture).__webglTexture=O;const P=Ae.get(A);P.__hasExternalTextures=!0,P.__hasExternalTextures&&(P.__autoAllocateDepthBuffer=O===void 0,P.__autoAllocateDepthBuffer||he.has("WEBGL_multisampled_render_to_texture")===!0&&(console.warn("THREE.WebGLRenderer: Render-to-texture extension was disabled because an external texture was provided"),P.__useRenderToTexture=!1))},this.setRenderTargetFramebuffer=function(A,V){const O=Ae.get(A);O.__webglFramebuffer=V,O.__useDefaultFramebuffer=V===void 0},this.setRenderTarget=function(A,V=0,O=0){b=A,x=V,E=O;let P=!0,Z=null,ge=!1,ye=!1;if(A){const we=Ae.get(A);we.__useDefaultFramebuffer!==void 0?(de.bindFramebuffer(36160,null),P=!1):we.__webglFramebuffer===void 0?xe.setupRenderTarget(A):we.__hasExternalTextures&&xe.rebindTextures(A,Ae.get(A.texture).__webglTexture,Ae.get(A.depthTexture).__webglTexture);const Pe=A.texture;(Pe.isData3DTexture||Pe.isDataArrayTexture||Pe.isCompressedArrayTexture)&&(ye=!0);const Re=Ae.get(A).__webglFramebuffer;A.isWebGLCubeRenderTarget?(Z=Re[V],ge=!0):ve.isWebGL2&&A.samples>0&&xe.useMultisampledRTT(A)===!1?Z=Ae.get(A).__webglMultisampledFramebuffer:Z=Re,R.copy(A.viewport),U.copy(A.scissor),v=A.scissorTest}else R.copy(z).multiplyScalar(X).floor(),U.copy(W).multiplyScalar(X).floor(),v=K;if(de.bindFramebuffer(36160,Z)&&ve.drawBuffers&&P&&de.drawBuffers(A,Z),de.viewport(R),de.scissor(U),de.setScissorTest(v),ge){const we=Ae.get(A.texture);H.framebufferTexture2D(36160,36064,34069+V,we.__webglTexture,O)}else if(ye){const we=Ae.get(A.texture),Pe=V||0;H.framebufferTextureLayer(36160,36064,we.__webglTexture,O||0,Pe)}S=-1},this.readRenderTargetPixels=function(A,V,O,P,Z,ge,ye){if(!(A&&A.isWebGLRenderTarget)){console.error("THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not THREE.WebGLRenderTarget.");return}let be=Ae.get(A).__webglFramebuffer;if(A.isWebGLCubeRenderTarget&&ye!==void 0&&(be=be[ye]),be){de.bindFramebuffer(36160,be);try{const we=A.texture,Pe=we.format,Re=we.type;if(Pe!==kt&&w.convert(Pe)!==H.getParameter(35739)){console.error("THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not in RGBA or implementation defined format.");return}const Ce=Re===Ri&&(he.has("EXT_color_buffer_half_float")||ve.isWebGL2&&he.has("EXT_color_buffer_float"));if(Re!==In&&w.convert(Re)!==H.getParameter(35738)&&!(Re===wn&&(ve.isWebGL2||he.has("OES_texture_float")||he.has("WEBGL_color_buffer_float")))&&!Ce){console.error("THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not in UnsignedByteType or implementation defined type.");return}V>=0&&V<=A.width-P&&O>=0&&O<=A.height-Z&&H.readPixels(V,O,P,Z,w.convert(Pe),w.convert(Re),ge)}finally{const we=b!==null?Ae.get(b).__webglFramebuffer:null;de.bindFramebuffer(36160,we)}}},this.copyFramebufferToTexture=function(A,V,O=0){const P=Math.pow(2,-O),Z=Math.floor(V.image.width*P),ge=Math.floor(V.image.height*P);xe.setTexture2D(V,0),H.copyTexSubImage2D(3553,O,0,0,A.x,A.y,Z,ge),de.unbindTexture()},this.copyTextureToTexture=function(A,V,O,P=0){const Z=V.image.width,ge=V.image.height,ye=w.convert(O.format),be=w.convert(O.type);xe.setTexture2D(O,0),H.pixelStorei(37440,O.flipY),H.pixelStorei(37441,O.premultiplyAlpha),H.pixelStorei(3317,O.unpackAlignment),V.isDataTexture?H.texSubImage2D(3553,P,A.x,A.y,Z,ge,ye,be,V.image.data):V.isCompressedTexture?H.compressedTexSubImage2D(3553,P,A.x,A.y,V.mipmaps[0].width,V.mipmaps[0].height,ye,V.mipmaps[0].data):H.texSubImage2D(3553,P,A.x,A.y,ye,be,V.image),P===0&&O.generateMipmaps&&H.generateMipmap(3553),de.unbindTexture()},this.copyTextureToTexture3D=function(A,V,O,P,Z=0){if(p.isWebGL1Renderer){console.warn("THREE.WebGLRenderer.copyTextureToTexture3D: can only be used with WebGL2.");return}const ge=A.max.x-A.min.x+1,ye=A.max.y-A.min.y+1,be=A.max.z-A.min.z+1,we=w.convert(P.format),Pe=w.convert(P.type);let Re;if(P.isData3DTexture)xe.setTexture3D(P,0),Re=32879;else if(P.isDataArrayTexture)xe.setTexture2DArray(P,0),Re=35866;else{console.warn("THREE.WebGLRenderer.copyTextureToTexture3D: only supports THREE.DataTexture3D and THREE.DataTexture2DArray.");return}H.pixelStorei(37440,P.flipY),H.pixelStorei(37441,P.premultiplyAlpha),H.pixelStorei(3317,P.unpackAlignment);const Ce=H.getParameter(3314),Ze=H.getParameter(32878),xt=H.getParameter(3316),jt=H.getParameter(3315),mn=H.getParameter(32877),Ge=O.isCompressedTexture?O.mipmaps[0]:O.image;H.pixelStorei(3314,Ge.width),H.pixelStorei(32878,Ge.height),H.pixelStorei(3316,A.min.x),H.pixelStorei(3315,A.min.y),H.pixelStorei(32877,A.min.z),O.isDataTexture||O.isData3DTexture?H.texSubImage3D(Re,Z,V.x,V.y,V.z,ge,ye,be,we,Pe,Ge.data):O.isCompressedArrayTexture?(console.warn("THREE.WebGLRenderer.copyTextureToTexture3D: untested support for compressed srcTexture."),H.compressedTexSubImage3D(Re,Z,V.x,V.y,V.z,ge,ye,be,we,Ge.data)):H.texSubImage3D(Re,Z,V.x,V.y,V.z,ge,ye,be,we,Pe,Ge),H.pixelStorei(3314,Ce),H.pixelStorei(32878,Ze),H.pixelStorei(3316,xt),H.pixelStorei(3315,jt),H.pixelStorei(32877,mn),Z===0&&P.generateMipmaps&&H.generateMipmap(Re),de.unbindTexture()},this.initTexture=function(A){A.isCubeTexture?xe.setTextureCube(A,0):A.isData3DTexture?xe.setTexture3D(A,0):A.isDataArrayTexture||A.isCompressedArrayTexture?xe.setTexture2DArray(A,0):xe.setTexture2D(A,0),de.unbindTexture()},this.resetState=function(){x=0,E=0,b=null,de.reset(),L.reset()},typeof __THREE_DEVTOOLS__<"u"&&__THREE_DEVTOOLS__.dispatchEvent(new CustomEvent("observe",{detail:this}))}class ep extends qo{}ep.prototype.isWebGL1Renderer=!0;class tp extends pt{constructor(){super(),this.isScene=!0,this.type="Scene",this.background=null,this.environment=null,this.fog=null,this.backgroundBlurriness=0,this.backgroundIntensity=1,this.overrideMaterial=null,typeof __THREE_DEVTOOLS__<"u"&&__THREE_DEVTOOLS__.dispatchEvent(new CustomEvent("observe",{detail:this}))}copy(e,t){return super.copy(e,t),e.background!==null&&(this.background=e.background.clone()),e.environment!==null&&(this.environment=e.environment.clone()),e.fog!==null&&(this.fog=e.fog.clone()),this.backgroundBlurriness=e.backgroundBlurriness,this.backgroundIntensity=e.backgroundIntensity,e.overrideMaterial!==null&&(this.overrideMaterial=e.overrideMaterial.clone()),this.matrixAutoUpdate=e.matrixAutoUpdate,this}toJSON(e){const t=super.toJSON(e);return this.fog!==null&&(t.object.fog=this.fog.toJSON()),this.backgroundBlurriness>0&&(t.backgroundBlurriness=this.backgroundBlurriness),this.backgroundIntensity!==1&&(t.backgroundIntensity=this.backgroundIntensity),t}get autoUpdate(){return console.warn("THREE.Scene: autoUpdate was renamed to matrixWorldAutoUpdate in r144."),this.matrixWorldAutoUpdate}set autoUpdate(e){console.warn("THREE.Scene: autoUpdate was renamed to matrixWorldAutoUpdate in r144."),this.matrixWorldAutoUpdate=e}}class Xa extends Dt{constructor(e,t,n,r=1){super(e,t,n),this.isInstancedBufferAttribute=!0,this.meshPerAttribute=r}copy(e){return super.copy(e),this.meshPerAttribute=e.meshPerAttribute,this}toJSON(){const e=super.toJSON();return e.meshPerAttribute=this.meshPerAttribute,e.isInstancedBufferAttribute=!0,e}}const ja=new ke,Ya=new ke,sr=[],np=new ke,vi=new zt;class ip extends zt{constructor(e,t,n){super(e,t),this.isInstancedMesh=!0,this.instanceMatrix=new Xa(new Float32Array(n*16),16),this.instanceColor=null,this.count=n,this.frustumCulled=!1;for(let r=0;r<n;r++)this.setMatrixAt(r,np)}copy(e,t){return super.copy(e,t),this.instanceMatrix.copy(e.instanceMatrix),e.instanceColor!==null&&(this.instanceColor=e.instanceColor.clone()),this.count=e.count,this}getColorAt(e,t){t.fromArray(this.instanceColor.array,e*3)}getMatrixAt(e,t){t.fromArray(this.instanceMatrix.array,e*16)}raycast(e,t){const n=this.matrixWorld,r=this.count;if(vi.geometry=this.geometry,vi.material=this.material,vi.material!==void 0)for(let s=0;s<r;s++){this.getMatrixAt(s,ja),Ya.multiplyMatrices(n,ja),vi.matrixWorld=Ya,vi.raycast(e,sr);for(let o=0,a=sr.length;o<a;o++){const l=sr[o];l.instanceId=s,l.object=this,t.push(l)}sr.length=0}}setColorAt(e,t){this.instanceColor===null&&(this.instanceColor=new Xa(new Float32Array(this.instanceMatrix.count*3),3)),t.toArray(this.instanceColor.array,e*3)}setMatrixAt(e,t){t.toArray(this.instanceMatrix.array,e*16)}updateMorphTargets(){}dispose(){this.dispatchEvent({type:"dispose"})}}class rp extends ci{constructor(e){super(),this.isLineBasicMaterial=!0,this.type="LineBasicMaterial",this.color=new qe(16777215),this.linewidth=1,this.linecap="round",this.linejoin="round",this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.linewidth=e.linewidth,this.linecap=e.linecap,this.linejoin=e.linejoin,this.fog=e.fog,this}}const Ka=new C,Qa=new C,Ja=new ke,as=new xr,ar=new Di;class sp extends pt{constructor(e=new Ut,t=new rp){super(),this.isLine=!0,this.type="Line",this.geometry=e,this.material=t,this.updateMorphTargets()}copy(e,t){return super.copy(e,t),this.material=e.material,this.geometry=e.geometry,this}computeLineDistances(){const e=this.geometry;if(e.index===null){const t=e.attributes.position,n=[0];for(let r=1,s=t.count;r<s;r++)Ka.fromBufferAttribute(t,r-1),Qa.fromBufferAttribute(t,r),n[r]=n[r-1],n[r]+=Ka.distanceTo(Qa);e.setAttribute("lineDistance",new Qe(n,1))}else console.warn("THREE.Line.computeLineDistances(): Computation only possible with non-indexed BufferGeometry.");return this}raycast(e,t){const n=this.geometry,r=this.matrixWorld,s=e.params.Line.threshold,o=n.drawRange;if(n.boundingSphere===null&&n.computeBoundingSphere(),ar.copy(n.boundingSphere),ar.applyMatrix4(r),ar.radius+=s,e.ray.intersectsSphere(ar)===!1)return;Ja.copy(r).invert(),as.copy(e.ray).applyMatrix4(Ja);const a=s/((this.scale.x+this.scale.y+this.scale.z)/3),l=a*a,c=new C,u=new C,d=new C,f=new C,m=this.isLineSegments?2:1,g=n.index,h=n.attributes.position;if(g!==null){const x=Math.max(0,o.start),E=Math.min(g.count,o.start+o.count);for(let b=x,S=E-1;b<S;b+=m){const y=g.getX(b),R=g.getX(b+1);if(c.fromBufferAttribute(h,y),u.fromBufferAttribute(h,R),as.distanceSqToSegment(c,u,f,d)>l)continue;f.applyMatrix4(this.matrixWorld);const v=e.ray.origin.distanceTo(f);v<e.near||v>e.far||t.push({distance:v,point:d.clone().applyMatrix4(this.matrixWorld),index:b,face:null,faceIndex:null,object:this})}}else{const x=Math.max(0,o.start),E=Math.min(h.count,o.start+o.count);for(let b=x,S=E-1;b<S;b+=m){if(c.fromBufferAttribute(h,b),u.fromBufferAttribute(h,b+1),as.distanceSqToSegment(c,u,f,d)>l)continue;f.applyMatrix4(this.matrixWorld);const R=e.ray.origin.distanceTo(f);R<e.near||R>e.far||t.push({distance:R,point:d.clone().applyMatrix4(this.matrixWorld),index:b,face:null,faceIndex:null,object:this})}}}updateMorphTargets(){const t=this.geometry.morphAttributes,n=Object.keys(t);if(n.length>0){const r=t[n[0]];if(r!==void 0){this.morphTargetInfluences=[],this.morphTargetDictionary={};for(let s=0,o=r.length;s<o;s++){const a=r[s].name||String(s);this.morphTargetInfluences.push(0),this.morphTargetDictionary[a]=s}}}}}const $a=new C,eo=new C;class ap extends sp{constructor(e,t){super(e,t),this.isLineSegments=!0,this.type="LineSegments"}computeLineDistances(){const e=this.geometry;if(e.index===null){const t=e.attributes.position,n=[];for(let r=0,s=t.count;r<s;r+=2)$a.fromBufferAttribute(t,r),eo.fromBufferAttribute(t,r+1),n[r]=r===0?0:n[r-1],n[r+1]=n[r]+$a.distanceTo(eo);e.setAttribute("lineDistance",new Qe(n,1))}else console.warn("THREE.LineSegments.computeLineDistances(): Computation only possible with non-indexed BufferGeometry.");return this}}class op extends ci{constructor(e){super(),this.isPointsMaterial=!0,this.type="PointsMaterial",this.color=new qe(16777215),this.map=null,this.alphaMap=null,this.size=1,this.sizeAttenuation=!0,this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.map=e.map,this.alphaMap=e.alphaMap,this.size=e.size,this.sizeAttenuation=e.sizeAttenuation,this.fog=e.fog,this}}const to=new ke,Ms=new xr,or=new Di,lr=new C;class lp extends pt{constructor(e=new Ut,t=new op){super(),this.isPoints=!0,this.type="Points",this.geometry=e,this.material=t,this.updateMorphTargets()}copy(e,t){return super.copy(e,t),this.material=e.material,this.geometry=e.geometry,this}raycast(e,t){const n=this.geometry,r=this.matrixWorld,s=e.params.Points.threshold,o=n.drawRange;if(n.boundingSphere===null&&n.computeBoundingSphere(),or.copy(n.boundingSphere),or.applyMatrix4(r),or.radius+=s,e.ray.intersectsSphere(or)===!1)return;to.copy(r).invert(),Ms.copy(e.ray).applyMatrix4(to);const a=s/((this.scale.x+this.scale.y+this.scale.z)/3),l=a*a,c=n.index,d=n.attributes.position;if(c!==null){const f=Math.max(0,o.start),m=Math.min(c.count,o.start+o.count);for(let g=f,p=m;g<p;g++){const h=c.getX(g);lr.fromBufferAttribute(d,h),no(lr,h,l,r,e,t,this)}}else{const f=Math.max(0,o.start),m=Math.min(d.count,o.start+o.count);for(let g=f,p=m;g<p;g++)lr.fromBufferAttribute(d,g),no(lr,g,l,r,e,t,this)}}updateMorphTargets(){const t=this.geometry.morphAttributes,n=Object.keys(t);if(n.length>0){const r=t[n[0]];if(r!==void 0){this.morphTargetInfluences=[],this.morphTargetDictionary={};for(let s=0,o=r.length;s<o;s++){const a=r[s].name||String(s);this.morphTargetInfluences.push(0),this.morphTargetDictionary[a]=s}}}}}function no(i,e,t,n,r,s,o){const a=Ms.distanceSqToPoint(i);if(a<t){const l=new C;Ms.closestPointToPoint(i,l),l.applyMatrix4(n);const c=r.ray.origin.distanceTo(l);if(c<r.near||c>r.far)return;s.push({distance:c,distanceToRay:Math.sqrt(a),point:l,index:e,face:null,object:o})}}class cp extends ft{constructor(e,t,n,r,s,o,a,l,c){super(e,t,n,r,s,o,a,l,c),this.isCanvasTexture=!0,this.needsUpdate=!0}}const io={enabled:!1,files:{},add:function(i,e){this.enabled!==!1&&(this.files[i]=e)},get:function(i){if(this.enabled!==!1)return this.files[i]},remove:function(i){delete this.files[i]},clear:function(){this.files={}}};class Oo{constructor(e,t,n){const r=this;let s=!1,o=0,a=0,l;const c=[];this.onStart=void 0,this.onLoad=e,this.onProgress=t,this.onError=n,this.itemStart=function(u){a++,s===!1&&r.onStart!==void 0&&r.onStart(u,o,a),s=!0},this.itemEnd=function(u){o++,r.onProgress!==void 0&&r.onProgress(u,o,a),o===a&&(s=!1,r.onLoad!==void 0&&r.onLoad())},this.itemError=function(u){r.onError!==void 0&&r.onError(u)},this.resolveURL=function(u){return l?l(u):u},this.setURLModifier=function(u){return l=u,this},this.addHandler=function(u,d){return c.push(u,d),this},this.removeHandler=function(u){const d=c.indexOf(u);return d!==-1&&c.splice(d,2),this},this.getHandler=function(u){for(let d=0,f=c.length;d<f;d+=2){const m=c[d],g=c[d+1];if(m.global&&(m.lastIndex=0),m.test(u))return g}return null}}}const up=new Oo;class Bo{constructor(e){this.manager=e!==void 0?e:up,this.crossOrigin="anonymous",this.withCredentials=!1,this.path="",this.resourcePath="",this.requestHeader={}}load(){}loadAsync(e,t){const n=this;return new Promise(function(r,s){n.load(e,r,t,s)})}parse(){}setCrossOrigin(e){return this.crossOrigin=e,this}setWithCredentials(e){return this.withCredentials=e,this}setPath(e){return this.path=e,this}setResourcePath(e){return this.resourcePath=e,this}setRequestHeader(e){return this.requestHeader=e,this}}class dp extends Bo{constructor(e){super(e)}load(e,t,n,r){this.path!==void 0&&(e=this.path+e),e=this.manager.resolveURL(e);const s=this,o=io.get(e);if(o!==void 0)return s.manager.itemStart(e),setTimeout(function(){t&&t(o),s.manager.itemEnd(e)},0),o;const a=Li("img");function l(){u(),io.add(e,this),t&&t(this),s.manager.itemEnd(e)}function c(d){u(),r&&r(d),s.manager.itemError(e),s.manager.itemEnd(e)}function u(){a.removeEventListener("load",l,!1),a.removeEventListener("error",c,!1)}return a.addEventListener("load",l,!1),a.addEventListener("error",c,!1),e.slice(0,5)!=="data:"&&this.crossOrigin!==void 0&&(a.crossOrigin=this.crossOrigin),s.manager.itemStart(e),a.src=e,a}}class hp extends Bo{constructor(e){super(e)}load(e,t,n,r){const s=new ft,o=new dp(this.manager);return o.setCrossOrigin(this.crossOrigin),o.setPath(this.path),o.load(e,function(a){s.image=a,s.needsUpdate=!0,t!==void 0&&t(s)},n,r),s}}class fp{constructor(e,t,n=0,r=1/0){this.ray=new xr(e,t),this.near=n,this.far=r,this.camera=null,this.layers=new ws,this.params={Mesh:{},Line:{threshold:1},LOD:{},Points:{threshold:1},Sprite:{}}}set(e,t){this.ray.set(e,t)}setFromCamera(e,t){t.isPerspectiveCamera?(this.ray.origin.setFromMatrixPosition(t.matrixWorld),this.ray.direction.set(e.x,e.y,.5).unproject(t).sub(this.ray.origin).normalize(),this.camera=t):t.isOrthographicCamera?(this.ray.origin.set(e.x,e.y,(t.near+t.far)/(t.near-t.far)).unproject(t),this.ray.direction.set(0,0,-1).transformDirection(t.matrixWorld),this.camera=t):console.error("THREE.Raycaster: Unsupported camera type: "+t.type)}intersectObject(e,t=!0,n=[]){return ys(e,this,n,t),n.sort(ro),n}intersectObjects(e,t=!0,n=[]){for(let r=0,s=e.length;r<s;r++)ys(e[r],this,n,t);return n.sort(ro),n}}function ro(i,e){return i.distance-e.distance}function ys(i,e,t,n){if(i.layers.test(e.layers)&&i.raycast(e,t),n===!0){const r=i.children;for(let s=0,o=r.length;s<o;s++)ys(r[s],e,t,!0)}}class so{constructor(e=1,t=0,n=0){return this.radius=e,this.phi=t,this.theta=n,this}set(e,t,n){return this.radius=e,this.phi=t,this.theta=n,this}copy(e){return this.radius=e.radius,this.phi=e.phi,this.theta=e.theta,this}makeSafe(){return this.phi=Math.max(1e-6,Math.min(Math.PI-1e-6,this.phi)),this}setFromVector3(e){return this.setFromCartesianCoords(e.x,e.y,e.z)}setFromCartesianCoords(e,t,n){return this.radius=Math.sqrt(e*e+t*t+n*n),this.radius===0?(this.theta=0,this.phi=0):(this.theta=Math.atan2(e,n),this.phi=Math.acos(vt(t/this.radius,-1,1))),this}clone(){return new this.constructor().copy(this)}}typeof __THREE_DEVTOOLS__<"u"&&__THREE_DEVTOOLS__.dispatchEvent(new CustomEvent("register",{detail:{revision:Ss}}));typeof window<"u"&&(window.__THREE__?console.warn("WARNING: Multiple instances of Three.js being imported."):window.__THREE__=Ss);const pp="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAEACAYAAADFkM5nAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TpSoVBzuIqGSoThZERRylikWwUNoKrTqYXPoFTRqSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxdHJSdJES/5cUWsR4cNyPd/ced+8AoV5mqtkxAaiaZSRjUTGTXRUDrwhCQDeGMSIxU4+nFtPwHF/38PH1LsKzvM/9OXqVnMkAn0g8x3TDIt4gntm0dM77xCFWlBTic+Jxgy5I/Mh12eU3zgWHBZ4ZMtLJeeIQsVhoY7mNWdFQiaeJw4qqUb6QcVnhvMVZLVdZ8578hcGctpLiOs0hxLCEOBIQIaOKEsqwEKFVI8VEkvajHv5Bx58gl0yuEhg5FlCBCsnxg//B727N/NSkmxSMAp0vtv0xCgR2gUbNtr+PbbtxAvifgSut5a/UgdlP0mstLXwE9G0DF9ctTd4DLneAgSddMiRH8tMU8nng/Yy+KQv03wI9a25vzX2cPgBp6mr5Bjg4BMYKlL3u8e6u9t7+PdPs7wdcEnKeFWeZ3gAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+YMAg4OIbtEZ28AAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAgAElEQVR42uy96ZLkNrImShDc9zUiMrM2qXt6nmCe5b7ZfbmeI1UuEcF9X0GAuD8S1NTRVc856ozIVpfiMysrWVaaQIJYPnf/3B1QSil3ww033HDDDTf8qcDfpuCGG2644YYb/nwQblNww3+FPM9LQsgCIRR933dvM3LDDTfc8O8P8EcMAYzjOHIcx6mqqv4rxieErBzHcRDCP7WHpKqqOkmStOu6BiGEJEmSDMOwdrtd6DiOfc2xsyzL5nleKKUcz/NAURTF8zzn12tEkiT5e/tORVGUTdO0GOOF53lomqYZhqH/r3qeLMtKQRD4a3/zbwhnASGE7zXe/+0cGIZhME3TuF0VN/yrkaZpvq4r4XkeXuo8+EN5ALIsK/I8z6ZpGjmOozzPi77vO/f39w/vNcFlWZbzPM8cx3G6rmsYY4IQmnieh4ZhGJ8+ffr4zpdBVVVVtSzLTCkFqqoqnz9//sRxHJckSVJVVYMxJoIgAF3XjYeHh/tLjR3HcXQ+n6OiKMp5nidZlhXP81xK6Xqtw5l9g7yu62ae55lSSgEAvGEYet/3jaZpRlmWZd/3HcdxVBRFyfM853A43F/jedZ1JS8vL2eEEAIAUAihoOu6HoZhcI3xkiRJj8dj1DRNOc8zEgRBtG3bxhiju7u7u/dce8fj8VgURTVN0wQh5A3DMK9J/s7n8znLsnKe5wEAIJimafi+7/m+7733WXQ8Hl/qum7GcUSSJImWZZkfPnz48Gc3Cm54fyCE0NevX5/quq4IIRhCKJRlWX758uWTJEnSd0EAxnEczufzOUmSuG3bdl1XqqqqPE3TSCnlHh4erkoC0jTNj8fjMc/zbLMuTdM0CSGk7/seQig4jmMvy7L85S9/+fG9CNH5fD4XRZEPw7A9k4ExXlRVVY7HY1zXdYkQWnie513XtTHGy+fPnz9f4iIqy7JOkiQ9nU7ncRx7VVV1Sumq67p5Pp/P+/1+x/M8vPTlV5ZlVpZlyb4DBQBAy7LMcRwny7KsLMvSoigqSumqqqra9/1ACAEPDw93l57/JEnioijKaZrGdV2pKIqCYRjmMAz9Jeb5Vxt9ybIszbIsiuM46bpu1HVdRgiF7AI2TNM032PtPT4+PkZRFOd5XvR934uiKDiO42CMsWVZxiW/O8dx3Ol0io7H4ynLsqzruk4URcG2bXue55EQQna7XfhehLvruvZ8Pp+KoijGcZxlWZYsy3IJIeTHH3/84XYl3fCeeHp6eo7jOIrjON0MscPhMHIcx/3tb3/763dBANI0zauqKqMoirMsSzHGxLZti+M4TlEU9doEoCzLMs/z7Hg8nuq6rgAAfBAEAcYYlWVZSZIkzfMcQgihYRjGfr/fvcNhlMVxnMRxHNV13UAIgeu6HoRQMAxDz/M8iaIoGsdxFEVRnOd5L4qiqOu6GQTBm6ymcRxHjDFGCM11XRfn8zn5+PHjw7IsuO/79vHxEWdZlnue513C64AQQnEcp2maxnEcJ3me513XVYQQLEmS6nmehzFel2WZ4jhOkySJ5nleLMuyN2/N4XDYX8pCq6qqPp/PUZIkUZqmWdd1LcdxFEIoeZ7nruuKAQD8JT1CVVVVbdt2ZVnWURTFaZpmruvaPM8LhmFYXdd170EAnp6eHqMoipMkieM4Tuq6riCE0uFwmAVBEM/nc3Tp/VhVVVEURXY+n09lWRYQQsn3fR9jvBBCKM/zIAiC4FrvzMheMk3TsK7rmmVZ/vLycuz7vlcURT0cDkhRFAljvHAcxzuOY+12ux13ww1XRBzHSZqmZZqmWRRF57IsK8dxLI7jOFmW1TRN87eEA/4wBAAhtAzDMA/DMERRFPV93//www9fxnH0EUJL27bdNWNx8zzP4ziOdV3XX79+fVQURVQURRqGof/555+fHMexZFmWXdf1pmmarj0feZ6XXdf1TdPUWZalp9PpJIqiyOI/AQCAFkVRM4KQeJ7nKoqiDsPgLcsyv8UT8/z8fCrLMu+6rqOUAu5VK0J4nueHYeifnp6eJEmSLcuy2Fy82fqOoiip67oqyzJnF8+pbdt+XVcOQshP0zTxPC9sZO14PJ6GYRg+ffr0eRxHbxxHhBCaVFXVLjT/OSNgKSMBKcYYmaZpE0IQz/NQlmXVdV3nUpcyIQQvy7IghOZxHLs0TXNFUSRKKVnXla7renW9zul0ipIkSeM4TpIkSc7n8zmKothxHNswDK3v+/Ea638cR9T3/VhVVf34+PgiSRJc13XhOI7jeR4yN7wtSZJ4jffevI9VVVWUUoAxXuq6Lp6fn89hGPq2bVt5nudZlmWiKApd13kY4/X+/v5wu6ZuuBbqum6GYWiapqmLokhPp1MCAKCe5/nLsmBKKfkuPAAAAA5CyAEAuHVd8bIsCwAA8DwPIIRAEAT4Xs+CEJo5jlsJIStCCA3D0Ou6rl173KIoqrZt62maZo7jwDzPaJ7naRzHPk3TMggCh80TmOcZTdM0tm3b1nXduK7rvE4XD5dlwf8sCXt6enpm1mdelmXRdV2DMUaU0jXLsrRt21oURdk0Td22bR8AAC5hfSOE0DRNY9M0fZ7nWdd148ePHx8kSVKTJDmXZZm7rusqiqK0bVuXZVlblqVJkqSIoijwPA8hhBdZz3mel3Vd10VRVEVR5HEcn4uiqAAAoOu6URAEQdd1o2O4FAFY15Vb15XjOI6jlFJRFIEoiiIAAEIIOQivvwWqqiqyLCsY+YlfXl5OAACqqqoKIRQlSYIAgIs/iCRJUBRFQRRFWdM0JU3TdLv8RVEUZFlWzudz/Pnz5w+XHvvl5WUsiiJn6z5elgXrum6ydTlRSukwDGPXdY/ruhJVVVWM8SKKohgEgX8tUnLDnxt5nhdd17UbqqqqBUEAiqKosiyLPM9Dnuff5PH8Q6YBbhf/e47JSAdZ15WwA0mEEPIrO5FFURQEQRB4nheuQUaKoiiPx+OpKIqc6R4AAIBijFHf9yMAgOq6riuKovR9jzDGCyMHg6Iooq7rhqIo8jAM/fl8Pv0zWoWXl5eXNE2zzfpL0zTJsixflmXGGON5nue+74dlWYimadqXL1+ES1rf67pSAMC6LMsiCAJ0XdfXdd2YpqlvmqYjhOB5nseyLCsAAHVd1/d937Ysy7Jt23irIOYbMjINwzAOw9A3TVOXZVmHYeirqqo/Pz8/j+M4EkLwpa1ynud5SZIgg7h5BSilK6X0F3JwLRyPx1PDkOd5GcdxDACgDw8PD/v9/hAEgWsYhmlZln7psXVdN2zbtne73chx3Mpxr5kgsiwrqqpqjuNYTdPU4zj6l84OWpZlmecZdV3XRVEUV1VVmaZpGoahi6IoDMPQPT4+/sc4jhPHcdzhcLjTNE0PgmCq67oLw/DiqbFpmmZMC7WKoiiYpml5nndLwf0TYRiGbhiGrm3bvm3btu/7wfM8zzAMyzAMQ9d1/a1hsVsdgP9MPCildN1IAMdx4DcOaQ4AcHFyUpZlyVz9577vG4wxByHkEUIYITQpiiIahqF3XTcOw/AzpRRM0zTO8zwbhqHLsixmWZaXZVk4juNTSnnTNM3/rngqz/OyLMs6z/Miz/MsiqJzmqaJruu6bdtGWZb13d3dvSzLUt/3PQCASv/ntuJ/a65+5+UHGKMVFEVRuq7r0jSN2rZVm6ZpmIUPh2HoqqqqVVXVTNO0DMOw7Fc4FyaElFJKtkwE0zTtlf1wI6cAAHrJpQAh5FnIhVvXldv+m1LK0Vdc3d1YVVXTNE1dVVXZ9/14d3e3D8PwcDgcdmEY7ne7XRAEwcUFeWEYhtM0zdtLYozxNE1DVVWl67pOXdeWYRhWURTlw8PDRQkA/7q4eEmSJMuyLI7j1mEYpmEY+rquq2maRlmWNQAAFUVR/Ednw6VwOp2i8/kcNU1TLcuCvxEhLmEY3nQHfwIQQkhVVW3XdT0zRBoAALBt2zZeYdq2/WbP440A/IPD/1c/unrsdRiGse/7viiK4ng8vszzPAEAgCAIkMXiaRzHESHkxFLjAMdx3DRNsyAI8PHx8euyLNh1XU+SJIVZ7ej3WL3jOA5d13VVVVVpmqYs5fBL3/dtnuclz/P83d3dAwCAX9cVb2mBpmkab7XKVFVVNE1TTdPUXdf1xnEcTqdTzHEcJwgCvLu728myrKRpGhFCsOu6e8uyTNM0DdM0zUsWKFrXlb46e15ZB8dxPAtPNCwNUBMEQZIkSbqkOxwAwAuCANlFZNzf399ZlmVKkiQJgiAKgnC1/ZplWdH3/TCOY991Xdc0TaUoiuz7vud5nhMEQXA4HPaXznzYYJqmcX9/f8dxHJ3nGfV937uu6z09PR3ruq5s2/Zc1x2GYeiv4H1QHcdxEUKzIAhi27ZeXddlFEUnAIAgSZIUhmG46YAY+dRkWVZs276oLqlt245536Lz+ZwghGZd17XdbjfLsgw9zwtuqYjfP+I4jvu+/8X933Vdq+u6xi5+fSMBNwJwBUeAIAjCsiwYIYQJIZQQAr65HLhrW2KbFcTzPM/c3ovjOCYhZN3+DULI930/QAh5QRDEZVkwxpiy8Mn2//lvWyks/kwppWSapnkcxzkIAk/TNL3v+3b7NVEURc/zfEmSBEmSlCAIvC9fvrz5UtjtdruyLKuu67ppmhDP86Dv+57juJUdtN40TWNRFKWqqoppmjbbDNaWCXAp6ytN02yapgkAwGuapsuyDE+n0xkAAO/u7na2bfuO4zjMc1NBCMXdbvfmXPUwDH2mdfAJISsjc1IYhp5t29Y1M0+GYRimaRq6rhv7vu+HYZjDMPRUVTVt2zZt23Y+fvz44Zrr3nEcexzHTddiGoZhybKcNk3TTdM09H0/NE3TV1VVX7IWQRAEsCxLo6oqmeM4MgxDmyRJjBBCP/744w9t21ZpmsaKosie5/m73S70fT/0fd+7ZPw/TdN8GIZumqYhy7IqSZKoLMsyDMNAkiTZdV3v+fn5Rdd17Vp1KG74Y6Cqqrqqqqaqqrau62pZFhyG4U7XdUPXdcM0TePbwmg3AvBGiKIoq6qq6Lpu7Ha7HYQQqqoqLcuiPTw8HDRNMwRBEJlY6eIaAF3XNdM0Tc/zPAihwPQH5Hw+H/u+n/b7/cG2bY95I8A0TePXr19/lmVZ/vLlyxdJksR5njF7B0tVVeX3xsQZcQCiKAqSJAl1XTdJksQYY8xxHA8AAE3TVAihybZt23VdbxNKvtUDACGEYRgGCKFFEASgqqqGEEI8zwNRFIVxHIcoil6maUJ3d3cHx3Es13UdXdeNeZ6nNE2ztx6Km+s1SZI4z/Msz/OcuYDndV3Jp0+fHu7v7+9t23a6rqu7rmvYHBDTNNVLxKbv7u4OlFKq67qCEFpY/Ne+tut3mqZpHMdpnudxGIaO1eHQDcNQmeVhwHdQId7d3R2yLMs1TVPZH6Vt224YhmGe5wkhNG+Fui78/mgcx6Gqqvrl5eXU9/1wf3+/PxwOe13X1Z9//vnxfD4fZVmWTNM0IIS8oigXEwY/Pj4+JkmSsbDesq4r7vu+zbKstCzLAgDQpmnqYRh6Rsr7SxDvPxqSJEmWZSEYY8Lz/PqtgcJxHG+apv69ayFOp9OpaZqayXHquq5LRVEUx3Ec8/WgMS8lPL4RAAbbtq22bd1hGGZVVWVKKe84jmUYhuF5nisIguj7vmMYhuE4zsUXoKqq6hbHDsMwHMdxfH5+fm7btpNlWZQkSXt4eDgoiqKVZVl8/fq17Pt+8jzP0TRNPxwOdzzPc5IkiduF8XsuRFmWRVmWFU3TdNu2nd1ut8vzPH98fPzKtAhLFEVRURSVaZqm4zjuMAzTuq6Y4zjuf/7P//k/LmABhxzHAWbhd4QQwnEcybLslxxYSZIkx3Fcx3Ecnud5VjeAkyRJKYqi8H0/+GdqIOR5XiRJkmRZFrNaFEkUReeu63rTNM15nmeMMRYEQRiGoTkej2dZlhWEEFEURS6Kwr5EbrxpmtZf//pXI8uybCsF7Pv+1ZXmryUf0LIsCxrHcRJFETISKQuCoGia9m5luTVNU5VXyKqqKlVV1dM0TewB0TRN6NJjMk+aIEmSZNu25bquezgc9p7n+bIsK33fT3Vd14QQgjFeCSHrpQSgWZaR1yzE5FxVVc2+B6aUAkY05L7v+6qqWkEQBN/3nXVdiSzL0ntXh7wW2rZtT6fTqSzLGmO8rAzfekUhhNCyLPt71kIghJbqFQ3T5JTjOKK7u7uDaZqmZVmm53kXq0FxIwAMDw8P98uyLDzPw2maPJ7ngSRJKiEELcuCIYSQiep2l1KbbyjLkmZZllZVlXdd1wMA+Hme52EY+mEYZs/zHF3XdUIIN8/zPE3T1LZtTyldTdPURVGUCSGY53lBkiTVtm3r9x4MQRAEdV3XwzD0TIXPG4ZhTNM0DMMwLstSL8uytm1bFkVRDMMwcBxHWcxaS5IkucSiDMMwCMMwyLKsqOu6Op1OJ+YOq/u+n+7v70Pbtu1pmuaffvrpK/danAfquq4Nw9AhhBb2Pr+LBLBNVyZJkmdZlry8vLzM8zzf39/vLcvy4zg+ZlmWu67r8DwPT6dTZNu26fu+ixBamJfkYpfRexSa+tXBg9d1XRjPWSCEgiiKkiiKkqIokq7r+ns9i6IokiRJkviaeiOt68oty7Ks60rYpUAuPaZhGIZlWfZutwtlWZYppcD3fcd1XU+WZXFZlsV1Xc9xHMuyLENRFCUIgosYAn3fj13XdVmWFcfj8YUV9oLzPI8cx5GiKPIsy3KMMWaWHzYMQ9+qg34PiOM4jeM4iqIo3YS334ixOXbWiMw7K3yvBIBlodRt29ab9W+apup5nmeapmXbtmmapnWp8W4E4Bt8+fLls2EY+jzPCADAiaIoMyaOOY6Druva12hQVFVVF8dxdj6fX4qiKAkhK4SQH8dxYBaRNs9z//PPP2+xeG6e5/FVGyZIdV3naZomoijKh8Nhx9zGxd3d3f73PIfjOB7GmLBqgqrjOO44jtPpdHpu23b48OHD3bqu3Pl8PsVxnCqKopimaU7T1HVd11+yMBoAgGcxsLIsy6YoilKSJIFlOKzPz89fq6pqOI5bJUlSbNt2NxU5hBC6ruv8HrHUxrjruq7SNI3HcZw/fvz4cDgc7hVFked57quqaoqiyC3LslhtBEopBduff+e1DwDYUhrJsiyEpbxCURQhhFCQJEl+r2fheV5kYTBBkiQBAABYBgYhr6b3xQnAp0+fPi7LskAIoW3bHs/znK7rpmVZBhNgShhjzC5+3/f9izVnYsYuWZZl6fu+L8uyBK+gGGPSNE2DMaaEkBUAABBCBGNMuXcQJ78Xuq7ryrJskiSJ8jzPOY5bEULkm7XJbd4Y13W9LMvKSxGwP5D1j+q6rpumqfM8r4uiyMdxRB8+fHhwHMdhRcecSxoHNwLwG5bwOy/8/3dd1/8HYzx3Xde9vLy8NE3TCYIAKaUYY7ykaRqfz+cIAEAJIZwkSfwwDBMAgL68vLys60oIIdxutwtM01SHYRgQQtPvXHwLi3cPEEIYBMHOtm3n8fHxhXVHXCGE8HA4HAAA4Keffvqp7/uWVVBEy7Jc1C3bNE3Rtm3N0tLKYRj63W4XyLKspWmabM/F8zwghFTDMHTcq2BQNAzDyLIs++9ulCiK1nmep77vh7Ztm6ZpuiAInN1ut9sqvXVd11qWVeV5Xm2eSVmWJQghDyF897oV7433VJ5/O6e/Srm9Ktn6y1/+8qNhGMY4jhMAgNN1XXNd1z0ej2fbti1CCJFlWXIcx7lkGWCe50VZliXDMIzD4XDwfd8FAAhVVeVZllUfPnz4IMuyihBaNm+XLMvSNbNC3huEkJUQQhBCKM/zipVc/qUmDAAAOo7jsAyoq6bEbgbYv8D6T+q6rquqalixs4oVXHMNw7B0XbeDIHAu+WzCH2wRcNxruIeu60oJIdvf3+3BqijK/9oONVZeVnZdFwAAQJ7nBStwo1JKCaUUCILAE0KWtm0Hz/McSZLELVwmSZLE87zAxFq/S7D1/Pz8fDqdTkwBP+u6LhuGYX2r+yKEUEEQREVRZAghjzEmhBC6/dul5iTP87Iois0ir4uiKLdYOABgzbIsgxByP/zwwxdRFOW6rovj8ZgoilIyT8D0e4RilFLC3gUjhGYmwjNN07Rd1/UAALSu6zYMw26apuF4PJ4sy9INwzBYVS5VVVXl330tsuxS/psDhlJKV2aNLe9V8Y6VRCYbtouApUny1xQj/hZp/PTp01WzHz5//qw2TeNjjLGqquq6rnQcx66u64rjuBUAINzf3z+wvc+5rmvatu0YhmFd43m+7UDKvWpy1Gt3QZUkSVRVVTZN0/nhhx/At2uSZeMCRVE0VVU1QRAkRVGushYfHx+f+r7v1nVdWQdY89rfn+Nee6/UdV13Xdcwb2TB0p0Pnuc5ruvavu9bu91uf8lx/zAEAEIIZVneUssCxni2Mq/8eyiQ/xUQBOF/KYoy6rpusLQvkRBCWBveyrZt7+Hh4QFCCCil/LIs6Pn5+VEQBCEIgjAIgh07IFfmvrRZkajfpVCuqqph3QePVVXVLNVov/UC4DiOE0URbMVRMMZElmVFkiQBQggvaY10XVf3fd82TdM2TVN3XdeHYegahmGx6oezpmm67/s7Xdd1SimNoihj5aNX5i7+XWXzWIEnDgDAs0uIYIyXqqqqrUKfoii6pmlalmWVqqq6pmm6qqoKK9f7b22N8TwvfOPuF4ZhWFi1Sbwsy9K2be/7vvMez7IsC36dfkwQQhgAQFlIgmdE4Lvzttzd3d1JkiT6vj+2bds+Pj4OrMwrzxTxgJEAyTAMlbVJvrgLvCiKirWBzlkIkt+8In/961//ci3L2PM8h7WB54dh8L897iGEPACAlyRJcl3XDcPQu0ZfmL///e9/j+M4raqqppQSURQFy7IcQgj64YcfrtoBtiiKom3bZrP+i6KomQfKsV7huK578e/9h2oGRAghrMKcoiiKpuu6raqqruu6fGnh3R8JHz58UIdh2EuSBPM8L15eXo5t29Y8z/O6rqu2bTtBEPhN07RPT0+Pbdu2siyLqqoqvu97rE3xKggC0HXd9F+l8N7vmPutFDLquq5/enp60XVdmecZmaZpbUVxEEI4iqKXl5eXaKuaJsuyqmmarCiKfKl10DRNV9d1+1qPpqkEQQC2bXuKomjzPM8AALgsC9qqtNV1XW+tgb8pmPPfXtusBr0oy7KkqqrGqipmiqJIfd+3lFJ+nuehKIq8ruvWNE19GIZhIymWZTVt2/5bxyQZkRNEURQghBIhZJznGaEt726ep/d6lq3CJWJgglxp0wWIovjdhS6DIPCCIPDO5/N5mqaR53nBtm1HkiTJMAydeT+kT58+fbhmDYCyLIuiKPLn5+dT27YVhJB3XdfneR6Ypqnd399fpSvrw8PDAzvvtHEcl28JAKWUFwSBZxlO1jUaMJ1Op1Oe5/n5fI7SNE1ZarMSBMHM8zzc0sOvNe91XbdN07Rd17VN05TLsiz7/X7HOoCbtm1bLEvquyUA05aH+/Xr10fWdIR3HMfq+35e15Vcugf5Hwm73S7s+35ECCXj+FqLRVVVRVEU3TAMzTAMg6WEjMMwzLZtm7IsK4qiKIfDYa8oispxHKeqqvZ7xX+SJHEQQp6FICRFUaQsy8q+7wdd1zUAAF2WZX5+fj7O87yoqiodDoe7MAwDJow0dV2/CCOvqqpmRWn6vu9bduGalmVZhmHoCKHJ933nfD4nf//73/83hJAnhGCWmuhomqbruq5qmqb8jsMXRFGkmaZpuq7rDMMQns/n+D/+4z8eVVVVmPAKYYwXx3EcwzCM0+m0CSF1wzAM27bta3esvCZEUZRkWZZkWZY0TVPKsiyZlmRk2oiW47ird77Lsiyt67obhmHoum4Yx7FnMXJFkiSJxb6/W2PA930/y7LicDgEoigKGONFlmUlCALfNE3jmpd/URQlE97WVVUVLy8vz5sYlO2L+Zrvfnd3d7fb7fasGdu3HgDhm5LjV0HPyu7VdV2eTqdTXddlEASBLMviMAzWpUXO36JtW67v+67v+67ruraqqkZVVcVmkn9d103btq8S7vnDEIDN1QwA4EVR5IdhmBBC07IseF1XMs/zfKlWr39EyLIsAwB+cV1rmmaapqkbhqHpuq5BCAFrlgMYI3QURdEURZEVRVF//PHHH94yvqIommVZhuM47ocPH7Cu63rf9z1CCG05z6qqSq7rupZlOUEQeGEY7oIgCMMw9C/ljgQAUI77P53xNE3Tt0vXsiyTELLudruR53nY931LCKGqqjreK3zXdR3DMOzf2yPbdV237/uh7/uZELIKgiC1bVsOwzBxHMfpuq7ouh56nheyKoxtWZZt3/fdPM/z1lDmQvU53h2qqios71/VNM2AEELmkmwNw2hM0yx++umnnz9//vzpWuG4NE2zNE3Tpmmqtm2bruuatm1H3/dtXddVRoiVS1RA+wN7YqT9fn/HCpHZAAAMIZRc17WvHYdvmqbt+77t+77tuq6uqqryfd/dRJfvEXmBEPLXyLT6r0AIoRjjFSGEl2UZq6rqTNM0McaEaayupjqcpokjhOBxHFHXdT1CCIVhGDLXv2lZlnktcfofhgBomqYahmG4rutijBeMMbIsy1ZVVWYCke/28t8OYEmSZFZ4KFBVVRdFUbBt2zMMw2JtUtUgCHxFUXRWp90xTdO6hNXpuq47TdPILj/ZdV1nmiZEKV3LssxPp1O02+32+/1+z8iC5ft+cDgc9pc8mGzbdnRd1yzLsna73U7TNF3TNNVxHNs0TUeWZWnrjIgQQtxrfrCgaZrh+74XBEH4z6RoPTw8PCCEZkLIKssy1HVd6/veQwgtAADK87yoqqrquq7NMgZ2siyrsiyrgiAIEEK46Qf+HWEYhqm9sk5N0zTTcRyLtUUudF3XJUmCrNw0+dvf/jEG9WsAACAASURBVPbXK1j+2fF4PGVZlmZZlhdFURZFkXMcx5mmabMSqLqu6/r3qgf6xhvo7XY7L03TfF1XAiEUrx1eQggtLOTWdV3XlmVZcxzHO47jsC6k6qXCfH9U4qWqqmJZlh6GYcg8iSYLCV41BM3kU3DLYLq7u7u3bdtka950Xde+lvfjD0MAXNf1hmEYMcarLMvKuq6EtdwMv2fG/y2CIPARQkgURWFZFszibmYQBIEgCHCaplEURYmVyxVYXCi4RGzo7u5uz3rQC6ZpGsz9i+u6roqiKFiaDjUMw2QL1LZt27m0QlaSJNHzvGArg8v+hq7ruvv9PmDV+CTHcbp5njeBGGSllO0gCHb/rDfihx9++JF1I5RN07SGYRgJIZjjXvvSswqFelVVDcZ4ZQ2R1K0h0nuJ5K4B0zQNZm3YjuO00zQFbdsO5/M5ghDyyyuwKIrC6XQyLh2HzfO8yLIsPZ/PUZ7naRRFcdM0bRiGnuu6nq7r1hYP5f4k+L1erDda/904jvM4jkNd111d161lWcZrM1DbZDUR7O91ri3LshzHccZxnHieFzHGiyRJku/7LsvBv9q7u67Lua5rjePoU0op67UihWHoOY5jX1N78IchAJ7nOeu6rpIkCbZt2wCAdes2d4kSq/8mzD/keR5almVRSgnHcUDTNHWr6icIAqyqysYYY1EUIWOLFysFen9/fzBNU2c1x0emiC1YSgygr4UJiCiKou/7u/v7+6sszPv7+wOrRGhu76rrurldOlt4AmNM2OUMVFVVWZWwNzHlz58/f9J13WCu0HFLQxMEgdc0TRUEQVYUJVcURaWUrkyY5BwOh/13cAi6tm13ruv20zRNd3d3KIqi8/Pz83FZFqKqqsKqQl48FjyO49h1XZfnefb4+PjcNE0bBIF7OBzufd/3wjB0XNd1bk1wrgfW3poCAOgWg2YkwHRd1/p31bf8N40vDyGEWCaVRQghoigKuq5b+/0+vDbxDMNwz3GvnvBf9wC5psdL+KN9hCAIvDzPS0optSzL+J7V//+3OfgHBGF/6TzQ37AETdM0zfP5HKdpmrMSyBYAgDdN0xQEQaSUgms0RPrVu+7+EfPdygW/xzdgRZD+UyEcy7IM3/c7SunK3LPfRYtW3/fdcRyHeZ4nQggGAFCWEVGwDDyOpeBdPBgMANjqEABRFIW7u7s9E5fsdrvdznXdwPM8n7vhWuTPkGVZ0XXdDIIgZERXZd02jT+D5+X+/v7guq5dlmVNKSUAACEIAu896l94nufYtm29dw8Q4Y96EN225L8WW83zIAh8AACPEEJbGVTLsqzvvSPXht+62DeS9D2+74cPHx62sIcgCFCWZdW2bXezjNRXXDwWrGmaZtu2GQRBKIqizPQvdhAEfhiGu/1+f/dnCQX+K8BqEARbOWTP83zW+tthNQe8P8M8sPWt/qvOmvfuAXIrBXzDP9wI+/1+RynlTdM0l2XB37q7vweL94bfxufPnz8DAKCiKLJhGMYwDCMjAGYQBME1vFCe5/nTNCF2+XgQQt4wDNNxHHe324X/TIfHG36/BcxCb/o8zwhCKNi2bf5ZQrB/Rly3qPIN//ZACC1ZlhWUUiwIwnfj7r7hv0ae52XXdU3f95MgCNA0Tf2a7WeLoqiY/mQSRZHXNE1/eHi4v32JG264EYAbbrjhhhtuuOFCuFlyN9xwww033HAjADfccMMNN9xww40A3HDDDTfccMMNNwJwww033HDDDTfcCMANN9xwww033PBvin9YB4AQshZFkY/jOGGMMaWUCoIgqKoqX7sa3YY4jpO+73tK6SoIgmCapv2exUAQQktVVdWyLJjjXlsVSpIkvVdOclVVNWt4w1FKOZ7n+fesD/7NPKA0TdNpmmaO46goipLrus41iuEURVFN0zRRSlee5zeCSr9Zl3Rd14XnecEwDMNxHPvaayBN02Qcx5nneWAYhn7N2txt27asx8EvP/ut1sYYY8KalIjXfPe6rmvWhZIDAHCKoqjvVRK2bdu267phXdd123+yLMu3QmE3/FlACFmvmXb9mwQgSZIky7KiaZqWteRdKKVUkiRBlmUly7LccRzv0g1BNozjOD4/Px+LoiiHYejYYSd5nudTStdrV6VCCC3n8/ncNE0zDMOwLMuyrivH8zyQZVnJ8zxzHMe5VtWmNE2z+hUtxnjmuNf2uIIgwCiKItu2rcPhcHiPEpVJkhRZlkXsYh5ZW2ClruvqcDgcLtGIaMPT09NLURR513Ub6YPbJth+h/UjwKIoioZhmPv9PrxWbnqe50Ucx0lRFMU0TSPP87xpmuYwDOOXL18+X3rNHY/H56IoGkop3lqwQgg5nueFX7dipZRylFJe13X10pXalmVBp9Mpapqm6ft+ZGSM4zgOsOJApuM47rUu4izLsqIoqrZtu2VZJoTQf+r5kOd5fusLcMN3euGTJEmSvu8H1p2Usm64KusUe9E99/8jAKfTKWIdubK6rutxHEeM8UIIoRBC0TAM1bIsq+/7EWOML90NLsuyIkmSNM/zNMuytKqqBiGETNPUCSFEURTpmgSAjZ+UZZlXVVUPw9DP84w2C1zTNMU0TatpmnaeZ3Tp9398fHxKkiRr27buuq4dhmFa15VuF4Fpmnpd11bXdf39/f3dNS3gtm3bOI6jJEmiLMvSpml6AMCqaZo2juNOEATB8zz/Egw1TdM0juMkjuNzXdcNpZRACIVtU/zqgsKyLIue53kcx1FJkpRLX0ZFUVSn0ylK0zRme6ERBAHatu2t67rKsixdkng8Pj5+jaIozrKsIISgjQDwPA9+qxkIIYSTJInXdd2Y53kGAPCX8I6x9R+VZVlUVdUMw9Avy7Juz6Lruso6JfbruuJLEkCO47iXl5djkiRJVVVVXdftOI7juq54239bq2hGDvC1jJAbbnhvnE6nqKqqgvVk7ud5npnnXTQMQzUMw2rbtv3w4cOHS3kF/hMBOJ/P8fl8jpIkidM0TYqiyNlFN1JKqSzLimmapud53rIsWytW/v7+/iLVupIkSeM4jvI8z5IkyfM8T5MkSaZpmg+Hw0FVVcPzvGFZFiSK4sWbBG3jp2ma5XmeFUVRNk3TjuM4cBxHIISSYRiGbdv2OI7zpd//8fHxKYqic5IkaVVVRVVV7TiOHcYYcxzHK4oi67puep7nYIwXjuOAaZrGtbpFJUmSFEWRxXH8y3chhJDdbhfKsqx0XedWVVVf4vJtmqZv27bOsiw7n89nhNAsCILILrvlVwSAmKapr+tKNU3T+77vLkkA2rZtoyiK8zxP4ziOkyRJ0jRNRVEUP378SG3b1odhGC/o8VmLoiiiKEriOI6GYei36AcAgP/196WUgq11s+/7HoQQWpZlvJUA5Hlenc/nc5IkMSPfddM0zbIsM6UUCIIg6LpueJ7nIIQWjuOgbdv2pRp2HY/H43b+5HmeN01Ts4NwWteVyrIsm6ZpOo5jz/OMWOc6cHd3t79dHzf8O+Pp6ekljuM4z/O8bdu6bduu7/uJ5/kVQijruq65rmsPwzBijPGXL1++XIIECN+4IFGWZVme52mapkkURdH5fD41TdPzPA/YwbNqmqYhhObNJa0oiuK6rvvWBgpt27ZJkkTn8znKsixNkiRP0zQ+nU7nrS49AIBy3Gv8UxQv6/0ex3GM4ziJoihKX5ElryykXJZl2SwgwzC0/X6/W9eVbs1SgiAI3+qOb9u2K8uyTJIkPZ1O5yzL0s0djjFeWfhBtG3bQQhtB7L48vKif/78+cOlF+TxeDynaVpkWVbmeV6kaRp9/fr1RdM0xXVdlxBC2Zq4yHiU0nVd13VZFtL3fX8+n8/gFfy6rvSb36MAAHA4HHZBECBCCNlixJdaB8/Pz8c0TZM0TZMsy/LT6XTKsiwLwzBECBGMMeW+0SW8FRhjghBaxnEcsizLTqfTmbndAcdxPPjG/79xgXVdqeu6riRJ8jzPaNOpvAVVVRV5nmfn8znJsiyO4ziu67rZmgMBAIBlWRZCCPE8L2iapp3P5+QS668oijJN0zyO4yRN05h5Q9KNaFEGwzD0/X5/wBhTQRCAKIqS4zjWpRq4MBdstq4rYfv93TvhpWmaIoSWf9X4f2b8K+a+KArKvJ9RlmVpnudlXddV13UtIWTVdV21LMsZhqFnxJsCAMCPP/74w8UIQBRFSV3XNduI2Xb5h2Ho27ZtUkpB3/d9lmXZ8Xg8QwglXdfVqqrMNE3zt7rC67puyrKs0zTNoig6s8moEEIzs3JF4RXwGt2aiqIo67qu0zQtWBjkXNd1xS58n+d5oe/7NkmSEmNM2PvrwzB0ZVmWb9UDIITQsizLOI5TXdcV68G+BEFgiaKochy3Nk3TRVGU8DzPSZIkm6Zp9n3fXWMukiTJyrIsmqYpyrLMiqKoJEkSIYQ8u5gvOqaqqoqiKKphGJqmaRqlFDiOY1iWZfE8D7cBCSGUXUSmJEmqIAgCz/MX84BEUXRmHqg4iqLkdDod0zTNHcexwjDc2bZtqKqqXNgDBQAAPACAl2VZchzHYj9fMcZk+wUIIb+uK4miKFUURVFVVZUkSZYkCX4jmPxnrf+yruu6aZq6qqoiiqJTWZa1bdu2rusaAIDr+77N87yQJEnQNM1wHKfr+76/xAQ0TdM2TVN/Q4JP67oS3/d9Xdd1AAAtiqJKkiTHGBNBEKCmabKqqsYlzh+O+yX8ETdNUzMPJ28YhnFNncmvz4CvX78+FUVRLssyb+PvdrvdvyLU8fj4+DJN08C9Cn/lMAyDawtAx3Ec+76ffut8YWEgcA0hOEJo+fr16+O3c2/btun7fnjtuW+aZuy6rknTND+fz+coiqJhGDoWAgV1XXdlWdbzPE8cx1Ge5wHTQGlvFeT/QgC6rmvbtm2rqmqKosjruu7u7+93Dw8PH03TdHieB03TtJIkwaenp3Nd10VZlrZlWW3XdW++hAghGGOMEUJTURRllmVVGIbuPM/at/0KKKXgGspIjPGyLAuapmmsqqosy7LZ7Xbefr8/uK7rQQjFrutqAMBjHMcZY2MIIbQwF/2bLwGO4wCEkJckSbRt29Q0TdvtdiHrxrfWdV18/fr156qqmiAIxmmaZoQQGsdxvCQpyrIsf+WCRZHneZ5lWaKqqrL1yt48MZcEAADyPA9YuH8FAADDMMyPHz/+IEmSsMWhRVHkAQBQ0zTF8zzX933/UpkR5/M5zvO8yF/dYPn5fD4lSZK5ruvc39/fHw6Hw36/3wdB4Luue7FD6O7uTjidTprjOA6llDBxGyCErN+u9WEYupeXlxdN09S7u7v73W63cxzH0XXdeuvBPM/zPAzD2LZt33VdXZZl7TiO9eXLly+WZTnMOkr6vp+apummaRrneZ4RQjNCCL01DDCOYz8MQz8MQ1cURYEQmj9//vzp7u7uTlVVgxBCdF0vIITgfD5nRVHkmvYqB7jE+cNxryGv8/l8TtM0QwghCCFkoaWr6Ex+ww38nCTJOYqibBiGfht/XVciiqLwXqJHhNDy+Pj4NY7jlM3tqqqqNk3T9PHjx4drWcVPT0/PeZ5XGGP0j34HQshnWWbudrvwkvqn5+fnI9M7JX3fDzzPC77vuwghIggCf83MH47j8LIsmBCytG3b9n0/+r4fsK6YsGmaJk3T9HQ6nXmeh7IsS4ZhGHVd2xchAG3bcn3fD9sGrKqqVlVV8X0/8H0/uL+/v+N5Hp5Op/M4jr2u60XTNM00Tf04juM0TWNRFNUbmRmEEEJBEGTXdW1ZlmVd142Xl5enX4vAroHXoQVBURTJtm1HURTFtm17t9uF+/1+hxAihBAkSZKwuWFZatRFTGHf9904jnXHcZxpmmbDMAxJkhQ2F8o8zzMhZJFlWa3rul3XlVBKV0LIL1biJfDy8nIsXlFWVVWwFDj0+fPnwzRNw7WaR1FKMfvOGGOMN5Y7TdPUtu2EEEIAAKAoimrbtiWKoiXLsm4YhnEp8lOWZVkURVWWZZXnecbU5taXL18+e54Xep5ny7IsYYxxWZaVpmn6pYhoGIYBAICapqlvLkiWfkf7vh++jcvv9/swDMOd7/vh4XAIfN/33irGo68xGLqu64oQmjHGq6ZphmVZzv39/d08z1Nd1zX7JvP2vQghdJ7nNxOAeZ4XRmbnYRgGpjeyPc8LD4fDflmWWRAEfp7nua7rrq7rNgzDYZqmeVmW+a0kOEmStG3bpizLzftRGYahsbCnPgxDe00CwDohNnEcF3Ecn7IsyxVFURBCiyRJsm3b9nsQgM0SjuM4jqIoLoqiIISsjuM4AAAqSZJ0DQKw6Z/SNM3GcZz+gZFAeZ4XPM9zKaXENE3zUvuv67q6qqpyE8CrqqpSShf4Ch5jvF7LEyBJkqyqqqKqqhYEgb+lN9u27UuSJDRNU8uyLD8/Pz8VRZGbpmk7jtM1TdNXVVW/hQgJ7OXxPM/zNE3zOI4DQgjtdjvfNE3LsiwrDMMQAMA1TVPJsqyIoqh0XdcwCwAhhJZfC7V+L0zT1E3TdPb7/QQhhJTSdWPil3Tx/iPYtm05jmOP4xiKoihijImqqqrned66rmtVVXld103bti0LQ8g8zwuCIMBLPV8QBDtCyKooijQMwzgMwzRN01SWZTWO49Q0Tdn3fSdJkgAAELbFKcvyRdzRWZYVWZblZVnmzBLOsiyrfN93mfCx/weei0t4YPCyLARjTDDGGELII4TwTz/99L9ZWiDdUhDDMAzneV4YIZAvkR6Tpmla13XXtm1T13WVZVkmiqJwd3f3IAiCMk3TEEXRlOd5adu25XletyzL8pe//OXHS7z/p0+fPqiqqrZt21JKVwAAGMdxStM06bouZjrETNd1PQiCfcDg+/7uErHA7YBl3h1AKaWEkGUcx4Htw2kYhmHbjwAAnud5IAgCt6VrvnVslm2Ml2Uhuq4rkiRJsizLjuPYhBDStu2gaVqlqqo8DMNICFkBuExDU4zxMs/zMk3T3LZt9/z8fAzD0Hdd1x/HEbHY69XAyBchhCxd1/UvLy9Hy7IM0zTteZ7RPM/Ltc9AJn6NmBg1SdM0PR6PLxhjvK7rJ8uyDNd1x2t4YL/VPzVNU1NK6a+jWkz4qmKMsaqqSpZl2SVSsRFCHNPgzOM4DnEcJ6IoChBCgWmQ1mVZMEJo3u/3u0uHoO/v7+U8z+1xHEdJkoR5nommacp+vw8hhFKWZcmyLEtVVWXbth3GeEIILWyvvEn/JLCJXQgheF3XZcs7l2VZVRRF0XVdMwxDn+d5EQRBlCRJEEWRf02NX/G6rps+502T4HmeizFeBEHgDcMwzudzwsICVFEUHrzudHCtxW+apnk4HA4QQjEIAm9ZFrIsC+66rsuyLE3TNE/T9FzXdReGoafrr8anKIqKqqrKJZ5ht9t5AABKCMFlWdbH4/FUVVUxvp5AU9/3A6WUbnFRTdMUTdOUS6iwCSEkz/OsLMsiy7KyLMucbQRxv9/vFEVRfy2243mep5RexPvAvBh4WRayruva9/1wOp1eBEGQDMPQFUWRmeVdR1F0FkVRlGVZ1XXddF3XfisB6Pt+mOe57/u+Z/Ufxt1uFzJhXowxXiVJkgzDMHzf99iFAERRFD99+vTxQl6AX8IZTJBbMlJWFkWRr+tKgyAIHMdxfN93d7udfykFvCiKvCAIoiAIkqZpuqZpcpqmKaUUGIZhzPM8V1VVrutKwzD02DdRRVGUL3EgUko5COGrGOI1FIQRQmiapjFN04R5GqZfrb+LegCZlkIQBEEQRVHkeZ7DGNNrhLx+gwBxTN8KeJ6HkiQJvyJlV32G0+kUFUWRMQ1YmqZpcjqdjlEUJa7rWtshf425QAhxm/6pqqry69evz+u6Lt96V7ej5+Hh4eB5nnsJo/MbC5wTBEHWNE1lWVZuHMfJ8Xh8ppQSZpygcRzHYRjGa4RB9vv9gWNZNRzHcYqiyKIoyn3fd9vUb17BrS7WJe7DjQBwjPFTjDFheeeQ53lBFEWJWcSYCZGAIAgCAABcUHy9XYA7juPAsizTRko4jltFURREURQEYTN6r1MZKQzDkFLKZVnGjeOYJ0mSsjB4niRJXBRFZVmWudvtdrZtOywj0LpkLApCCDHGaJqmoW3b+uXl5cjCLJPjOPb9/f297/uB4zi2qqq6oigXYaOPj49PaZpmWZblVVUVWZYl8zyjjx8/ftjtdiEh5D+xPAAAP47jGEVRxHEcCcPwTUycHfiEsdoFIbSYpqk9PDx8dF3XV1VVQghhVVXPT09PL2VZlq7ruuM4Dn3fvzklbxiGbZrHvu/7aZqmqqrKPM9zlmhA13UlsixLWxaGLMuSpmnaNaoi1nXddF3XtG3bNk1T1XXdOI5ju67rOY7jvGbDOd6lxjVN0zJNU3ccx2rb1ru/v19Op9Px69evj4ZhqNM0LTzPg/v7+w9hGIaappmqqiqXKkYlCAJkpE42TVPJ87wuiiKHEIpN0zSEENp1XVsURc7SJMVLeZ8YAZJEUVQMw1Adx3F+/PFHXpZlyTRNXZZlSRAE4ZoXsKZpqqIokqIoqmVZ1sePHz9tgmdRFKVrFv1K05Rs6cdFUWxZP6c4jhPHccz7+/t7x3FcXdd1SZKUS5+/zH7hWXxb9jzP3rJ9vvUQrevKaZqmQgjhJpq91DMEQWD3fe+O4zgBANbNK/j8/PyEEFqGYdgMMCLLsnjp/b7b7cLdbhdmWZaxsdrsNRUnfY1MFllZlg3bc9IrP+V5COGb9sB/uajZoU8ppRwAgGKMOcJugysxYcAEEWRdV4wxXiGEwuu7vgbqr7URxnEcsyzLNjdYURQ5SwVLp2ladrudHwTB3vf9MAgC3/M8x3GciypSGdEReZ4XdV1X9/v9DmOM+r4fxnEc8jxPeZ7nFUVRHMe5SBZAlmUrE/v9kv5YVVUdhqHveZ5PCFmnacLM4gc8z8NhGLrn5+d5GAYfAEAdx3Heooxn62klhKyUUuq6rnt3d3e32+0Oh8Nhx0Qv9TRNY57n6fB6Y8/LslxECIkQQhhjNM8zmqZpEgSBl2VZtm3bkWVZXtd1HYZhYKmiMTuoTdu2u67rukseCHmeV1VVNV3XtX3fd8wlChjpNB3HsT3Pcy8Zk2zbthvHcZ6maWYEdCKErJqmKSwTgLBU1UwQBAAhFFRVVTVNM9I0zd8qxGReB03XdcOyLK9pmuF4PB67rus1TdN4nqdN03RMJDXs9/sDhFCCEIrbs7zRA+m0beui/4+9N1uOJDmyBd3MfN/3iACQQLHIK5zht80n3j+Y9x6R7iYrEwlEuIfv+2rm8wCzvGB2sbsSESjemYaKQJKsXDzC3Mz0qOrRo/M8IYT4eZ4ZE9wKwzCgolPvZoqiyJZl2b7v9xzHEcMwTCo8ZTiO41iWZb7Hc9d1/b/LsvwjvfdOTAOlLMvS8zz7cDjcBkHgh2G4o7yTd1kH27aNtm2dbdtWz/PcX+NWUTVa0XVd59qB183NzR3j3rDgV5IkKUmS+PHx8bFpmoYQstDzYF+beP3qO4K6rsvj8RhRHlKeJMk5juOYAhXfNE1T0zSdlqgv8j/89w8HACC6MVbqgBfqkNd5njEhBBNCCEII0oUCEELuWm1hhNYUMMbMKXAQQp46f8TU4d7DTqdTTA/C+Xw+R1QTIFMURby/v791Xdd3Xdf1PM+jhPD9tecCVFVViaIoBEHgQwiBbdvusixz13VdHMfn4/F4XJblRDXZDcMw2jRNU9/330wQatt2oB0gZZqm59PpdJqmaSaEkCiKjs/PzxhCCLuuGwAAhCLSwrZti6WIkyTJL3FIgiCIkiTJuq4bYRgePM9bPc9zd7tdsN/vD5qmqfM8T6IoijzPC5QohKlGwDXA6EZZ9+s0TYvjOM79/f0ffN93eJ4XCSFbXdcNhJB7enqKuq7rKAdmYfMarmVd1zV937cvgmBt0zRNaxiGZpqmbRiGoSiK8apV8FoAoKEtgCXd/CdVVZX9fv9gmqaNMSZlWWbH4/H49PR0RAjxuq5rfd+3fd+3HMddBABM09R1XTcdx3Go892SJEnLsqzKsqwQQpB2otjrum40YpYoTUC6xhpotK5H+QATAAAoiqIpiqL+Hv3g9/f3n+h50qZpGiGEQJZl7Rokz39kRVH8z3Vd72mJpz4ej8ckSTLXdS3f93eu63qu6/qO43i+77vvBQD0FzPnecaKooz/KEgQBEE0TdM2TdNQFEW91vMxxpjyWnha+lZ0XTe6rmvquk4oMN4oH/1dgl+WUaXZmDPV44mzLMsEQRDu7u5udrvdzrZtx7Is6xrBJ88crCAIvCRJvCRJAoQQvHTkzcuyLMs0TXPf99NLZvaF9MfzPHrJRAg81Srnr/QiCIv+l2XBVArxmySqIAjvQgikqmdUgr8qmCCQbdvWfr8/eJ7n+77v0hSsZVmWfc3DgDEmf/vb3/4ax3HCyHamaZq+7/uMAIIQQtM0DVmWFcMw9HRTLkyo6ALUidd1xeM4LsMwTDQVK9Z13b4exEIIweu6zlEUJdu2AUEQeIzxTDNC+EIHYPu+323bhlVVVQghm2EYmu/7fhiGu2EYOnLtmtPfZ57+QyaKdsIEvu970zTNT09PT0VRaJSgOL8CyVftUmnbtmnbtqPev1qWBe/3e9uyLN00TcN1XfPabUnzPI+UgT9UVVUDAOB+v7+5u7u79TzPW9cVJ0kiE0LwL7/88rVpmmZ4sZEOibo0BRt0XfdN9pfneUFVVZOeBUJr8uh4PB5FUeRp94dKL+qrRGK0/fMcRdG57/uW53nkOI5H09TS79GLfzgcdggh0Pf9wPM8kGVZe6+ZIzTzYNCyKq8oiuS6rgMh5Nq27dI0jSkdQpAkSVQURVIURX0P+fG6ruuiKPLz+Xxq27ZlQmPfnclN0zSN3RHXjMKZFDfNgORUATWqqqoNwzA4HA573/cd0zR1TdPU99aiiaIoPh6PT6wdbv5F8gAAIABJREFUl2ZgfNd1/f1+v/N937/GfuQ5juMkSRIYAYjVePq+H6ZpGvq+74uiqJZlGfu+74Zh6IdhGBVFkek0MpHnedF1XfMKBzDvuq6hmYhvCmiEEI4RHtq27ZIkSa6NiPu+H1kfNN2Mha7r6qdPnz7tdru9ZVk2I/vRFshpGIaeOoiLgUAURRGTwaWtN9g0TeP+/v7Otm0PIQTKsmxkWZapFgKmdbFt27aLHKMoiqKiKLJhGNrt7e0+DEMPAACog9uYIl+WZeeiKKq7u7sbnudFWZZVSZIUURQFJtt7AQBaWO2fDf+hTGBE6/Fl27b9MAzjPM+zIAgIAAC/KxVeAoK+/W9GQluWZcIYr6qqahzHfWvLe50xu/YlcDwe46ZpWjoJry3LslFVVbJt29J13TRN0zQM4+rpYKpu+EJDX5ZZFEVR0zTdtm3ncDjcjOM403OvCoKAqBooZtyha3yGh4eHewAA4HleNAxD9zyvpW1+a9d1Hb0QS9u2HV3XDcuyNEpG1K5w9xR1XTM1zlMcx2dVVWXaeaK0bfvuEwjneV6+fv36JU3TvOu6DgAADMPQx3EcrtXp8SuR9/+l6/pk27Y1z/NOURTFcRzr+fn5dD6fU/puMcYYs1kM7wEAhmEYm6bpsizLHh8fn6Zpml5zDehdAA+Hw0EURcW27b6u6/YajjiKojVJkiyKojiKonNRFOc4js/TNOHD4RD4vr/3fT8Iw9Cn/Kt3yYLQoHee53moqqqsqqoOgsA9HA43TBfA930vCILwp59++ukaz+Rp/YuTJElWFIUxIdW2bbuqqmrDMAo6gW0py7Km43GXIAg8SZJkmraVL22FG4ahPx6PpyRJznVd16+ACaDoj6RpmrLOQ4QQf83JSOu6zsuyzLT+OWCMsed5rmmaFsYY53meEkI2OhQJqKqqOI7jzvO8XoMUMo7j2HVdT1UAH4dhGD99+nQrSZLY9z0T/ZmWZVlo65/AyiKXZl9ub2/lPM+ddV1X0zSNeZ4x7UbAHMcBnudBURRVlmXnbds2WZa129vbG1mWVdd1TcMw7EtKEDQDUzAZ3DiOYwAA8DwvWNd1kyQpbZqmz/M8L8uyGIZh9H3fl2VZliRJFARBvPQi4HkeUaVJUVVVsW3bF0bg/2qDm1iUzHTpeSZNecWyFI3+267rurZt63Ech91uF6iqqtMygP7Okejr0cuEjcSmPf8rzfYQxge5+sMpmKVMfHGe57nruo7KhJ9EURSDIPApL8Gks0nsKzz32/OnaZrjOE4sy9J93w9plmfl3tkeHx8f6SyEc1VVFYQQmKZpL8uCJUmSbm9vb97juTc3N9I4jntBECTTNKssy+Rt2yAAYDufzylCCCmKok3TNL/XOrzqdmBj1/nvkf13EzKvVnZelmWepmlsmqZJkiQ6nU6RKIri/f39PgiC4KXb1vODIAh3u134XiPZt20DCCFBkiTZcRxbEARkWZYTBMHO932P/vjX6jr6BgAoEtQURdFVVTVs23ao7OyRXs7luq64KIoiSZKURouOqqovuq2qenEtJsuyoizL4ng8xmmaxvM8j9M0LRBChDGeqEBDttvt9qIoSp7n2dcEANThbYQQvCzLTAeugCiKjvTSJxBCjhDCUaU+axzHVRRFsSgK61IAsG0bx/SOCCFrnue5JEk8fZ6IMV6pOl9hGIamKIomSZIsy7J4jTbAm5ubG1VVFTbxj0ruYo7jYF3XZZqmxSviJxYEQTocDocgCFzP8/xLmcG01DRVVdWdTqd4nud527aNAZ5lWaY8z7Moik6iKIqO47D9p2madvH+o1LEMgXAep7nTZ7nhaqq+jAME21BLIqiKKgMp07/jiSK4lVq0GwELq39t1VVVRzHQU3TDNaOq+v6u9SiKdCGPM/zqqrKVBo4PR6PKMuyHACwtW3bV1VV0Bq8JsuySEuHVynLff78+cvxeDzGcZx0XVd3XddT5n+R53m6bRtHU7Eeq4Oapmld6fyzcwhpeVPkOO7d5rD/SubnmKZpliRJejwe4yRJIgAA/PTp06qqqvreGYiff/75D4ZhGEmSpAghRKttGw0yREpOBtwVOy9eG+3A0D3P8yRJkn8NXAIANjaOWpIk5VqyxLT2DwEAkE44dajGhsc4X57n+WEYhtfmfH0H/ttxHPtpmiZCCBZFUdY0TXMcxz4cDrvdbre7dunvGwCgjGajqirLcRx3HMc+SZL0l19++YWyoHHf95MkSQKVx3Vc17WoKMrFi8JQ9rIsQ5Ikyel0SgRBAOu6rmmalkmSVHQugTXP83RtYQzW48qIkBhj8vT0dGRO+fv+1/1+v5imadE0+cU1YFEURaptrjqO4w/DMJ7P57Qsy4YKrZBxHCdZlqUgCEJGhNE0Tb+GQpnneQ6VvlwYEQ5jjKuqKvM8zwEAhDpk1hJMEELItm33GilBCCEviqJgGIZs27Z5Op2iL1++fNF1PYUQommaprZte57n4X6/3zmO4zqOY9m2beq6fnFKXFVVTdd1zTAM3TRNxzCMOo7jBGO8yLKsEkLWuq7raZrm3W63M03TMgxDpwf0Kin5cRzHvu8H5viqqmoNw1D0lwEJmiRJGi1HXN1EUVR1XVcNw9Adx/Hath2en59PVVU1kiSJ27ZtjCPgeZ7lOI6raZpO2fsXf6amadqyLMssy9Ioio5pmqZd1zVN07TjOC6apilBEARhGIau6/pBELivdRMuNdu2TVmWJVmWZcMw9Pv7+zuEEJJlWQIACO/dBkjHHxe0/nw+nU6R67rWPM+YlmYI984WBIEv0ClrTIhHURQFQojo+zZUVVXe6dkezexCFoD9GgDgeV6wbdsKgsC9Vh2eZr41z/NsjuPwNE3YMAzNdV3XcRx3t9t5+/3+8B51/9cBMM0A12maZp8/f/4CAICapmm0HR69hxzxt00dhmHIiD10/OcmiqJc13VDR7Oi3W7nmaZpB0HgB0EQvGAA17nGwgBGweR5pGmatt/vWe0ZsOhc0zQNISRS4uFVyYAv/EJBlGVZYg6Nzn3nX8qSPKRchG0cx1HTNI2260F4BUUSx3Gssixtz/P6eZ4XhBAoiiJv27ab53lFCEHaeui6rhuEYei/7E/HvrIjEDiOE9g7palIiBASNE0zaK0Y0bZQcq1uUNM0Teulp66b53nleZ4viiLvuq4nhBBKyDIdx3F93/eDIAgoQ9m9BgAKwzAsiqKg8rMDxniN4/hclmXLcVzLZFD3+/2BpQVd13WoHsNVLgaa/sYcx63zPM88z/OM+U/T/9p7RSAv1ALDchynm+d5AQCAPM9zKn7SIIQ4URTF3W4Xuq7rhWHoO47j2LZtXeNiejUMa2rbtj2dThGVxFbv7u4Cx3Fs0zRteiEHQRCE10yF0pHKpud5zrquq2EYFm29NDzPs96zCyDPc65pmraqqqaqqjLLslRVVSkIgtAwDIN2PMjc72CvwDwQRVE0TbMXBAGqqqrvdrvwvYYiUU4X1HVdo/fdf/gzGGOO53lkGIZxzRHQnueBpml8OnlPxxgTSZJENmvkWuPe/6sMMCuDULL1pqoq/yrjsr2HAiP/3SW4m6Zpocx7wTAMfRiGgdZ9ACUGqRSBBbvdbnd7e3t7JRSmqKqqOY7jAQAge+brNWI60JQMddVIyHVdJ8/zou97jxCyeZ7n08gUMeEJhBC3riuhuuSC67qeruvmNdpRDMMwgiDw13VdIIRAURTZNE2LMqzJtm2cKIoSTZM5lHwYvvOQCo5O59Nt23bWdcVBEPiKoqimaWqSJMmmaV4lDXd3d3czDEPHprApiiKxljCMMREEgbXn6I7j2J7n+bvd7mpOACGEaOZr4l5Y51BVVW0Yhp7jOFaXli3LMkzTtMMwDCgT92qjmIUXk5gmuCzLsq7rGk11m6Zpmu/1noMg8KZpGikLHwiCING5FCOLPgVBEGRZVmzbNizLcsIwDIMg8NGv3dY/noFRKJdDVhRF3e12IYQQGlSP3DAM07Is0zAMZ7fbeQ8PDw/XXoOHh4d7evnLNAgCiqIoQRB413Q4v+bY6FyP9QVv9cv9/f3+Zc5V4BuG4b4H8e4/AwG2bVvPz8/HcRwHFom+ZzcC24PvVV//r+ynn356kCRJHoahJ4QQQRB4GgzYv8fzNU2TKbFasSzL/vnnn39CCEFd1w1FURRJkqT3EMDjv3NC+qdPn255nuclSVLGcWyHYRiXZSEQQo7neUFRFFnXddP3fffh4eH+Wh/E9/2g7/sJIQRN0zQpyx28TtFDCKGu6waNwK7aBaAoirLb7QKO44imaeqyLCshhHuZNvsCAqg06EojdGSapnlNUsjhcNhzL/KyEh1A0s/zPLHvz/O8QOvA1m63+12QaRiGYd/3AwBg03VdoYQk0XEcx/d975oKZQ8PDz9BCCGT3J2maRjHcaGkMyTLsqjruqppmuV53lX3H1t/qooJJEmSLctqxnGcX60/T2dWWK7rug8PD/fXPJRhGAZlWVbruo4QQn5d14lKU3uu67rvPZL27u7uFgDACYIgGIah9X0/LMsyUfIrJ4oiokN6NMuyvmUCr3X+DMMwbdu253meqc4BUBRFYdkJ0zRN27bt9yRB/vzzzz9lWWZRmVlA+U7vqgFAJZAhHXGs3d7e7k3TtFn9d7/f+9fkO/1Wey/S4f+u9p4g77fsf9d1nWEYBpoRduhEVN15sXcBIv9wkgYdi9gOwzCxCEAURZ6SL4z3moxFW2B6OhDlP5QJFEVR3xMlNtTmeV5pBgAKwgvhHgDA0fG/C0XF+nuswzAMQ57nef8yDWh+HaXquq7Ri9D6PTfo6XQ6tW3bLsuCKQnSfC9xkuPxGLFyFCFkwRhzCCEgCIKo67rKQOB7fdckSbK6rqu2bb/tA7r+UNM01TRN872c8TzP89evX5/KsqwJISuEEFmWZd7f33+6Btnzt1iWZUXbtnXXdQMVOdowxhwF4LKqqtp+v99dI/J/bYQQ/O///u+/vJrJDthUUMdxrDAMd+8lA/7Ptn/5l3/5f6IoOkZRlGKMZ0EQpDAMvf1+f/OXv/zl/+Q+7L+FPT4+fs3zvFjXdd62DciyLNMyxLuA3t80SosBgP+/Hr7/ne01+ee/2/rT+vNICOEQQsCyLOPaTue/en7btiMd1MIhhMDvlRKkmQAMAICUnPRPewev9h//npr0r4KPhM6EB4IgSP+stPDvaawPvWmaCmOMWQqallk87sP+29iyLHNVVS3HccD3/XfN/ID30vT/sA/7sA/7sB8DW0VRVNu2YQgh8jzP+z0A14f997UPAPBhH/ZhH/ZhH/bf0D5S+h/2YR/2YR/2YR8A4MM+7MM+7MM+7MM+AMCHfdiHfdiHfdiHfQCAD/uwD/uwD/uwD/sAAB/2YR/2YR/2YR/2AQA+7MM+7MM+7MM+7P8r9psnXGVZlmM2Go8aQoi3bdv6EAj6sPcyOpN+Wtd1mecZi6Io8DzPK4oif6zOh33Yh33Y2+0/1QFomqbJ87yo67oZx3EkhKyvf18QBEFRFIUNaHlvzezfy5IkyYZh6Jdlmdd1xUwFThAEUdM09T1laP8zG4Zh5Dhua5pm4DhuhRBCx3HcawOw0+l0ovr/QFEUJQzD8PcCeUxymBCyQQjBQg1jTNhQII7jgCRJApsJHgSBf63nN03T1HVdretKIITcsix4WZaV47iN4zgOQggghIKqqpIkScp7KwNmWVZ2XdfQQT2EzkqQNE1Tfd/3r/28eZ7nJEmSYRgmOoznH67v+XzOx3HsuJfhSdK19gnb5+z/r+uK6Rha8HsGG03TNNM0rQAADiEECSF/N/1y2zbONE392mI9TdO0TdO0bF2/VwLM87xkEzKvLQmeZVk5jmO7ritRFEXyPC/4ZwV48zzPZVlWHMdxvzaTIUmSZJ7nBUIIdF3Xr+F/kiRJx3Hst23bBEEQ/zPJ7zzPi2maRvpnhff0C03TtNM0LUweHwDA6bp+8d77hxmA5+fnY57n2cuY6qqZpmlY13V5/WdEURRlWVY1TdOrqiodx/Hu7u7eZYAExpg8PT0du65rMMZE13XFNE3n2lr8X758+XI+n9Ou6/p1XWd6+XMAACDLsqRpmlYUReE4zu8yuappmqYoipKOyZ0BANs8zysdVoIcx8lubm4OhmFcPCmOEIL/+te//i1Jkmwcx27bNqBpml4URWFZlhUEQfCeymR//etf/5amaVJVVYMxxgghRKfELhzH4XVdN0EQEIQQiKIoSpIk67puDsMw3t/fXzyV73w+53Ecn8qyLNZ1XSGEcF3XlU4o3Og+gIIgCDzPS6qqKkVRmGEYBu8Bfh8fH5/yPM/btq2oJDKGECJJkiRVVbUsy3LXde0gCK4ypS1N0/x0OkVVVZXjOA509sSvru/pdDodj8e4bduaEEJkWVbquq5ubm5u3roWj4+PTxTsLGy9mdGBXBAhxPM8z8uyLB0Oh/21x4LTdSjyPM+apmm3bVu3bQMQQkQIIa+BCcdxnCzLomma1t3d3d01HOXT09NzmqZZ3/cdHUcuN01TPzw8PMzzPH39+vW5bdsGY4whhLxt2+anT5/urjEngu63rGmaBmOMFUWRi6IoL3mnb7Xj8RhlWZb2fd9xHMdJkiR7nud8+vTpnuM47l//9V///dXMCKjruh6GYXiJZv7nz58fkyRJ+75v6Z6WiqKobm5uDt8Drefn56coitJhGPpt24ggCFJRFNVPP/10f82ZHXQuSj0Mw8B8EQveZVkWVFVVdF033zqpkf8HTvDxfD6fsyxLi6Io67qu+77vxnGcWMYAQghlWZYVRVE1TTP6vrfp9LDx/v7+p2ujxs+fP3+m89lLQghWVVUPgmDgOG7zPM+91mKfTqfz+Xw+lWVZT9M0E0JWjPEGIUSqqkqqquqO41j0uy7vOZnsdDqd0jTNyrIs6frPdDIbHsdxkiRJGIbB4ziO+/Of/3wxAPjy5cuXKIriKIrisiwrhBCks+gN27arPM8L0zT19xiTeT6fkyRJsuPxeMrzPJvneRVFkacAYCaEYDYRkuM4RCcGqq7rujRCkC5B4BhjnKZpfD6f4/P5fJ6maYQQIgYA2EwG6oAFCgbVtm3taZrmv/zlL//Hlff7YxzHER1MVPR931MgwquqKquqqrmua9Z13Q7DMF8DAGVZlp7P5yiO46Tv+xYhJDCQqyiKzDIB9D3FSZJEaZqm67quqqrqy7KMCCHhLc7iy5cvX06nU1yWZT7P80wI+TtHS6dxMgCAZFlWqqpqPn36dHMN8Ps6uj6dTscsy85lWdbrui7btgGEENq2jXz/uRRFkV3Xddd1Xf/4xz/+fGH0mZxOp/h8Pkd1XdcYY6zrur4sy4wQgvM8z1EUHdM0LdZ1XQVBEIdhcLZt2/70pz/98dJnx3F8jqIoqqqqoO9Um6ZpBACg3xMAnM/nOIqiUxzH56qqKkLIZhiGNk3TBCFE0zQtcRyzfdohhJDneQ4hBAuCwL8lI/j8/HyM4zg6n89xURQlIQRrmqZN0zQjhMBrAJAkSXI8Hs9RFEVlWZYAAKIoirosyyQIAvzDH/7w8xXOYp6maVIURVXXdU0D0uUVIAaiKIqqqqqaphlN0zS3t7e3P1oa5X9lIZ6Px+M5TdMoTdMkTdMsz/O8qqp6Xdd5XdeNHkioKIqoaZrhuq47DEM/juOEMcbbtoGff/75D1eOyuMoiuI0Tc/zPK+u69oQQiJJknQtANC2bVvXdRXHcRrH8YmN4yWEEFEUBVVVddM07WEY+nmeF4zxhhCC7zEn++np6fl0OkVpmqZlWRZ1XTfDMIwsMmqapjMMQ+M4jlNVVU2SJLnEAWZZVhRFUaVpmsdxHJ/P5xgAACzLsg3DMJumMRRF0U3T1E3TtNq2bW5vb2+vBfTGcRynaRqqqqqPx+OxbduW53l+mqaRAh8MANi2bQM0ApQdx3EIIURVVbVpGufS709LXlkURac8zwsIIcQYL8uyLOu6Yo7jOEEQeDquWdV13QjDcBIEQTidTqdrTQj88uXLI7uM4jg+53meF0VRLMsyCoIg0n1otm1r+b4/IoS+za2/xPF1XdfWdV2dz+coSZJEVVWV4zhO0zStaZqWXax0YGaVpmn69PT03Pd9HwRBIAiCaJqm3TRNaxiG/lufXRTFdj6f0/P5HJ3P53PbtgPHcZim2QFNwSMK/BCEkDcMQ5/neeF5Hl4TANR1Xed5nj0/P0dJkqTzPI8AAMDzvLBtG2ZAkO5FqGmaTtPQvGEYRhiGwSXPrqqqSpIkiaIoWpZl8X3f53meV1VVa9u2pU467vu+1zRN37aNyLKspGmaXlISquu6a9u2zrIsOR6Px2maJvZsXdfNPM/L32sQVpqmRZqmGbvv13VdHcfxEUK8aZr6PM/z+Xxm5zSTJEnCGK90WqjxFgBQVVVdlmURRdH5fD6fhmFY6HpCnucFWZYf2fhx9p6yLEtOp9NpnufZ8zwfQshrmqYZhpH7vu9e8P3z0+l0SpLknGVZ/lKVrNt1XSdWgkIIQVEUZTod1lqWZdq2jTw8PPz0I1la/jsUmEVRlGRZlrBNGEXRsWmaXlVVmRL+EK3PLF3X9U3TxCxFwTgCCCEkSZJ4e3t7e4UoOM6yLE3TNKPI6ziO40jrgpphGG1ZltU1amHbtmFCyLqu61wURZ7neckWs6oqAgDIbduu1nUdt20jPM9DRVFE3/e9a06pe35+fqKRQBzH8TnLsizP86zv+5amweE4jvPhcDh4nucuyzINwzBd8sx1Xdd5nqdhGIa6rpsoik7TNM2maZqWZTlVVVmGYehVVRm2bTfDMHTjOA5hGO6utPYbxngDAJBpmqY4jmP+xRBNufOvPuuc53lPkb+/rium6dmLnk8I2dZ1xX3fD8fjMaKgAwIAAIQQvCqVbDzPwzAMQxolTNM0zdfKQp3P5+T88vLPURRFx+PxxDISGOMGQpjR2nwIIYSqquq2bTeXAAD8YoRyHqY4jlPXda3dbhcuy7Ju2/aNADxN09D3/VDXdZfneVrXdW8Yhrmu67Ku6zKO4/IjAeM4jus4jkNd13UURXEcx/G6rmwcNwAAAJYAYAAsCIIdz/OCYRhGmqal7/tXcU4Y42Waprlpmi6O41Oaphn9DIhmPzEhhCOEbKIo8ofD4aDrurYsy0QnGF50Btd1nfq+H9M0Tbqu62VZlpdlmdu2bc7nc5KmaXo6nU55nuc3Nzc3tm2bwzCM8zxPF+5/8oJz16WqqqIsy1rXdXVd15UQgjHG2+/k/NOu69qmaeqqqvKvX78+r+u6CILAt21rd13XT9M0DcPQVFWVf/78+cmyLF1VVc2yLLvv+/5HASg90+uyLMs4jkMcx0lVVS0AYFMURVYURVZVVVFVVQuCwGPvaRiGIUmStGmahud53rZtq+/7tuu6/q0AIMuygjn/+MWSoiiysixLysPbOI7jWBbMcRw7CAKfBd4IIf5Hgm/+OyReNE1TlGWZJUmSPD8/H6dpGm9ubnZBEIS6ruv0EtoIIWtd101RFOn5fE6fn5+f6IHlZVmWVFVVXdd1FUVR3roZ5nleiqLIsyzLsxdLoyiKtm0jvu+74zhOL35rnq9w8AmtrROMMWE1oD/84Q8PEEJhHMehLMsiy7KC1qd5RkyJ4zi5VikgjuPzSwD+4vzjOI6Ox+OxaZpeURSR53m+bdueMuPXdV3xyxm9zAECADh60XIcxxGMMdE0TVmWZXl6enouyzIxTdOybdtt27alJZB5XVfCcRx3KQiAEHIQQm7bNoheTDgcDqFt2w7P8wJCiGfviR6+UZZlWdM0VZIkkZIDL/r+9DOAlyoDxwVB4FqWZQEAEM/zPCMFzvM8dV3XS5Ik0f8OIftLF9g8zzPb62maZufz+fz8/PyMMV5vb28PmqYZ0zRNVVWVZVk2siw30zTNNE2NL3UAtMyC13UlAICNpt1Z9h28clSEOoV1GIZJEAQky7Iky7LI87yo67r8g88G4CWkhuwdE0I4WZZFSsZdVFVV6QWIJUmSTdM0CSGY4zhCf65lAEIIEEKQRr8qxphACDnqaFeabib0v0MGUjiOA5c+m/47HCUcbhSAwrIsSxqUxefz+SyKoiTLsgwhFFh25Ar7H7DPQAh5eSkv74V9rHe3vu8Han3btl1d19W2bYSu/TiO4zTP89h13dC2bT+OYyMIAqAZxGkcx2kcx+lHAcC2bez9AZoNrtM0FdQXk6kpNCjhOY6DAABeFEVhmqa567puGIZpHMd5WZY3g7H0xZI4juMois6n0+k5y7IMQoh0XVfZi1iWZaWZgYaNzmZl0TRNf3MGgn8d/dM0SF0URZkkybnv+/7m5uZwf39/LwiCMk1TN89zs23bZpqmc3t7e6dpmi5JkvL4+Ph0Pp9jRVFUwzB00zSbPM+L29vbNwOA0+l0zPO8KMuyzPO8SJLknOd5pWmaMk3TQlvDrgIAXkeC27ZtLOUriqJ6c3NzmKZp1nXd5HkeJUmS5Xle2bbtDsMwjuM4XuPZwzAMWZalWZZl5/M5jaIoPh6PT33fT4fDIfQ8z5ckSUzTNP369eszbcvc3mOiIwAAOI7jWJblZFmWlWVZPj09RUVRVEEQBLROS17XaH/00H0HAHgIIS8IAkIIIULIhhBCruvufN+3eJ4X6cWExxebWO3PNE1H13Xz2msgSZJyc3PzSdM0TRAEtG0bIISs9JIZEUK867qOYRi6oijqpc+LouhMnXuZ53keRVE0z/N8e3t7e3d3d2uapj3P81xVVVnXdU15CIqiKLIgCNIVvzqhXRiQ1t0RQkh49a7BP9gyECGEfhT0+77PR1Gk6LquB0EQqKqqIYTgtm3bly9fPhNC8N3d3QPHcXie55XjOM40TVNRFEkQBEGSpKuRrmRZllVVVR3HMQkhnxgHaNs28vXr1y9VVTU06jfneZ4VRZEMwzBVVVWu/A4m651tAAAgAElEQVQ4AAAQBIFf15WUZZmw2jgAAB4Oh73jOJ7rupZhGIbjOBcBcAq6vl0lrwHf72nMgbMzNk1TjzHmhmGYhmFg1/2yLMtCgfgky/K8LMu8LMvCIvkffS7P84Cm+yENOFBd132apoksywpDAJqmqYwArOu6rKqqihCCr/hiK8Z4fWPm95hTS9M0jeP4OUmS1LZtMwiC0DAMUxRFAWO8zfM8N01TPT8/P8dxfJZlWTUMw6DBWcdx3I8BgL7vu77v26Zp2rqu66IoatM0jcPhsJckSaTkoKiu61YURfGnn37a/vSnP3msDj2O4xhFUdw0Tdk0jdM0Td91Xf/WjZAkSZamaUHvwiLLsrRpmvb29jbs+34Yx3F82QPzRYjru03wvVNCAIBN0zR1t9uFPM/DYRi6NE3zZVkGRhC7NPp6Hf3T71oURZHHcXwahmG8u7s7HA6HG1aTwhjjdV1XwzAMtlkvPbAUWAJaY4cAAEgIIY7jeJZl2UVR5MVLXaR4fn5+/u6QQUmSxAsBAHhdRUEIcYQQAgDYHMdxZVlW8csthdd1XTDGWBAE0bIsa7fbhdfuBmFeUBAEZNu2zbgOFB+Sbds4QRAE9vxLO0KoY6/atq3LsqwZC3232/n03w8Oh8NhWZa1KIq87/tREATedV3b8zzXcRz7QgewLsvyLZtEMyGQRecAgO18Pp/HcZwxxtx/FhJijMmPcENEUeRs23bGcZwEQRDoZ1mfnp6e5nmeWfTj+77PnLEkSbLjOJ5pmtYl++57u729vWnbtiOEbLqumzQYWJ6fn6NXTnLzfd9TVVUBACDHcSzP867eFUTPBGiaponjOH5+fj6t64pvb29vPc8LfN/3bdv2XNd1Lsm0sgzQtm2YlgHw92TH38MwxmSapmlZlnkYhnkYhnFZlg0hxK3rOjKn3/f9yAxCCAghmLYKr5Svs7xhrXlBECDNdnGO4xj03quTJDlLkiTzPC9JkiTvdrtQVVWZ53lRFEVREARECFnp3YRZOe1H+VFVVZVVVVVFURRpmqZJkmSmaRqfPn365Pt+6DiORUEmGcdxSpIkI4Qsv/zyy3PbtvU4jgNdluGHSwDjOA7DMIy0htJwHEc8z/MkSVKTJDk/Pj4+DsMwU+a1JcuytCzLbFmW57quU9d1maZp1rZtt67rRNPDb47My7LMaTmiZIhI0zTF87zdNE1P8zyPwzDMy7LM4zheAwBs67oyCPy65xsxILCuK6E1MQ4AgDiOAxQ5XoyWWaaDZTvSNE3qum52u50fBMHe9/3dzc3NblmWdZqmxTRNk+d5wXVdzzRNU9M0/cLLhmfBHktrYow3QRCg7/s7TdMMy7Is0zTNiDJlqBOAkiSJiqKopmm+mYjILp5t2wgAgHAcBwVBEERRFOihyJdl+XbIIIRA0zRTlmWRZWuubRBCOM/zejwen1+BPIAQ4kVRlIIg8GVZllgnwqW1T+b8m6apyrIs6H4PHcfxfd8PJEnSBEHAgiDw8zxjAMAmiqKIMd7qui6v4Ai3bdsIBVobz/MvlHuE+PP5nE/TNNKODEIvOkLf18Xlj59//vkPCCHBcRy7bdshiqLnF5+AOZ7nISGE6Lquu67rcRy3QQh53/ed+/v7+2u/9z//+c//Q1EUmRJQlyiKnmm54ds60cxFaNu2qaqqdnt7e3H7MyEEM9DLcS+EUwAALIoiPR6PT7QcezgcDnvf94MgCPwwDP1rcK2+3wT/jOi/aZpmWZZ5mqYZYzwvyzIyUDUMw8TC/2EYBtoVtdzf398MwzBO08RKom/KANBSC3rhe/KI4zhk27YLIeSyLMtoGUBUFEVUFEUCAAg0M4YAAICVYhmX6S3BX13XLSUY1kVRZDzPo91ut/N9PzgcDjvXdQPDMNRt27ayLMtlWeayLA1JkniaiZ6naVqmaVp+KwDhX0cgLKIehmEQRZExbYfT6RQty7I+PDx8chzHX9d1zPM8wxhviqJImqapsizLPM8jepETWkfe3oKEKMmlSNO0LIqizLIsXZYF393dhZZlaUmSoGEYJkp+m6dpmodh6C9Jw7KUNkVvKyEE8zwv8DyP2rZt0zRNqS5CASHk1BclGJHneVEQhIt749u2bZumaaqqquu6LvM8z1VVVakD8MIw9IMg2K3ruiKEEOtTZz2g7xEBb9u2TdO0yLIs//TTT05RFLqiKIooiuLj4+NjkiSpqqqqbdta0zQmKw+89f6jh4fQrMoGIeTpYUgZQFip54EQwjAMA4QQVFVVv3Y7Jo0syOl0eur7vmcqmC80CYQsy7IJIZuqqkpZltWlEVjXdV3TNB0VgSmHYZhvbm72vu+7juPYy7JMz8/PjxjjhbaCctu2rRzHcYqiqF3XBRzHwWs4otdZIZ7nYVmWVRRFMSW6LbQt8hshThAEyGrFlzzv4eHhLk3TfBiGLzTTgDmOIwghCADgOY6DjuPYtm07EEJ0CdP6v7L7+/tPHMdx//Zv//Zvv8avoREyoODs4rNHuS2E/ooxxivP86jruv50Oh3ruh4Oh8Nuv9/vfd/39/t9uNvtgmt2W/2zbZ7n6VV6f56mabEsy+A4jqvruqXBHnNRHYQQ2LbtYYwTqtmyzPO8vIWQKwgC43K8LgdJsiwf6roeoiiKRVGUX+uPUG4yuoYWRdu2/TAMfdd1fdu2ddd1QxAEnkPN8zz/cDgcbNt25nkexnEcBUEQBUHgqV4JppyRHwIf/OsNSJ3fsq7rwhxb3/ddVVWt53nOzc3NgeM48Le//e0xy7JCVVVlWRZM66PfH5C3RkJ5URRF9eIJyyzL8rIsK9/3baZKJcuyXNd1S1HfNE3T1DRNdwkAGIZhYhENdUScoih83/fd4+Nj13XdWNd1UVVV7Xme47quY5qmoeu6do36b9d1bdu2HW1FLOd5Xj99+rTzPM8NgsBxXdfd7XYBz/O8ZVkWxniFEEJFUZRrtUG+cvyAOpmVXUiU0KmyrohpmqbHx8cvRVHklmXZpmm2b2Xg0n9vnud5oQ5+AwBsZVlmeZ5n0zQtLDpil6RhGLqmaRpL/b0FaH4Pdth737YNbNtG4jiOv9/H7DkIIYQxZkqR+Arvf5imaRjHcWiaphVFkXccx1ZVVS3Lsqiqql3XdaRE1ZXjXljjEELoeZ7LWqT2+/3+Levw+nuyrgeEEOq6rs2yLGcELHY30/dEMMaLqqoyhBC9JAtekkgXgA7IWu3oHiTfvSfukla7S6JhCkoR/QFsrd7r2eu64iiKTnVdN4fDIby5ubnd7/dhGIY7z/OCu7u7u2s96/XX2LZto6ALXiO7+QN38DhN0zjP8zhN07AsC5YkSaJrUS7LMlICYN+2ba8oiqiqqlJVFWLZAUYKn+d5/hFBnl/zVxBCzrIs5+bmZvf169fnOI4jURRFVnbleZ6/1vun2aZpnueR1vChYRiWYRiGZVmWbduO67quJEnCsiwja8e/1Phf2wCU3SpwHAfneZ4xxkSSJGldV65pmiyO44yRJbqu65ZlWfq+75ZlWShJArF6yo9cBsMw9KwFghKdqjzPM5pm9cdx7KjWABnHcaEbZbpGGxyLPmmaH2/bhrMsK2hJYyOEzMMwTPv9PvQ8L1AURaVKdXgYhp7jOO/Szc/Yr03TdLIsS7S+adq27Xie5yuKoi/LMoqiKGzbxv+aPOZ7XYI8z/OHw2EPACB9309t2zZ5nqu0LWdgGaRhGOa3fCRaW2X1s4UQgpum6VkL2KuWQB5CCAVBUERRFCn6vjgFzUoLlESECSGcYRiKpmkGzW7xHMcR6vg4y7IMhJB4DQZ2WZYcJTFP40stblRVVdE0TaWckKxt23YYhoG22mFassKKomiiKAp078zzPI9vAaQQQsgcOO1sAFSApOu6bqCE25VJNFNFRDjP88rzPBJFEfE8DwVBuDQa2i78/TdbkiQplYFeAQBA0zRt27ZtWRZWlmKdIhAhhARBQKqqSu/0ceDpdIo0TVOo47+lUX+4e7H9pVmn74Dt30WQ9NwhqrzJS5IkvPc903VdP47j0Pf92Pd9T9vwFNqiy9GzMbVt2/V9P4Zh6EqSpNAWwWGapvmFGrYsdV23l2aIMMYbFZzbD8MwRVF0FkXxREuAIt3r4ErrvzDwMgzDJIoiT1sQFUVRVF3XTfYOWPcbJT6u9HMi7qUzAXA/IJnNv0bWzOhfJgCA7UXsTxFpyn/puq7btm3FGKM0TbO6rhvGTN62DRiGYcqyrAiCIDP09lsty7KiLMuCCtGc0zTNqqqqDMNQkySJ2rYdlmXB27YRCCFmzNC+76dpmsYLHQChGYBvrHpN0xRZlmWMMZ7nWYAQorZt+yiKTtRJEY7jOFEUkaIo6ltJQHmeczSS/dbVwJjltLZu6Lqu0g6BpOu6fts2TpIkxfM8m6Urr3XxAgA2CCHgeV543QZmWZYxDIPJ1kUURalt235ZlpWl6Dkq4PKWlDsAAG7bBgEAQJIk+f7+/l7TNJkQAmk9mkVfiHYdGJZlGaZpaldWniSEkM22bfvu7u5BlmWFKcFt20a4lxq0sN/vfcMwrEsZ2NM0bdwLwx3T2idWFEXetg1EUXT8+vXrads2IoqiIIqiaFmWOo7j2Pd9HYahIgiCRLsn3hyxQQgBlbwFAABAlRFznud5QRB4WZZl0zQl2ooqSZIkzvO8PD4+PrIWPgrULgUA4MLff5OdTqfodDqdqODSzHEctG3blCRJoxEowRivv2dUTMm4gGaYCD1nC82Uzdd8Fgt82M/r30MIcdfstPgHd2DJeGgUBPRU8EuhWvuIkr+Hruuabds2VVV1RVEkURSldV3bl6rBNLOg8AqBDxEEQQjD0KdtiPP5fM4RQoIkSbxt2961MgCs/ZwRGem5E19+EQRVVUV6VywsGGBB47quK8/zkkBNkqTf3BLNv3b6rN1HFEWRMe1N07T3+/3+dDrFp9PprKqq+unTp/u6rqvT6RSz6E2SJH6/34eu63qGYZh0OIP2oygUY4ynaZqyLMtOp9Np2zYyjuOCMeZkWZZd15UFQZCSJEnGcRwY4XCapunSGx8AwA7Buq4r8X3fvLu7+8RxHJjnee77vq/ruizLsvzy5csz7VWWTNM0L8kC0LQzeV3jpoInAtvgRVEUVAgkY/KYVBWrRwihS2u/lPi10g4D1gbG8TwPaMSNCCGsB3yjAcPGWOKUMPnm80AzAIQyeleadRJvbm7uZFmWAQDfIlQqCINEUVRd13UOh8O7zJ+gWuzi3d3djaIoGpXiJTRFigzDsPb7/e7SSIyKT2FCyDrP8woA2Cj5jgEv3TAM0zRNXZZlbV1X/PT09EWSJNF6MZux4d/6WVhf/7IsK430uTAMA8/zPNZqRx2/IIqitG0b+fz58+d5nleEkEDBGbyUB0DBPaIZRoQQ4ukmIwynPz8/P0uSpFyTA5CmaZ4kyfl4PMbDMPQIIdT3vbvf728AABvTJmCf6SVhwqNrReFUcZRln/C2bXi/34c8zwun0+lMhbpmxjBntfJrqU9+n/GjxGfIZJCrqqqXZVn7vu/meV4lSeJ1XTeupYJKy19j3/cjJflNmqbJiqIo67oSURRF+vt913WtJEmCYRi6JEmSKIoS7SAYaJls+hEm/H9llmW5u91uos1Xy+l0iuk9dDUOALs3GQhnZFxW8qQqsKht26Ysy5IKJbVt29Y0WNU0TZNkWRZ/JPD+BgBkWZYlSZJlWVY0TdPrum7Ksqwcx3EfHh7uNU1Tx3GcZFmWWFnCdd2a1iFZhGC6rusFQeC5ruuEYbj7kUWgm2umh4BgjMnt7e2NaZo2FXuRZFlW5nkeyrIsh2GYmArbsixTlmX5W+vh7AWwVjSWj5YkSQnDcIcxXpum6bquq+M4jj9//vxIkdjMJta99eUjhDjm4FgfPEP7wzBMSZJk8zyPtD0wy7IswxhjqkDIG4ZhvLX2+yvrsLG1fx0gruuKkyRJyrIsu65rX85oP4qiyL8io/AQQv6KFxEBAPD7/f5GlmWZparpu4Kapsmu63rvOaCITeDb7/c7SZJE1gIIIQS+73uCIIhXWHMmwAKZE6Xy0+L9/f2D53mjZVm6LMvaOI7j09PT16Ioal3XFVmWVV3XFVVVdcMw7Eu+56sOGMJxHCdJkrTb7fa2bZsIIZ7KEEscx6HT6XRCCAGe5yETpHrNYH+LPT8/H+u6rgAAhOd5gbUfvhK72pIkSdM0zVRV1bquax4eHh6uEH1xVImwzbIsPZ1OJ03TVEEQeNu23W3b8LZtKxUhkhjYeY9BRFRimFBuFR8EwZ59b3YmKVBet21bm6ZpHMdxL52IyVpsMcaEZQAhhEiWZQFjTI7H41PTND0NuhYq+2xN03SVORRMT2We54HOvVgkSXJoZLupqiqXZVm1bdt2XTdalmUoiqIxAAAAAAw4UBAx/iAvaPtu2j1HCOEwxljTNCUMw5DxjeZ5foyi6ETV/2ye59GlQmy09IYEQRBkWRabpulYt0NVVcXj4yOnKIo0DMOUvTiCsmmasiiK2jAMhWpRaIqiaEzC+4cAgKqqmq7rmmEYum3bVp7nRZ7nmWVZxn6/v7m/v/+JZQo0TVMZUiOELBzHAUEQJMMwdMuy7CAIvMPh8EPO/3Q6xXVdN1VV1RTdVJIkCbZtu3TIgcKcwPPz8xMdhFHTTEXfdV3fdV13bUIchJBjAIMq0PUMdVGiJFOBe7PzdV2XE148vyCKoixJktQ0TVtVVZGmqdx1Xbssy9w0TZ2maXY6nSKEEJQkSby09vtrIOj72jDHceR0OsVUeKPN87wsiqKgGvCeoihMKkt6azfCtm2/RsQBEEIwz/M4z/NI1ck2GiUDRVG0dV23a/aB/6NaIIQQHA6Hdxn8RNN9f1d/p3yAdb/fB7ZtAwaQsyxLzudzzHEcsW3bpRwFXdM0NQxD9wIA8EK+WBZCQT0DW9B1Xd91XYeCcDFN0/TV3vim17Cu67YsC3mj838+Ho8nqr8/0clnaNs2IAgC5HkeFUVRJEmS8DyPLMuy1nVdRFEUL42Cqa7BRiP9tSzLCgCwjeM49y9N50zghTDVSUmS+EvVJ39DWh4riqJ8+vTpftu2LcuygomUEULWvu9H27aNvu9HjuPAJToEFPizUhShpR5ECNlOp9NpeIm2RvrrJEmSHIZhACFEmqZpl3YhUQ7MTH8GysFQFEWRmH/Ksqw4n88xVSk1qAqoTF2D0Pf9wBjyXdcNWZaVv/VM0LLHSoEQi77XZVlWhBB0XTdg4+EJIeuXL1++Pj09faXlCThN00UAgAIZWdM0WVVVI8/zmgJdFQCw1XVdC4IgrOuKm6apkiRJ4ziOMcbEdV3PsizTMAxD0zT9Rzqx+FdOyCmKIrdt22zb1tntdt3T09Px8fHxads2EARBoGmaIYqiuK7ryoQj2rbtp2labNs2bdu2LcuyfN8Pf5Sc1vd9y6xpmqppmj4MQ0fTNN22bZuSXqQsy1JaE1HoqNS6aZqOMui7Kzt/WFVVnaZpPk3T2Pd9X5Zl1fd9H4ah57quq6qqrmmaqqqqfMmzaCSn0WmDztevX5+fn5+f1nWdZVmWp2la6rqusywrAABbGIY+I6KwEbGX1gBfC50wEtCyLEsURRH9MwtVK8zjOI5FURRt23YNwzB0Xdc1TdPe+nyE0N/pvVOW9UYRf0/HH7PPukIIoWmaNiFkFQQBXYkH8e0ypJ+Fp4Svd635+r7P/fLLLwKdMiirqiq1bdsWRZESQrAoivw4jlPTNHWSJGfabuk7juPZtm3Rs3eREiITAeJeWvsIG7pEU/HQcRz3cDjs8jwv0zRNXmlkQEIIpt0Jb+6IOJ/P+fl8Ph+Px6ht22rbto3q25Nt20CWZVkURdG6rquiKMp+v99LkqS0bdteuv6iKHKviI2iLMtS0zRtURQJHXomU4GZTZZlCSEkvkhUiO9aFyeEcAgh6DhOwHQI8jzPKSFzNE2zbtvW3bYNqKqqXgIAWHWBXQCsmyOO4zONzKdhGIau67pxHAfXdT1ZlkXHcexpmgaO4y4CAK9JcNM0TQghKAjCt+he0zQVIYTiOD4fDoc9jXYVURSll9EzklTXddN1XU+JBMM4jj33GxXxXoN9SgbfXt03WxAE1gv2WpkYGXl6enp6fn5+5jhu0zRNuUQNXNd13TRNvaoqw/M8t23bLs/znOM4MgxDpyiKRsthpG3b9nw+x3VdN0EQ+FQLxrYs64f5SN8AgKIoim3bTtd1Xd/3I60xrXEcn798+fK5ruvaMAxDFEURAAAoW7EtiqJaloU8PDx8UlVV9zxv5N5AkmEZha7ruqqqKoQQtCzLsW3bpLOefUEQpLZtW0mSVF3XdVEUBTolr2dpn7e2obEI9JUD3PI8L6nWMl6WBXMcR3ieF+lshJ3v+6Hnea5hGFYYhhfVwmzbNqqqMl3XtcdxHJZlWdI0TT5//vxI5y8QqkqoeJ4XuK4beJ7nUW0e7QrpyG8EINZ/zSIOSZJkAAA3z/M6juNQVVUDIeQOh8ON4zjui2Lwi10AAASa/hJEUZSXZcmTJEn6vu+2bYMUdG400lxFUZRvbm5WwzBUyr+4ujE1PJaee8/LXtM0VVEUTdd13bZt93g8nj5//vxZVdUYAIAwxmvf9wMhBHue5x0Oh30QBL7nea7rus7Nzc1FPIjXXTDLsiyUBMrzPI+oY0T0npC+9fq9OH+OciO2C5wPhzGeuq7ry7LMHx8fH6m890Y5sVNVVS07m0z9kEZoVxGBogrAmmmaRhiG/tP/y957pblxpNuiaSIjvTdAoYostdS99/n6/YzlTuBM4zzdOdzZbYll4NJ7E2niPiCCG+IW1aoqFNXNxnpRfxKbADLD/Gb9az0/7z59+vRUVVVjWZZRlmUJAGAhhDLps8JLS/9++T7IhbQYhqFTkiaEUDgej0ld1w+O45g8z3NkNrx/7SgsUVLHlIeEMZ4QQuN+v99SsbWBzumO40wdAmlbhrnAZAbhNSzzPE9936MzEhyAEAqSJMmKotCWl6ZpmkLaNFCWZUgcEbOu6xqiGjgQMucfroDRShbhQHzmNtFphPV6HYzjOBIiMMMwDPP8/PxMyvA6JSgDALiXvgfXdb0sy3Lbtpuu6wY6iZIkSV5VVUUCT0D5AMuyzEEQuOQM9lzXtW3btl5aDftVCevu7u6WRHsjFT4RBEEg0feRlt8wxix5USPLstgwDJ0oKf1q8b6wDMrRB6goikoNPzRN0wkL3lAURQrDUNF1XTUMw/A8zxvHceR5nocQvomFTF82cVkSTdM0EEKIZVlOlmXBsiwoiqJMMl2dVCX8IAhWnucFb+2/bzabTdM0DZmxnlmWZYnNbTWOI4IQCnQcxDAM03Vdx/M81/M8/xJz0WS0RZAkCWqapq7X63VZluXxeEzOSoQMEd6RPc/zVquV73me7ziO6ziO85YyIHXdkiRJJoS3inhSZFRpi5gFsRhjTAxZJhKxv/kAIsEfQw4zTN0BGYbBb2zv/SGYpmmYpmnWdd10XYc4jmPTNM3atu0ICYpTFEW2LMs66YK4VBzKc13Xu8SdQ8vgtL9NxX04juMxxvxv7BmOiIAJbzFD4nmeOSeTkukTThRPUv/ke2DCOeBPnFQBCoLAX2DskGEYhiFGV1bXdR05+1gixxqFYRizLItd17U1TdMlSVJIICBd6v3T0U4qwcvzPEeJsYIggNVqtaLnsSRJSpqm6ZlTJfMW8iWE8Fc8JEmSZJJNN4QEyEEIgXqqySuqqqqmaVokAxfOfSLeUnU7+z4QACDQ/r4syyJxfjUYhmFs27ZJC0BjWZaTTqNqKpkGmIZhoBk8fsHzpyz8hWVZVpIkURAESEnHAAAQBIFPWlO/OjfI2aVQ19LXPA8IoeD7vkcE7iZqQa3rulZVVUm4bh0Rf1MMwzBt23aJSmDgeZ7vuu6L74H/0cP6+PHjR1LeZ3me/3wZFkVRNk3TDMMwEIKS4jgOpL7olmXZhKUsvUYZ6yz6diGEPIl8DdM0TU3TDHq56LpumKZpE3tUblmWxTAM8zR0oKuvZeW6rmsfj0dN13XTdV0PACCQLJAnphyCKIqQkiRJ2dVZrVbrt/Rez7Fer9fjOM7k5UPDMIy2bXuM8cRxHA8hFFVVVQzDMGzbtlzXdVer1eoSPXDbtu0kSWLTNC2E0KTrullVVUX1HWiQRoiYimVZumVZdhAEwWq1Ct5agndd1yGfb/Z93wEAQNu2DdH9X4gDIw8A4DDGLBlFVE9nxNtJgNQJkLg8aqvVaqVpmsYwDDhze3s33N7ebpqmaamOuaZpommaDvH5xmQNioZhaLquG7ZtO67rekQq9M3rj4w2QUmSZMdx/JOtua6RQ5ATBIGjlUJJkmQiy2uT9ghPREuU11yKPM8zkiTJhmFojuP4EEKJGuEIhGRzlijwGGPONE2N7PuL8D/u7u42wzD09OzjOA6Qw7do27bjOA44juPYtm1almXouq45jmNd6v3ruq5xHCdACEVFURRCOlsAAABjzLqu65CxOFHXdZVIIjOGYdjqqQcpvSUJEQRBIkZu1mazGUkllyYGAEIoAgDgiSsuUuMmKlD25kkAQjCVVFU1Pnz48JHnedY0TVPXdVUURdk0TfPm5ubGcRyPrBObBARY0zTNtm2LjK2LmqZJREaZf8H9I0mSpKiqqt3c3KzJGahrmqaQ6TObYRjmnHS6LAvDcRxn27bJ87xgWZZJOqGv4mL5vu+P4zgTfheQJEkyTdNomqYjEyBUcwOSd2UQobhgtVrdvCYBA78Vidzf3//AsiwviqKgaZpKStMtLU2TTJAnl6Ioy7Jsmqbhuq7nuq77yg14SxzeOMMwTIzxoqqqYpqmc/53fvjw4XZZlpHjOEZVVZ1lWawoiuw4jmtZlvOGA5D3PM8Zx3HgOI6zLMsiBCRKRAJE815SVVU1DMMIgiC4pBiHruv6hw8f7mi233ED2ogAACAASURBVLZt03Vdz5xY2ZwoilBRFIUQNa23ln2/fO+e5/nzPM+Koih1XTdVVdUIoYEqz5HMEJDRR900TdN1XfcSTGwIIXQcx0MIjTzPc5qmGfRAPptKYGnGBwAQSM/Lfmv/m/bgTNPU27Z1p2layPib6Lquq6qqfgm1x3+EH3744SN9FmVZarZtt+cZBwBAIHtCtyzLchzHu5QBjWEYumEYtuu6AwBAmOd5lmVZ9n3fs0743N5xHMch65KxLMtmWZYlAYH/2laY67rOOI4DAEDouq6jew5CyJ9PlgAAOLIXZFICvdgY3E8//fQjDfpVVZWrqtKbpnGmaZoYhmFlWVZIEGDbtn1x6W1S2bOIzKtIzjbNMAzNsizLMAwdQijpum70fd9jjLGqqqrnef5bzj7y/K2+7zuM8aIoiky4HJ8dHkk1BtIgUVEU1bIs88OHDxdRIzRN0/Y8ryVryqRVMcdxXFEUxXmekSiKMuHEQGK+5A3DgFzXdTHGi2VZDgCAI1Uy5yUOoSSjtsdxRFThlgRazpetTXreEe6F1Pf9AADgDcPQXdf13jKSvdls1mTdQ1VVtaqq6q7rOjIaPbMsy4miKMiyLGuaphMjKv+1SQD7e+X63W53yPM8L8uy6vu+JaMV9EDiRFEEJ2lkUTYMQ7dt234p+/9LbLfbXdu2zbIsCyl52l8SCud5np+fn5/7vu/neWZEURRt27YuEZGHYRhmWZaRuUuGHj4nvp0AZVmWPM9zLjH+9XuIoihp27YZhqGfpmkhZCxInQnf63OrqqqyLCuapqkI4QfRd0574aQSodq2bb501PMfYb/fH7Msy2i1iTmN58zzPGMAwOf3AQDgVVXVXNf1LuUDEIZhuN1uj1VV5bTiYBiGudls1m/leLzwGezLsizruu7OTWgEQQCKosi6rutBEKwuLH7EhARVVTWECCkQw50fvvysKIriLMtShBCiB6Hrut55oPCKNR9nWZZTTY9TJRgI4Mymk5SpOUEQxEu7752de7s8z/OiKKqu61oqjCPLsmRZlum6rn9p7wmK//qv//o5juOoaZqeZVksiqLouq73n//5n39jmM+mMUXf94hhTuxx13WdS0w/bbfb58PhEHdd11IdEBp0E2MuKJ4gK4qiXNqLYb/f7+M4zugMvyRJMtGWsZ+fn7dN09RkLFewLMvcbDbreZ7nX3755VOWZfk8zyPGmNU0TT0VJlf+C9/7IUmSmHh/LJIkwd9LcHa73aGqqnIcR0SnIW5ubm4uUZGc53k5Ho9hXdc1MeEal2WZKUmaVmHeug7Zf9Svn+d5SZIkIfy84ewy4IhCmChJkvwGE5h/SnRd15F+I7j0QfuvgHmelzRNMyI5OdFLl2VZHkIouq5rvefnE8vlgYgOzYSYw3AcB1iWZViW5S3LMi5ZgWGYkxpl0zQ1dbkjQYb9Z7yDqqqqvu8n2n2g4ifvve6rqqqZEyeA/yOX7Ft9GN7773vld5iPx2PU931Hx08hhIKu6+Zbgpw/ehER/hEWBEH4rUof1QS49HNK0zQfhoH+Zoa0I3mO4+B77/mzvZ8xDMN82Ur+vXURx3G6LMvEMAz7VkEs8vkLz/Pg0mPlr12LJBmkBnDqpbRP2JcS9t5r4V1xxRVXXHHFFd8O7J/l/XzFFVdcccUVV/x5uGbxV1xxxRVXXHENAK644oorrrjiimsAcMUVV1xxxRVXXAOAK6644oorrrjiGgBcccUVV1xxxRXXAOCKK6644oorrvhXAbg+giuuuOKKK674n9jtdodpmgbii6N9KzGkPy0ACMMw7LquH4YBLcQGjed5lpg1yN9SEvUcCKGRKJQxDHOy71VVVbq0ElxVVVVd1zUxemCo57kkSdJ7q7D9M2AcR1QURUPU51hd1xWe5wHP8xxCaByGYRBFkVrCcu/1rud5niCEkOd5/nfeVY0QQrqua5fyZp/neU7TNCXuc8LXVACjKIoQQiPHcaxGnKgutP7qrus6hmEwx3Hgt+RWiXXqQFww4Vd+Q0bUDPmXSOYihFCe5wXDnKRvf+93xXGczvM8cRzHX0KW9+yzMQAAfk3aO0mSbJ7nkWVZ1nEc5/fWyEtBLMl7qr4IIRQURZHpZ5B38xlfewdvAXmu85m76mevkm9xBkRRlFDZ2feSW/49pGmaj+M4fvH7Wdu2nW8lQJfnebHb7Q5lWWZd1/UAAKDrut40TfVW47N/2gDg4eHhYb/fh33ft+eWwET2V5BlWc3zPLdt2/0WC4PoIUd1XVfDMHTjOCLylejmBMQ+VruEMQ7Rgo6KoqioFjb1QxdFkTrxme+pxf9bh2KSJCnxYRjnecbEKhXKsixdMiDbbrfbKIqSaZoQcYNkiRQyPWAX4ptNPa9ZnucBw5w0yQ3DMN/ixzDP8/Lp06ensixLYvoBTNM0v9xwXdf1h8NhVxRFNU3TSJ0T7+/vP77lgAjDMIrjOKqqqsYYLxBCsSiK/P7+/p7+vV3X9U9PT89pmmbjOA4sy3KapmlBEARv1eV+fn7exnGcDMPQ0QCkLMvi/PP3+/0hy7K06zrE8zyn67oaBEFAL+ovfwPP8yDLsiQIgtU/krDdbre7NE3Ttm0b8k4l27btu7u7u/PnihAan56entI0zeZ5HgEAIE3TxHVd77WX1JkOe4MxxoIgSFVVOff39x/P/9zPP//8S57nOUJoYFmWOxwOked5zs3NzZu1+R8eHh7TNC2maULk3GPo+gcAsNM0MURuljlLRFhZlkVd1823vn+EEHp8fHyiuvbUgZKuhTzPs48fP364dMBxfulRr4F5nie6dm5ubtbUive98fDw8CnP87xt24GaQBKxOt4wjDgIguC9AyGEEDoej4cwDA9hGEZN0zQAAMG2bbPv+wFjvFzCAO2fKgA4Ho/hbreLDofDPs/zfJ7nESFEXeB4URQhjYC6rmvHcRzfyxCDZlhRFKVVVRV1Xddt27YIIUT9shnmszuabBiGnud55fv+q93R4jhOwzA87vf7Q57n2TzPE8aYI66HHABA1HVd1TTNKIqidE94N434eZ6X/X6/z7Isr+u6pk6MRA+bJ65c0vF4jC9xAO73+/1ut9uTClBLLFFZQRAAx3E82YjLOI6zIAg89SIHAPAMczLucBzHXZZlee0GfXh4eNjtdoeiKBKE0CQIAvQ8z8UY4/OL4Onp6fl4PB6yLEuHYRglSRJt23YYhsE//vjjX177/unvL4oiG4ZhVlVVadu243mevb+//4FcVDtyOIRN07Q8z/Ou69rLssyCIADf973XfP7xeAz3+/0hDMNjXdfVOI5Y0zSZGOOwP/744w/7/f643+/3YRhGbdu2PM/zlmWZwzCMf/3rX3+qqqr58jfIsgyJmyb3ewEA2f/7OI5DGgAbhqG1bdtN0zRRpzx6SB8Oh2MYhknXdZ0sy9CyLGue55lYo/IvfPbL4XDYH4/HsCiKAmO8aJqmTdM0sCzLffz48Y6898fD4XAgGv0tcWAzEEIIACC8JSn5+eeffzkcDoc0TVPqdEjsoXme53l6GRNnwF8FAKqqyrZtO9M0TfS7vgafPn16PB6PhyiKYmJ3zpKoG4uiKFFfgv/4j//463ucOcfj8bDf7w/E+wXRtbMsC/O3v/1Ne+/s++Hh4XG73e6TJImrqmrpMlqWBQMABMdx7HmeZ+KJob/Xufvp06fHMAyjMAyj3W63z7IskSRJ6vveJ1U1BmPMUvfO7yIAaE6oiqJIt9vttq7rehxHhDHGxJ5WNU3T7vu+GccRTdPEAAC492gJPD8/b6krX5qmWdM0VV3XLUKop14EzMmlCqqqqhIr4nYYBoQxXoIgeLExUVEUZZ7neZIk0X6/P7Rt23Acx5MSH3Vf0i3LMvq+b4dh6Od5nl7zWX+kDXE4HA5xHKdFUWTUlWwYBjTP80L8uSHxUDfGcRwYhmHf4sSY53mepmlCHCBpCRzQ8j8972hmwDAMyzAMx/M8x/M8q2maPk3TRGyFnVdcwHGSJGmSJOF+v983TdOSTb7IsiwR/289iqIoTdPseDxGh8Nh33VdSy6LRVEUOUmS7DWBWVmWZZ7n2fF4PIZheBiGAdm2bQMAOGJ1W/d932dZliZJkux2u2OWZTGEECKERgihaJqm+doAoCiKsiiK/PTx4ZFYEtukxaDu9/tjHMfx8XgMj8fjIU3TTBAEoes6n+M4TlVVbZ7nKc/zLAzDYxiGx7ZtO03TDI7jWFVVtTiO06+9mzzPiyzLsv1+f0ySJJznGdu27RB7ZlYURfHu7u72559//kSefXg4HI5VVeWyLCvjOE6KoshhGIYvteitqqpK0zSlgdU8z7Pv+x6xJJdVVdUURRHpdwzD8JAkSQIhFIIgWEEIxbqujbcEAHme58fjMdrv9/u2batlWTBZ2wAAwGOMWXJBTAzDYNIuYyGErK7rOkIIcRzHk7Xy4ipYFEUJef/Rfr/fVVVVMgzDsCyLl2VhDMMwl2XBkiRJURQll67AkvO2iOM42m63u7qua1VVdRp8HA6H/e3t7e07Zt1jWZZlmqbp09PTriiKbFmWhWVZFmOMZVlWhmEYIYQwyzLzvQKAx8fHT3EcR8fjMYrjOI6i6Hg4HA4QQjhN00Qvf2KNzr0l4PunCgBIdomGYUBlWeZpmuaSJInTNOGmadooijLLsgqEUE/K40CWZfHSAcB2u33e7/fHMAyPyQlZWZZZURRF13U9tebkOI6VJEkyTdNs29all6MgCCyJFl+0CadpQl3X9WVZNkmSRLvdLiT9f1YQBF5RFNm2baeua6dt2560SBaWZZlLOiF2Xdc9PT1toygK4ziO0zTN8jzPq6oqh2HoyG8EkiTJ5LDxyLNQ3hIAIISGpmm6pmnKx8fH57ZtWwghEASBxxizNAOiPV9SgeFZlmUFQYCbzWajqqrSEbyUm9G2bdc0TVuWZRVFURSGYXx7e3tjWZbRdV3f9/2g67pO+Cld27Z1kiRRGIbxZrO5MQzD7Pu+p1ayLwXGeEYIjU3TdFEUhVmWVQAArmkaZxiGrqqqpuu6Os/zIj8heXp6ejYMwzBN0yQfjV77/KdpmqdpGvu+76MoCuu6bjiOYzRNM9oT6qIoiiRJsiiKoqenp62qqjKEUCBBaYMQmqqqKvM8Lw6Hw74oiuqHH3742Pf9QPb39LXPH8dxGIahb5qm3u12+6Zp+tvb24HjOA5CKEiSJC3LsiRJkkRRFKdpGodhuA/DMPR933/tM6jr+v9bluX/maZp7Lqu3e/3h67resK7kVVVlXVdV9u2FYqiKMuyLIuiKB4fH59VVVVUVdW6ruu7rnv1s4+iiEEIDX3ft3mep4+Pj8/jOI6CIPAcx/G0ykUzxLOLeRYEQVyv12tZlmXP83raPngplmVZ5nmexnEcyrIsHh8fH4dhmHieZ3ieFz5+/Igdx3HGcZwwxvOlL4JxHKdxHFHbtn2e59lut9uv1+tA0zRF13U1yzLVcRzn0pwrimEYBoTQhBAa+75vnp+ft33fdzTw2mw2a8dxrGmaJspNuzSenp4eSSUuTJIkJhW5yLIsc5qm8fn5eUfufcp/4iRJgn8WL+6iAcDZAsfTNM2iKML7+/u/QAiFruu6sizz4/GYLMuyAAAE0nvX9/v94RL9N9rf3O12YRiGR5oJhWF4LIqi5HmeE0VRkmVZYlmWHcdxquu6LcuyapqmnaZpJr16KMuy8tIAgHoisSyLSUYHfN93Mca4bdu+LMuG8ANr4oG+YIxZCKHguq5DL8W34nA47OM4jkgpLiI93YSUOXmO49iyLBeMcaaqqsrzPHRd1yX8gFfbqC7LwguCwPM8L+i6rvI8j8dxXMZxnFmW5YZh6Hme5zRNU4ZhGJdlweM4jtM0TbIsy9M0zcS6F9MM6YXPf8EYz/M8z+M4DsSTez7nfJyv0XmeF3JgtcQHfJmmacEYL68MAFiO41gAAMtxHDcMA+q6bkAnjAihvq7ruiiKOs/zMk3TZJ7nxTAMXRRFCUL4K9/6F29EYj0tCAKUJElOkiSjrZ++71uy9sqmacosy7I4juN5nq22bfu+74eu64ZxHMe2bfu2bZuiKCoAAA8hlAEAwnkr57fAcRzHsizP87ygKIpalmV1OBwOoiiKkiRBRVGUaZpwkiRJlmVJGIbh4XA4AgB4TdMUCCGgWdFLfrckSf+bHPIcy7KCpmkyrcQpiqLIsixrmqZJkiTmeZ5nWVZkWZYNw9B7nmeR38YLgvDq/cdxHMOewAEAgKIoMsYY0qCXAkIo8DzPz/OM9/v9fhiGIQiCAAAAaEuMVMZeBZLtshzHMaQCpIuiKGGMeVmWJQgh4Hme+/J7XQLkt/MAAEFRFBUAwCdJkmqaZmqapmuaVoZhmNzf379LxiuKIiQVFx5CCEm5X5vneY6iKGNZFpNHxL7H51dVVSdJksVxnMRxHB4Oh10YhgdFUeTNZnO3LMv48PDwuN/vd4QczoqiKOZ5bnxXAcA5BEGAd3d3t+M4TmmaZhzHAdKnyx3HsYdhaGm/7AIlIHSqAidxFEXR8Xg8bLfb56ZpOtd1bcdxXFVVVUEQRPrnm6apjsfjMU3TGADAEyKaQkrFLyqT8TzPknOQFwQBkN6uZ1mW3XVdR7KvOIqimJTBaQtCfnp62l9iY8RxnGZZVuR5nkVRlOz3+10URbGu6/JqtbojG5NDCA1d1/U0ACEH+5v6c4ZhqIZhmEEQeBBCSDNGlmUx6Ys9YIznDx8+3LMsCxiGmUk0jlmWZU3TNGRZFkm7SHlNBjzP80z/SQ5k9ncO7M8H5pf//jUgFziEEIqSJCkAAJZUGvq+7ztaoWjbtmnbti7LsrEsS3ccx7Usy1JV9U1lSVVVFUVRFJJx6YIgCIT70jRN00qS1FZV1VRVVRdFkdd1Xeu6rk7TNJLsbUII9Qihvuu6bhzH2bZtm0woqKqqKo7j2L/z+SqptngMw2CWZdntdrsLw/AoiqIkiqJMKwBpmsaHw+G4LAu+ubm58TwvsG3b0TTNeOm7BwD8b1mWB03TNM/zzL7v/bqumyRJMlmWVVmWZcMwdFmW1bIs66qqiizLMkVRFMdxXF3XdVmWVUVRlNc+e9d1md1up5IWzkrXdf18XdE2F0Joruu63O12zzzPCx8+fAh83/c9zwtM0zQVRVFfOynE8zyLMeYI3wgIgsBrmqbf3d3dC4IAIISCaZqWoiia4zgXJ8H5vu+kaRrbtm2R9V1ut9t9URSpruumZVklIQfeXHLq4mz/QV3XVdu2jb7vV5qm6RjjJYqikASzAgmSXxxk/tH7hwTQXVEU+X6/DwEAwmaz2QRBEGCMF4TQ9Pj4+BzHcWaapoMQGr/khHw3AQApfTGKosi6rps8z/Nt21ZhGMbDMHTTNI0IoflS5ajD4RAWRVHQXvTxeNw3TdOt1+vVarW6cV3X0XVdkyRJxBizwzD0ZVmWEEL48PDwKcuy1DRNsyxLozyhekkAQC5+jkbY4zjOCKGRsv6zLMtVVdUghNs4jlNJkmRJkjTDMIymaepLPANa6i3LsirLMouiKFYURb6/v//BcZxA13UVAMB1XYemaRrneZ5lWZYtyzIMw1DfQtIJgsAfhgFxHMeZpmlP0zQyDLMghObj8bhflmWZpmnmOI6/vb29EwQBIIQQyZ6xqqqK53n+W6YAzkqrC8YYv8M+/ypkWZZkWRYlSZIURZEBAKCu65Zc/n3TNO1pQqxpq6qqMMbYNE1T13XNMAzTdV3rLS0YwzA0WZZVSZIUVVVVTdOUhkYcTdMpitL1fd82TVP3fT84jmMQUiYax3Ga5xmR9dP3fd+yLMtqmqaRwEJVVfV3SVx3d3d3hGT7uQqGEOqjKEpFUTySduCc53kahmHYtm17c3Oz9jzP9zzPIxMAryLh3t7eikVROH3fd1VVdb7vt23bdkmSxJqm6aZpGtM0LW3b1lVVFX3fjx8/fvR0Xbcsy7JM07Teeim6ruuN4zhCCIVxHBHLsizHcRzGmEUIDXmeZ2ma7h8eHh4RQuj29vYmCILAtm3bdV1vtVoFrut6ry2Rk+oLRyohlHC4SJIEN5vNLYRQtixLW61W3ntcwIIgQNu27bqu66qqKsuy7DRNszRNC3L516Zp1q/heLzgDAqGYRgFQZCGYejDMIwOh8P+zLKeJS2p9zgYOJoAkqqubZqm7nle4HmeR0dvSf2flWVZEUURQgiF7yIAWJZlGsdxYVn2vITKMgzDYYwX8t8nlmUxx3GA9kFYlr3IyyDrrsrzvEySJC2Kog6CwN9sNrfr9Trwfd8zDMOSJEnmeZ5tmqY9Ho/hMAxjVVXV09PTc1EUWVmWDhkaaN9SEqeXEcYYe54XGIZhQghFjPFcFEVTVVUxDENL+qvDa/rev4F5HMd5mqaxbdt2GAZ0c3MT6LpuKooiETYyZxiGJggCAABAwk2w3zqWouu6/pe//OVe0zS167p2nucZITTt9/s9OSAAQmgiHAB2s9nckHLowrIsBgBAz/Pc9yQKkUNxmud5pkECz/PMebb232fFy2CapgkhlGVZJjGAIldV1fR933Zd11EeQN/3TV3XtSiKgqIoGsn6VMuy9Dc+f8MwDEVVVYUEARppbzWkx911XdfVdV2zLMuu1+tNnuc5GY2dh2FAVVV9rk5IkiQqiqLKsiwriiIriqL+gwyUv7+/vyeBHiJB5tB13XA8HiPKB8qyLIqiKLUsywiCwHccx3Fd1/V933tLFczzPIc+a4RQ37Zt/fz8fMyyLLZt22QYhqOZqSzLUNd1U9d1zTRN3XEc6617b7PZrAVBAKZpGqTvxCGE+jRNkyiKosPhcNhut0/jOA53d3e3vu+vT8m/5zqOY/u+v3qPqShyhgmbzeYmCAL3vchvJBC7LcuyME4wHcexySx8Qc/nqqqq9woAdF3X//73v/+v7Xa7fXp62mGM599q978HBcB1XSuOY822bXMcx7VpmiadQvJ9P1iWZcYYz4qi6AzDMJZlmbZt25ZlfZPxyHcPAKZpwgzDLNM0LdM0zYIgAEEQYNM0bUpAmJlYlmUZQigIp6bxmyMghBBDk52u66o8zwtRFKHv+67neW4QBKv1er1yXTfQNE1hGIbNsiwhh15VFIUpSVLUNE2LEOoQQsM4jkNRFOUfzUjJfPsyzzO9WD5H4xhjDADgJUkSBUEQIYSAZEsz6T0zlJz4FlCyHcaYAwAAnue5sizLw+Gwy/M8o4QkQRBEVVUV0zRNjuP4ZVkWhBB663ywLMsynbnvuq5/fn5+4jiO4Xmelt2WZVlmUvbibm5uNqIowmVZ3kWU6feCMnJRTRhjlvTeedLHflXAByGEqqrK5xdmnudV27btOI49ya4bMgbbkfK6evrjimJZ1pvLsoqiqKTvraiqqnIcx5OR275t245+B1EUoaIoSlEUBS1DkiChLsuy6vt+WK/XHg1QFEVRf6/8f/4MHMfxTvHOgEiVb3x8fHw8HA5HhmHYKIpCCKHg+35gmqZzSoBdd7PZvOlSCIIgIL+xb5qmbZqmJqz/oq7rEgAAyrIs67rugyBwVFXVdV3XiAaDd4m1RcaIPYY5jaSGYRju9/swjuPocDgcqqrqNpvN2vf99Wq1Wq9WK9/3fe8t+gd/EIsgCPx7Xv5nlSiLZPwGrQIUJ/ZlTTkw7zGF8MU6FL9IRL8JXNf1EEKTKIrSOI6I53neNE3D9/3VNE0Tx3FgHMeeaD/Iq9XK9zzPZ/6FAc4izZlcgjMmN1HXde3z8/MDCQKyPM8Lz/Msy7IsRVE0cvi9+dBP03SZpgkhhNAwDH3f94Nt24aqqrphGIbruo7ruoEsy3Acx5HjOP7ULtU0RVFk6QSRjAqOVMPgS9GOP5BZTuSCm1mW5SRJEhnmJJAyDENXlmWd53nS930ny7LCsixgWZbjeZ6VJAleYOFDURQlTdNk27btruu6JEmyqqoeIISAEnVkWRY1TTMsy3LJbPDMcRz3008//XTJkjhZBp+Z0FQHgDLWAQD8H7lY/mDwc37J/6q3/8V7ojyBha5TIlhESVyvLo+S7Fsm70ABALAkMB2apumqqmrIjDyjk8Y6+Yd6iVIgrTzIdGPJskTY0UPf933btk3f94PrujapCHHTNKF5nseu6/qmaZqyLCtBEHjTNE3S/9dUVdX/aCXM932XcDtmuhemaZr2+/328fHxkeM4frPZ3DiO4/u+7/q+73ie510i+FutVgHhOVR1XZemaVpkNj9jGEaoqqpgWZal2T9pceiCIFxUGGe32x2IDklM2pHHNE2L1Wrlrlarte/73mq18m5ubtYfPnz48C3KwOf6J++J29vbTVEUOY2uDMPQ0zRN2rYtSUuqbpqmfs8AAL+2jPdGeJ7nqKoqZ1mWIoQmOl5Lx4rJuDHCGLOKokjfIiD7ZgEA6b3S5GrO87ztum4gzO6J4zjW933b9/21ZVmObdsWIc24F3jh8zzPCylljgzDLJIkiYT1LyuKorVt2+z3+y1CaIIQ8oQQI4qiKAiCIEAIBYzxciKRjwv5HX94IRHG+UIul0kURWFZlmW32z1TCdy6rss4jnMIIQiCwDMMQ9M0TZZlWbrEIXRzc7Muy7IchqEdhmEmQi9G0zTNOI7zsixz3/d9HMdpHMf5er2mgiVQlmXFMIz4tXPov7k4AOBJ9s9zHAfmeWbIbC7GGONxHC82jkT4F0R0DQCO4xhS5l/OyIHLdMJMW1IAAA6cQMWJXt3ykSRJVhRFUhRFkiRJEQRBHIaho0x8evjJsixRYhoJAi4iEe26rnU8HhVVVWU6Apemad73fS8IQkckqrGmaRoAQOJ5HvR9j8ZxRF3XdVVV1W3bNqQyYdAYwDTNFx1UhA9B956sqqomCIKYJEn+8ePHO1L2Ny2CS2lhyLIsm6ZpFEWha5pmOI5j53lehGEYcxwnlGXZGoahqqp6YjUqinbpQziO4+x4PEZRarkNGAAAIABJREFUFCVZlqVkFjy2LEsNgmDluq4XBIHv+75/4csfn5e4WZZlRVEE70l8+xoI618lAYCRpmma53lpWVZNOQKXqDj+TpWPYRiGJWqktLL3TYIC+YTfbGO+ld/0T18BmKZpIWNcjK7rKmGDL5IkiZqmaaZpWrZt24T56rqu+25lL5LcYULIG47H4zGO42gcx0kQBOB5XmuapsXzvEBL1MuyvDp6JONntAqytG3b/fLLL7+QuVs6+8sZhqF6nuevSP3Ptm3bMAzzEr+ZqFyJpml6HMcB0zTVPM+rvu97QsyaEEJdWZbF4XA4RFEUEi0ErWmamki4XiwAIGOBPCW+kOiQ9uDnS84jk03+mfhPA0IaBDAMs7Rt25DSNCKl75GOLoEThR+8hSDluq59PB4PoijKiqJImqbJcRznhFnX1nVddV2HVquVL0kSSUBV1XGci2VDpA0hES6CvCxL1vd9DwBoCfcA6rpu0OC3qqqGzrCXZVmM47iYpmlYlqXSCsA/kgD+cg2eZBhOSmhRFMXH4/FQ13Vr27ZJphAKwzD0uq6btm2bNE3zSx2Ovu+7RVEUpN1cB0HQPD8/b5+enh5ZlmWDIAgMw9BVVdVN09QumYnO87wkSRLneZ6kaZoSzYMjAID1PG9lWZZLesLeZrPZXDLzJ2t9mmkm8t997vckvn1tDeq0faRpmq6qqlKWZd22bVVVVd00TZ0kSfJeXIAzjhkPAOBoVYC0aOdLtFuvOAsA5nlm5nnGZBZ7mqZptm3buL29vaeqB5IkSaqq6o7jWI7jOCQavkgAQBmwdAaU53mOZF193/ddnuf54XA4Hg6HXdM0raIoKiHqLKTfP5JskGVZ9tUXAKl2LJTYNo7jRARguiAI3M1mc2OapmkYhuWeGn++bdvOJTKgeZ7nn3/++efj8Rh1XddgjFnDMEwqMUr6/GPbtm2e5xnGmPnll1+e2ratSetkumRG/rXncx5gXXIkl86Ck3UAifb6RCo6IxGYaWmLiGBUVVUh43sChFB460UkiiKt6MiKoqgY46zruqau65bqUegnxp5Cs6RLSqSSUUoRQijKsixxHMf2fd8zJxJcT8RYdAihIIqimGVZ2bZtz3GcUFVVqSiKqOu6Kcuy/hqTok+fPj2EJ0RxHMdhGO7iOI4cx3Fs23aenp6ewjA8Eh6QRCp14qUCACKo45AqWD9NExqGYXh4eHiwbdsyDMPUNE2zLEvTNO2iBKztdrslY45pmqZJGIbHpmm6m5ubG9d1Pd/33SAIfNu2XTp2+Fai8W+chZgI3SznbbB30r75aiUqSRKNVDg1XdeNoih2hFPVkHHMdyMD/lZLACH0mfj7Z7UIvtsA4DcWIcucVI6UzWaz4nkeQAghqQKYtm07lyyH+L7PPT09iZIkibIsy6IoSnVdN0VRlFmWZQAA0DRNRRSCQt/3PQghz7IsV9d1XZZl2TRNfdIskWQIIQQACNSo5gWRJ0vJf7quG5vN5qau6zrLsqJpmobIlC6EiAfFE4TD4XC4vb19kxnR8/PzluqxZ1mWEilW8+7u7s51XY/nebbrurHruhIhhERRlDiOY8lhgS99IZ9VYVhiBgV4nufo3ntLteUr1QYoSZLA8zyUJElkWZZr27arqqprmobOhYt1XTdkHK4Zx3Em4zjSiT4him/9HoRT8pkICCEEhPzXtG3b6Lou0+yI4KIOkWT9QwJJFEWh7/uByJBytm2bpmlq5JmJRJCpr+uaadt2cF3XJaVbTVVV/SXB6cPDw0MUReHhcIhp5n88HiNVVTWi9Kj1fd/vdrtdHMchqVKItF13KZc0Il4kCoIgEJ4NKwiCZJqmdZq8POGSrPsz5c0kiqKUSE6nhATme57nep7n8jwvZCcktFoqiqJIpiKC7+ViIJm/RlpchiRJERmv/mZkwCv+pADgvAQvy7KyXq9v6GzUe7BdeZ5nVFVVZFlWSXRvPT097aIoCmVZljmOo2Q7fhiG/kybmWnbtj4ej4e+78e7u7uAcFdUVVWVl2jCExU2Snhjx3EcZVlWPM/zTdMsaD/w6enpua7rar1e99M0DURelYUQwrf03/u+76uqqrMsSx8fHx+nU537FgAgEDtKFiE0ks2XN01TsSyLBUEQ6Xe+dJuQCiKxLMthjDlShqOtkumSpTjxdOt95n2oqiqRSnAex7FKg4S6rossy8o8z3PKjKZM/LeIwVBYlmWcTQLIiqLITdP0oigWCKHJdV3fMAxaHlUvbQhFyLUiqQRIkiRJeZ4Xy7JgysvSNE1DCCFZlkUAANe27cDzPCKCTCYlyNJA4Q9e/s+Hw+F4anlHRIvreIAQws1ms/F935ckSem6bqjruomiKJMkSaElAEjGgi6hCkoMwIo8z4uyLJMkSRJFUSTLsmx6+V86+4+iKI3jOEnTNM2yLA7DMIQQwvV6feM4jqcoipxlWZEkSfpl4CtJklTXtTNN0/KeBmnfEqvVykuSJNZ1XSVaF3ocx1nTNBUVw6qqqroGAN9pAHBWelokSRLf2/TgVFk/zZ+6ruvSyQOe57l5nif7VHbwxnEcnp+fd7vdbtv3PSIHRRkEget5nmcYhmlZlmGa5qsPiGVZGEoItCzLNQzj86FKPAqyYRjGeZ5nMh4okUzhLf13TMt/Xde1aZrmJAPCaZqmPM+zRKmqTZIkjeM4UhRFMU3TkiRJoZWT93xHtC//HoQcx3GsMAxVquRo27ZTluXz8XjcMwzDtG1bAQAEopW/r+u69jzPM02Tkt1UwzDeTAijDHxVVWWizKfneZ4/Pz/vZFlWdV3XJUlSyMF4cRYwz/MchFAiioSiJElK0zR7lmXZ29vbW13XNRLoYEEQIABAKIqi4DiOPTusNVmWX6QYl+d5HIZhfDwewziOj/v9fo8xZjebzcbzvMD3fR9CKA7D0HVd17Rt24RhGEqSJJGWjSAIgui6rvMWchj1viCXf5EkSdb3/fjhwwefiP6YlmUZl7x44jjGZVlWRVEUhPUfLssy397e3um6rg3D0D4+PjbjOI5fXv5EcEkl3CTBtm3zNRMRtLRNpa9J+Z/9oir7zcDzPE8mXTRCtjTjOP48EkjGr6uqqqpL74N/9Fu/1w7AmdHdV8+Gdw8AyCJczlst36LnQsZPyq7rWoTQQJXooihKm6bpHcdJFUXRRFGULcsyn5+f93meF4IgSJvNZn1zc3Pj+35AenTOK0vymF7EBAsAgHNdd6MoihzHsSyeqLkCJUgRO1ZEphde/yJOJyiUZVmybdtp23Y4HA6HoigKWZZFhmG4aZpOji19PxqGQVnJrm3bFrk0L5qNLstCe4+YZdmFqqO910Y0TVMvisI0DKN2HKcdhgFFURQ/PDw8KIoisyzLz/OM6rruLcsyV6tVYBiGRcbCzEt5luu6rpLWgqJpmkZkeYv1er0m/X+NVKveZQxIlmWJyO+KJMOGGGNWkqTPzPxpmkYIoaQoivzw8PCoqqq6Xq9vaNnWNE3tj15E+/1+qqqqLcuyiOM43m6322mapg8fPtwR/3XPdV2ftENaMqffbLfb3eFw2AEABFmWJcuyjDiOs81m82pFxGEYetL+q4gqaGaapuK6rkP5N4ZhWJfUnCAyyoioi1ZpmmaKokjDMAyfPn2iRGCWZdl5HMflbH9gURQFUiGVLcuyqqqqX/Pd6MjlsizTsiwTx3EM0QLhBUEAlJ/03pa8X7QBKAeAii4opAdQVlVVEV+K4tIBwDRNiAidcTzPA47jeIzx58rj75la/Ssiy7I5iqL/+kd3CKnynov3zfM8zxzHCbZtv0qJFNASPCmDswAAwbZtS5IkiXmDscVrEASBR81XyLgZDyEUsyzLnp+f99Rwg2VZbFmWTvrQE7mUWBqsIITGMAyPQRD84QdymjzjADFDMURRHDmO46dpWmhmBSEUiT0uDyjzTBAgIUpyb7x0NEVRDNu2XWKrC8uyzKuqasqybFmWxTzPc5IkSZ7n+ZZl2ZZl2d5/w720EA81veB5HkiSpN7e3q51XTdJfxYIggAu+Xk3Nzc3xACnJ5ucUxRFqaqq6rpuYBhm4Hke3NzcrBzHcYMg8IIg8E4xkG1d8ODTSbavklK/gTHmDMMw6Pz56X/q2nvsAwCAdMYDEDVNMzHG85lZDH82sqgoiqIYhnGSEiSVqhceypiOWxKba3xzc7PyfX/l+36wWq38IAh8lmVZojZIxBGHIUmSouu6dpomRC7KNx3ONPAmSpQDy7KcaZo2IcSatm2b71GNJFUtTN6/yrIszvO8YFmWBQCwXyPhkUGVhe6XCwXen7MvAAA/z/Oy2+0ORVHknuf572FB/lvwPM/Jsiwhl79uWZZVFMVzlmWZ4zhO3/c9sSK/CMjI57EoinpZlgkAINDzfpqmhVojf084HA7/bxRF/2e/3z8ihPp/cOdygiBwZ2sPz/M8CYIA+75vBUHgXipM9PkAp2QP0uuTAQDQtm2TlJfFb7TgPCI3u5DIV1AURbYsy2qapkEIIZZlMVFt0zHGeLfbPT8/P28RQmiapplE8wPR8+b/aFmeGnkQYRu8LMtiWZah67oqiqJs27ZFJxV4nudkWZYBALxpmo51oiSrbwx+Vm3b9izLLgAAXlVVjeqjMwwz09lgSZIg6ZHrlJC5Wq2C92DkUqEVy7Js6tEOABBd13VJu+biGfB6vV4TB0JWFEVomqbedV1HBDgWhmF4URQly7J00zRt3/eD9XodXJKUSiRmtTRNdcuyzPV6vXYcZ3AcxyFBgHGpasNvwbIs/Xg8yrquq47j2DzP8xhjTFjZKoRQJiIlKgl8WEmSRDL+ZxiG8aJ2FHENFGRZlh3H8YgQku6f4AVBENzf339kGIap67qdpmlACE0Mw8zED0EXRVERBEF8qx4GIRz/6rvQs4lYL9uXft4kmBVlWVZ83/fpuqZ7jrj94bPS/K+CBlEUVV3XNVEU5UsGhRzHcQihabvdPmOMWVJtnAEA4FICXH/gDDBoIYCMgdfEI+Hi4kTH4/FAXFCTcRxpIMkz3zGaphmKosjDMDzGcZwCAL4qggYA4DDG3Pn6G4ZhtCzLILbg1qsDAN/3XYQQEgQBDMPQkzl0Y7VaBS/JpN+KzWazBgBQD3KRZFsmQgjR0g/HcTxVSSP/t10YhhEpo6FxHGcAgEAOsT90EAZB4DdN0y7LshAnKqxpmmLbtkO0D1wyVcCRXrTDcRynKIq2Xq+DS2jg//DDD/fEB10yTbOybbsZhmGYpmmhVRpSoZA0TVN1XTc9z3Pei32s67q+Wq18lmUXVVWVeZ5nnud5YpC0eo8eOPUkoLbOtm03Xdf1pCXEYIxZMguvmqZpEflW/x2+h+U4Tj1NEwIACNR4yXEcz7Zt652lUAXXde1hGDqGYTjHcZxlWbCiKJJt247rulbXdZ3jOO48z9hxHO9sQsCyLOtFwZDv+1ye5w6VP53neeR5XvB931uv16tzdn8QBP48z9M8z1gURX4YhlmWZeicYL41O12tVgHRM/jVd3Ecx7Ysy3oPkp3neWyapvY0TUiSJKnv+/FcToKoS7Jfq1gIggBd13V833cu3JoYnp6eHok+iIQQWomiKFZVZXyrAODm5mZVlmVe13U9DENLq5+maZqqqsrUnfWtCMMwzLKsiOM42m63u7quayo//rUL8XsAqWjMCKExjuMwy7L8t6rJpAIMvlx7pCsoUTv2V7UAyCHgsyzLG4ah930/AACAruvqt5r1/OIyDkivUzUMw6jrupmmaSBCRQyEkCeM8JKqRe33+10cx+k8z1iWZaltWxsh1P/Rvpksy/KHDx/uyAHQzvOMNU2TKSmRYU5ENU3TlCiK9K7rBo7jWFVV1dVqdbEL+OPHjx9s27bSNC1I1WNgzlTCeJ4HiqJIsixrq9XKf++e4GazWYuiKNZ13TAMM7Ms+yt5zPeALMvyTz/99KNhGAkR/+mGYfhcWib+9Op6vV69hzMaLX8uyzIDADhd1w2yAQXP8+yPHz/ev/ce2Gw2G9IC0Uj7A8uyDE3TtG5vbzdJkmTzPM+qqirjOE4cx7GSJKlEpvbF6/H+/v6eZVlOlmWNSEvzpmkaHz9+/HBO6iPPhaEyqcuyLIIgAMMwDM/zLrIPfuu76Lquv8Vt8Y8E36IoCqZpWgih6XxZkTXGfq11wHGcYFmW+Zbg5GysFpPLf354eNjSg973fVfTNGMYBnSWHX8TOI7jkjXImKZpEcdQ0/O84Pb29iL3A7G0Rm3b9nmeZ8/Pz7tlWTDHcew0TZNhGCqpzH7XMgBUffW3/tt5u4msGSyKIn5LgAS+PPTe2dTiRZmgrut6mqY5bTie9do4nue5OI5TMiPMkdKhSIhqgGVZHmP8ot68rusaLeF9LXCAEML3dryjv51hTj7VM6UGk8vxW78L13Xt97zwfyczdRmGcen7ODuQvwkRKggCX9d1raqqmjkRIflvOfa02WzWm81m3XVdR9aeSH+767q2ZVnG8XiMGYaZGIZhRVGUX/ueIITwr3/9609pmuaEf8O5rmt95bk4ruuaaZqmxI/hos/lJd/lklUX6qhJn/f5nvs9hvYl1iNL1dA4TlBVVb29vV2dn/WSJClE6ZLjeZ79xvvQYxiGVRRFQQj1hA+j3tzcbC4VgFMZbyI6J/u+751PAxBvDoFII383FQHCG+EhhAIJqDbnZf4vMv7py+hnmiYsSdKrnwv4Z39ApLf7m5uf9BupYY1AytTYcRzbNE2dRI2vwrdk2/6jw5C54k97H0QbXP4zf/vXPp/nef4tjPvf2W9/5H3w79F6ec13+RbP+73Xn+/7Tpqmsed5DsZ48TzPO6s6YIwxZ1mWpaqqLkmS+q2fCTF+erfgV9d1VdM0zXVdG2O8kCrW59/PcRxwHMcm/BuN+U6gqqpomqYVBMHKNE2TxoFfqQBMvxEUYEEQoGVZ1mueC/hXfniEL0AJTFJd1xaJFlVi2vHdKHNdccUV3y8EQYBBEAQYYyzLskyUH1mGObUHAACMoiiq53n+e7ZC/ixQvhHDMJhKvdPfjzHGHMdxZ8RU/3v53ev1+v9CCGdRFCuEEKLS+79x+ePzSvBZ5QTTMcDXPBf2e2iodF3XRVEUdl3Xz/PMqKoq39zcrK/Z8xVXXPGvhDzPi6ZpqnEcl3M9HI7jGFIa97/n358kSVbXdYMx/tXvZ1mWJVMa/7bKg19rQ72lOsVejRWuuOKKK6644t8P3PURXHHFFVdcccU1ALjiiiuuuOKKK64BwBVXXHHFFVdccQ0ArrjiiiuuuOKK7wL/YwwwDMNjXdctxngRBEFcr9crCKFwfVRXUHRd10EI4Xup8P2z/uZpmmYAAP9n6wK8N6j41L/bO/5nATFCQjzP83/GJFPXdV3bth3DMO8uwHTFn4tfTQE8PDw87Ha7qGmaalmWWVEUxXEcZ7VaBa7rOv8OD2S32x3oGA4AgLcsS/+WXgj/zNjv9/s8z3OE0MBxHFAURQuCwPuehDm+PAjjOI7qum4QQghjzLAsy0iSJDuO47zEcOdfAVVVVWEYhlVVNfM8LxBCYBiGcXd3d/ethZiI+iLzZ64thNA4DMPwLb/Dw8PDY1mWJZEj5nRd1zabzUaWZelbfP7j4+NTkiT5PM8D8SVRiAjQdz1++G9fAdjtdofD4XCMouhANPVnwzAMhNBABAi++2jw8fHx+Xg8HrMsy6gdZVEU1jiO03vL//6zY7vd7na73S5N06Rpmg5CKOi6rg/D0P/973//X9/b743jOA3D8JCmaVbXddV1HcIYLxzHsZqmaV3X9QzzWSb1u8DT09MuiqIjMeOZIYSC4zjOPM/Ljz/++MO3+A5hGIZxHKfEA4OBEIq2bdvfUvymqqo6DMO4bdsKITRDCAXDMMz7+/u79/zcn3/++Zfj8bhPkiRDCI2CIPCWZdnLssx/+9vf/vYtLv/9fr+Loihu27bjOI43DEMfhmHkef6bORBe8ScEAF3XtXVd13EcJ09PT0/DMAyr1WpFzDg4QRAEy7L077UkGMdxGsdxdDweD2EYhk3T9JqmSX3fI0mSRN/3g3/XVsg8z3OWZWkcx9Hz8/O+rutKEATB8zxPEARhv9/v/wzTqPdCkiT5fr/fh2F4jKIoLoqi6Pu+o8Y09CBUFEX6XgKA/X6/L4oiDcPweDgcwnEcB0mSVIzxIoqiXFVV+Z4WyPTyf3p62qVpGpVl2ZIKgEIssfHNzc36W6z1p6enbRiGYV3XBUJokmVZtCzLmud5/PHHH//yTmsuy/M8PxwO8X6/3/d930AIpa7rBgAAr2ma9p57DCE0ElvaZLfb7aqqynmeB77vB6IowrIsjWsA8B0HAAzD4HmeF4TQ3Pd9H4ZhPM/zzHEcDwAQBEEAsizD+/v7H77HB9G2bVPXdU024SFJkvTm5iYgDlx9WZa153n/lhsgTdO8ruuuqqqmKIr08fFxa5qmrmmaMpwwfk+/N8/zNEmSeL/fh3EcH6MoisqyLIdhGDVNU8nFZLRt23Vd130PnACE0Nh1Harrug3D8JimaXZ3d3drWZY1DENf13X33gFAHMdpmqbR8/PzviiKDGPMWJZlLcuCBUEQv0UAsN1u92maJsfj8RBFUYQQQqqqKm3bIkEQBF3Xtfcoh0/TNCGEhq7rujzP0+12u3Nd19E0Tem6DiGE3nWPVVVV932P+r7vqqoqfvnll0dVVRVFUdS2bbthGPrrdfkdBwCCIEBZliXTNLXVanXDsiyOoigVBEHgeR5IkiRKkqTqup59j5HgOI4jQgh1XTfUdV1WVVXf3NwEDMMwHMedG1P9O2JhWXbBGM8IobGu61rTNIU5KXR+V8+mqqr/n703WZIcybbEoArFPE9mbh4eETm0CH+LC/InSOGGi/4m8hfYC+6ecFGvYnB3M8M8jzqAC1dkxcuukteVDovKyvIrkqtIcRgAxR3OPffcpqqqpq7rpqqqgiMBqaqqimEYqizL0raNUhAE8EdExBhjrO/7cVmWhTFGGGNUEAR26wCEMZ6aphnqui6/fPnymVLKPnz48NE0TXOapjHLsvLWSfiyLEvf92Pbts3lcjlXVVUeDoejLMvqMAwuJ8ftbgCAbSOgIAgCG8dxIoQQ/m/rlqS1bdut6yrYtm3eCpGklArTNM2yLCNK6XdVil2WZamqquZJtvmPSK63bZB/dLLvf0gAoigK6rpuCCEEQigKgsCmaZrTNE11Xdcdx7F93+85HOd9j4OQ53kxz/NMCKEQQoAQklRV1Q+Hw80IiQCAlVJKJUmCG/IhiiKq67qq67rSNE09HA6HWzv+tm3bsiwrDj0LAAABISSbpqmHYRjempS1Pf8tCPDAsAqCwF62lsJfrn8LNemqquphGPppmmbG2CqKoiCKoqRpmnpLQlLbtsM8z2Pbtj1PBkrTNI3379+/tyzL2vqihmHYlmWZt2Rp53lezvM8b0RdAABQFEW5xWpmAABECEHOPJf4+Vp5AgzHcZy/fPnyJMuyqCiKdotNfdte83VdBUopI4TQdV0ZXzn8XVTLAQArD7grY4zN80z5SuKVMSZ8J+V0CF5umBJCKP8dy5/+9Kc/9X0/AABWRVHUIAjC+/v73VERCKEAIQTffuN/ZQ/N7nY+n695nmfDMPSCIAi6rhsbB4FPpcmmaeoYYzxN0ywIwooQQrquG3u04rIsK4uiyMdx7Akhq6ZpsqqqOsZ4xhhjCCFSVVW1bdsOw/CmpPhxHMeyLGtK6cIYEyCEAkJIPhwO0Z6x55cEQNM0/f7+/oQQgowxtizLNI5jjzG+YowxpZTyAHDzL+D5+fmpKIq67/t2HMdpWRaKEAKyLMuqqupVVRW+7we3qga+LWlFUYRFUZRpmmaCIEDTNLWyLEvXdf1bfHzbhxDHcdp1Xd33/cCrL6goiqzrup7neXFLZu7T09O5LMuy7/t2GIZ5XVe6JQCEEMIhWYQQEvkO892SEUope3p6eirLsuy6bksAKN+DLhmGoRVFUfq+v+t6Ukope35+Pndd1+IXW6ZpGggh6/39fXA6ne583w9EUUSGYehBEPiu697ECSRJklRVVfH7nwghbEOiNE3TqqoqPM/z90TieMCHv0a7AABwWRby9PT0yJEg2bIsveu69sOHD+/3uv5W7TmOY4Zh6EMIV8bY6rqua9u2qaqq6jjOzdn4kiTJuq5rpmnaDw8P76IoCk3TNPgqXtMwjO9SFfJsZ3vvsOu6hvuEDgCwGoZhYYwxAADsTZDkG+aAKIpIlmURIQQlSbppwZGmaXa9Xi9xHCd1XdeCIAiu69p1XVvjOA6EEKZpmqqqqk4IWeZ5nvmkimQYhjkMQ//x48ePrwi4AydAJnVdt+u6MsMwNN4C6THGGCGEVFXVXNd1l2VZbuX/v3z58tQ0Tdm2bY8xXiilK0JIkGVZKcuy8Dwv2Oud/wcdgCAIfMYYG8dxGsdx6Pt+lmVZ03VdsyzLkCRJQQjdrOJZlgU/Pj5+TZIkK8uybJqm4Rv+MIQQyrIs67pudF3nzvM8AwDWW44nAgBgVVXt9XpNAQAMQogsyzIsy3L6vh8JIeTDhw+7MoPP5/P1crlceR+26Lqu24iYiqIohmGYjuM4y7IsHLmJdj58X67Xa1yWZVFV1S/PHyGE1nUF8MUAAADyLZ1AluXdnMPXr18/X6/XOM/zoqqqdlmWiTFG1nWFiqLIhmHoruu6nIW/7kXC265b13W9Mf4xxliSJNE0TVMQBLGu61aSJAQhhMuybND43h//1yRJ0rquq6Zp2nmeJ0opXddVgBBCwzD0ruu8tm27ruv6vc4fh/oJIYRgjCmvujfnfO37fljXFfDd49bWE94zCUAIiYqiKKqqGrZtr5IkQcdxHM/zfNd1ve9BwrVt23Ycx+m6rqeU0mEYBkVRZEmSJOFlP/13gQA4AiC+vAawZlmWXy6Xc57nBfd7kSiKUFVV/cYTEoDEwtNVAAAgAElEQVSjQzdFPLuua6qqqrIsi+M4jiml6zRNR8Mw2rqu62VZqGVZumma5jAM0zAMIwCAKYoi27btMcYIhBC8f//+w29MQPKqqsrL5XJN0zSjlJIgCDyecDdclwHZtm1sE0CyLMt7IwGfPn36cxzHSZZlZd/3zTAMM2+/QtM0Ncdx3Gma5nVd1z0SkP9OCCiKonCapgFjvAiCIC7LMsuyLLmu60VRFBwOh5vBr4+Pj1958EvzPM+rqqqGYWiGYZgRQlDXddO2bXscx3Fd11VVVflWCYAoipBSSh4fH780TdOs60plWVY9z3Nd1+0ppUQQBKCqqnw4HA57XDPLsiJN05gzseMsy5I8z/N5nieEkGSapuE4jrP1qGRZlvdMAL5+/fp4vV6366dVVZVt2zYY4xlCCB3HcbbESPzGAABoj+t//vz5y+Vyifk4alqWZdF1XYcxnhFCSFEUbbv/dV1Xfv+vTgDKslyzLCvP53OSZVk8juPIGKNbO2xZlvnr169/ppRSXdf1IAiOfEbb2BOF2JKvOI6ToijyqqqaYRg6jPHCGFtlWZYdx3E3wiqllGmapt1gRSoDAABJkhROjC37vm8FQYC6ruvTNAWCIAiKoqimaZp7IBFVVdV1XTd5nudxHF+LoshVVVUFQQC2bTv0e2DQgiBgjMk8z1Pf912apkmWZYmmaQZjjKmqqrZt6916JH7r+YuiKKqqKk3TNGdZlqVpmn79+vVRFEWEEFKCIAgopZhSyl7bEvzPeDyM3ZQCIkzTNA3DMLVtO1wul2RZlllVVbWu6zqO45gQsriu69u27bRt2zRN0wiCwAzDsMIwXERRhAghxTAM67ecx2VZ8DAM8zAMQxzHl77vewihUBRFlSRJPE3TIMuyGoZhuK4rUFVVsSzL2jMB+Pd///fPcRzH3AdnbdtW/DvHoihKtm07vu+PvB2yFaPWrgmAIAjC+/fvP6zrCnVdN/k8KnIcx3p4eHh/qwPw/Pz8lCRJliRJuukRxHGcYoyxKIqIZ8GF67o2pXTVdd3wfX/AGC+SJO2OSlBKWRzHVw4HrRyWq+q6rk+n0wIAgJIkyRwO3qUnX7xYmed5lmVZ8vT09DQMw8jvf+ZOshYE4ZdZ9L1Y6Hmel0VRFEmSZHEcJ9fr9XK9XmOe+cJ1XdeyLBtFUSTej0KiKEIAANyjBZBlWZbneZG/3Pwv12eMMYSQyHvCheM4zTaa5jiOk6Zp/toAOE0TmaZpHIahu16vSRzHV8YY5a0v9vnz56+EELau6xoEgWcYhjnP87Ln9MOW+CZJEicvdk3TNOPZPoAQgnVdWdu2bd/3HaWUGIZhtG3b3mJHOkIIAADW5+fnp+v1mlBKMWNs3c6arut627bOMAzDHgkA73VTQggex7F7eno6u65r+77vzfO8YIzJ90gAGIdBOBmwP5/PSRRF3vF4PBBC1nVdmfCdDEIoAABAmqbF9Xq9Xi6X52EYhtPpdCfLssy5WnAvf0cpZeu6rrcO9n/j/a+CIDCugjhDCIV5nuc4jhNOCmTzPM993/d5nueEEEIpZaqqNqIoihsy+FvPIwBAEEVRAAAIjDGyLMsyDMOY53lW13UnyzJq27Zf13XVdd1wXddpmqZflmXZgwfEx42LNE2zNE2TOI4vSZIk0zTNW+7reV7NE1FZ13WjKIryJgkAh/YeBEF4aNu203VduzXprSzLqizLMs/zPE3T+Hw+nxFC6OHh4d62bRtjvHz+/PnzOI7jN0QdYZqm3RMAAADouq5njK1hGPqWZTlbH+56vSZJksSqqmqmaRq2bTt5nuevRUa2efO6ruuyLKvr9RpP0zS9f//+3rIsj1JKiqLIh2EYGXvhRW2Oc4973tjvHIbLLpfLVZIkeDgc3um6rq/rSquqqs7nc6woigz/YiIAQNzj+k3T1FVVlXmeZ/w60v39/Z2maQYhBHMtgoKPLE3LsswY4/m11+YwK+BOAMgvJrZt24uiCIMg8DkiJGiapqiqqoqiKCGE0I7nv66qqkzTNOfO/gwhFE+n01HXdZ23o+otKQqCwF+WBW/8jFu0v+q6rquqqjRNU33fv1uWZb5er0nbtu00TfPCbY/rreu6JQCMM88p/8bB95zC2dj425nYKvKXLswLKe87JgBwmqa5ruvq6enpqa7r/nQ6HU+n0ymKIv+FHmEbexQfnOjICCGUvBjbkdrzd5skSaiqqqLruvbh4eGOEEKenp7OwzDMlmUZYRgGbdv2l8vl2nVdRwiZX1zC688jf/9rlmXpsizLhw8fHkzTtC6Xy2PXdd08z+M0TQshZGmapg/D8NXxh39rdV3XVZZl2fl8vgAAwP39/Z0kScowDF2WZUWe55lt21Zd123TNO1r0Z//1IF9DxnMOI5Z27Z90zRNVVVVHMcJhFB8eHh4fzqdTqqqaufz+cyhl80URVHkW/y+dV1XTdO0jx8/vg/DMNoSgKIoCkrpGsdx0jRNPY6jP03TMI7jsEMAbLuua/kzKJumaYIg8I/H4ymKogNjjNi2bQzDsLiuaxuGoWmapu51/8PLUHvXtm1bVVXBGFsfHh4ewjC8M03T4PB3Ns/zMk3TtBEAOQfw1d6558bFqEoIofDhw4eHIAgO8zxjjHG5LMsCNsjhpSLeZQYxDEN0vV41y7Lsu7u7u8PhEG2s63Vd1yiK7hzHcblTBnwixn1t9v3Nuxf6vu/quu6apqmzLEsopcIPP/zwEEXR0bIsa1mWBWNMGGMrR1wAf+63iIxwGIZ+nucpDMPo/v7+wXVdt2mavCiKCmOMGc9C96Lmr+sqcKnldV1XSikVRFGUfiGcfKdgxM8TkCQJvkxAi5CvRmD8Xr8LB2BdV8AJyMXj4+PjOI7jw8PD8e7u7t2BWxRF4el0ut8r8eFth1984PcM+C+dRAAEQYAIIamqqkZRlPn4Yqc0TS8IIXQ8HsPj8XgyTdOWZTnmPhnzaQ3CW7OvTryapmk5x+v9+/fv3wMAYFEUadu2A6WU7X0O+r7v2rbt6rpuiqLICSHs48eP77kMtF4URTHP89J1Xcuno6ZpmuayLOvXkOGR8DuweZ6neZ6ncRynruu6aZrmu7u7QxRF4fF4vJvneWKMYfaCTUHxpQkm7TmC9evzDiEUVVXVwzCMTqfT3ebsmqap0zTNh2EYeAGE94An+74fhmEY+r7vqqqqEUJiEASeZVkO1yMQTdM0CSFU0zTNdV1/T/7DsiwLh1rnvu9H27Z127bd0+l0CIIg4hV6L8uyNE3TxJ0ygBBCTdOU1wbAaZrmcRynvu/HcRxH3/ddVVWNPM/TLMvSqqoaHqxD3/c927ZN0zQNTdP01967LMtCEAQBxpiYpmkMwzB8+fLlK/sGCw3D0NuSAEVRNO58dyFfdV2Ht3ZO13Vd3/fT4XAIPM/z7+7uToZh6I+Pj1+XZfmFfLQhELciZxFCVsMwtCiKouPxeNQ0TWuapuLkz5VSyjDGbA+HuyFZGGO6tVq2YMw1F0QI4XfxVRBC8RtoHX4TINcNJr9VzP82+EuSJHRd12dZlhdFUd7d3UVhGB749FMURdHheDze3YIYyRFWAAAQefvpeyQA21QRFEURLMtCjsfj8d27dw+apqlpmgoIIWSaphlF0UFVVaVpmgohJHEkDPPxwN/UluPoEx92YwwAAD3Pc8MwDF3X9bqua28lelIUhcARtWme56Hv+8F1XYMXgEdd1/V5nmdN0/RhGPJ5nhdCCGaM4XVdX/X9/S4SAEop5T0dvCzLCCEUTNPUdV23TNM0NtLbX/lYwY6H/hdIe4N2+QwysCzLopSuqqrKXBgJMsbYdlj2cILLsuBpmjBX1hsNw9BN07Rt2zZd13V1XTcsyzIIIUyWZclxHPt4PB52ev4Cxpjw/hvGGFPHcWRFUWRN03Tbtu2u61oIIbtFZTBNk4AxphhjsmnAI4TQ8/Pz16qqmnVdgW3btud5XhRFoe/7oW3bjqIo0l6Q7MePHz9KkqTkeZ5//fr1y7f3ua7rSghZ3RfzVFVV95yDp5QSXr1gPvWwqqqqa5pmuK5r87OxzPOM+dSFLMuytHcS/O39Kooi82TL933fm6ZpXJYFvxx3SnggZLx3u0vQ4ZdmhBACAFgRQnALQN8TAeDV6OYLRIwx4eeMbUnA3joclFKKMabLslDGGMMY4+fn59jzPPtwOERcIKniLPhhHMeJE7Vv2oKQJEmEEKJbTwH8NQSWj7oGhJAFAAAZY+s8zxgAsG4CdZIkQUIIXpYF88kc/Ft4URjjX94vxniTf3Ycx3ENw9Dqui4JJ4H86iy++vzzOmPFGDM+gky/mb6ztkQfIQQJIYz/DsYYezU583eRAGxBaHv5XPgA8owQSZIkAgD+wyGEEO7KTH3h1PwFWv7lASEEeFDE8zwTxhjlwjRAFEXAIcpXfRzLsgj87xJeBa0IIQkhJPP+OyzLsm6apiaEEFmWEWOMcie9GyWZMbZu/AouerJupJytMvn1O6OUrpti2Wvgx+2j4tfEaZqmAADkOI7j+77vOI7ruq4NIUTzPE9ZlmV8PA56nufu4ZDv7+/vqqoq/lqSs1WAt5j9/bao2FAHLsiDGGNC3/fdPM/TxndQFEWSXoQYJD6etnsFuK4rkGUZaZqmm6apTdM0btUPHxFkfAyS3igIgK0aXNcV/Prs3co0TVM4wghFUQS/DkwAgN3h32+DwCZEtq4ruLu7C47H4z2EUPj06dPny+VyVRRFFkVRUhQFybIsm6a5i1reX0Ngfn0sbvncue/bFEdXTjQWVVWV+r4nGweNJ0pkO5+iKKJ5nsctiV6WhfZ9P/29z4QXoRsPhXLNGcM0TVOSJLQlulwLhXGoYNdRYFEUf2m98PsVIYSQceN6ABC9MHQhJ4n+8ycAEELAlfdEhJAMAADLsmza2ANjTOAVjyj8hSmKNzGIW6ji8RJAnKZpuVwu52EYluqFpdGs60p1Xdc2pcDXwnCyLAt8jAUihCCEELyMY2Pc931/uVzOTdN02ypewzC0w+Ew8eDnv5agKYqiACEUJUlCkiTJCCHU933fNE2bpmnG2zMzn8cHhJBNEIgwxug8z+Q17fCNa8XVHhGfeTXv7u5O9gvWb6uqqlBKaRzHaZqmqeM4JqWUGYahZ1mW7YGG8Opu/c/+n1upMPJqF3GHvMzzvOR5XtR1XVQvVm8tcV6pQkEQxFt9k1yBcgUv2beIEBIhhCInilH+7um2JGnP5yAIAuOch125Bv8DCYDGuSVQEARRkiSRvhjmQYIty7LsLRPLURW6OXsIoeg4jn93d3dijJGu68anp6fHy+USK4qiclEc7XK56HtsavwWgdl0374nCXCbAiCEMIzxpka7nfNfWkCUUszHcwH3VwhjzLi/JFsi8Br0gTHGRFGEXOwMAQBEHp8QY2zlEzFbsYb3PPe82odciG+q67qilLJxHIdpmmZJkpCiKDJCSIIQSqIoviqG/y4SAEmS5K2i4bO/QtM0dVmW5bIsCx8N6ud5xtvD4KTBLs/zbK85/F9lpJQvJGEIIZlSSpqmqZIkSbgcpGsYhqnrur6HM5BlWZIkSVZVVTUMQ2/bti2KIgEAMISQsizLmOd50fd9H0VRqCiK4jiOUxRFsYcWgKZpqqIoqqIoum3bNp9HPfNsWKWULkVRFGVZFpRSgY9DMcYYfS0TPQgC4fPnzzLfN6GapqkNwzBO0zRqmqZxcmTZdV1fFEWeJEmxruspDMNpWRa850f4D0qAZVmWZUVRFNM0TVmWUVmWVZqmSdM0bdu2TZZl6V/a4XBjYEKEENzpvLNv4+zmeAVBELkD3NA4NM8zJoTgF5VugsuybF4rT8yDPuDjtSJ4gRi+63to27YZhmFWFEXi6AMUBAHeQvDpb6AuK4//v7RYFEWRZFm2gyAYp2ka0zRNsyxLVVXV+LIesyiKam9pZp5ogm/ezz+kKOTF8EYQgGQbUXhRpUW8YGCcmEp4K5ns8D7ghjp9y0Ph7cANDd6F/xWGofD161dZVVXlRetO1+q6bsuyTLl/ZWmapl3XNZ7n+YZh6BsR/rXf3e8iAdB1XVZVVVNVVTdN0/Y8z66qqmaM/VlRFJlSSqdpmgRBEPhYTO26rjdNU9d1Xb9n/N8O+jiO05cvX54VRUk35IFnYMrxeDy6vEFkWZYdhmG4wzMwTNM0t783TdP0+Pj4XJZlJcuyNM/zwsfSkOu67rc66XvcN7+25bquPQxDOI7jlKZpzmdgZcYY5nvBIaWU8c1hmPdH8Q7X180X0oN7OByi8/l8/vTp09c4jlOEkLiu6zpN00IpJZZlbdCcwlslu8DgXNfguzG9NzudTuL1ejUdx7HatnXCMAzTNE0/ffr0Z4SQNM/zbFmW6Xme13Vdt6EAoihCRVF24QBwCHhTAsSb4+OSsBAAIMmyLCmKInVdN/D55IUQgvdwuKIoijzwww0S31j330OD/9OnT38uiqIhhCzbBtQN9cAYYx6cV66UOt9iUczWBviWBMkYWx3HsZZlCZdlmfq+75MkyU3TtB3H8TDGEyFkVy4AAIBx5Ee8hdz3/0DwF5ZlIfy5U0mSRO6DIUeFqCiKAnqBYBFHDjAn8ZHfEpS/nUKAEEK+g4Os67pysTNJkiQJALByEh7BGNPXoA2/8n+GaZomnzCKpml6+vz586NpmjmllHVdN2zjjy+reRxrjymk30UC4Lqu4DiO5Xmew/XXMUJI6l8o4ZMsy7LneZ6maUYcx9dxHKdpmvBGmNk7GZdlWb67u4sQQvI4jpsqIgzDMPA8zwuCIAqC4OBx2wMStm3bcV3X7bpuWJZlgRAKaZrmL6TwfkAIib7ve67rur7vBzwAqntJM9/d3R3qum6GYRgIIRgAALIs07uuaxhjGAAgHg6HTRqz4k5ht76g7/tu0zTdOI6zIAgUQojKsiz5tAXmH4lmGIa1PQPf913TNK0wDHcRwhnHcdx6gdyRUEVRvgv5KQgCbxzHfpqmiTFGZVmW+75vl2XBrut6YRgGcRxft94or8jFHQPR1nOHfMoB6rquyxya4nK4sqIomiRJAyeqrXslTN+O32mapp9Op6NpmiZHO8CeZ+2vBP/26enpnGVZMU3ToGmapiiKyhMfsBF9eSuA8epzd+P3KSqKonme50iSpAAAoK7rWhRFPp+SDYqiKL/hY+wyCvotoQwAgHRdNyVJktZ1hbwFdNMEgFf5oizLkq7rJoRw/KY18Qv3ZEsKAQBAlmURQigBANCyLL8sTvotYk3cn4nb+d/e/ZYIK4oi8oRD2eSZCSHrXmfBcRzXcZyW7z0gAIA1z/OMLz0SPM9z7+7uoiAIDlEUea7rOnssBfvdkAA9zwuGYZi45Kdo27bJV2JSURQRXwQiIoQkURSBZVkGd0q73MML50PVObv+njFGJUmSCCEzZwCDjXTjOI57PB6j4/F43GsULAxDf57nkRCCRVEUJElSTNO0x3GcGGOYM741y7Isz/OcIAg2kra70/3Ld3d3EaWUQAiBoiiyaZoW3/64Ci/qg5ogCKLrup6maYrjOLamabqiKK8OQlEUHcZxXHjLA0iSpPm+743jOG+b4Pg2wP/wDO7u7sK9RKr4e94qvVWSJImTwtC3ugc3QsF013VdLjktO47jcNEnqiiKPAzDyMmY8FvnvxcngS/AMTzPc/kyMMKrEUdVVYNrsRvH4zGyuOm6roqiKO3hR4Ig8LIss3zfD+Z5po7juLIsS0EQBKZp2nuMe/41O5/P/2dd1/9zmqbZ+Xw+13XdOI5j3N3dnaQX+y4+UlVVTdd1w3Ec93Q6za7ruqZp6oZhmIqiGKIoSq7rtoQQ7DiObxiG4TiOpb2Y+trrK4oi6bqu6LpuHA6Hg23bNl98Y+m6rkmSpNzy/g3D0PmeE/f+/v6eUkr4/gcNY7wghGTf911RFEW+g0RUFEW1LMv54YcfgGEYuizL6taz/3uvL0mSYhiG6jiO/f79+3eMMeY4jmMYhiZJkqJpmuq6rsuFwhRN03Rd15W9ROiOx+NhmqZxk7xGCCHXdZ1t4RNCSHZd1w5fZkAPQRBEfxgEgAeAYF1XJooi1DRN6/u+25ayAACgYRi6IAiraZqGIAggCALH9/3AsixnrxfQdV0nCALlzPtVFEWRV58bMxuZpmlwmD54eHh4t+czePfu3TsAgCBJksSXHnXzPC+MMcIhUnVrE0RR5O+5iIW/g4gnOhJ3Ru23ylqyLCuiKMJ5nhc+iujysbxdkpAPHz488CpUsSzL5qIby1ahIIRETn4yfd+3994IqOu6LkmSzJMM+6effvrIyVi2pmn6xk+5heV5nnGhkU4QBMGyLNdxHHdd17Xruj5N07Tv+8V1XRshJPOEEO2VkJxOp9O2694wDIsLP2m+74dRFPmiKMJpmg4QQplSijVNUz3P88Mw9PZYzy2KohhF0YFveJM50Quapmnf398fbyF3zJM+xmVfp7quq69fvz4FQeApimJIkvTdJAijKIqGYRh4wqdgjBdFURTf973D4eBz3sWiqqrKJ4HkIAj8MAyDPQJBGIZh27bNsix4uwZCCHHEM7jV89/scDhEfd8PlFKqqqqyJQVBEAR93/eHwyHkZGvA9VFMTdM0SikdxzFSFEXkGzo9z/P+7pjguq7b931PKV1t2zYppUzXdSMIgtA0TYsQMmOM1yAIQgAADILA2fbj7PUMtm2GCCGoaZrO0We6ritQFEU0DMPk2izH0+m0yzTS7yYB2A6BpmlqURQ2VyJbuCIY0HVdpZRSz/MGfjgMLoyxG/nl559//klVVcX3/Z6PokCuB//LaAaX/7Vd13Vu8Qzu7+/fKYqim6ZZ8zWUy8ayliRJ4Qsg7FvshOeOKPR934vjOOYJCP4WJeDKaBghhCzLsvbeRPbu3bt727atsizrcRx/gf/584ecJGjeYimVKIrQcRwvDMMBALAOw+BDCIFlWZbv+4Hruu4tnvn1eiVFUeRPT0+XPM9TSimRJElRVVVZ11Vo27ZN0zRljFHLsoyt6ts7Ifn5559/0jRN9zyvI4RQXddV7mxtVVVVAACwLMthjBFJkmTbtu0PHz582PHsBaqqymVZOuu6UgAA1DRNv2Xw4S0VKMsy2mD/oiiKMAwDTdO0bdaejyJuZL2b/JaPHz9+RAhJ/Bljjjja2+gpQkh0HMellFKOCDh7jqX++OOPP/G/624+xzRNa++Np3/NNE3T+Dp6tH1nuq6rpmk6bds2W5BmjEHLsowgCCIIIVBVVeVTA9C2bXNTzvx7r+/7vst1/rWmaXoAANN1XfM8z+cLx6Asyxrv+YMt/uyNTH38+PGjqqqa4zh13/fjFntkWYa6rhu2bTt7fg/ge0s+/j02juNICKG88tMEQRCyLCt5y8C5FRz7rX2r/PU9rvdrq6qqJoRQvo3P/t6/4R99//+I6z8/P1/qui7HcVxEUQS82o32qHT/mj0+Pg5/+tOf/p8vX758/vd///c/F0VRcva3JAiCMAzD1Pf9GIahezqd3j88PDx8/Pjx/Y8//vjDHgTU33IeuRS1JvyTW57n/zVJkv/1y5cv/+/T09Pj169fvz4/P188zzNlWdayLEtt27Y/fvz448PDw/sff/zx488///zz95BI/1e1TfhtO1/jOI5ZlqV8BTBQVVXfkpI0TXOeLALf9/092oEL137+9flu27aZ55kIggC+R/zZfsc3xdfufCT0ez4If83BvEb3+LdWhf/IZ3ArpOGf5f7/Edd/9+7d6e7u7rgJIN060G0qlOu6wo10N88zHoZh3uadT6fTMXyxKAxD13Ec+3sH/9/DedzbgiD435ZlwXVd223buofDYcQY46IoCoxxa5qmHoZh5LyYxRU534L/d/T7mqZp79+///C3UKO9r/+31DUty7J3Wv/xqt/xL5MAvNmb/SMTj+9V4WqapvB+o7uuK/M8z+da3yuEUBBFUdoIWb7vB0EQHIIgCN/e0j52Op2kpml8PtpIBUGAtm3bvB+t+S8P3Xccx/M8z3t7Ym/2R7G3BODN3uwfX4WCtm1DQsiq67oxTdP0rbyyKIqiqqrKNoEShuFxz0VQbyYIx+PxyNXvoKIo6rZ2HCEkWpblhGHoR1EU3poM92Zv9j3td80BeLM3+1eyy+USt23bconrbxMAuKkEHo/H4y16gW8mCG3bdkmSJHVd1/M8L1wMB+q6bgZB4N9iD8SbvdlbAvBmb/Zmb/Y7siRJtskfEARvRf+bvSUAb/Zmb/Zmb/Zmb/YHMfj2CN7szd7szd7szf717I0E+GZv9mZv9mb/neV5XgqCsPKFp9rbE3lLAN7szd7szd7sOwXgbRoEIYRupf75a0tfLO/7vgcArKIoKkEQuHtLj//auB5Dvcm/O45jf49Z+LcE4M3e7M3e7M1+F5amaVqWZfmtFLeiKFKe5/nhcIhuKcZUFEX59PR0TdM0btu24Zr42rIsgyiK4rt37+5vcd3n5+dzWZZF3/fDlgDouq7btm0/PDy8e5t8eUsA3uw729+SxHyzN3uz29jlcrmcz+e4ruu8qqp2WwfLd5R4hBBi27YJIbxJQGyapm2apkySJI7jOKGUEt/3PVEUkW3b9i0SgDiOl/P5fM7zPGvbtt3k3/lCMmdZlvnnn3/+6Vb3/JYAvNmbfWNt27ZJkiRt2/Z8O5vkOI7zPZaCvNmb/ataURRlkiRJmqbXJEnSoiiqaZo6xphg27a5LMuCEJIul8v13bt3727xGyileJ7nZZqmMUmSuO/7UZIkeZ7naZ7nZRzHcc+CoCzL/1qW5f+SZVn69PR0qes6X5ZlkWVZsSzLHoZh5IvZxJ9//vmnt1PylgC82Y3t8fHxnKZp3DRNjTGmsiwrURT5giCst+4Dvtmb/ataVVV1VVV1mqbF9Xq9Xi6Xa9d13bqu4Hg8hqqq6n3fj9M0TTf8GQBCCARBAIIgMEopvuWoOCGEYSJkYmwAACAASURBVIzxOI5z27bVly9fnodhGGRZRkEQBIwxihCSDMMw8jwvvxcP4l86AdjYn5ZlWdtGsjf7x1mWZRnf1c7WdRVN0zRu9SGcz+drXddFkiTx9XpNlmVZDMOwBEFgiqJoURSFby2BP7Yty4IppeRvbSDb/INt26YkSW8krZ2s7/u+e7Emy7Ks67ouDEMPISRrmqYhhJAsyyIA4LtA4eBlSxWA8KbT4kAQBCiKoqgoihKGoYcxtpqmadI0TXVd103TdIZhGMZxHARBeEsAbpUA/Jr9qSiK5jiOIwjCynWxJUVRNN/33Vv/sLZt26Zp2m0lMADgl39TVVW9v7+/u4XzieM42TJsTdOUdV1B13XDuq5MEAQBQggty7JurQmepmna9/3ArR+GYaSUUkmSRNM07WVZptPpdNrzmkmSJH3ft9M0zV3XtXEcX+u6rh8eHh5c17WnaZrP5/NF0zT9FrKoGOPlfD5f+fNfJUmSXdd1fd/3zufzueu6jhCyKoqC+F7scO/751K8RNM0yTRNe7tGkiTxdg4QQsiyLNv3/V2dUVEUZdu2PQCACoIAdV03wzD0LpfLBWO8AACApml6GIbh5XKJt22FkiTJe7yPZVnw4+Pj8zAM7bIsVJZlxFs/77/xD2nf95tOvhyGYfDw8LALHP3tfaqqqkVRFP2tf5dlWT0ej4dbJMC86qaiKCIIoYgQEgkhdPMBgiCs/Aw4e/nCoigqjPGyLMs8juM0DEPvOI71/v37H0zTNBFC0LZtxzRNy7Zt448SgKIo+t+HYeiCIHCXZVkcx3EIITRJkuuf//znz8MwdBjjeZ7nGWM8v4XsGyUAeZ4Xz8/PF+4Em3VdmWEYVl3XNaWUrOu6IoQk0zSNtm3rjx8/frzFD6KUsqenp6eyLKuu67ppmkaMMdmyUMaYYJqm0ff98P79+3fWjvsZv3z58uVyuSTTNA08AdAEQRCGYRiWZaEAgFWWZcmyLIcQsuwdgLcglGVZ0TRNO03TME3T2Pf9MI7jsCwLkWVZCsMwEAQBmKZp7nX/272P49iP4zgBAETGGGvbdpjneSaEsKZpqr7vW9M0jXEchz17csuy4D/96U9/Looi5UzgVVEUdRiGvqqqOkmStKqqkjHGFEWRXdf1CSF4r3fw9evXpziOt7bHIsuy4jiOhzGmhJBlCwyMMaooimzbtkcIIYfDIdrpvafPz8/Xvu8rjDGFEELHcdw8z5OyLMtxHCdRFIFpmnZRFEVZltUWiHVd1/Z4H58/f/4Sx/G1qqoKY0wkSZIPh0OwriswDEO9cquqqlnXdTUMQ+cBWXhtT/rf/u3f/r+6rstpmmZ+n+Y4jvOHDx8exnEcP3369LWu62KapnnbCT+O4/jDDz983Pv7r+u6WdcVQwiRJEmIr2qmhBDGfdCqKIrs+36wrivbYzETY2yllDJCCGOMYUIIVRRF1TRNf/fu3b1hGLokSWoURWEYhtEfKQh9/PjRXJblDiGkEkKWoijyqqoqURThsiyEMcbWdV0ZY9/tNyVJkvJCZENd9e09F0VRruv6h0DIf0kAqqqqyrIs4jiOsyxLCCE0iqKoruuaB4QVIaS4rmsOwzAQQujepIxlWfCXL18+p2maFUVRtG3bdl03YIyxIPyyG351XddZ15UqiiLvFQDP5/P1crkkl8vlUtd1KQiCYFmWxRhbeeVJeAKgHg6HESEEZFlW94TiNwZwnucZf+7D+GJD27bdNE2T4zgOAACapmk3TdPtcf/LsghZlhVpml6zLMswxhgAICqKgiCEQBRF0Pd9++nTp0aSJMnzPJcxJui6ru0VgK/Xa1wURfH09HQpiqIEAKyWZZkAgNUwjDlN0ziO4xhjvKiqamCMF1EUoeu6jqZp+ivRpjXLsjSO42uWZek4jpOqqvr9/f2iaZo8juMSx/E1z/N8WRZimqZ2OBxmhJAYBIG/x4hSHMdJmqbXOI5TQsgsiqJ0d3c3K4qixHGcDMPQQQjFKIrCvu+tJEmSpmlafk5t3itFHz9+/PBbHV5RFGUcx+nlcrlijEdd121BEKgoimLf93qaplkcx8k37HAfQghUVdV+awJACPlvX79+/Z+SJInTNE36vu8hhKLv+z6lFEiSJC3LMmVZlsRxfO37vl/XFbqu63KUSNqDmZ4kSXy5XJIkSS55nmfjOGJZlhFCSFzXFQIAGCGEbgiArus6pZQqiqLskQB8i3AKgiCsfzEqiiIIw/DgeZ4fBIEr/AHtp59++tkwjJR/B7kgCP8Qifo0TfOiKLKyLJt5nkfGGJMkSdQ0zRqGoRvHcanrugEAMEVRVN/3/VuNRn7XBGAcx6Hv+76u6/rp6enMb1zCGMccllw1TTP6vveWZSEQQqAoirIX/MersK/X6zVOkiTJ8zwry7JqmqaZpmkUBEFQFEUWBAESQu51Xdcty+oZY3SP8ZBhGPqu65qyLPOnp6endV1ZFEXHZVmmoijKaZomAADYnJ6mabrnec1eCUCapvn5fI43R5jnedG2bdN1Xdf3/TDP80QIoRBCuCzLgjFe5nneBRLLsmzu+76r67q+XC7noihKWZYlSimGEApd1zVt29YYY8b3pK+O49icl7CLTdPUd13X1XVdPz8/Py7Lsrx79+6d67ouAECs67riyVl1OByOiqIojuN4bdv2r00A6rpe2rZti6Ion5+fz1VVVcfjMdI0TXVd18UYL0VRlOfz+blt2zYMw0hRFNlxHKcoiuLXUPVvSH5o27ZNlmVFHMfnPM8r13Vt5cXkOI6vSZIkqqqqEEJICFnzPM+en5/PgiAIx+PxpGmaYpqmSyllPFH+u2ye53GapqFt2zbLsiTLsux0Oh0QQgAhhFRV1a7Xa5Kmaf709PQ0zzMWRRH5vh8sy4LHcRx+y3vIsuz/atv2jj/7a5ZliaZpGp8+UQzD0JdlmfM8L+M4TuM4viKEEKX0naZpqud5uwTEruuGrus6noQ+1XXdSJKEEEJoQx4ppZQz0uH9/f29ZVk239z4m575rxIAsOWR67oCAAAgHA4ghDAIIfijBv+tuNN1XSWEYMYYoZRSxtj662d0y9+QZVn29PR0Kcsyy/O8GMdx2EiIruu6hJBlHMcxy7JCEASqqqrB4wL8Z90Uib7JxCkhhFBKyTRNE0IIJElyLYqiWpaFAABWSZKkeZ4nCKGo67pqGIZ1Op3u9qiArtfrOc/zPE3TjGfj1zRNU8YYU1VVRQiJdV03lFIhiqIQY0wZY6zv+9GyLPO11yeEUIwxmaZpKcuyopQSCKG4EfAQQiJPfATXdb1xHKdtRncPe6E8NFWe5+n1er2ez+dL27aNIAjQNE3Vsiw/z/MKYzxTSgl3RrtgYowxRimlGGM6TdNSVVW3risTRVGY53nJ87xa1xUwxlZJkmQOVa57ZunLsmBCyEIIWTgPYeH3t1JKKQ8yfZZlleu6u/be13Wl2zOYpmkqiqLQNE0Zx7Hvuq7HGC/TNPVVVZV1Xbc8KWDrurKdGNKUOzzc932fJMlVFEWwLMtECCE8Mbu4ruuEYRhpmqYNwzDneZ4BAKDneQFjjAEA2CueAYAQQkmSoKZpsiAI7Pn5+SKKosS/PylN0/h6vT41TVOHYejLsixLkiTy4Ad+49lbKaWUEELmeR7TNE0VRVE2+VnTNE2M8dK2bd00TXm9XmPTNM0oig78ne32DTDG6DRNZBiGqSzLhsPOqyAIgiRJIiGETNM03d3dHbZYBADY5RsghCyCIECEkCRJEvp1QvGvsLSNv8v1L4j/C+b/TUJE90i2/ob/7ZIkSYqiSM/n87UoiqyqqnKe58U0TXPjhY3jODw/P58xxjgMQ18URWQYhvVPnwAghESEkAghhKIowizLymmaFsdxLMMwTMYYjeM4TZIkMwzDchzH8Tyvz/O82KMPWhRFXZZllWVZmSRJer1eL7Isy/f39/cb2erTp0+f+r4fNnaqKIoCQmgXRiwAQNh4BhBCoa7rdmNCf/z48UGSJOXx8fHrNE0zY4yu67ryILhXBTwOw/BLBdY0TRWGYRCGYajrukUIwcuyYJ5sbY5pl+tLkiRrmqaapqn7vh8ahmEAANa+7/vz+RyfTqejbdsOY4xKkiRblmXouq7s2f/iQYBijMlGQAMAIMhfCqV0ZYwxhBCAO9OSEUIy77dqtm3bnIHd9pyFOU3T/NKN6nrTNHV+/6osywpC6NVEVA4xw5ciUETrugJKKd4C4zAMQ9/3g6IoKsZ4nqZpxhiPGGPiuq6taZqCEJIghOi3Okdd1w3TNE3P8/wXgAnTp6enpyRJYl3XdQghfH5+fs7zvPB93z+dTu/CMAwsy7ItyzJ+62QIQghILyYbhmG6rmu/AIB5ZhiG0TSNTQihdV03eZ4XjDHquq6j67qqKIq81xmUZXn7Bszj8Xi0bdveKnCEEKSUrkmSxLw9Eei6biqKovJJiVedx6qq1ufn57goimLjH63rCngLQKCUCv9KBsDLrfPzASmlrGmaLsuyTJIk+OOPP+6uB5CmaZbneRHHcZrnefr8/PxclmUjiqJQVVXN27/ysizL9Xq9juM4SpKkhGE4M8bwrRKT75YAQAghAAAKgiAihERN05T7+/v7KIoOtm1b0zRNhBB2vV6TcRy7aZqmeZ7nPWZS27YV+r4fuJOtsyzLAADg/v7+3el0euc4jn29XmPGGOGQnMh/K7iFRCQAAEzThH3f9x8eHt77vu8XRVFxBjpc13V3KIqji4wQQodhGHVd16MoOhqGYem6rgovzOOfRFEUTdO0NU1T93J+x+NRzPPcjaJoFkVRopSyaZrGL1++/JkxtoqiKB0Oh5OmaRoAYPU8z3Ecx9+7Et/iIWNsfZl4kiUIobSBFOu6rqIoiujFRB40X/3RnU4nlGWZYVmWaZqmadu2WVVV07Zt17ZtP8/zzBXKiGVZtmEYJidgmnuwwBVFQXzCBqmqKkuSJG7Bf5qmZRzHQZZliTFGt/YPn45hhmHosiyriqKoqqqqv/U3hGHoT9M0ciSCYozxNE3T9XpN4jiOKaU4z/PqcDiEx+PxFIbh8XA4HIIgCD3P+80TMYfD4f8YhqH1fd/l0y59XddtlmWFZVmOaZoOAGBt27apqqq1LMtwXdc1TdMxDMOybdvc49BFURTUdd0sy7KoqqoQQjAAYKWU0rZtu6enp6/jOC6n0+kYBEHk+77neZ5rWdarZHkJIf8tTdP/UhRFer1er1sLkFJKFEVRRFGEPOn9l4j9oigCHoMUVVXVruu6JEkuEEK6rivj6LO59wRI0zRNVVVVXdclJ7rWp9Mp0jTNOJ/Pz9fr9YoQUiRJEruua/l0CAAv2Qr4Z33g6JseDEIIbXCegBCSHcexj8fjwfd9/3K5xFuWP47jTAjBGGO8Lat4jXVdh3kuMXEnMIVh6Lqu6x+PxwN/yARjzLZ+Ef9PFEVxdzEjxhizbds8HA7Hw+FwZ9u22fd9fzgcQg5PKrIsS4qi7H1tAACAiqIoVVW1T09PX1VVlRVFMTzPcx3HcS3LsqMoCoMg2JUNfH9/f0IIIc/z3K7rus+fP3/d2LdcDIRGURRsyUcYhoc9JzD+mnHnJ/LfwAghlDG2QggFPgq2SwLAK2DLeDGTM+3rtm3bvu/7eZ6nvu8b3ve3TdM0DcMwuT7Cq833fUGSJAlCKCGEZEmSEMaYcIGUbhzH5e7u7m4cx35ZlnlTZOOz07qu64qu68pr9RkeHh7e8eofT9O08GRjfnp6OkuSJN3d3R0eHh7eBUEQRVEUHo/Hw+FwuHttEvTDDz9YVVW5fAa+DYLAe3x8vBRFUWxJZlVVlSAIzHEcz7Zty7Zty/M8+3A4HPd4B5qm6afT6aSqqhwEQTDP81TXdXO9XuMsy7IkSTLXda0wDA9hGAb8GwxOp9Orrl/X9f89TdO7tm3bPM/zx8fH56qqisPhEEVRFFmWZZumaSiKogt/cAuCwM2yzHRd1+66zu37Pnp+fn7+8uXL47quqyzLiuu6Tt/3/Z7XXZZF4En11LZtW5Zl7TiOdX9//05RFJUQQs7n8/nTp0+fFEWRMMbkcDj4qqpq8otJ/4zV/39IAH5lkBcgWBAEUXkpUUSEEIIQ/kJO2WiqO1S/hPMP8DzP8zZiZFmWbpqmMQxDzxh7aQRx+w5ZF5RlWbZt23Bd153nedY0TRVFUfQ8z3ccx96De7CZaZqaYRiG67r26XS6kyQJtW07FEXRMsaqoijy4/F4d3d3t/LAy/bclOW6ruO6rvP8/Hxu27bn/XfIq5Rf+nFhGPoPDw8P3+PA874sEwQBcC7Wr8/Nbteybds0DMPkULitaZrKCZj9sixD3/ej7/uetcEEpmnyccxdTFEUJMuyhBCSZFlGvCc+t23bKooi2rZt83HUhc+JT7quy4qiaLIsa5IkqZqmqa/5DVmWZdM0jXzsE1P+gCmlWNM0xbZt2zAM07Isx/M8P3wxfy/n3zRNa9t24ziOl+d5WVVV1bZtDSGETdM0qqpqrus6LyCNbb+2+v5rKAhPOFMuxlMkL3ZFCImHw+EYhqEfRVF4OBwOP/300487nHEBALD1vVcOfwNRFMUXSoaEIISSLMv/EqqtkiRJfPrihQyBEMAYr5yjxLa4s+c1ZVkWNmSR60tE3Bd7qqrqPA4KZVkWlFISBEFwPB6jTZfBNE3zn/V5o29KTwghhLIsi7Isi1wOUhBFUeDbmdZvy9TNAe/xLr4ld3JC1goAgAghSRRFiDHGPEmgmqYpmzgHt5sEIn6/K2OMmaZpKoqidF3XQQihaZpWFEXRXtUHv2+gKIrquq4HABBd1/WmaRrHcRzrum7TNE3iOL6qqqpalmW6rmvneZ7t+Rs48oM4D0SUJEn8lnnLHRS4xTPnPU8AAICyLENOSqWMMSrLssL722g7l3tbFEVhmqa5bdumZVmGZVlWlmVZ3/ftMAwdAADatu1u0L9t27vOAG/Bf+uHz/PcT9M0D8PQ6y+MOJ0TAxdCyDTPM3Zd1zZNU+O9a+01W+I2HYI0TeM8z7M4juPHx8evbdu27969e9c0TXO9Xi+KosgIIdkwDJUrUoZ7nAfXdT3Lsmrbtm3HcdwgCILz+Xypqqrmky/k4eEh5JwDy7Zt8xbEqzzPyyRJksvlkuR5niVJEg/DMN/f35983w/DMIxs2/YURVH3uJ6maa4sy+qWUHKiLcrzvNQ0zfA8zxrHsd9z4ub3apRS2vd91zRNu2l/YIzZhw8fPkRRFLmu6/IkfXclUtM0rW3Zkuu6NkdDfdu2LVEUBVmW5SAI/HVdmSzLsuM4/uFwiIIg8P4QY4CSJG1wKvg2KViWBadpmrVt243j2FNKGe9NIZ4owD2DAHf0wrIsZBzHqSzLZhzHeZ7nhVJKEEL61gKQJOnmkpi8Bw4Ph8MDpXRZ1xVomqbvqQR4vV5xHMfJ9Xq91nVdMMaYruuW4zg+Yww3TdNBCIWnp6frOI49fpEMoxjj3dlB29z/3wq0vx7N2RHuF2VZRqIoIlEU0bIshFLKhmGYuBCUyPuDNzPHccyiKAzDMAzLsqyiKIqmaapxHEfTNA0Ox9qaplmmae7a/tg4D4qiSJIkKZTSfnw5+CQIglDTNIU/l6Xv+1EQBGYYhqGqqsYZ86+CiHnvOU6SJE6SJI3j+FxVVR1FUXQ6nU6SJElcB+giSZKkKIrEqzS0hyiYZVlWGIZO3/dt2/7/7d1bdtvIdgZgAIVCAQUU7gBJuXVZ3T2FM4MMIoPIcDKrvPebz7LdtkxRvAMk7kAeVHAULzudjgjb3fm/tfRMCqKIH1V778py3/eD3W6332w2G9kixoQQ/tO9/ykkXPrv//r1638+PDys1uv14+Pj4/rx8XG52Wx2aZqG8/l8LgNWv9/vH2URZvPSWSiO4/xbFEV9VVUzuadMyrIs1uv1vqqqQq6MXqzj50d2Pp8LWefSNk1TFUVRcM6tJEmS+Xx+NW67XPqhR1EUJU3TZBiGzrIs83Q6lZRSIotNbUqpwTm3y7Is5UA84jiOiKIoTpJk9le+5vrzpajPnU6n8++///47IUTN8/y0Wq3WjDEqn4hN0zTZSwqPnt10DMMwDMYYcxzHMQzD2G63G845l/3vQ57nx7quG/lFqU+1//+VZNoHQeBNNQM/z/NTlmVHuQf4rqqqKk3ThHNua5pG6rquxilk49bHpdqPvhDCvrgUL9su267r2iledxy6QinVKaX60+jv4nR/f38/bkf1E48Ck/v/tvwRtm3z+/v7B8aYkSTJ7GnV+Wn15dJnMTBZ0k4IMUzTNFVVHfb7/VZ+2bi6rjPDMFhVVeeiKE6EEJ1zbjPGXhwA7u/v6+12e9hsNuvlcvlwf3//fr/fZ0mShPP5/Eo++ajn87lYr9dbwzBMuVRLTdNklmWJNE1fvBUwm83mT6v+WZbn+fF4PIZv3rx5OwxDf3t7eytv/MJ1XefSY6Dfvn37Xp5/8fB0IN/jw8ePH1e2bVthGIZVVTUPDw8f1+s1UVV1nMapyGLpF81CiaJIK8syyrIsl+22vQziVG4FaN9q/v/3JLtNdDl/gY71P4oc+OR5nn99ff1qihVI3/c9IYRYrVaPVVVVmqZpQRC4csVpdTgc3LIsa1VVB03TiG3b9mKxmF9yG/a7BoDP78llWZZv3779XT5la03TNLqua1dXVwvP83zP84Rpmk4QBC/eh1ssFuTh4cH2PE9kWebP5/PZcrl8ePPmzTvbtleK8tQnL5eD6X8VguuTBIBL1Tb8b7Rt+x+KovzSdV0vW+DKh4eHzfF4PMp+bK2u67osy8pxHM45t8fCk6l+/8+vhaIovdynnOx1KKUGIcRgjBm2bTu73e745s2bN4wxJlv0ezmhUJuq/kPuLzuc80+rAMvlci2HTnlCiE/L/xMEIMaeUjWzLItrmqY+Pj7u7u7ubhzHsXVdZ7ZtW9vtdnM4HPI0TWPLsrhlWZY8pCl6wWew7bquKYqi2O122/1+n6VpGs/n86vFYpF6nhcMw6DWdV02TVM9PDw8yGJYOwiCsG3b8kLXwPA8zzscDge5z+/Zts1VVR08z/Nc1xVyFr536S/e0+mUHQ6Hw+PTyMn7d+/efTgcDgdN0/x379697/u+U57qgnTXdT05hdItiuIis0BkbVWvKErb930ri101uRVKhmHosyzLL1l39KPZbrcbmXc+Ffc+7Xi1nQz/w5QHkRFCtC8VdabSOHPir1rw92cCQG+aJouiKKyqqu77vhdC2J7neWmaxnEcJ2EYBnEcX+ypOAzDoCiKU1VV9fiFeDgcDnVdV3Iuuns8HvNxiZgQolNKL3kD/FSEI5ejDeX/ONzkTy79/oNSWnDOLbkMOiOE0CzLsqIo6r7vFUKIFkpRFIVyNoMdBMHF2/CGYVDGASh93yumaTLDMExCCB3b76a4DqZpciEEl4fsxF3X9XImfaPruu77vtP3fX88HvOxEEgWTE2yCiCJNE1Dy7K4bdtc7v+7L5389yVJkkTL5XJpWRYfVyCGYVBd13Uty7INwzBs23YZY6aqqrXneZ8KEl96UxiLaSilzPM8T9M0XVa4zxeLRWrbtquq6jAeyrJardbPV2Mu+Tfouq5umqaW8w6Kruv6KIp8x3Fc+fuKS03/+2yVq2uapi2Kolyv17thGIbZbBbLm6/KGGNVVTXn87kwTZP1fd9dchaH3P6UJTaEyu3Xumma+nQ6FR8+fLjf7Xb72WyWTHEGyff2+vXrf8o5CGVVVXXTNNX4wCfbfce6s+/Wb/93uvH/UQBQVFXVoiiKHcdxm6ZpKKU659x0HMefzWZJmqaz6+vrm0u9kZubm+umaZphGBTDMHT5dHHWNK3ruq7/+PHjfZZl57FX8fmYzgvdiHVKqWGaJp/NZgtFUQbbtgVjzGSMGVMmz9vbW2u73Qbz+byilOqyDaZ4dggTkdsjQs4Dj8MwDKc4iGK8FxiGYYVhGOq6ro9DYuTsAXOKa3B1dTXLsuxYVVXZ9/1gWZYZhmE8DEOrqqouJ/OdXdc927btUEoNVVV1SulF/ykXi8Vst9ttXNd14ziOTNOkiqIQIYSwLMt2HGeydizXdYXneeJwOIgkSVLf9/04jiPP8zzDMKjv+/6rV69e9X3fB0EQBkHg+b7vCiHcl7zuq1ev2Gq18mezWalpmto0TWXbtp0kSTqfzxemafK+7/u6ruu+7zvZDcCFEC7n3DIMg13oCXC33W6PWZYdD4dDtt/vd/K6+GLc/Jf7MJe+9pqmEUqpblmWuVgsZqqqqowxk1Kqj0+jXde1dV3XlFIqhLBlQeRFvoNkXQUzTZNxzi3GGJWjsXNCyEdCiD5uO13yELAfwf39fSWnvz7Ic1caWXNWe57nj9/Bpmkaf8eb8A8ZAMZl8DiOI9u2+TAMqmmaTBY/hFOcBvjLL7/8rOu6zhjjvu8fy7Ksy7KsV6vVsq7rWo4qHavBiaqq+gW/fL0gCIK2bRvHcexhGIZxiVMeiTypV69ezXVd1yzLsvM8z+u6Luu67oZhGCilRNd1Q06h82azWXJ1dTVJ5Wkcx2GWZfuqqirDMLSyLCNCCI3jOArDMLzU6XdfCQFz5WngERNCCDlnvZPdCHpZlqeyLFvOuRnHcej7vvuSyvevCYIgLMuy0jRNq6oqUhRFc13XCcMwTJIkner3D8MwOBwOxyRJCjnvvhdC2HEcR4QQUhRFJVcABjkON5arQsEFPn8zXdcVzjlvmqaRHSn+YrG4EkI446hkQght27allNIwDP3oaSjARQpi8zzPz+dznmXZaTwHQ3ZkuGPrpeu6k/wvynAdNE3TjoFK1iXp47ZTK4efUEo113W9MAyjS7UihmEYPD4+rmzb5pxzx/d97/HxcbVcLt9vt9uV4ziufJ+uvC6TBQDZ/Tk8Xwnsum6SiYR5nv97nuf/KrdfVsvlclnXdZ1l2WE8b8OWF4Vz/rc5swFSGAAABnhJREFUBvmHDwBjDlCUpwpJIYSraRqdogDns6fhG8dxhDyY6Pjhw4f3ZVnWTdO0Y38spZRomkYYYxd7ApbnC7SUUhaGYaGq6iBPewqmutk+J6+xs9lstqfTKS+Kohr3vsfQI6fUuJcouPqD1Zi7YRg0x3HspmkaQojmuq53c3NzPWUCF0KIX3/91XYc52Oe53lRFFXXdb1s+9ROp9NZjiOmYRj619fXP03xPuT5FprjOHZd17U8DpcnSZJMWfQjhBDz+XwmWw69YRgGeeBNZBgGUVVVj6LoLM/lMOM4Dm9ubq4v9PlLLcuyjsfjUd7g2Ww2S8ZJm3d3dzeapmmu6woZBHTP88RLjwH+7GZwkj/58Xg8Nk3Tjzd/13Udx3FEHMfBRJ/5n4Zh6HVdZ3VdV3JVkMqfTysAbds2YxFYEATxJSZBjsvLtm0Lx3Fcz/O8JEniuq7rzWaz77pOub6+Jn3ft/L8icmKcTRNU+TkQUIIYZZlteP/HyHk4pNXi6LIZI9/WxRFtd/vd8fjMTNNk8khdIEr//iu67oKTB8Anh+CIQ/rUH3fj5MkCb7Fm4qiKIiiKPjtt99+a9u2fjaU6NPWIyFEvfR+9NXV1fzq6mq+Xq93iqIMrus637LK07Isazxdsa7rOs/zszx7nHLO+bda/iKEaD///POdPCGylNtB/rd67fGmUj+NvasZY0xRFOV4PO7ljAhyyTbMr90Q0zRN5Rf/N9t3lL3F4Xa73fZ9rzDGjHGVw3EcpyiKUq6SGJe6+TwPIP/Tk+XNzc1PU/3eRVEUcspheT6fi+PxmFFKqezGsGUb5qTL3re3tzdCCFcWmyqEEF0I4ciTMfvz+XyuqqrWNE279LWXwTM9nU7HsiyLtm0bQogeBEGmPLV8ClmLo0/ZETC2lQohnMViMavruvE8z7Usy5LnX1z0+zAIgn/Z7/dUnu5pz+fzRRRFMWOMyVWWOI7jIAzD8O9cAPkjBIDnB8xolFI6hgFVVfuJT2L8KlkFqzHGLF3XmaZp45CiSc4BUBRFmeop488wDMMIw/C7tpjIKnP+Pa/B8y+cS44+/rOh5Bu/HvlSoeEf3aD/ytq27caJeIqidMMwqLIYknPOLc65HUVROPX7+NqNnRCiyS6QST/vSZKk8tRRTfaeV+NBNGEYhr7v+1EUTRZ+F4vFYrfbHcqyrHRdZ8Mw9LZtW1EURb7vXzz06Lr+D8/zWrn90liW5ciVGMI5t8MwDJIkSacMnwgATx9wKgstrDRNZ2EYhq7repxzS56WRr/1mzNN05KFV9719fXN2A7EObenSKMA8H0IIRxN06hlWUwI4d7d3V0TQnTHcdyxJWOKotcfTZIk8TAMGuec+77vVVXVyM4k6rquSJIknfo6PDsX5NR13TBOwRtXxCZY9dLrul7ICZiFfOjUOOeW7/ve1dUVbv5TBwBZgexXVVVblmV1XddblmXKivPgeyy/eJ7nJ0lyVhSl933flXuxVhzH0RQtcADw/YRh6JdlWfR9P4y99kIIV45k9f6/XIc0TcM0TcP1er3ruq6VQ7/0qWt/RuO5IEVRFHJlgk29CiYPmQqzLMsVuQrt+76Lh7xp/bdG6tVq9bjZbNZ5np8VOX0pCILg9vb25nu9wc1ms91sNtuqqipFeZqYFgRBMPUeMAB8e+/fv/+wXq83TdPUiqKojuPwKIriKbtPABAAnqnrWum6Tpmw9R0A4KuKolDkISy4GADfMgAAAADA3xumKgEAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAMBE/hNyK+kOdDNOggAAAABJRU5ErkJggg==",ao={type:"change"},os={type:"start"},oo={type:"end"};class mp extends Pn{constructor(e,t){super(),this.object=e,this.domElement=t,this.domElement.style.touchAction="none",this.enabled=!0,this.target=new C,this.minDistance=0,this.maxDistance=1/0,this.minZoom=0,this.maxZoom=1/0,this.minPolarAngle=0,this.maxPolarAngle=Math.PI,this.minAzimuthAngle=-1/0,this.maxAzimuthAngle=1/0,this.enableDamping=!1,this.dampingFactor=.05,this.enableZoom=!0,this.zoomSpeed=1,this.enableRotate=!0,this.rotateSpeed=1,this.enablePan=!0,this.panSpeed=1,this.screenSpacePanning=!0,this.keyPanSpeed=7,this.autoRotate=!1,this.autoRotateSpeed=2,this.keys={LEFT:"ArrowLeft",UP:"ArrowUp",RIGHT:"ArrowRight",BOTTOM:"ArrowDown"},this.mouseButtons={LEFT:Vn.ROTATE,MIDDLE:Vn.DOLLY,RIGHT:Vn.PAN},this.touches={ONE:Dn.ROTATE,TWO:Dn.DOLLY_PAN},this.target0=this.target.clone(),this.position0=this.object.position.clone(),this.zoom0=this.object.zoom,this._domElementKeyEvents=null,this.getPolarAngle=function(){return a.phi},this.getAzimuthalAngle=function(){return a.theta},this.getDistance=function(){return this.object.position.distanceTo(this.target)},this.listenToKeyEvents=function(w){w.addEventListener("keydown",mt),this._domElementKeyEvents=w},this.saveState=function(){n.target0.copy(n.target),n.position0.copy(n.object.position),n.zoom0=n.object.zoom},this.reset=function(){n.target.copy(n.target0),n.object.position.copy(n.position0),n.object.zoom=n.zoom0,n.object.updateProjectionMatrix(),n.dispatchEvent(ao),n.update(),s=r.NONE},this.update=function(){const w=new C,L=new Ln().setFromUnitVectors(e.up,new C(0,1,0)),le=L.clone().invert(),ue=new C,ae=new Ln,pe=2*Math.PI;return function(){const Te=n.object.position;w.copy(Te).sub(n.target),w.applyQuaternion(L),a.setFromVector3(w),n.autoRotate&&s===r.NONE&&T(U()),n.enableDamping?(a.theta+=l.theta*n.dampingFactor,a.phi+=l.phi*n.dampingFactor):(a.theta+=l.theta,a.phi+=l.phi);let Ie=n.minAzimuthAngle,Ne=n.maxAzimuthAngle;return isFinite(Ie)&&isFinite(Ne)&&(Ie<-Math.PI?Ie+=pe:Ie>Math.PI&&(Ie-=pe),Ne<-Math.PI?Ne+=pe:Ne>Math.PI&&(Ne-=pe),Ie<=Ne?a.theta=Math.max(Ie,Math.min(Ne,a.theta)):a.theta=a.theta>(Ie+Ne)/2?Math.max(Ie,a.theta):Math.min(Ne,a.theta)),a.phi=Math.max(n.minPolarAngle,Math.min(n.maxPolarAngle,a.phi)),a.makeSafe(),a.radius*=c,a.radius=Math.max(n.minDistance,Math.min(n.maxDistance,a.radius)),n.enableDamping===!0?n.target.addScaledVector(u,n.dampingFactor):n.target.add(u),w.setFromSpherical(a),w.applyQuaternion(le),Te.copy(n.target).add(w),n.object.lookAt(n.target),n.enableDamping===!0?(l.theta*=1-n.dampingFactor,l.phi*=1-n.dampingFactor,u.multiplyScalar(1-n.dampingFactor)):(l.set(0,0,0),u.set(0,0,0)),c=1,d||ue.distanceToSquared(n.object.position)>o||8*(1-ae.dot(n.object.quaternion))>o?(n.dispatchEvent(ao),ue.copy(n.object.position),ae.copy(n.object.quaternion),d=!1,!0):!1}}(),this.dispose=function(){n.domElement.removeEventListener("contextmenu",q),n.domElement.removeEventListener("pointerdown",et),n.domElement.removeEventListener("pointercancel",lt),n.domElement.removeEventListener("wheel",Et),n.domElement.removeEventListener("pointermove",je),n.domElement.removeEventListener("pointerup",Ye),n._domElementKeyEvents!==null&&n._domElementKeyEvents.removeEventListener("keydown",mt)};const n=this,r={NONE:-1,ROTATE:0,DOLLY:1,PAN:2,TOUCH_ROTATE:3,TOUCH_PAN:4,TOUCH_DOLLY_PAN:5,TOUCH_DOLLY_ROTATE:6};let s=r.NONE;const o=1e-6,a=new so,l=new so;let c=1;const u=new C;let d=!1;const f=new Se,m=new Se,g=new Se,p=new Se,h=new Se,x=new Se,E=new Se,b=new Se,S=new Se,y=[],R={};function U(){return 2*Math.PI/60/60*n.autoRotateSpeed}function v(){return Math.pow(.95,n.zoomSpeed)}function T(w){l.theta-=w}function D(w){l.phi-=w}const X=function(){const w=new C;return function(le,ue){w.setFromMatrixColumn(ue,0),w.multiplyScalar(-le),u.add(w)}}(),oe=function(){const w=new C;return function(le,ue){n.screenSpacePanning===!0?w.setFromMatrixColumn(ue,1):(w.setFromMatrixColumn(ue,0),w.crossVectors(n.object.up,w)),w.multiplyScalar(le),u.add(w)}}(),N=function(){const w=new C;return function(le,ue){const ae=n.domElement;if(n.object.isPerspectiveCamera){const pe=n.object.position;w.copy(pe).sub(n.target);let fe=w.length();fe*=Math.tan(n.object.fov/2*Math.PI/180),X(2*le*fe/ae.clientHeight,n.object.matrix),oe(2*ue*fe/ae.clientHeight,n.object.matrix)}else n.object.isOrthographicCamera?(X(le*(n.object.right-n.object.left)/n.object.zoom/ae.clientWidth,n.object.matrix),oe(ue*(n.object.top-n.object.bottom)/n.object.zoom/ae.clientHeight,n.object.matrix)):(console.warn("WARNING: OrbitControls.js encountered an unknown camera type - pan disabled."),n.enablePan=!1)}}();function z(w){n.object.isPerspectiveCamera?c/=w:n.object.isOrthographicCamera?(n.object.zoom=Math.max(n.minZoom,Math.min(n.maxZoom,n.object.zoom*w)),n.object.updateProjectionMatrix(),d=!0):(console.warn("WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled."),n.enableZoom=!1)}function W(w){n.object.isPerspectiveCamera?c*=w:n.object.isOrthographicCamera?(n.object.zoom=Math.max(n.minZoom,Math.min(n.maxZoom,n.object.zoom/w)),n.object.updateProjectionMatrix(),d=!0):(console.warn("WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled."),n.enableZoom=!1)}function K(w){f.set(w.clientX,w.clientY)}function j(w){E.set(w.clientX,w.clientY)}function G(w){p.set(w.clientX,w.clientY)}function te(w){m.set(w.clientX,w.clientY),g.subVectors(m,f).multiplyScalar(n.rotateSpeed);const L=n.domElement;T(2*Math.PI*g.x/L.clientHeight),D(2*Math.PI*g.y/L.clientHeight),f.copy(m),n.update()}function J(w){b.set(w.clientX,w.clientY),S.subVectors(b,E),S.y>0?z(v()):S.y<0&&W(v()),E.copy(b),n.update()}function F(w){h.set(w.clientX,w.clientY),x.subVectors(h,p).multiplyScalar(n.panSpeed),N(x.x,x.y),p.copy(h),n.update()}function k(w){w.deltaY<0?W(v()):w.deltaY>0&&z(v()),n.update()}function Q(w){let L=!1;switch(w.code){case n.keys.UP:w.ctrlKey||w.metaKey||w.shiftKey?D(2*Math.PI*n.rotateSpeed/n.domElement.clientHeight):N(0,n.keyPanSpeed),L=!0;break;case n.keys.BOTTOM:w.ctrlKey||w.metaKey||w.shiftKey?D(-2*Math.PI*n.rotateSpeed/n.domElement.clientHeight):N(0,-n.keyPanSpeed),L=!0;break;case n.keys.LEFT:w.ctrlKey||w.metaKey||w.shiftKey?T(2*Math.PI*n.rotateSpeed/n.domElement.clientHeight):N(n.keyPanSpeed,0),L=!0;break;case n.keys.RIGHT:w.ctrlKey||w.metaKey||w.shiftKey?T(-2*Math.PI*n.rotateSpeed/n.domElement.clientHeight):N(-n.keyPanSpeed,0),L=!0;break}L&&(w.preventDefault(),n.update())}function ee(){if(y.length===1)f.set(y[0].pageX,y[0].pageY);else{const w=.5*(y[0].pageX+y[1].pageX),L=.5*(y[0].pageY+y[1].pageY);f.set(w,L)}}function re(){if(y.length===1)p.set(y[0].pageX,y[0].pageY);else{const w=.5*(y[0].pageX+y[1].pageX),L=.5*(y[0].pageY+y[1].pageY);p.set(w,L)}}function H(){const w=y[0].pageX-y[1].pageX,L=y[0].pageY-y[1].pageY,le=Math.sqrt(w*w+L*L);E.set(0,le)}function Ee(){n.enableZoom&&H(),n.enablePan&&re()}function he(){n.enableZoom&&H(),n.enableRotate&&ee()}function ve(w){if(y.length==1)m.set(w.pageX,w.pageY);else{const le=Me(w),ue=.5*(w.pageX+le.x),ae=.5*(w.pageY+le.y);m.set(ue,ae)}g.subVectors(m,f).multiplyScalar(n.rotateSpeed);const L=n.domElement;T(2*Math.PI*g.x/L.clientHeight),D(2*Math.PI*g.y/L.clientHeight),f.copy(m)}function de(w){if(y.length===1)h.set(w.pageX,w.pageY);else{const L=Me(w),le=.5*(w.pageX+L.x),ue=.5*(w.pageY+L.y);h.set(le,ue)}x.subVectors(h,p).multiplyScalar(n.panSpeed),N(x.x,x.y),p.copy(h)}function ze(w){const L=Me(w),le=w.pageX-L.x,ue=w.pageY-L.y,ae=Math.sqrt(le*le+ue*ue);b.set(0,ae),S.set(0,Math.pow(b.y/E.y,n.zoomSpeed)),z(S.y),E.copy(b)}function Ae(w){n.enableZoom&&ze(w),n.enablePan&&de(w)}function xe(w){n.enableZoom&&ze(w),n.enableRotate&&ve(w)}function et(w){n.enabled!==!1&&(y.length===0&&(n.domElement.setPointerCapture(w.pointerId),n.domElement.addEventListener("pointermove",je),n.domElement.addEventListener("pointerup",Ye)),$(w),w.pointerType==="touch"?_(w):He(w))}function je(w){n.enabled!==!1&&(w.pointerType==="touch"?M(w):Ue(w))}function Ye(w){ne(w),y.length===0&&(n.domElement.releasePointerCapture(w.pointerId),n.domElement.removeEventListener("pointermove",je),n.domElement.removeEventListener("pointerup",Ye)),n.dispatchEvent(oo),s=r.NONE}function lt(w){ne(w)}function He(w){let L;switch(w.button){case 0:L=n.mouseButtons.LEFT;break;case 1:L=n.mouseButtons.MIDDLE;break;case 2:L=n.mouseButtons.RIGHT;break;default:L=-1}switch(L){case Vn.DOLLY:if(n.enableZoom===!1)return;j(w),s=r.DOLLY;break;case Vn.ROTATE:if(w.ctrlKey||w.metaKey||w.shiftKey){if(n.enablePan===!1)return;G(w),s=r.PAN}else{if(n.enableRotate===!1)return;K(w),s=r.ROTATE}break;case Vn.PAN:if(w.ctrlKey||w.metaKey||w.shiftKey){if(n.enableRotate===!1)return;K(w),s=r.ROTATE}else{if(n.enablePan===!1)return;G(w),s=r.PAN}break;default:s=r.NONE}s!==r.NONE&&n.dispatchEvent(os)}function Ue(w){switch(s){case r.ROTATE:if(n.enableRotate===!1)return;te(w);break;case r.DOLLY:if(n.enableZoom===!1)return;J(w);break;case r.PAN:if(n.enablePan===!1)return;F(w);break}}function Et(w){n.enabled===!1||n.enableZoom===!1||s!==r.NONE||(w.preventDefault(),n.dispatchEvent(os),k(w),n.dispatchEvent(oo))}function mt(w){n.enabled===!1||n.enablePan===!1||Q(w)}function _(w){switch(se(w),y.length){case 1:switch(n.touches.ONE){case Dn.ROTATE:if(n.enableRotate===!1)return;ee(),s=r.TOUCH_ROTATE;break;case Dn.PAN:if(n.enablePan===!1)return;re(),s=r.TOUCH_PAN;break;default:s=r.NONE}break;case 2:switch(n.touches.TWO){case Dn.DOLLY_PAN:if(n.enableZoom===!1&&n.enablePan===!1)return;Ee(),s=r.TOUCH_DOLLY_PAN;break;case Dn.DOLLY_ROTATE:if(n.enableZoom===!1&&n.enableRotate===!1)return;he(),s=r.TOUCH_DOLLY_ROTATE;break;default:s=r.NONE}break;default:s=r.NONE}s!==r.NONE&&n.dispatchEvent(os)}function M(w){switch(se(w),s){case r.TOUCH_ROTATE:if(n.enableRotate===!1)return;ve(w),n.update();break;case r.TOUCH_PAN:if(n.enablePan===!1)return;de(w),n.update();break;case r.TOUCH_DOLLY_PAN:if(n.enableZoom===!1&&n.enablePan===!1)return;Ae(w),n.update();break;case r.TOUCH_DOLLY_ROTATE:if(n.enableZoom===!1&&n.enableRotate===!1)return;xe(w),n.update();break;default:s=r.NONE}}function q(w){n.enabled!==!1&&w.preventDefault()}function $(w){y.push(w)}function ne(w){delete R[w.pointerId];for(let L=0;L<y.length;L++)if(y[L].pointerId==w.pointerId){y.splice(L,1);return}}function se(w){let L=R[w.pointerId];L===void 0&&(L=new Se,R[w.pointerId]=L),L.set(w.pageX,w.pageY)}function Me(w){const L=w.pointerId===y[0].pointerId?y[1]:y[0];return R[L.pointerId]}n.domElement.addEventListener("contextmenu",q),n.domElement.addEventListener("pointerdown",et),n.domElement.addEventListener("pointercancel",lt),n.domElement.addEventListener("wheel",Et,{passive:!1}),this.update()}}var gp=Object.defineProperty,vp=(i,e,t)=>e in i?gp(i,e,{enumerable:!0,configurable:!0,writable:!0,value:t}):i[e]=t,lo=(i,e,t)=>(vp(i,typeof e!="symbol"?e+"":e,t),t);const Pi={};var ko=[{n:"props",t:"array",type:{t:"datatype"}},{n:"validate",t:"function"},{n:"getDef",t:"function"},{n:"setValid",t:"function"},{n:"fromBinary",t:"function"},{n:"toBinary",t:"function"},{n:"getBinarySize",t:"function"}];function xp(i,e){return Pi[i]?(console.warn("This datatype already exists : ",i),!1):Rs(e)?(Pi[i]=new Ho(e),!0):!1}function Rs(i,e){let t=!0,n=!0,r=[];ko.forEach(l=>{i[l.n]===void 0&&(t=!1,n=!1,r.push(l))});let s=i,o=Object.assign({},i);if(!t&&(e&&console.warn("Missing properties:",r,"to be a valid trueDatatype"),s=Pi[i.t],s?s=s.datatype:i.getT&&(s=Pi[i.getT(i)],s&&(s=s.datatype)),s))for(var a in s)o[a]==null&&(o[a]=s[a]);return s?(t=!0,!n&&s.validateDatatype&&(t=s.validateDatatype(o,e))):e&&console.warn("t:",i.t," isn't a registered datatype"),t}class Ho{constructor(e){if(lo(this,"trueType"),lo(this,"datatype"),!Rs(e,!1))throw Error("Invalid datatype : ",e,". For more details run 'validateDatatype(yourDatatype, true);'");let t=!0,n=Pi[e.t];if(ko.forEach(s=>{e[s.n]===void 0&&(t=!1)}),!t){for(var r in n.datatype)e[r]==null&&(e[r]=n.datatype[r]);e.initDatatype&&e.initDatatype(e)}this.datatype=e,this.trueType=t}validate(e){return this.datatype.opt&&e==null?!0:this.datatype.validate(e,this.datatype)}getDefault(){let e=this.datatype.def;return this.datatype.opt?e:(e==null&&(e=this.datatype.getDef(this.datatype)),this.setValid(e))}setValid(e){if(!(this.datatype.opt&&e==null))return this.datatype.setValid(e,this.datatype)}fromBinary(e,t){if(t||(t={pos:0,view:new DataView(e)}),this.datatype.opt){let n=t.view.getUint8(t.pos,!0);if(t.pos+=1,n==0)return}return this.datatype.fromBinary(e,t,this.datatype)}toBinary(e,t,n){if(!t){let r=this.getBinarySize(e);t=new ArrayBuffer(r)}return n||(n={pos:0,view:new DataView(t)}),this.datatype.opt&&(n.view.setUint8(n.pos,e==null?0:1,!0),n.pos+=1,e==null)?t:this.datatype.toBinary(e,t,n,this.datatype)}getBinarySize(e,t){let n=0;if(this.datatype.opt){if(e==null)return 1;n++}return this.datatype.getBinarySize(e,this.datatype,t|0)+n}getInputUI(e,t){if(typeof t=="function"&&(t={onchange:t}),this.datatype.getInputUI){let r;if(this.datatype.opt){let s=e;if(s==null){let d=this.datatype.def;d==null&&(d=this.datatype.getDef(this.datatype)),s=this.setValid(d)}let o=Object.assign({},t);o.onchange=function(d){s=d,t.onchange&&t.onchange(d)};let a=this.datatype.getInputUI(s,this.datatype,o),l=[];if(a instanceof DocumentFragment)for(var n=0;n<a.children.length;n++)l.push(a.children[n]);let c=d=>{if(l.length)for(var f=0;f<l.length;f++)l[f].disabled=d;else a.disabled=d},u=Ti({elt:"input",type:"checkbox",on:{change:function(){this.checked?(c(!1),t.oninput&&t.oninput(s),t.onchange&&t.onchange(s)):(c(!0),t.oninput&&t.oninput(void 0),t.onchange&&t.onchange(void 0))}}});e==null?c(!0):u.checked=!0,r=Ti({elt:"frag",children:[a,u]})}else r=this.datatype.getInputUI(e,this.datatype,t);return r}else return Ti({text:"No getInputUI provided"})}}const Pt=Ho,Ei=Rs,Fe=xp;var co={},Zo={};function fn(i,e){typeof i!="object"&&(i={});var t=i.elt||"div";switch(typeof t){case"string":if(co[t])t=co[t](i,e||i.opt);else switch(t){default:t=document.createElement(t);break;case"frag":t=document.createDocumentFragment();break}break;case"function":t=t(i,e);break}for(var n in i){var r=i[n];switch(n){default:["value"].indexOf(n)<0?t.setAttribute(n,r):t[n]=r;break;case"opt":case"elt":break;case"getRef":r(t);break;case"class":for(var s=r.split(" "),o=0,a=s.length;o<a;o++)t.classList.add(s[o]);break;case"HTML":t.innerHTML=r;break;case"Text":t.innerText=r;break;case"child":for(var o=0;o<r.length;o++)t.appendChild(fn(r[o]));break;case"children":for(var o=0;o<r.length;o++)t.appendChild(r[o]);break;case"setAttr":for(var l in r)t.setAttribute(l,r[l]);break;case"setProps":for(var l in r)t[l]=r[l];break;case"style":for(var l in r)t.style[l]=r[l];break;case"on":for(var l in r)switch(l){case"down":t.addEventListener("mousedown",r[l],!0),t.addEventListener("touchstart",r[l],!0);break;case"up":t.addEventListener("mouseup",r[l],!0),t.addEventListener("touchend",r[l],!0);break;case"pick":t.addEventListener("click",r[l],!0);break;case"drag":Mp(t,r[l]);break;default:t.addEventListener(l,r[l],!0);break}break}}return t}function Mp(i,e){typeof e!="object"&&(e={}),i.addEventListener("mousedown",function(t){let n=t.button,r=function(o){e.drag&&e.drag(o)},s=function(o){o.button==n&&(window.removeEventListener("mousemove",r,!0),window.removeEventListener("mouseup",s,!0),e.endDrag&&e.endDrag(o),e.end&&e.end(o))};window.addEventListener("mousemove",r,!0),window.addEventListener("mouseup",s,!0),e.startDrag&&e.startDrag(t),e.start&&e.start(t)},!0),i.addEventListener("touchstart",function(t){let n=t.changedTouches[0],r=function(o){o.changedTouches.indexOf(n)<0||e.drag&&e.drag(o)},s=function(o){o.changedTouches.indexOf(n)<0||(window.removeEventListener("touchmove",r,!0),window.removeEventListener("touchend",s,!0),e.endDrag&&e.endDrag(o))};window.addEventListener("touchmove",r,!0),window.addEventListener("touchend",s,!0),e.startDrag&&e.startDrag(t)},!0)}fn.setTranslations=function(i){for(var e in i)Zo[e]=i[e]};fn.getTranslation=fn.getT=function(i){return Zo[i]||i};fn.translatePage=function(){for(var i=document.body.querySelectorAll("[trad]"),e=0,t=i.length;e<t;e++)i[e].innerText=this.getT(i[e].getAttribute("trad"));for(var i=document.body.querySelectorAll("[tradVal]"),e=0,t=i.length;e<t;e++)i[e].value=this.getT(i[e].getAttribute("tradVal"));for(var i=document.body.querySelectorAll("[tradHover]"),e=0,t=i.length;e<t;e++){let o=this.getT(i[e].getAttribute("tradHover"));i[e].setAttribute("hoverTitle",o)}};fn.addHover=function(i,e){typeof e!="object"&&(e={});let t={position:e.position||function(o,a){let l=window.innerHeight,c=o.getBoundingClientRect(),u=0,d=0,f=0;return c.top>100?f=l-c.top:(u|=1,f=c.bottom),d=c.left+c.width/2,u|=2,[d,f,u]},marginV:e.marginV||0,marginH:e.marginH||0,timeout:e.timeout==null?500:e.timeout,hide:e.hide||function(o){o.style.display="none"},show:e.show||function(o){o.style.display="block"}},n=bt(i);t.hide(n),n.style.position="fixed",n.style.pointerEvents="none";let r=null,s=0;window.addEventListener("mousemove",function(o){let a=o.target;a!=r&&(r=a,s&&clearTimeout(s),t.hide(n),s=this.setTimeout(function(){let l=r.getAttribute("hoverTitle");if(l){t.show(n),n.innerText=l;let[c,u,d]=t.position(r,o);d&1?(n.style.top=u+t.marginV+"px",n.style.bottom=""):(n.style.bottom=u+t.marginV+"px",n.style.top=""),d&2?(n.style.left=c+t.marginH+"px",n.style.right=""):(n.style.right=c+t.marginH+"px",n.style.left="")}},t.timeout))},!0),document.body.appendChild(n)};fn.watch=function(i,e,t){let n=i[e];Object.defineProperty(i,e,{get:()=>n,set:r=>{n=r,t(n)}})};const bt=fn,wt=(i,e,t)=>{let n={elt:"input",type:"text",on:{change:function(){let r=e.setValid(this.value,e);this.value=r,t.onchange&&t.onchange(r)}},value:i};if(e.hasOwnProperty("min")&&e.hasOwnProperty("max")&&e.hasOwnProperty("step")){let r=bt({elt:"input",type:"range",min:e.min,max:e.max,step:e.step,style:{margin:"-0.3em 0 0 0"}}),s=bt({elt:"input",type:"text"}),o=bt({elt:"br"});return r.value=s.value=i,r.onchange=s.onchange=function(){let a=e.setValid(this.value,e);r.value=s.value=a,t.onchange&&t.onchange(a)},r.oninput=function(){let a=e.setValid(this.value,e);r.value=s.value=a,t.oninput&&t.oninput(a)},s.oninput=function(){let a=e.setValid(this.value,e);r.value=a,t.oninput&&t.oninput(a)},bt({elt:"frag",children:[s,o,r]})}return t.oninput&&(n.on.input=function(){let r=e.setValid(this.value,e);t.oninput(r)}),bt(n)},yp={props:[{n:"min",t:"number",opt:!0},{n:"max",t:"number",opt:!0}],validate(i,e){return!(typeof i!="number"||e.setValid(i,e)!==i)},getDef(){return 0},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;switch(typeof i){case"number":t=i;break;case"string":let n=parseFloat(i);isNaN(n)||(t=n);break}return e.min!==void 0&&e.min>t&&(t=e.min),e.max!==void 0&&e.max<t&&(t=e.max),t},fromBinary(i,e,t){let n=e.view.getFloat64(e.pos,!0);return e.pos+=8,n},toBinary(i,e,t,n){return t.view.setFloat64(t.pos,i,!0),t.pos+=8,e},getBinarySize(i,e){return 8},getInputUI:wt},Ap={props:[],validate(i,e){return typeof i=="string"},getDef(){return""},setValid(i,e){return typeof i!="string"?i.toString?i.toString():"":i},fromBinary(i,e,t){let n=e.view.getUint32(e.pos,!0);e.pos+=4;let r=new Uint8Array(i,e.pos,n);var s=new TextDecoder("utf-8");return e.pos+=r.byteLength,s.decode(r)},toBinary(i,e,t,n){let r=new TextEncoder().encode(i);return t.view.setUint32(t.pos,r.byteLength,!0),t.pos+=4,new Uint8Array(e,t.pos,r.byteLength).set(r),t.pos+=r.byteLength,e},getBinarySize(i,e){return 4+new TextEncoder().encode(i).byteLength},getInputUI:wt},_p={props:[],validate(i,e){return!(typeof i!="string"||new TextEncoder().encode(i).byteLength>255)},getDef(){return""},setValid(i,e){let t=i;typeof i!="string"&&(i.toString?t=i.toString():t="");let n=new TextEncoder().encode(t);return n.byteLength>255&&(t=new TextDecoder("utf-8").decode(n.slice(0,255))),t},fromBinary(i,e,t){let n=e.view.getUint8(e.pos,!0);e.pos+=1;let r=new Uint8Array(i,e.pos,n);var s=new TextDecoder("utf-8");return e.pos+=r.byteLength,s.decode(r)},toBinary(i,e,t,n){let r=new TextEncoder().encode(i);return t.view.setUint8(t.pos,r.byteLength,!0),t.pos+=1,new Uint8Array(e,t.pos,r.byteLength).set(r),t.pos+=r.byteLength,e},getBinarySize(i,e){return 1+new TextEncoder().encode(i).byteLength},getInputUI:wt},bp=_p,Sp={props:[],validate(i,e){return typeof i=="boolean"},getDef(){return!1},setValid(i,e){return!!i},fromBinary(i,e,t){let n=e.view.getUint8(e.pos,!0);return e.pos+=1,!!n},toBinary(i,e,t,n){return t.view.setUint8(t.pos,i?1:0,!0),t.pos+=1,e},getBinarySize(i){return 1},getInputUI(i,e,t){let n={elt:"input",type:"checkbox",on:{change:function(r){r=this.checked,t.onchange&&t.onchange(r)}}};return i&&(n.checked=!0),t.oninput&&(n.on.input=function(){t.oninput(this.checked)}),bt(n)}},wp=Sp,Ep={props:[{n:"properties",t:"array",type:{t:"datatype"}}],validateDatatype(i,e){if(!i.properties)return e&&console.warn("Missing properties in ",i),!1;for(var t=0;t<i.properties.length;t++){let n=i.properties[t];if(n instanceof Pt)n=n.datatype;else if(!Ei(n))return e&&console.warn("This is not a valid datatype",n),!1;if(!n.n)return e&&console.warn("Missing property name 'n' in ",n),!1}return!0},initDatatype(i){for(var e=0;e<i.properties.length;e++)i.properties[e]instanceof Pt||(i.properties[e]=new Pt(i.properties[e]))},validate(i,e){if(typeof i!="object")return!1;for(var t=0;t<e.properties.length;t++){let n=e.properties[t];if(!n.validate(i[n.datatype.n]))return!1}return!0},getDef(i){let e={};for(var t=0;t<i.properties.length;t++){let n=i.properties[t];e[n.datatype.n]=n.getDefault()}return e},setValid(i,e){let t=typeof i=="object"?i:{};for(var n=0;n<e.properties.length;n++){let r=e.properties[n];t[r.datatype.n]=r.setValid(t[r.datatype.n])}return t},fromBinary(i,e,t){let n={};for(var r=0;r<t.properties.length;r++){let s=t.properties[r],o=s.datatype;n[o.n]=s.fromBinary(i,e)}return n},toBinary(i,e,t,n){for(var r=0;r<n.properties.length;r++){let s=n.properties[r],o=s.datatype;s.toBinary(i[o.n],e,t)}return e},getBinarySize(i,e,t){let n=0;for(var r=0;r<e.properties.length;r++){let s=e.properties[r],o=s.datatype;n+=s.getBinarySize(i[o.n],t+n)}return n},getInputUI(i,e,t){let n={elt:"table",class:"inputObject",child:[]},r=d=>{let f=Object.assign({},t);return t.onchange?f.onchange=function(m){i[d]=m,t.onchange(i),s()}:f.onchange=function(m){i[d]=m,s()},t.oninput?f.oninput=function(m){i[d]=m,t.oninput(i)}:f.oninput=function(m){i[d]=m},f},s=()=>{for(var d=0,f=e.properties.length;d<f;d++){let p=e.properties[d].datatype,h=o[d];if(p.cond&&!wi(i,p.cond))for(var m=0,g=h.length;m<g;m++)u.children[h[m]].style.display="none";else for(var m=0,g=h.length;m<g;m++)u.children[h[m]].style.display="table-row"}},o=[],a=0;for(var l=0,c=e.properties.length;l<c;l++){let d=e.properties[l],f=d.datatype.n,m={elt:"td",Text:f};t.translate&&(m.Text=bt.getT(f),m.trad=f),Array.isArray(d.datatype.properties)?(m.colspan=2,n.child.push({elt:"tr",child:[m]},{elt:"tr",child:[{elt:"td",colspan:2,style:{paddingLeft:"1em"},children:[d.getInputUI(i[f],r(f))]}]}),o.push([a,a+1]),a+=2):(n.child.push({elt:"tr",child:[m,{elt:"td",children:[d.getInputUI(i[f],r(f))]}]}),o.push([a++]))}let u=bt(n);return s(),u}};function wi(i,e){if(e[0]=="&&"){let n=wi(i,e[1]);for(var t=2;t<e.length;t++)n=n&&wi(i,e[t]);return n}else if(e[0]=="||"){let n=wi(i,e[1]);for(var t=2;t<e.length;t++)n=n||wi(i,e[t]);return n}else{let n=i[e[0]],r=e[2];switch(e[1]){case"=":case"==":return n==r;case"===":return n===r;case"!=":return n!=r;case"!==":return n!==r;case"<":return n<r;case">":return n>r;case"<=":return n<=r;case">=":return n>=r;default:return console.warn("Unknown operator",e[1]),!1}}}const Tp=Ep,Ip={props:[{n:"rt",t:"datatype"},{n:"args",t:"array",type:{t:"object",properties:[{n:"name",t:"string"},{n:"type",t:"datatype"}]}}],validate(i,e){return typeof i=="function"},getDef(){return function(i){return i}},setValid(i,e){return typeof i!="function"?(console.warn("Could not set a valid function that do what you should expect. You may encounter bugs from now.",i),()=>{}):i},fromBinary(i,e,t){let n=e.view.getUint32(e.pos,!0);e.pos+=4;let r=new Uint8Array(i,e.pos,n);var s=new TextDecoder("utf-8");return new Function("return "+s.decode(r))()},toBinary(i,e,t,n){let r=new TextEncoder().encode(i.toString());return t.view.setUint32(t.pos,r.byteLength,!0),t.pos+=4,new Uint8Array(e,t.pos,r.byteLength).set(r),t.pos+=r.byteLength,e},getBinarySize(i,e){return 4+new TextEncoder().encode(i.toString()).byteLength},getInputUI:wt},Rp={props:[{n:"type",t:"datatype"},{n:"length",t:"number",opt:!0}],validateDatatype(i,e){return i.type?!i.type instanceof Pt&&!Ei(i.type,e)?(e&&console.warn("Invalid datatype",i.type),!1):!0:(e&&console.warn("Missing type in ",i),!1)},initDatatype(i){i.type instanceof Pt||(i.type=new Pt(i.type))},validate(i,e){if(!Array.isArray(i))return!1;let t=e.type;for(var n=0;n<i.length;n++)if(!t.validate(i[n]))return!1;return!(e.length&&e.length!==i.length)},getDef(){return[]},setValid(i,e){let t=Array.isArray(i)?i:[],n=e.type;for(var r=0;r<t.length;r++)t[r]=n.setValid(t[r]);if(e.length&&e.length!==t.length)if(e.length>t.length)for(var r=t.length;r<=e.length;r++)t.push(n.getDefault());else t=t.slice(0,e.length);return t},fromBinary(i,e,t){let n=[],r=t.type,s=0;t.length?s=t.length:(s=e.view.getUint32(e.pos,!0),e.pos+=4);for(var o=0;o<s;o++)n.push(r.fromBinary(i,e));return n},toBinary(i,e,t,n){n.length||(t.view.setUint32(t.pos,i.length,!0),t.pos+=4);let r=n.type;for(var s=0;s<i.length;s++)r.toBinary(i[s],e,t);return e},getBinarySize(i,e,t){let n=e.length?0:4,r=e.type;for(var s=0;s<i.length;s++)n+=r.getBinarySize(i[s],t+n);return n}},Cp=Rp,Lp={props:[],validate(i,e){return i instanceof Pt?!0:Ei(i)},getDef(){return new Pt({t:"number"})},setValid(i,e){return i instanceof Pt?i:Ei(i)?new Pt(i):typeof i=="string"&&Ei({t:i})?new Pt({t:i}):(console.warn("Could not automatically set a valid datatype. You may find bugs"),this.getDef())},fromBinary(i,e,t){},toBinary(i,e,t){},getBinarySize(i){return 8}},Pp=Lp,zp={props:[{n:"min",t:"number",opt:!0},{n:"max",t:"number",opt:!0}],validate(i,e){return!(typeof i!="number"||e.setValid(i,e)!==i)},getDef(){return 0},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;switch(typeof i){case"number":t=i|0;break;case"string":let n=parseInt(i);isNaN(n)||(t=n);break}return e.min!==void 0&&e.min>t&&(t=e.min),e.max!==void 0&&e.max<t&&(t=e.max),t>255&&(t=255),t<0&&(t=0),t},fromBinary(i,e,t){let n=e.view.getUint8(e.pos,!0);return e.pos+=1,n},toBinary(i,e,t,n){return t.view.setUint8(t.pos,i,!0),t.pos+=1,e},getBinarySize(i,e){return 1},getInputUI:wt},Vp=zp,Dp={props:[{n:"min",t:"number",opt:!0},{n:"max",t:"number",opt:!0}],validate(i,e){return!(typeof i!="number"||e.setValid(i,e)!==i)},getDef(){return 0},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;switch(typeof i){case"number":t=i|0;break;case"string":let n=parseInt(i);isNaN(n)||(t=n);break}return e.min!==void 0&&e.min>t&&(t=e.min),e.max!==void 0&&e.max<t&&(t=e.max),t>65535&&(t=65535),t<0&&(t=0),t},fromBinary(i,e,t){let n=e.view.getUint16(e.pos,!0);return e.pos+=2,n},toBinary(i,e,t,n){return t.view.setUint16(t.pos,i,!0),t.pos+=2,e},getBinarySize(i,e){return 2},getInputUI:wt},Up=Dp,Np={props:[{n:"min",t:"number",opt:!0},{n:"max",t:"number",opt:!0}],validate(i,e){return!(typeof i!="number"||e.setValid(i,e)!==i)},getDef(){return 0},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;switch(typeof i){case"number":t=i|0;break;case"string":let n=parseInt(i);isNaN(n)||(t=n);break}return e.min!==void 0&&e.min>t&&(t=e.min),e.max!==void 0&&e.max<t&&(t=e.max),t>4294967295&&(t=4294967295),t<0&&(t=0),t},fromBinary(i,e,t){let n=e.view.getUint32(e.pos,!0);return e.pos+=4,n},toBinary(i,e,t,n){return t.view.setUint32(t.pos,i,!0),t.pos+=4,e},getBinarySize(i,e){return 4},getInputUI:wt},Fp=Np,qp={props:[{n:"min",t:"number",opt:!0},{n:"max",t:"number",opt:!0}],validate(i,e){return!(typeof i!="number"||e.setValid(i,e)!==i)},getDef(){return 0},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;switch(typeof i){case"number":t=i|0;break;case"string":let n=parseInt(i);isNaN(n)||(t=n);break}return e.min!==void 0&&e.min>t&&(t=e.min),e.max!==void 0&&e.max<t&&(t=e.max),t>127&&(t=127),t<-127&&(t=-127),t},fromBinary(i,e,t){let n=e.view.getInt8(e.pos,!0);return e.pos+=1,n},toBinary(i,e,t,n){return t.view.setInt8(t.pos,i,!0),t.pos+=1,e},getBinarySize(i,e){return 1},getInputUI:wt},Op=qp,Bp={props:[{n:"min",t:"number",opt:!0},{n:"max",t:"number",opt:!0}],validate(i,e){return!(typeof i!="number"||e.setValid(i,e)!==i)},getDef(){return 0},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;switch(typeof i){case"number":t=i|0;break;case"string":let n=parseInt(i);isNaN(n)||(t=n);break}return e.min!==void 0&&e.min>t&&(t=e.min),e.max!==void 0&&e.max<t&&(t=e.max),t>32767&&(t=32767),t<-32767&&(t=-32767),t},fromBinary(i,e,t){let n=e.view.getInt16(e.pos,!0);return e.pos+=2,n},toBinary(i,e,t,n){return t.view.setInt16(t.pos,i,!0),t.pos+=2,e},getBinarySize(i,e){return 2},getInputUI:wt},kp=Bp,Hp={props:[{n:"min",t:"number",opt:!0},{n:"max",t:"number",opt:!0}],validate(i,e){return!(typeof i!="number"||e.setValid(i,e)!==i)},getDef(){return 0},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;switch(typeof i){case"number":t=i|0;break;case"string":let n=parseInt(i);isNaN(n)||(t=n);break}return e.min!==void 0&&e.min>t&&(t=e.min),e.max!==void 0&&e.max<t&&(t=e.max),t>2147483647&&(t=2147483647),t<-2147483647&&(t=-2147483647),t},fromBinary(i,e,t){let n=e.view.getInt32(e.pos,!0);return e.pos+=4,n},toBinary(i,e,t,n){return t.view.setInt32(t.pos,i,!0),t.pos+=4,e},getBinarySize(i,e){return 4},getInputUI:wt},Zp=Hp,Gp={props:[{n:"min",t:"number",opt:!0},{n:"max",t:"number",opt:!0}],validate(i,e){return!(typeof i!="number"||e.setValid(i,e)!==i)},getDef(){return 0},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;switch(typeof i){case"number":t=i;break;case"string":let n=parseFloat(i);isNaN(n)||(t=n);break}return e.min!==void 0&&e.min>t&&(t=e.min),e.max!==void 0&&e.max<t&&(t=e.max),t},fromBinary(i,e,t){let n=e.view.getFloat32(e.pos,!0);return e.pos+=4,n},toBinary(i,e,t,n){return t.view.setFloat32(t.pos,i,!0),t.pos+=4,e},getBinarySize(i,e){return 4},getInputUI:wt},Wp=Gp,Xp={props:[{n:"min",t:"number",opt:!0},{n:"max",t:"number",opt:!0}],validate(i,e){return!(typeof i!="number"||e.setValid(i,e)!==i)},getDef(){return 0},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;switch(typeof i){case"number":t=i;break;case"string":let n=parseFloat(i);isNaN(n)||(t=n);break}return e.min!==void 0&&e.min>t&&(t=e.min),e.max!==void 0&&e.max<t&&(t=e.max),t},fromBinary(i,e,t){let n=e.view.getFloat64(e.pos,!0);return e.pos+=8,n},toBinary(i,e,t,n){return t.view.setFloat64(t.pos,i,!0),t.pos+=8,e},getBinarySize(i,e){return 8},getInputUI:wt},jp=Xp,Yp={props:[{n:"length",t:"number",opt:!0}],validate(i,e){return!(!(i instanceof Int8Array)||e.length&&e.length!==i.length)},getDef(i){return new Int8Array(i.length||0)},setValid(i,e){let t=i;return i instanceof Int8Array||(Array.isArray(i)?t=new Int8Array(i):t=new Int8Array(e.length||0)),e.length&&e.length!==t.length&&(e.length>t.length?t=new Int8Array(e.length).set(t):t=t.slice(0,e.length)),t},fromBinary(i,e,t){let n=0;return t.length?n=t.length:(n=e.view.getUint32(e.pos,!0),e.pos+=4),new Int8Array(i,e.pos,n)},toBinary(i,e,t,n){return n.length||(t.view.setUint32(t.pos,i.length,!0),t.pos+=4),new Int8Array(e,t.pos,i.length).set(i),e},getBinarySize(i,e){let t=e.length?0:4;return t+=i.length,t}},Kp=Yp,Qp={props:[{n:"length",t:"number",opt:!0}],validate(i,e){return!(!(i instanceof Uint8Array)||e.length&&e.length!==i.length)},getDef(i){return new Uint8Array(i.length||0)},setValid(i,e){let t=i;return i instanceof Uint8Array||(Array.isArray(i)?t=new Uint8Array(i):t=new Uint8Array(e.length||0)),e.length&&e.length!==t.length&&(e.length>t.length?t=new Uint8Array(e.length).set(t):t=t.slice(0,e.length)),t},fromBinary(i,e,t){let n=0;return t.length?n=t.length:(n=e.view.getUint32(e.pos,!0),e.pos+=4),new Uint8Array(i,e.pos,n)},toBinary(i,e,t,n){return n.length||(t.view.setUint32(t.pos,i.length,!0),t.pos+=4),new Uint8Array(e,t.pos,i.length).set(i),e},getBinarySize(i,e){let t=e.length?0:4;return t+=i.length,t}},Jp=Qp,$p={props:[{n:"length",t:"number",opt:!0}],validate(i,e){return!(!(i instanceof Int16Array)||e.length&&e.length!==i.length)},getDef(i){return new Int16Array(i.length||0)},setValid(i,e){let t=i;return i instanceof Int16Array||(Array.isArray(i)?t=new Int16Array(i):t=new Int16Array(e.length||0)),e.length&&e.length!==t.length&&(e.length>t.length?t=new Int16Array(e.length).set(t):t=t.slice(0,e.length)),t},fromBinary(i,e,t){let n=0;t.length?n=t.length:(n=e.view.getUint32(e.pos,!0),e.pos+=4);let r=e.pos%2;r!==0&&(e.pos+=2-r);let s=new Int16Array(i,e.pos,n);return e.pos+=n*2,s},toBinary(i,e,t,n){n.length||(t.view.setUint32(t.pos,i.length,!0),t.pos+=4);let r=t.pos%2;return r!==0&&(t.pos+=2-r),new Int16Array(e,t.pos,i.length).set(i),t.pos+=i.length*2,e},getBinarySize(i,e,t){let n=e.length?0:4,r=(t+n)%2;return r!==0&&(n+=2-r),n+=i.length*2,n}},em=$p,tm={props:[{n:"length",t:"number",opt:!0}],validate(i,e){return!(!(i instanceof Uint16Array)||e.length&&e.length!==i.length)},getDef(i){return new Uint16Array(i.length||0)},setValid(i,e){let t=i;return i instanceof Uint16Array||(Array.isArray(i)?t=new Uint16Array(i):t=new Uint16Array(e.length||0)),e.length&&e.length!==t.length&&(e.length>t.length?t=new Uint16Array(e.length).set(t):t=t.slice(0,e.length)),t},fromBinary(i,e,t){let n=0;t.length?n=t.length:(n=e.view.getUint32(e.pos,!0),e.pos+=4);let r=e.pos%2;r!==0&&(e.pos+=2-r);let s=new Uint16Array(i,e.pos,n);return e.pos+=n*2,s},toBinary(i,e,t,n){n.length||(t.view.setUint32(t.pos,i.length,!0),t.pos+=4);let r=t.pos%2;return r!==0&&(t.pos+=2-r),new Uint16Array(e,t.pos,i.length).set(i),t.pos+=i.length*2,e},getBinarySize(i,e,t){let n=e.length?0:4,r=(t+n)%2;return r!==0&&(n+=2-r),n+=i.length*2,n}},nm=tm,im={props:[{n:"length",t:"number",opt:!0}],validate(i,e){return!(!(i instanceof Int32Array)||e.length&&e.length!==i.length)},getDef(i){return new Int32Array(i.length||0)},setValid(i,e){let t=i;return i instanceof Int32Array||(Array.isArray(i)?t=new Int32Array(i):t=new Int32Array(e.length||0)),e.length&&e.length!==t.length&&(e.length>t.length?t=new Int32Array(e.length).set(t):t=t.slice(0,e.length)),t},fromBinary(i,e,t){let n=0;t.length?n=t.length:(n=e.view.getUint32(e.pos,!0),e.pos+=4);let r=e.pos%4;r!==0&&(e.pos+=4-r);let s=new Int32Array(i,e.pos,n);return e.pos+=n*4,s},toBinary(i,e,t,n){n.length||(t.view.setUint32(t.pos,i.length,!0),t.pos+=4);let r=t.pos%4;return r!==0&&(t.pos+=4-r),new Int32Array(e,t.pos,i.length).set(i),t.pos+=i.length*4,e},getBinarySize(i,e,t){let n=e.length?0:4,r=(t+n)%4;return r!==0&&(n+=4-r),n+=i.length*4,n}},rm=im,sm={props:[{n:"length",t:"number",opt:!0}],validate(i,e){return!(!(i instanceof Uint32Array)||e.length&&e.length!==i.length)},getDef(i){return new Uint32Array(i.length||0)},setValid(i,e){let t=i;return i instanceof Uint32Array||(Array.isArray(i)?t=new Uint32Array(i):t=new Uint32Array(e.length||0)),e.length&&e.length!==t.length&&(e.length>t.length?t=new Uint32Array(e.length).set(t):t=t.slice(0,e.length)),t},fromBinary(i,e,t){let n=0;t.length?n=t.length:(n=e.view.getUint32(e.pos,!0),e.pos+=4);let r=e.pos%4;r!==0&&(e.pos+=4-r);let s=new Uint32Array(i,e.pos,n);return e.pos+=n*4,s},toBinary(i,e,t,n){n.length||(t.view.setUint32(t.pos,i.length,!0),t.pos+=4);let r=t.pos%4;return r!==0&&(t.pos+=4-r),new Uint32Array(e,t.pos,i.length).set(i),t.pos+=i.length*4,e},getBinarySize(i,e,t){let n=e.length?0:4,r=(t+n)%4;return r!==0&&(n+=4-r),n+=i.length*4,n}},am=sm,om={props:[{n:"length",t:"number",opt:!0}],validate(i,e){return!(!(i instanceof Float32Array)||e.length&&e.length!==i.length)},getDef(i){return new Float32Array(i.length||0)},setValid(i,e){let t=i;return i instanceof Float32Array||(Array.isArray(i)?t=new Float32Array(i):t=new Float32Array(e.length||0)),e.length&&e.length!==t.length&&(e.length>t.length?t=new Float32Array(e.length).set(t):t=t.slice(0,e.length)),t},fromBinary(i,e,t){let n=0;t.length?n=t.length:(n=e.view.getUint32(e.pos,!0),e.pos+=4);let r=e.pos%4;r!==0&&(e.pos+=4-r);let s=new Float32Array(i,e.pos,n);return e.pos+=n*4,s},toBinary(i,e,t,n){n.length||(t.view.setUint32(t.pos,i.length,!0),t.pos+=4);let r=t.pos%4;return r!==0&&(t.pos+=4-r),new Float32Array(e,t.pos,i.length).set(i),t.pos+=i.length*4,e},getBinarySize(i,e,t){let n=e.length?0:4,r=(t+n)%4;return r!==0&&(n+=4-r),n+=i.length*4,n}},lm=om,cm={props:[{n:"length",t:"number",opt:!0}],validate(i,e){return!(!(i instanceof Float64Array)||e.length&&e.length!==i.length)},getDef(i){return new Float64Array(i.length||0)},setValid(i,e){let t=i;return i instanceof Float64Array||(Array.isArray(i)?t=new Float64Array(i):t=new Float64Array(e.length||0)),e.length&&e.length!==t.length&&(e.length>t.length?t=new Float64Array(e.length).set(t):t=t.slice(0,e.length)),t},fromBinary(i,e,t){let n=0;t.length?n=t.length:(n=e.view.getUint32(e.pos,!0),e.pos+=4);let r=e.pos%8;r!==0&&(e.pos+=8-r);let s=new Float64Array(i,e.pos,n);return e.pos+=n*8,s},toBinary(i,e,t,n){n.length||(t.view.setUint32(t.pos,i.length,!0),t.pos+=4);let r=t.pos%8;return r!==0&&(t.pos+=8-r),new Float64Array(e,t.pos,i.length).set(i),t.pos+=i.length*8,e},getBinarySize(i,e,t){let n=e.length?0:4,r=(t+n)%8;return r!==0&&(n+=8-r),n+=i.length*8,n}},um=cm,dm={props:[{n:"values",t:"array"}],validate(i,e){return!(typeof i!="number"&&(i|0)!==i&&i<0&&i>=e.values.length)},getDef(){return 0},setValid(i,e){return typeof i!="number"&&(i|0)!==i&&i<0&&i>=e.values.length?0:i},fromBinary(i,e,t){let n=e.view.getUint8(e.pos,!0);return e.pos+=1,n},toBinary(i,e,t,n){return t.view.setUint8(t.pos,i,!0),t.pos+=1,e},getBinarySize(i){return 1},getInputUI(i,e,t){let n={elt:"select",on:{change:function(a){a=this.value|0,t.onchange&&t.onchange(a)}}},r=[];for(var s=0,o=e.values.length;s<o;s++){let a={elt:"option",value:s,HTML:e.values[s]};i==s&&(a.selected="true"),r.push(a)}return n.child=r,t.oninput&&(n.on.input=function(){t.oninput(this.value|0)}),bt(n)}};function rt(i){let e=i.charCodeAt(0);return e>47&&e<58||e>64&&e<71||e>96&&e<103}function ls(i){let e=i.toString(16);return i<16?"0"+e:e}const hm={props:[],validate(i,e){if(typeof i!="string")return!1;let t=!0;switch(i.length){default:return!1;case 4:t=i[0]=="#"&&rt(i[1])&&rt(i[2])&&rt(i[3]);break;case 7:t=i[0]=="#"&&rt(i[1])&&rt(i[2])&&rt(i[3])&&rt(i[4])&&rt(i[5])&&rt(i[6]);break}return t},getDef(){return"#000"},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;if(typeof i!="string")return t;let n=0,r;switch(i.length){default:return t;case 4:if(i[0]!=="#")return t;n=1;case 3:if(r=rt(i[n])&&rt(i[n+1])&&rt(i[n+2]),!r)return t;t="#"+i[n]+i[n]+i[n+1]+i[n+1]+i[n+2]+i[n+2];break;case 7:if(i[0]!=="#")return t;n=1;case 6:if(r=rt(i[n])&&rt(i[n+1])&&rt(i[n+2])&&rt(i[n+3])&&rt(i[n+4])&&rt(i[n+5]),!r)return t;t="#"+i[n]+i[n+1]+i[n+2]+i[n+3]+i[n+4]+i[n+5];break}return t},fromBinary(i,e,t){let n=e.view.getUint8(e.pos,!0),r=e.view.getUint8(e.pos+1,!0),s=e.view.getUint8(e.pos+2,!0);return console.log(n,r,s),e.pos+=3,"#"+ls(n)+ls(r)+ls(s)},toBinary(i,e,t,n){let r,s,o;switch(i.length){case 4:r=parseInt(i[1],16),r=r*16+r,s=parseInt(i[2],16),s=s*16+s,o=parseInt(i[3],16),o=o*16+o;break;case 7:r=parseInt(i.slice(1,3),16),s=parseInt(i.slice(3,5),16),o=parseInt(i.slice(5,7),16);break}return console.log(r,s,o),t.view.setUint8(t.pos,r,!0),t.view.setUint8(t.pos+1,s,!0),t.view.setUint8(t.pos+2,o,!0),t.pos+=3,e},getBinarySize(i){return 3},getInputUI(i,e,t){let n={elt:"input",type:"color",value:e.setValid(i,e),on:{change:function(r){r=this.value,t.onchange&&t.onchange(r)}}};return t.oninput&&(n.on.input=function(){t.oninput(this.value)}),bt(n)}},fm={props:[{n:"min",t:"date"},{n:"max",t:"date"}],validate(i,e){return!(!i instanceof Date)},getDef(){return new Date},setValid(i,e){return!i instanceof Date?new Date:i},fromBinary(i,e,t){let n=e.view.getUint16(e.pos,!0),r=e.view.getUint8(e.pos+2,!0),s=e.view.getUint8(e.pos+3,!0),o=e.view.getUint8(e.pos+4,!0),a=e.view.getUint8(e.pos+5,!0);e.pos+=6;let l=new Date;return l.setUTCFullYear(n,r,s),l.setUTCHours(o,a),l},toBinary(i,e,t,n){return t.view.setUint16(t.pos,i.getUTCFullYear(),!0),t.view.setUint8(t.pos+2,i.getUTCMonth(),!0),t.view.setUint8(t.pos+3,i.getUTCDate(),!0),t.view.setUint8(t.pos+4,i.getUTCHours(),!0),t.view.setUint8(t.pos+5,i.getUTCMinutes(),!0),t.pos+=6,e},getBinarySize(i){return 6},getInputUI(i,e,t){let n={elt:"input",type:"datetime-local",value:pm(i),on:{change:function(r){r=uo(this.value),t.onchange&&t.onchange(r)}}};return t.oninput&&(n.on.input=function(){t.oninput(uo(this.value))}),bt(n)}};function cr(i){return i<10?"0"+i:i}function pm(i){return i.getFullYear()+"-"+cr(i.getMonth()+1)+"-"+cr(i.getDate())+"T"+cr(i.getHours())+":"+cr(i.getMinutes())}function uo(i){return new Date(i)}const xi=(1<<30)*2,mm={props:[{n:"min",t:"number",opt:!0},{n:"max",t:"number",opt:!0}],validate(i,e){return!(typeof i!="number"||e.setValid(i,e)!==i)},getDef(){return 0},setValid(i,e){let t=e.def==null?e.getDef(e):e.def;switch(typeof i){case"number":t=i|0;break;case"string":let n=parseInt(i);isNaN(n)||(t=n);break}return e.min!==void 0&&e.min>t&&(t=e.min),e.max!==void 0&&e.max<t&&(t=e.max),t},fromBinary(i,e,t){let n=e.view.getUint8(e.pos,!0);if(e.pos+=1,n<128)return n;switch(n){default:return n;case 129:return n=e.view.getUint8(e.pos,!0),e.pos+=1,n;case 130:return n=e.view.getUint16(e.pos,!0),e.pos+=2,n;case 131:return n=e.view.getUint32(e.pos,!0),e.pos+=4,n;case 132:let r=e.view.getUint32(e.pos,!0);return n=e.view.getUint32(e.pos+4,!0),e.pos+=8,r*xi+n;case 133:return n=e.view.getInt8(e.pos,!0),e.pos+=1,n;case 134:return n=e.view.getInt16(e.pos,!0),e.pos+=2,n;case 135:return n=e.view.getInt32(e.pos,!0),e.pos+=4,n}},toBinary(i,e,t,n){if(i>=0)if(i<255)i<128?(t.view.setUint8(t.pos,i,!0),t.pos+=1):(t.view.setUint8(t.pos,129,!0),t.view.setUint8(t.pos+1,i,!0),t.pos+=2);else if(i<65536)t.view.setUint8(t.pos,130,!0),t.view.setUint16(t.pos+1,i,!0),t.pos+=3;else if(i<xi)t.view.setUint8(t.pos,131,!0),t.view.setUint32(t.pos+1,i,!0),t.pos+=5;else{t.view.setUint8(t.pos,132,!0);let r=Math.floor(i/xi),s=i%xi;t.view.setUint32(t.pos+1,r,!0),t.view.setUint32(t.pos+5,s,!0),t.pos+=9}else i>-128?(t.view.setUint8(t.pos,133,!0),t.view.setInt8(t.pos+1,i,!0),t.pos+=2):i>-65536?(t.view.setUint8(t.pos,133,!0),t.view.setInt16(t.pos+1,i,!0),t.pos+=3):(t.view.setUint8(t.pos,133,!0),t.view.setInt32(t.pos+1,i,!0),t.pos+=5);return e},getBinarySize(i,e){return i>=0?i<255?i<128?1:2:i<65536?3:i<xi?5:9:i>-128?2:i>-65536?3:5},getInputUI:wt},gm=mm;Fe("number",yp);Fe("string",Ap);Fe("shortString",bp);Fe("boolean",wp);Fe("object",Tp);Fe("function",Ip);Fe("array",Cp);Fe("datatype",Pp);Fe("uInt8",Vp);Fe("uInt16",Up);Fe("uInt32",Fp);Fe("int8",Op);Fe("int16",kp);Fe("int32",Zp);Fe("float32",Wp);Fe("float64",jp);Fe("int8Array",Kp);Fe("uint8Array",Jp);Fe("int16Array",em);Fe("uint16Array",nm);Fe("int32Array",rm);Fe("uint32Array",am);Fe("float32Array",lm);Fe("float64Array",um);Fe("enum",dm);Fe("color3",hm);Fe("dateTime",fm);Fe("cleverInt",gm);const vm=Pt,Ti=bt;function Ar(i){this.init(i)}Ar.prototype.init=function(i){this._seed=(i|0)%2147483647,this._seed<=0&&(this._seed+=2147483646)};Ar.prototype.next=function(){return this._seed=this._seed*16807%2147483647};Ar.prototype.nextFloat=function(i,e){return(this.next()-1)/2147483646};const Go=new Ar;function xm(i){Go.init(i)}function De(){return Go.nextFloat()}function ho(i){return De()*i|0}let Mm=[200,2e3,2e4,2e5,2e6],ym=[200,"2k","20k","200k","2M"],Wo=["Arial","Times"],Am={t:"object",properties:[{n:"label count",t:"enum",values:ym,def:2},{n:"method",t:"enum",values:["instances","points","mesh"],def:2},{n:"colors",t:"enum",values:["random","shadowed","blue","RGB"],def:2},{n:"box",t:"boolean",def:!0},{n:"text",t:"boolean",def:!1},{n:"size",t:"number",min:.5,max:5,step:.1,def:1},{n:"letter spacing",t:"number",min:.1,max:2,step:.01,def:1,cond:["&&",["method","!=",2],["text","=",!0]]},{n:"font",t:"enum",values:Wo,cond:["&&",["method","==",2],["text","=",!0]]},{n:"outline",t:"boolean",def:!1,cond:["text","=",!0]},{n:"thickness",t:"number",min:.9,max:1.5,step:.01,def:1,cond:["&&",["outline","==",!1],["&&",["method","!=",2],["text","=",!0]]]}]},Xo=new vm(Am),st=Xo.getDefault(),jo=["size","letter spacing","outline","thickness"],As={},fo=st.font;jo.forEach(i=>{As[i]=st[i]});let Yo=document.getElementById("infos");Yo.appendChild(Xo.getInputUI(st,{oninput:function(i){let e=!0;jo.forEach(t=>{i[t]!=As[t]&&(e=!1,As[t]=i[t])}),fo!=i.font?(fo=i.font,Qo(),bs()):e?bs():Ko()}}));let hr;Yo.appendChild(Ti({style:{width:"12em"},getRef:i=>{hr=i}}));const _m="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit. Ut velit mauris, egestas sed, gravida nec, ornare ut, mi. Aenean ut orci vel massa suscipit pulvinar. Nulla sollicitudin. Fusce varius, ligula non tempus aliquam, nunc turpis ullamcorper nibh, in tempus sapien eros vitae ligula. Pellentesque rhoncus nunc et augue. Integer id felis. Curabitur aliquet pellentesque diam. Integer quis metus vitae elit lobortis egestas. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi vel erat non mauris convallis vehicula. Nulla et sapien. Integer tortor tellus, aliquam faucibus, convallis id, congue eu, quam. Mauris ullamcorper felis vitae erat. Proin feugiat, augue non elementum posuere, metus purus iaculis lectus, et tristique ligula justo vitae magna. Aliquam convallis sollicitudin purus. Praesent aliquam, enim at fermentum mollis, ligula massa adipiscing nisl, ac euismod nibh nisl eu lectus. Fusce vulputate sem at sapien. Vivamus leo. Aliquam euismod libero eu enim. Nulla nec felis sed leo placerat imperdiet. Aenean suscipit nulla in justo. Suspendisse cursus rutrum augue. Nulla tincidunt tincidunt mi. Curabitur iaculis, lorem vel rhoncus faucibus, felis magna fermentum augue, et ultricies lacus lorem varius purus. Curabitur eu amet.",pr=_m.split(" "),po=[14,16,24,36],Wt=new Xe(0,0,0,0),mr=new Xe(0,0,0,0);function Ko(){Wt.x=st.size,Wt.y=st["letter spacing"],st.outline?(Wt.z=.7,Wt.w=.5,mr.x=1):(Wt.z=.5*st.thickness,Wt.w=.2,mr.x=0)}function mo(i,e,t){let n=new C(De(),De(),De()),r=new C(De(),De(),De());return{col:n,col2:r}}function bm(i,e,t){let n=Math.abs(i),r=Math.abs(e),s=Math.abs(t),o=1;r>n?r>s?o=e+1.3:o=1-.5*s*s:n>s?o=1-.4*n*n:o=1-.5*s*s;let a=new C(De()*o,De()*o,De()*o),l=new C(De()*o,De()*o,De()*o);return{col:a,col2:l}}function Sm(i,e,t){let n=Math.abs(i),r=Math.abs(e),s=Math.abs(t),o=1;r>n?r>s?o=.4*(e+1.5):o=1-.6*s*s:n>s?o=1-.4*n*n:o=1-.6*s*s;let a=.6,l=.4,c=.3,u=new C((De()*a+c)*o,(De()*a+c)*o,(De()*l+c)*o),d=new C((De()*l+c)*o,(De()*l+c)*o,(De()*a+c*2)*o);return{col:u,col2:d}}function wm(i,e,t){let n=Math.abs(i),r=Math.abs(e),s=Math.abs(t),o,a,l;r>n?r>s?(o=a=0,l=.5):(o=l=0,a=.5):n>s?(a=l=0,o=.5):(o=l=0,a=.5);let c=.5,u=new C(1-De()*c-o,1-De()*c-a,1-De()*c-l),d=new C(De()*c+o,De()*c+a,De()*c+l);return{col:u,col2:d}}let Em=[mo,bm,Sm,wm,mo],_s=st.colors;function Tm(i){let e=[];for(let t=0;t<i;t++){let n=pr[ho(pr.length)],r=po[ho(po.length)],o=50/2,a=De()*2-1,l=De()*2-1,c=De()*2-1,u=new C(a*o,l*o,c*o),{col:d,col2:f}=Em[_s](a,l,c),m=new Se(De()*50+10,De()*50+10);e.push({word:n,size:r,pos:u,col:d,col2:f,box:m})}return e}const Im=document.getElementById("app"),zn=new qo({antialias:!0});zn.setClearColor(new qe("#F4F4F4"),1);zn.setSize(window.innerWidth,window.innerHeight);Im.appendChild(zn.domElement);const un=new tp,ai=new Lt(45,window.innerWidth/window.innerHeight,1,1e4),Cs=new mp(ai,zn.domElement);ai.position.set(0,80,100);Cs.update();Cs.autoRotate=!0;let cs=60;const Rm=new ui(cs,cs,cs),Cm=new Es({color:65280,wireframe:!0}),Ls=new zt(Rm,Cm);Ls.visible=!1;un.add(Ls);const Lm=new Mr(1,1),oi=new Se(window.innerWidth/2,window.innerHeight/2);window.addEventListener("resize",Pm);function Pm(){ai.aspect=window.innerWidth/window.innerHeight,ai.updateProjectionMatrix(),zn.setSize(window.innerWidth,window.innerHeight),oi.x=window.innerWidth/2,oi.y=window.innerHeight/2}function zm(i){const e=zn.domElement.getBoundingClientRect();return{x:(i.clientX-e.left)/e.width*2-1,y:(i.clientY-e.top)/e.height*-2+1}}const go=new fp,li=new Xe;window.addEventListener("mousemove",i=>{const e=zm(i);go.setFromCamera(e,ai);let t=go.intersectObjects([Ls],!1);if(t.length>0){let n=t[0].point;li.set(n.x,n.y,n.z,30)}else li.set(0,0,0,0)},!0);const Vm=new Oo,Ps=new hp(Vm).load(pp);Ps.mipmaps=!1;const Dm=new Ht({uniforms:{screenSize:{value:oi},alphaTexture:{value:Ps},parameters:{value:Wt},mouspos:{value:li}},vertexShader:`
  uniform vec2 screenSize;
  uniform vec4 parameters;
  uniform vec4 mouspos;
  varying vec3 vColor;
  varying vec3 vColor2;
  varying vec2 vUV;

  void main() {

    vec3 color = instanceMatrix[2].xyz;
    vec3 color2 = instanceMatrix[3].xyz;

    vec3 posi = instanceMatrix[0].xyz;
    float L = length(mouspos.xyz-posi);
    vec3 color3 = vec3(1.-color.x,1.-color.y,1.-color.z);
    vec3 color4 = vec3(1.-color2.x,1.-color2.y,1.-color2.z);
    float param = 1.;
    if(L<mouspos.w){
      posi -= (mouspos.w-L)*(mouspos.xyz-posi) * (-0.04);
      param = 1.-(mouspos.w-L)/mouspos.w;
    }

    vColor = mix(color3,color, param);
    vColor2 = mix(color4,color2, param);

    //vec4 mvPosition = modelViewMatrix * instanceMatrix * vec4( position, 1.0 );
    vec4 mvPosition = projectionMatrix * modelViewMatrix * vec4(posi, 1.);
    mvPosition.xy +=  (position.xy+instanceMatrix[1].xy*parameters.y) /screenSize * instanceMatrix[0].w * mvPosition.ww * parameters.x;
    vUV = uv*vec2(1./16., 1./8.) + instanceMatrix[1].zw;
    gl_Position = mvPosition;

  }
  `,fragmentShader:`
  uniform sampler2D alphaTexture;
  uniform vec4 parameters;
  varying vec3 vColor;
  varying vec3 vColor2;
  varying vec2 vUV;
  void main() {

    vec4 Text = texture2D( alphaTexture, vUV );
    if(Text.a>parameters.z){discard;}
    if(Text.a>parameters.w){
      gl_FragColor = vec4( vColor, min(1. , 15. * (parameters.z-Text.a) ) );
    }else{
      gl_FragColor = vec4( mix(vColor, vColor2, min(1., 20. * (parameters.w-Text.a))) , 1. );
    }
    
  }
  `,transparent:!0}),Um=new Ht({uniforms:{screenSize:{value:oi},alphaTexture:{value:Ps},parameters:{value:Wt},mouspos:{value:li}},vertexShader:`
  uniform vec2 screenSize;
  uniform vec4 parameters;
  uniform vec4 mouspos;

  attribute vec2 offsetSize;
  attribute vec3 color2;
  attribute vec3 color;

  varying vec3 vColor;
  varying vec3 vColor2;
  varying vec2 vUV;

  void main() {

    vec3 posi = position;
    float L = length(mouspos.xyz-posi);
    vec3 color3 = vec3(1.-color.x,1.-color.y,1.-color.z);
    vec3 color4 = vec3(1.-color2.x,1.-color2.y,1.-color2.z);
    float param = 1.;
    if(L<mouspos.w){
      posi -= (mouspos.w-L)*(mouspos.xyz-posi) * (-0.04);
      param = 1.-(mouspos.w-L)/mouspos.w;
    }

    vColor = mix(color3,color, param);
    vColor2 = mix(color4,color2, param);

    vec4 mvPosition = projectionMatrix * modelViewMatrix *  vec4( posi, 1.0 ); 
    mvPosition.xy +=  (vec2(offsetSize.y,0.5)*parameters.y) /screenSize * offsetSize.x * mvPosition.ww * parameters.x;
    gl_PointSize = offsetSize.x * parameters.x; //size * ( 300.0 / -mvPosition.z );
    vUV = uv; // + instanceMatrix[1].zw;
    gl_Position = mvPosition;

  }
  `,fragmentShader:`
  uniform sampler2D alphaTexture;
  uniform vec4 parameters;
  varying vec3 vColor;
  varying vec3 vColor2;
  varying vec2 vUV;
  void main() {

    vec4 Text = texture2D( alphaTexture, gl_PointCoord*vec2(1./16., -1./8.)+vUV );
    if(Text.a>parameters.z){discard;}
    if(Text.a>parameters.w){
      gl_FragColor = vec4( vColor, min(1. , 15. * (parameters.z-Text.a) ) );
    }else{
      gl_FragColor = vec4( mix(vColor, vColor2, min(1., 20. * (parameters.w-Text.a))) , 1. );
    }

  }
  `,transparent:!0});let Ii,Mi,Rt,fr={},yi=[0],Ai=0;function Qo(){let i=1024;Ii||(Mi=Ti({elt:"canvas"}),Mi.width=Mi.height=i,Ii=new cp(Mi),Rt=Mi.getContext("2d")),Rt.clearRect(0,0,i,i);var e=32,t=2,n=1,r=29+t,s=e+2*t,o=e+2*t+n,a=Wo[st.font],l=Rt.font=""+e+"px "+a;Rt.font=l,Rt.textBaseline="bottom",Rt.textAlign="left",fr={},yi=[0],Ai=0;for(var c=0,u=pr.length;c<u;c++){let d=pr[c];if(!fr[d]){let f=Rt.measureText(d),m=Math.round(f.width)+2*t,g;for(var c=0;c<=Ai;c++)if(yi[c]+m<i){g={top:o*c+s,line:o*c+r,height:s,left:yi[c],width:m},yi[c]+=m+n;break}g||(Ai++,yi.push(m+n),g={top:Ai*o+s,line:o*Ai+r,height:s,left:0,width:m}),fr[d]=g,Rt.fillStyle="#F00",Rt.strokeStyle="#0F0",Rt.lineWidth=t,Rt.strokeText(d,g.left,g.line),Rt.fillText(d,g.left,g.line)}}Ii.needsUpdate=!0}Qo();const Nm=new Ht({uniforms:{screenSize:{value:oi},mouspos:{value:li},alphaTexture:{value:Ii},parameters:{value:Wt},param2:{value:mr}},vertexShader:`
  uniform vec2 screenSize;
  uniform vec4 parameters;
  uniform vec4 mouspos;

  attribute vec2 offsetPos;
  attribute vec3 color2;
  attribute vec3 color;

  varying vec3 vColor;
  varying vec3 vColor2;
  varying vec2 vUV;

  void main() {

    vec3 posi = position;
    float L = length(mouspos.xyz-posi);
    vec3 color3 = vec3(1.-color.x,1.-color.y,1.-color.z);
    vec3 color4 = vec3(1.-color2.x,1.-color2.y,1.-color2.z);
    float param = 1.;
    if(L<mouspos.w){
      posi -= (mouspos.w-L)*(mouspos.xyz-posi) * (-0.04);
      param = 1.-(mouspos.w-L)/mouspos.w;
    }

    vColor = mix(color3,color, param);
    vColor2 = mix(color4,color2, param);

    vec4 mvPosition = projectionMatrix * modelViewMatrix *  vec4( posi, 1.0 ); 
    mvPosition.xy +=   offsetPos.xy /screenSize * mvPosition.ww * parameters.x;
    vUV = uv;
    gl_Position = mvPosition;

  }
  `,fragmentShader:`
  uniform sampler2D alphaTexture;
  uniform vec4 param2;
  varying vec3 vColor;
  varying vec3 vColor2;
  varying vec2 vUV;
  void main() {

    vec4 col = texture2D( alphaTexture, vUV );
    if(col.a<0.1){discard;}
    else{
      float a = col.r+col.g*param2.x;
      if(a<0.1){discard;}
      gl_FragColor = vec4(col.r*vColor2 + col.g*vColor, a);
    }

  }
  `,transparent:!0}),Fm=new Ht({uniforms:{screenSize:{value:oi},mouspos:{value:li},alphaTexture:{value:Ii},parameters:{value:Wt},param2:{value:mr}},vertexShader:`
  uniform vec2 screenSize;
  uniform vec4 parameters;
  uniform vec4 mouspos;

  attribute vec2 offsetPos;
  attribute vec3 color;

  varying vec3 vColor;

  void main() {

    vec3 posi = position;
    float L = length(mouspos.xyz-posi);
    vec3 color2 = vec3(1.0,0.6,0.); // vec3(1.-color.x,1.-color.y,1.-color.z);
    float param = 1.;
    if(L<mouspos.w){
      posi -= (mouspos.w-L)*(mouspos.xyz-posi) * (-0.04);
      param = 1.-(mouspos.w-L)/mouspos.w;
    }

    vColor = mix(color2, color, param);
    vec4 mvPosition = projectionMatrix * modelViewMatrix *  vec4( posi, 1.0 ); 
    mvPosition.xy +=   offsetPos.xy /screenSize * mvPosition.ww * parameters.x;
    gl_Position = mvPosition;

  }
  `,fragmentShader:`
  varying vec3 vColor;

  void main() {
    gl_FragColor = vec4(vColor, 1.);
  }
  `,transparent:!0});let Ot,_i,dt=0,us=0;xm(0);let Kn,vo=0;function bs(){let i=Mm[st["label count"]];if(Ot&&(un.remove(Ot),Ot=null),_i&&(un.remove(_i),_i=null),(vo<i||_s!=st.colors)&&(_s=st.colors,Kn=Tm(i),vo=i),dt!=i){us=0;for(let t=0;t<i;t++)us+=Kn[t].word.length;dt=i}if(st.text){if(st.method==0){Ot=new ip(Lm,Dm,us),un.add(Ot);const t=new ke;let n=0;for(let r=0;r<dt;r++){let s=Kn[r];for(let o=0,a=s.word.length;o<a;o++){let l=s.word[o].charCodeAt(0)-16;t.set(s.pos.x,o/2+.5,s.col.x,s.col2.x,s.pos.y,.5,s.col.y,s.col2.y,s.pos.z,l%16/16,s.col.z,s.col2.z,s.size,1-Math.floor(l/16)/8,0,0),Ot.setMatrixAt(n++,t)}}Ot.instanceMatrix.needsUpdate=!0,hr.innerHTML="Requirements : WebGL 1.0 + instanced drawing extension"}else if(st.method==1){let t=new Ut;const n=[],r=[],s=[],o=[],a=[];for(let l=0;l<dt;l++){let c=Kn[l];for(let u=0,d=c.word.length;u<d;u++){let f=c.word[u].charCodeAt(0)-32;n.push(c.pos.x,c.pos.y,c.pos.z),s.push(c.size,u/2+.5),r.push(f%16/16,1-Math.floor(f/16)/8),o.push(c.col.x,c.col.y,c.col.z),a.push(c.col2.x,c.col2.y,c.col2.z)}}t.setAttribute("position",new Qe(n,3)),t.setAttribute("color",new Qe(o,3)),t.setAttribute("color2",new Qe(a,3)),t.setAttribute("uv",new Qe(r,2)),t.setAttribute("offsetSize",new Qe(s,2)),Ot=new lp(t,Um),un.add(Ot),hr.innerHTML="Requirements : WebGL 1.0"}else if(st.method==2){let t=new Ut;const n=new Float32Array(dt*4*3),r=new Float32Array(dt*4*2),s=new Float32Array(dt*4*2),o=new Uint8Array(dt*8*3),a=new Uint8Array(dt*8*3),l=new Uint32Array(dt*6),c=1024;for(let u=0;u<dt;u++){let d=Kn[u],f=fr[d.word],m=d.size*f.width/f.height,g=(c-f.top)/c,p=(c-f.top+f.height)/c,h=u*4*2;r[h]=f.left/c,r[h+1]=g,r[h+2]=f.left/c,r[h+3]=p,r[h+4]=(f.left+f.width)/c,r[h+5]=g,r[h+6]=(f.left+f.width)/c,r[h+7]=p,s[h]=0,s[h+1]=0,s[h+2]=0,s[h+3]=d.size,s[h+4]=m,s[h+5]=0,s[h+6]=m,s[h+7]=d.size,h=u*4*3;for(var e=0;e<4;e++)o[h+e*3]=d.col.x*255,o[h+e*3+1]=d.col.y*255,o[h+e*3+2]=d.col.z*255,a[h+e*3]=d.col2.x*255,a[h+e*3+1]=d.col2.y*255,a[h+e*3+2]=d.col2.z*255,n[h+e*3]=d.pos.x,n[h+e*3+1]=d.pos.y,n[h+e*3+2]=d.pos.z;h=u*6;let x=u*4;l[h]=x,l[h+1]=x+2,l[h+2]=x+1,l[h+3]=x+2,l[h+4]=x+3,l[h+5]=x+1}t.setAttribute("position",new Qe(n,3)),t.setAttribute("color",new jr(o,3,!0)),t.setAttribute("color2",new jr(a,3,!0)),t.setAttribute("uv",new Qe(r,2)),t.setAttribute("offsetPos",new Qe(s,2)),t.setIndex(new Ts(l,1)),Ot=new zt(t,Nm),un.add(Ot),hr.innerHTML="Requirements : WebGL 1.0"}}if(st.box){let t=new Ut;const n=new Float32Array(dt*8*3),r=new Float32Array(dt*8*2),s=new Uint8Array(dt*8*3);for(let o=0;o<dt;o++){let a=Kn[o],l=o*8*2;r[l]=0,r[l+1]=0,r[l+2]=0,r[l+3]=a.box.y,r[l+4]=0,r[l+5]=a.box.y,r[l+6]=a.box.x,r[l+7]=a.box.y,r[l+8]=a.box.x,r[l+9]=a.box.y,r[l+10]=a.box.x,r[l+11]=0,r[l+12]=a.box.x,r[l+13]=0,r[l+14]=0,r[l+15]=0,l=o*8*3;for(var e=0;e<8;e++)s[l+e*3]=a.col2.x*255,s[l+e*3+1]=a.col2.y*255,s[l+e*3+2]=a.col2.z*255,n[l+e*3]=a.pos.x,n[l+e*3+1]=a.pos.y,n[l+e*3+2]=a.pos.z}t.setAttribute("position",new Qe(n,3)),t.setAttribute("color",new jr(s,3,!0)),t.setAttribute("offsetPos",new Qe(r,2)),_i=new ap(t,Fm),un.add(_i)}}Ko();bs();function Jo(){requestAnimationFrame(Jo),Cs.update(),zn.render(un,ai)}Jo();
